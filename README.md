# MaddTech

MaddTech is a technical mod from Minecraft. Gradually build your way up from stone age, to minor industry, to industrial-scale nuclear power plants capable of powering a whole city; build an economy with rubies as the currency; achieve exponential growth with ever more efficient tools; challenge yourself with this beautifully difficult mod, perfect for multiplayer servers; and enjoy some good teamwork!

This repository contains the very latest source code for MaddTech. It is also used to report bugs. Releases will be made from stable points.

## Building

To build the code, simply run `./gradlew build`.