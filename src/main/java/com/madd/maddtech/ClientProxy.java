/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;


import net.minecraft.util.*;
import net.minecraft.item.*;

import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import net.minecraftforge.client.*;
import net.minecraft.client.renderer.entity.*;
import net.minecraft.client.*;

public class ClientProxy extends CommonProxy
{
	@Override
	public void initRender()
	{
//		LibTech.lampGlowRenderID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.lampGlowRenderID, new LampGlowRender());
//		LibTech.fakeRenderID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.fakeRenderID, new FakeRender());
//		LibTech.renderCableID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderCableID, new RenderCable());
//		LibTech.renderDynamoID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderDynamoID, new RenderDynamo());
//		LibTech.renderFluidTankID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderFluidTankID, new RenderFluidTank());
//		LibTech.renderCentrifugeID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderCentrifugeID, new RenderCentrifuge());
//		LibTech.renderLightmapID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderLightmapID, new RenderLightmap());
//		LibTech.renderRotatingID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderRotatingID, new RenderRotating());
//		LibTech.renderRedstonePulserID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderRedstonePulserID, new RenderRedstonePulser());
//		LibTech.renderAlternatorID = RenderingRegistry.getNextAvailableRenderId();
//		RenderingRegistry.registerBlockHandler(LibTech.renderAlternatorID, new RenderAlternator());
//		
//		MinecraftForgeClient.registerItemRenderer(MaddTech.itemCapsule, new ItemRenderCapsule());
//		MinecraftForgeClient.registerItemRenderer(MaddTech.itemBattery, new ItemRenderBattery());
//
//		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFluidTank.class, new TileEntityRenderFluidTank());
//
//		RenderingRegistry.registerEntityRenderingHandler(EntityNukePrimed.class, new RenderNukePrimed());
//		Integrator.registerAllRendering();
	}

//	@Override
//	public void registerMessages(SimpleNetworkWrapper network)
//	{
//
//	};
//	
//	@Override
//	public World getWorld()
//	{
//		return Minecraft.getMinecraft().theWorld;
//	};
}
