/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;

import com.madd.maddtech.api.*;
import net.minecraft.world.*;
import net.minecraft.util.*;


public class MTDimensionInfoOverworld extends MTDimensionInfo
{
	private boolean canBlockSeeTheSky(World world, int x, int y, int z)
	{
		y++;
		while (y != 256)
		{
//			if (world.getBlock(x, y, z) != Blocks.air)
//			{
//				return false;
//			};
			y++;
		};

		return true;
	};

//	@Override
//	public int getSolarEnergy(World world, int x, int y, int z)
//	{
//		float f1 = world.getCelestialAngle(1.0F);
//		float f2 = 1.0F - (MathHelper.cos(f1 * (float)Math.PI * 2.0F) * 2.0F + 0.2F);
//
//		if (f2 < 0.0F)
//		{
//			f2 = 0.0F;
//		}
//
//		if (f2 > 1.0F)
//		{
//			f2 = 1.0F;
//		}
//
//		f2 = 1.0F - f2;
//		float sun = f2 * 0.8F + 0.2F;
//
//		boolean sunny = (sun == 1.0F);
//		boolean rain = world.isRaining();
//		boolean thundering = world.isThundering();
//		if (sunny && (!rain) && (!thundering) && canBlockSeeTheSky(world, x, y, z))
//		{
//			return 1;
//		};
//
//		return 0;
//	};
};
