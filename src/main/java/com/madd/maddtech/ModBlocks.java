/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;

import net.minecraftforge.fml.RegistryObject;
import net.minecraft.block.Block;
import com.madd.maddtech.block.*;

public class ModBlocks {

	public static final RegistryObject<Block> BASALT_BLOCK = ModRegistry.BLOCKS.register("basalt_block", BlockBasaltBlock::new);
	public static final RegistryObject<Block> ACACIA_CHISELED = ModRegistry.BLOCKS.register("acacia_chiseled", BlockAcaciaChiseled::new);
	public static final RegistryObject<Block> ACACIA_PANEL = ModRegistry.BLOCKS.register("acacia_panel", BlockAcaciaPanel::new);
	public static final RegistryObject<Block> ACCUMULATOR = ModRegistry.BLOCKS.register("accumulator", BlockAccumulator::new);
	public static final RegistryObject<Block> BASALT_ROCK = ModRegistry.BLOCKS.register("basalt_rock", BlockBasalt::new);
	public static final RegistryObject<Block> BASALT_BLOCKS = ModRegistry.BLOCKS.register("basalt_blocks", BlockBasaltBlocks::new);
	public static final RegistryObject<Block> BASALT_BRICK_LAVA = ModRegistry.BLOCKS.register("basalt_brick_lava", BlockBasaltBrickLava::new);
	public static final RegistryObject<Block> BASALT_BRICKS = ModRegistry.BLOCKS.register("basalt_bricks", BlockBasaltBricks::new);
	public static final RegistryObject<Block> BASALT_CHISELED = ModRegistry.BLOCKS.register("basalt_chiseled", BlockBasaltChiseled::new);
	public static final RegistryObject<Block> BASALT_COBBLESTONE = ModRegistry.BLOCKS.register("basalt_cobblestone", BlockBasaltCobblestone::new);
	public static final RegistryObject<Block> BIRCH_CHISELED = ModRegistry.BLOCKS.register("birch_chiseled", BlockBirchChiseled::new);
	public static final RegistryObject<Block> BIRCH_PANEL = ModRegistry.BLOCKS.register("birch_panel", BlockBirchPanel::new);
	public static final RegistryObject<Block> BRASS_BLOCK = ModRegistry.BLOCKS.register("brass_block", BlockBrassBlock::new);
	public static final RegistryObject<Block> BRASS_FIBRE_CABLE = ModRegistry.BLOCKS.register("brass_fibre_cable", BlockBrassFibreCable::new);
	public static final RegistryObject<Block> BREAKER = ModRegistry.BLOCKS.register("breaker", BlockBreaker::new);
	public static final RegistryObject<Block> BRICKS_COLORED = ModRegistry.BLOCKS.register("bricks_colored", BlockBricksColored::new);
	public static final RegistryObject<Block> BRONZE_BLOCK = ModRegistry.BLOCKS.register("bronze_block", BlockBronzeBlock::new);
	public static final RegistryObject<Block> BRONZE_FIBRE_CABLE = ModRegistry.BLOCKS.register("bronze_fibre_cable", BlockBronzeFibreCable::new);
	public static final RegistryObject<Block> CABINET = ModRegistry.BLOCKS.register("cabinet", BlockCabinet::new);
	public static final RegistryObject<Block> CENTRIFUGE = ModRegistry.BLOCKS.register("centrifuge", BlockCentrifuge::new);
	public static final RegistryObject<Block> CHEMICAL_REACTOR = ModRegistry.BLOCKS.register("chemical_reactor", BlockChemicalReactor::new);
	public static final RegistryObject<Block> COMPRESSOR = ModRegistry.BLOCKS.register("compressor", BlockCompressor::new);
	public static final RegistryObject<Block> COPPER_BLOCK = ModRegistry.BLOCKS.register("copper_block", BlockCopperBlock::new);
	public static final RegistryObject<Block> COPPER_ENERGY_CABLE = ModRegistry.BLOCKS.register("copper_energy_cable", BlockCopperEnergyCable::new);
	public static final RegistryObject<Block> COPPER_ORE = ModRegistry.BLOCKS.register("copper_ore", BlockCopperOre::new);
	public static final RegistryObject<Block> CRATE = ModRegistry.BLOCKS.register("crate", BlockCrate::new);
	public static final RegistryObject<Block> DARK_OAK_CHISELED = ModRegistry.BLOCKS.register("dark_oak_chiseled", BlockDarkOakChiseled::new);
	public static final RegistryObject<Block> DARK_OAK_PANEL = ModRegistry.BLOCKS.register("dark_oak_panel", BlockDarkOakPanel::new);
	public static final RegistryObject<Block> DENSE_BRICKS = ModRegistry.BLOCKS.register("dense_bricks", BlockDenseBricks::new);
	public static final RegistryObject<Block> DENSE_BRICKS_REDSTONE = ModRegistry.BLOCKS.register("dense_bricks_redstone", BlockDenseBricksRedstone::new);
	public static final RegistryObject<Block> DEWAR = ModRegistry.BLOCKS.register("dewar", BlockDewar::new);
	public static final RegistryObject<Block> DOLOMITE_ROCK = ModRegistry.BLOCKS.register("dolomite_rock", BlockDolomite::new);
	public static final RegistryObject<Block> DOLOMITE_BLOCK = ModRegistry.BLOCKS.register("dolomite_block", BlockDolomiteBlock::new);
	public static final RegistryObject<Block> DOLOMITE_BLOCKS = ModRegistry.BLOCKS.register("dolomite_blocks", BlockDolomiteBlocks::new);
	public static final RegistryObject<Block> DOLOMITE_BRICK_LAVA = ModRegistry.BLOCKS.register("dolomite_brick_lava", BlockDolomiteBrickLava::new);
	public static final RegistryObject<Block> DOLOMITE_BRICKS = ModRegistry.BLOCKS.register("dolomite_bricks", BlockDolomiteBricks::new);
	public static final RegistryObject<Block> DOLOMITE_CHISELED = ModRegistry.BLOCKS.register("dolomite_chiseled", BlockDolomiteChiseled::new);
	public static final RegistryObject<Block> DYNAMO = ModRegistry.BLOCKS.register("dynamo", BlockDynamo::new);
	public static final RegistryObject<Block> ELECTRIC_FURNACE = ModRegistry.BLOCKS.register("electric_furnace", BlockElectricFurnace::new);
	public static final RegistryObject<Block> ELECTRIC_FURNACE_OFF = ModRegistry.BLOCKS.register("electric_furnace_off", BlockElectricFurnaceOff::new);
	public static final RegistryObject<Block> ELECTRIC_FURNACE_ON = ModRegistry.BLOCKS.register("electric_furnace_on", BlockElectricFurnaceOn::new);
	public static final RegistryObject<Block> ENERGY_PROBE = ModRegistry.BLOCKS.register("energy_probe", BlockEnergyProbe::new);
	public static final RegistryObject<Block> ENERGY_SHOP = ModRegistry.BLOCKS.register("energy_shop", BlockEnergyShop::new);
	public static final RegistryObject<Block> ENG_TABLE = ModRegistry.BLOCKS.register("eng_table", BlockEngTable::new);
	public static final RegistryObject<Block> FAKE_GLASS = ModRegistry.BLOCKS.register("fake_glass", BlockFakeGlass::new);
	public static final RegistryObject<Block> FLUID_CABLE = ModRegistry.BLOCKS.register("fluid_cable", BlockFluidCable::new);
	public static final RegistryObject<Block> FLUID_DISPENSER = ModRegistry.BLOCKS.register("fluid_dispenser", BlockFluidDispenser::new);
	public static final RegistryObject<Block> FLUID_HOLE = ModRegistry.BLOCKS.register("fluid_hole", BlockFluidHole::new);
	public static final RegistryObject<Block> FLUID_MONITOR = ModRegistry.BLOCKS.register("fluid_monitor", BlockFluidMonitor::new);
	public static final RegistryObject<Block> FLUID_SHOP = ModRegistry.BLOCKS.register("fluid_shop", BlockFluidShop::new);
	public static final RegistryObject<Block> FLUID_TANK = ModRegistry.BLOCKS.register("fluid_tank", BlockFluidTank::new);
	public static final RegistryObject<Block> FLUID_VALVE = ModRegistry.BLOCKS.register("fluid_valve", BlockFluidValve::new);
	public static final RegistryObject<Block> GAS_EXTRACTOR = ModRegistry.BLOCKS.register("gas_extractor", BlockGasExtractor::new);
	public static final RegistryObject<Block> GLOWNET_CABLE = ModRegistry.BLOCKS.register("glownet_cable", BlockGlownetCable::new);
	public static final RegistryObject<Block> GLOWNET_SWITCH = ModRegistry.BLOCKS.register("glownet_switch", BlockGlownetSwitch::new);
	public static final RegistryObject<Block> GOLD_ENERGY_CABLE = ModRegistry.BLOCKS.register("gold_energy_cable", BlockGoldEnergyCable::new);
	public static final RegistryObject<Block> GRANITE_ROCK = ModRegistry.BLOCKS.register("granite_rock", BlockGranite::new);
	public static final RegistryObject<Block> GRANITE_BLOCK = ModRegistry.BLOCKS.register("granite_block", BlockGraniteBlock::new);
	public static final RegistryObject<Block> GRANITE_BLOCKS = ModRegistry.BLOCKS.register("granite_blocks", BlockGraniteBlocks::new);
	public static final RegistryObject<Block> GRANITE_BRICK_LAVA = ModRegistry.BLOCKS.register("granite_brick_lava", BlockGraniteBrickLava::new);
	public static final RegistryObject<Block> GRANITE_BRICKS = ModRegistry.BLOCKS.register("granite_bricks", BlockGraniteBricks::new);
	public static final RegistryObject<Block> GRANITE_CHISELED = ModRegistry.BLOCKS.register("granite_chiseled", BlockGraniteChiseled::new);
	public static final RegistryObject<Block> GROUND = ModRegistry.BLOCKS.register("ground", BlockGround::new);
	public static final RegistryObject<Block> IRON_CRATE = ModRegistry.BLOCKS.register("iron_crate", BlockIronCrate::new);
	public static final RegistryObject<Block> IRON_ENERGY_CABLE = ModRegistry.BLOCKS.register("iron_energy_cable", BlockIronEnergyCable::new);
	public static final RegistryObject<Block> ITEM_CABLE = ModRegistry.BLOCKS.register("item_cable", BlockItemCable::new);
	public static final RegistryObject<Block> ITEM_GATE = ModRegistry.BLOCKS.register("item_gate", BlockItemGate::new);
	public static final RegistryObject<Block> ITEM_MONITOR = ModRegistry.BLOCKS.register("item_monitor", BlockItemMonitor::new);
	public static final RegistryObject<Block> ITEM_SORTER = ModRegistry.BLOCKS.register("item_sorter", BlockItemSorter::new);
	public static final RegistryObject<Block> ITEM_TRANSPORTER = ModRegistry.BLOCKS.register("item_transporter", BlockItemTransporter::new);
	public static final RegistryObject<Block> JUNGLE_CHISELED = ModRegistry.BLOCKS.register("jungle_chiseled", BlockJungleChiseled::new);
	public static final RegistryObject<Block> JUNGLE_PANEL = ModRegistry.BLOCKS.register("jungle_panel", BlockJunglePanel::new);
	public static final RegistryObject<Block> KAOLIN_ROCK = ModRegistry.BLOCKS.register("kaolin_rock", BlockKaolin::new);
	public static final RegistryObject<Block> KAOLIN_BLOCK = ModRegistry.BLOCKS.register("kaolin_block", BlockKaolinBlock::new);
	public static final RegistryObject<Block> KAOLIN_BLOCKS = ModRegistry.BLOCKS.register("kaolin_blocks", BlockKaolinBlocks::new);
	public static final RegistryObject<Block> KAOLIN_BRICK_LAVA = ModRegistry.BLOCKS.register("kaolin_brick_lava", BlockKaolinBrickLava::new);
	public static final RegistryObject<Block> KAOLIN_BRICKS = ModRegistry.BLOCKS.register("kaolin_bricks", BlockKaolinBricks::new);
	public static final RegistryObject<Block> KAOLIN_CHISELED = ModRegistry.BLOCKS.register("kaolin_chiseled", BlockKaolinChiseled::new);
	public static final RegistryObject<Block> LAMP_BLACK_OFF = ModRegistry.BLOCKS.register("lamp_black_off", BlockLampBlackOff::new);
	public static final RegistryObject<Block> LAMP_BLACK_OFF_INV = ModRegistry.BLOCKS.register("lamp_black_off_inv", BlockLampBlackOffInv::new);
	public static final RegistryObject<Block> LAMP_BLACK_ON = ModRegistry.BLOCKS.register("lamp_black_on", BlockLampBlackOn::new);
	public static final RegistryObject<Block> LAMP_BLACK_ON_INV = ModRegistry.BLOCKS.register("lamp_black_on_inv", BlockLampBlackOnInv::new);
	public static final RegistryObject<Block> LAMP_BLUE_OFF = ModRegistry.BLOCKS.register("lamp_blue_off", BlockLampBlueOff::new);
	public static final RegistryObject<Block> LAMP_BLUE_OFF_INV = ModRegistry.BLOCKS.register("lamp_blue_off_inv", BlockLampBlueOffInv::new);
	public static final RegistryObject<Block> LAMP_BLUE_ON = ModRegistry.BLOCKS.register("lamp_blue_on", BlockLampBlueOn::new);
	public static final RegistryObject<Block> LAMP_BLUE_ON_INV = ModRegistry.BLOCKS.register("lamp_blue_on_inv", BlockLampBlueOnInv::new);
	public static final RegistryObject<Block> LAMP_BROWN_OFF = ModRegistry.BLOCKS.register("lamp_brown_off", BlockLampBrownOff::new);
	public static final RegistryObject<Block> LAMP_BROWN_OFF_INV = ModRegistry.BLOCKS.register("lamp_brown_off_inv", BlockLampBrownOffInv::new);
	public static final RegistryObject<Block> LAMP_BROWN_ON = ModRegistry.BLOCKS.register("lamp_brown_on", BlockLampBrownOn::new);
	public static final RegistryObject<Block> LAMP_BROWN_ON_INV = ModRegistry.BLOCKS.register("lamp_brown_on_inv", BlockLampBrownOnInv::new);
	public static final RegistryObject<Block> LAMP_CYAN_OFF = ModRegistry.BLOCKS.register("lamp_cyan_off", BlockLampCyanOff::new);
	public static final RegistryObject<Block> LAMP_CYAN_OFF_INV = ModRegistry.BLOCKS.register("lamp_cyan_off_inv", BlockLampCyanOffInv::new);
	public static final RegistryObject<Block> LAMP_CYAN_ON = ModRegistry.BLOCKS.register("lamp_cyan_on", BlockLampCyanOn::new);
	public static final RegistryObject<Block> LAMP_CYAN_ON_INV = ModRegistry.BLOCKS.register("lamp_cyan_on_inv", BlockLampCyanOnInv::new);
	public static final RegistryObject<Block> LAMP_GRAY_OFF = ModRegistry.BLOCKS.register("lamp_gray_off", BlockLampGrayOff::new);
	public static final RegistryObject<Block> LAMP_GRAY_OFF_INV = ModRegistry.BLOCKS.register("lamp_gray_off_inv", BlockLampGrayOffInv::new);
	public static final RegistryObject<Block> LAMP_GRAY_ON = ModRegistry.BLOCKS.register("lamp_gray_on", BlockLampGrayOn::new);
	public static final RegistryObject<Block> LAMP_GRAY_ON_INV = ModRegistry.BLOCKS.register("lamp_gray_on_inv", BlockLampGrayOnInv::new);
	public static final RegistryObject<Block> LAMP_GREEN_OFF = ModRegistry.BLOCKS.register("lamp_green_off", BlockLampGreenOff::new);
	public static final RegistryObject<Block> LAMP_GREEN_OFF_INV = ModRegistry.BLOCKS.register("lamp_green_off_inv", BlockLampGreenOffInv::new);
	public static final RegistryObject<Block> LAMP_GREEN_ON = ModRegistry.BLOCKS.register("lamp_green_on", BlockLampGreenOn::new);
	public static final RegistryObject<Block> LAMP_GREEN_ON_INV = ModRegistry.BLOCKS.register("lamp_green_on_inv", BlockLampGreenOnInv::new);
	public static final RegistryObject<Block> LAMP_LBLUE_OFF = ModRegistry.BLOCKS.register("lamp_l_blue_off", BlockLampLBlueOff::new);
	public static final RegistryObject<Block> LAMP_LBLUE_OFF_INV = ModRegistry.BLOCKS.register("lamp_l_blue_off_inv", BlockLampLBlueOffInv::new);
	public static final RegistryObject<Block> LAMP_LBLUE_ON = ModRegistry.BLOCKS.register("lamp_l_blue_on", BlockLampLBlueOn::new);
	public static final RegistryObject<Block> LAMP_LBLUE_ON_INV = ModRegistry.BLOCKS.register("lamp_l_blue_on_inv", BlockLampLBlueOnInv::new);
	public static final RegistryObject<Block> LAMP_LGRAY_OFF = ModRegistry.BLOCKS.register("lamp_l_gray_off", BlockLampLGrayOff::new);
	public static final RegistryObject<Block> LAMP_LGRAY_OFF_INV = ModRegistry.BLOCKS.register("lamp_l_gray_off_inv", BlockLampLGrayOffInv::new);
	public static final RegistryObject<Block> LAMP_LGRAY_ON = ModRegistry.BLOCKS.register("lamp_l_gray_on", BlockLampLGrayOn::new);
	public static final RegistryObject<Block> LAMP_LGRAY_ON_INV = ModRegistry.BLOCKS.register("lamp_l_gray_on_inv", BlockLampLGrayOnInv::new);
	public static final RegistryObject<Block> LAMP_LIME_OFF = ModRegistry.BLOCKS.register("lamp_lime_off", BlockLampLimeOff::new);
	public static final RegistryObject<Block> LAMP_LIME_OFF_INV = ModRegistry.BLOCKS.register("lamp_lime_off_inv", BlockLampLimeOffInv::new);
	public static final RegistryObject<Block> LAMP_LIME_ON = ModRegistry.BLOCKS.register("lamp_lime_on", BlockLampLimeOn::new);
	public static final RegistryObject<Block> LAMP_LIME_ON_INV = ModRegistry.BLOCKS.register("lamp_lime_on_inv", BlockLampLimeOnInv::new);
	public static final RegistryObject<Block> LAMP_MAGENTA_OFF = ModRegistry.BLOCKS.register("lamp_magenta_off", BlockLampMagentaOff::new);
	public static final RegistryObject<Block> LAMP_MAGENTA_OFF_INV = ModRegistry.BLOCKS.register("lamp_magenta_off_inv", BlockLampMagentaOffInv::new);
	public static final RegistryObject<Block> LAMP_MAGENTA_ON = ModRegistry.BLOCKS.register("lamp_magenta_on", BlockLampMagentaOn::new);
	public static final RegistryObject<Block> LAMP_MAGENTA_ON_INV = ModRegistry.BLOCKS.register("lamp_magenta_on_inv", BlockLampMagentaOnInv::new);
	public static final RegistryObject<Block> LAMP_ORANGE_OFF = ModRegistry.BLOCKS.register("lamp_orange_off", BlockLampOrangeOff::new);
	public static final RegistryObject<Block> LAMP_ORANGE_OFF_INV = ModRegistry.BLOCKS.register("lamp_orange_off_inv", BlockLampOrangeOffInv::new);
	public static final RegistryObject<Block> LAMP_ORANGE_ON = ModRegistry.BLOCKS.register("lamp_orange_on", BlockLampOrangeOn::new);
	public static final RegistryObject<Block> LAMP_ORANGE_ON_INV = ModRegistry.BLOCKS.register("lamp_orange_on_inv", BlockLampOrangeOnInv::new);
	public static final RegistryObject<Block> LAMP_PINK_OFF = ModRegistry.BLOCKS.register("lamp_pink_off", BlockLampPinkOff::new);
	public static final RegistryObject<Block> LAMP_PINK_OFF_INV = ModRegistry.BLOCKS.register("lamp_pink_off_inv", BlockLampPinkOffInv::new);
	public static final RegistryObject<Block> LAMP_PINK_ON = ModRegistry.BLOCKS.register("lamp_pink_on", BlockLampPinkOn::new);
	public static final RegistryObject<Block> LAMP_PINK_ON_INV = ModRegistry.BLOCKS.register("lamp_pink_on_inv", BlockLampPinkOnInv::new);
	public static final RegistryObject<Block> LAMP_PURPLE_OFF = ModRegistry.BLOCKS.register("lamp_purple_off", BlockLampPurpleOff::new);
	public static final RegistryObject<Block> LAMP_PURPLE_OFF_INV = ModRegistry.BLOCKS.register("lamp_purple_off_inv", BlockLampPurpleOffInv::new);
	public static final RegistryObject<Block> LAMP_PURPLE_ON = ModRegistry.BLOCKS.register("lamp_purple_on", BlockLampPurpleOn::new);
	public static final RegistryObject<Block> LAMP_PURPLE_ON_INV = ModRegistry.BLOCKS.register("lamp_purple_on_inv", BlockLampPurpleOnInv::new);
	public static final RegistryObject<Block> LAMP_RED_OFF = ModRegistry.BLOCKS.register("lamp_red_off", BlockLampRedOff::new);
	public static final RegistryObject<Block> LAMP_RED_OFF_INV = ModRegistry.BLOCKS.register("lamp_red_off_inv", BlockLampRedOffInv::new);
	public static final RegistryObject<Block> LAMP_RED_ON = ModRegistry.BLOCKS.register("lamp_red_on", BlockLampRedOn::new);
	public static final RegistryObject<Block> LAMP_RED_ON_INV = ModRegistry.BLOCKS.register("lamp_red_on_inv", BlockLampRedOnInv::new);
	public static final RegistryObject<Block> LAMP_WHITE_OFF = ModRegistry.BLOCKS.register("lamp_white_off", BlockLampWhiteOff::new);
	public static final RegistryObject<Block> LAMP_WHITE_OFF_INV = ModRegistry.BLOCKS.register("lamp_white_off_inv", BlockLampWhiteOffInv::new);
	public static final RegistryObject<Block> LAMP_WHITE_ON = ModRegistry.BLOCKS.register("lamp_white_on", BlockLampWhiteOn::new);
	public static final RegistryObject<Block> LAMP_WHITE_ON_INV = ModRegistry.BLOCKS.register("lamp_white_on_inv", BlockLampWhiteOnInv::new);
	public static final RegistryObject<Block> LAMP_YELLOW_OFF = ModRegistry.BLOCKS.register("lamp_yellow_off", BlockLampYellowOff::new);
	public static final RegistryObject<Block> LAMP_YELLOW_OFF_INV = ModRegistry.BLOCKS.register("lamp_yellow_off_inv", BlockLampYellowOffInv::new);
	public static final RegistryObject<Block> LAMP_YELLOW_ON = ModRegistry.BLOCKS.register("lamp_yellow_on", BlockLampYellowOn::new);
	public static final RegistryObject<Block> LAMP_YELLOW_ON_INV = ModRegistry.BLOCKS.register("lamp_yellow_on_inv", BlockLampYellowOnInv::new);
	public static final RegistryObject<Block> LASER = ModRegistry.BLOCKS.register("laser", BlockLaser::new);
	public static final RegistryObject<Block> LEAD_BARREL = ModRegistry.BLOCKS.register("lead_barrel", BlockLeadBarrel::new);
	public static final RegistryObject<Block> LEAD_BLOCK = ModRegistry.BLOCKS.register("lead_block", BlockLeadBlock::new);
	public static final RegistryObject<Block> LEAD_DOOR = ModRegistry.BLOCKS.register("lead_door", BlockLeadDoor::new);
	public static final RegistryObject<Block> LEAD_ORE = ModRegistry.BLOCKS.register("lead_ore", BlockLeadOre::new);
	public static final RegistryObject<Block> LEAD_SHELL = ModRegistry.BLOCKS.register("lead_shell", BlockLeadShell::new);
	public static final RegistryObject<Block> LEAD_SHELL_ENERGY_IN = ModRegistry.BLOCKS.register("lead_shell_energy_in", BlockLeadShellEnergyIn::new);
	public static final RegistryObject<Block> LEAD_SHELL_ENERGY_OUT = ModRegistry.BLOCKS.register("lead_shell_energy_out", BlockLeadShellEnergyOut::new);
	public static final RegistryObject<Block> LEAD_SHELL_FLUID_IN = ModRegistry.BLOCKS.register("lead_shell_fluid_in", BlockLeadShellFluidIn::new);
	public static final RegistryObject<Block> LEAD_SHELL_FLUID_OUT = ModRegistry.BLOCKS.register("lead_shell_fluid_out", BlockLeadShellFluidOut::new);
	public static final RegistryObject<Block> LEAD_SHELL_ITEM_IN = ModRegistry.BLOCKS.register("lead_shell_item_in", BlockLeadShellItemIn::new);
	public static final RegistryObject<Block> LEAD_SHELL_ITEM_OUT = ModRegistry.BLOCKS.register("lead_shell_item_out", BlockLeadShellItemOut::new);
	public static final RegistryObject<Block> LEAD_SHELL_REDSTONE_IN = ModRegistry.BLOCKS.register("lead_shell_redstone_in", BlockLeadShellRedstoneIn::new);
	public static final RegistryObject<Block> LEAD_SHELL_REDSTONE_OUT = ModRegistry.BLOCKS.register("lead_shell_redstone_out", BlockLeadShellRedstoneOut::new);
	public static final RegistryObject<Block> LIGHT = ModRegistry.BLOCKS.register("light", BlockLight::new);
	public static final RegistryObject<Block> LIGNITE_ORE = ModRegistry.BLOCKS.register("lignite_ore", BlockLigniteOre::new);
	public static final RegistryObject<Block> LIMESTONE_ROCK = ModRegistry.BLOCKS.register("limestone_rock", BlockLimestone::new);
	public static final RegistryObject<Block> LIMESTONE_BLOCK = ModRegistry.BLOCKS.register("limestone_block", BlockLimestoneBlock::new);
	public static final RegistryObject<Block> LIMESTONE_BLOCKS = ModRegistry.BLOCKS.register("limestone_blocks", BlockLimestoneBlocks::new);
	public static final RegistryObject<Block> LIMESTONE_BRICK_LAVA = ModRegistry.BLOCKS.register("limestone_brick_lava", BlockLimestoneBrickLava::new);
	public static final RegistryObject<Block> LIMESTONE_BRICKS = ModRegistry.BLOCKS.register("limestone_bricks", BlockLimestoneBricks::new);
	public static final RegistryObject<Block> LIMESTONE_CHISELED = ModRegistry.BLOCKS.register("limestone_chiseled", BlockLimestoneChiseled::new);
	public static final RegistryObject<Block> MACERATOR = ModRegistry.BLOCKS.register("macerator", BlockMacerator::new);
	public static final RegistryObject<Block> MACERATOR_OFF = ModRegistry.BLOCKS.register("macerator_off", BlockMaceratorOff::new);
	public static final RegistryObject<Block> MACERATOR_ON = ModRegistry.BLOCKS.register("macerator_on", BlockMaceratorOn::new);
	public static final RegistryObject<Block> MADD_AIR = ModRegistry.BLOCKS.register("madd_air", BlockMaddAir::new);
	public static final RegistryObject<Block> MARBLE_ROCK = ModRegistry.BLOCKS.register("marble_rock", BlockMarble::new);
	public static final RegistryObject<Block> MARBLE_BLOCK = ModRegistry.BLOCKS.register("marble_block", BlockMarbleBlock::new);
	public static final RegistryObject<Block> MARBLE_BLOCKS = ModRegistry.BLOCKS.register("marble_blocks", BlockMarbleBlocks::new);
	public static final RegistryObject<Block> MARBLE_BRICK_LAVA = ModRegistry.BLOCKS.register("marble_brick_lava", BlockMarbleBrickLava::new);
	public static final RegistryObject<Block> MARBLE_BRICKS = ModRegistry.BLOCKS.register("marble_bricks", BlockMarbleBricks::new);
	public static final RegistryObject<Block> MARBLE_CHISELED = ModRegistry.BLOCKS.register("marble_chiseled", BlockMarbleChiseled::new);
	public static final RegistryObject<Block> MOTION_SENSOR = ModRegistry.BLOCKS.register("motion_sensor", BlockMotionSensor::new);
	public static final RegistryObject<Block> NATURAL_GAS_ORE = ModRegistry.BLOCKS.register("natural_gas_ore", BlockNaturalGasOre::new);
	public static final RegistryObject<Block> NETHER_BLOCK = ModRegistry.BLOCKS.register("nether_block", BlockNetherBlock::new);
	public static final RegistryObject<Block> NETHER_BLOCKS = ModRegistry.BLOCKS.register("nether_blocks", BlockNetherBlocks::new);
	public static final RegistryObject<Block> NETHER_BRICK_LAVA = ModRegistry.BLOCKS.register("nether_brick_lava", BlockNetherBrickLava::new);
	public static final RegistryObject<Block> NETHER_BRICKS = ModRegistry.BLOCKS.register("nether_bricks", BlockNetherBrick::new);
	public static final RegistryObject<Block> NETHER_CHISELED = ModRegistry.BLOCKS.register("nether_chiseled", BlockNetherChiseled::new);
	public static final RegistryObject<Block> NICKEL_BLOCK = ModRegistry.BLOCKS.register("nickel_block", BlockNickelBlock::new);
	public static final RegistryObject<Block> NICKEL_ORE = ModRegistry.BLOCKS.register("nickel_ore", BlockNickelOre::new);
	public static final RegistryObject<Block> NICKEL_REDSTONE_CABLE = ModRegistry.BLOCKS.register("nickel_redstone_cable", BlockNickelRedstoneCable::new);
	public static final RegistryObject<Block> NUCLEAR_REACTOR = ModRegistry.BLOCKS.register("nuclear_reactor", BlockNuclearReactor::new);
	public static final RegistryObject<Block> NUCLEAR_REACTOR_ON = ModRegistry.BLOCKS.register("nuclear_reactor_on", BlockNuclearReactorOn::new);
	public static final RegistryObject<Block> NUKE = ModRegistry.BLOCKS.register("nuke", BlockNuke::new);
	public static final RegistryObject<Block> OAK_CHISELED = ModRegistry.BLOCKS.register("oak_chiseled", BlockOakChiseled::new);
	public static final RegistryObject<Block> OAK_PANEL = ModRegistry.BLOCKS.register("oak_panel", BlockOakPanel::new);
	public static final RegistryObject<Block> PEAT = ModRegistry.BLOCKS.register("peat", BlockPeatBlock::new);
	public static final RegistryObject<Block> PLACER = ModRegistry.BLOCKS.register("placer", BlockPlacer::new);
	public static final RegistryObject<Block> POWER_SWITCH = ModRegistry.BLOCKS.register("power_switch", BlockPowerSwitch::new);
	public static final RegistryObject<Block> PUMP = ModRegistry.BLOCKS.register("pump", BlockPump::new);
	public static final RegistryObject<Block> RED_ALLOY_BLOCK = ModRegistry.BLOCKS.register("red_alloy_block", BlockRedAlloyBlock::new);
	public static final RegistryObject<Block> REDSTONE_BRIDGE = ModRegistry.BLOCKS.register("redstone_bridge", BlockRedstoneBridge::new);
	public static final RegistryObject<Block> REDSTONE_CABLE = ModRegistry.BLOCKS.register("redstone_cable", BlockRedstoneCable::new);
	public static final RegistryObject<Block> REDSTONE_EMITTER = ModRegistry.BLOCKS.register("redstone_emitter", BlockRedstoneEmitter::new);
	public static final RegistryObject<Block> REDSTONE_LATCH = ModRegistry.BLOCKS.register("redstone_latch", BlockRedstoneLatch::new);
	public static final RegistryObject<Block> REDSTONE_NOT = ModRegistry.BLOCKS.register("redstone_not", BlockRedstoneNot::new);
	public static final RegistryObject<Block> REDSTONE_PULSER = ModRegistry.BLOCKS.register("redstone_pulser", BlockRedstonePulser::new);
	public static final RegistryObject<Block> RUBY_ORE = ModRegistry.BLOCKS.register("ruby_ore", BlockRubyOre::new);
	public static final RegistryObject<Block> SANDSTONE_BRICKS = ModRegistry.BLOCKS.register("sandstone_bricks", BlockSandStoneBricks::new);
	public static final RegistryObject<Block> SANDSTONE_BRICKS_MOSSY = ModRegistry.BLOCKS.register("sandstone_bricks_mossy", BlockSandStoneBricksMossy::new);
	public static final RegistryObject<Block> SANDSTONE_HEIROGLYPH = ModRegistry.BLOCKS.register("sandstone_heiroglyph", BlockSandStoneHeiroglyph::new);
	public static final RegistryObject<Block> SANDSTONE_HEIROGLYPH2 = ModRegistry.BLOCKS.register("sandstone_heiroglyph2", BlockSandStoneHeiroglyph2::new);
	public static final RegistryObject<Block> SHOP = ModRegistry.BLOCKS.register("shop", BlockShop::new);
	public static final RegistryObject<Block> SILVER_BLOCK = ModRegistry.BLOCKS.register("silver_block", BlockSilverBlock::new);
	public static final RegistryObject<Block> SILVER_ENERGY_CABLE = ModRegistry.BLOCKS.register("silver_energy_cable", BlockSilverEnergyCable::new);
	public static final RegistryObject<Block> SILVER_ORE = ModRegistry.BLOCKS.register("silver_ore", BlockSilverOre::new);
	public static final RegistryObject<Block> SOLAR_PANEL = ModRegistry.BLOCKS.register("solar_panel", BlockSolarPanel::new);
	public static final RegistryObject<Block> SPOTLIGHT = ModRegistry.BLOCKS.register("spotlight", BlockSpotlight::new);
	public static final RegistryObject<Block> STEAM_GENERATOR = ModRegistry.BLOCKS.register("steam_generator", BlockSteamGenerator::new);
	public static final RegistryObject<Block> SPRUCE_CHISELED = ModRegistry.BLOCKS.register("spruce_chiseled", BlockSpruceChiseled::new);
	public static final RegistryObject<Block> SPRUCE_PANEL = ModRegistry.BLOCKS.register("spruce_panel", BlockSprucePanel::new);
	public static final RegistryObject<Block> STEEL_BARREL = ModRegistry.BLOCKS.register("steel_barrel", BlockSteelBarrel::new);
	public static final RegistryObject<Block> STEEL_BLOCK = ModRegistry.BLOCKS.register("steel_block", BlockSteelBlock::new);
	public static final RegistryObject<Block> STEEL_DOOR = ModRegistry.BLOCKS.register("steel_door", BlockSteelDoor::new);
	public static final RegistryObject<Block> STEEL_SHELL = ModRegistry.BLOCKS.register("steel_shell", BlockLeadShell::new);
	public static final RegistryObject<Block> STEEL_SHELL_ENERGY_IN = ModRegistry.BLOCKS.register("steel_shell_energy_in", BlockSteelShellEnergyIn::new);
	public static final RegistryObject<Block> STEEL_SHELL_ENERGY_OUT = ModRegistry.BLOCKS.register("steel_shell_energy_out", BlockSteelShellEnergyOut::new);
	public static final RegistryObject<Block> STEEL_SHELL_FLUID_IN = ModRegistry.BLOCKS.register("steel_shell_fluid_in", BlockSteelShellFluidIn::new);
	public static final RegistryObject<Block> STEEL_SHELL_FLUID_OUT = ModRegistry.BLOCKS.register("steel_shell_fluid_out", BlockSteelShellFluidOut::new);
	public static final RegistryObject<Block> STEEL_SHELL_ITEM_IN = ModRegistry.BLOCKS.register("steel_shell_item_in", BlockSteelShellItemIn::new);
	public static final RegistryObject<Block> STEEL_SHELL_ITEM_OUT = ModRegistry.BLOCKS.register("steel_shell_item_out", BlockSteelShellItemOut::new);
	public static final RegistryObject<Block> STEEL_SHELL_REDSTONE_IN = ModRegistry.BLOCKS.register("steel_shell_redstone_in", BlockSteelShellRedstoneIn::new);
	public static final RegistryObject<Block> STEEL_SHELL_REDSTONE_OUT = ModRegistry.BLOCKS.register("steel_shell_redstone_out", BlockSteelShellRedstoneOut::new);
	public static final RegistryObject<Block> STONE_BLOCK = ModRegistry.BLOCKS.register("stone_block", BlockStoneBlock::new);
	public static final RegistryObject<Block> STONE_BLOCKS = ModRegistry.BLOCKS.register("stone_blocks", BlockStoneBlocks::new);
	public static final RegistryObject<Block> STONE_BRICK_LAVA = ModRegistry.BLOCKS.register("stone_brick_lava", BlockStoneBrickLava::new);
	public static final RegistryObject<Block> STONE_LAMP = ModRegistry.BLOCKS.register("stone_lamp", BlockStoneLamp::new);
	public static final RegistryObject<Block> SUBSTATION = ModRegistry.BLOCKS.register("substation", BlockSubstation::new);
	public static final RegistryObject<Block> SULFUR_ORE = ModRegistry.BLOCKS.register("sulfur_ore", BlockSulfurOre::new);
	public static final RegistryObject<Block> SUPER_TRAMPOLINE = ModRegistry.BLOCKS.register("super_trampoline", BlockSuperTrampoline::new);
	public static final RegistryObject<Block> TRAMPOLINE = ModRegistry.BLOCKS.register("trampoline", BlockTrampoline::new);
	public static final RegistryObject<Block> TERMINAL = ModRegistry.BLOCKS.register("terminal", BlockTerminal::new);
	public static final RegistryObject<Block> TIN_BLOCK = ModRegistry.BLOCKS.register("tin_block", BlockTinBlock::new);
	public static final RegistryObject<Block> TIN_ORE = ModRegistry.BLOCKS.register("tin_ore", BlockTinOre::new);
	public static final RegistryObject<Block> TIN_SHELL = ModRegistry.BLOCKS.register("tin_shell", BlockTinShell::new);
	public static final RegistryObject<Block> TIN_ENERGY_CABLE = ModRegistry.BLOCKS.register("tin_energy_cable", BlockTinEnergyCable::new);
	public static final RegistryObject<Block> TIN_SHELL_ENERGY_IN = ModRegistry.BLOCKS.register("tin_shell_energy_in", BlockTinShellEnergyIn::new);
	public static final RegistryObject<Block> TIN_SHELL_ENERGY_OUT = ModRegistry.BLOCKS.register("tin_shell_energy_out", BlockTinShellEnergyOut::new);
	public static final RegistryObject<Block> TIN_SHELL_FLUID_IN = ModRegistry.BLOCKS.register("tin_shell_fluid_in", BlockTinShellFluidIn::new);
	public static final RegistryObject<Block> TIN_SHELL_FLUID_OUT = ModRegistry.BLOCKS.register("tin_shell_fluid_out", BlockTinShellFluidOut::new);
	public static final RegistryObject<Block> TIN_SHELL_ITEM_IN = ModRegistry.BLOCKS.register("tin_shell_item_in", BlockTinShellItemIn::new);
	public static final RegistryObject<Block> TIN_SHELL_ITEM_OUT = ModRegistry.BLOCKS.register("tin_shell_item_out", BlockTinShellItemOut::new);
	public static final RegistryObject<Block> TIN_SHELL_REDSTONE_IN = ModRegistry.BLOCKS.register("tin_shell_redstone_in", BlockTinShellRedstoneIn::new);
	public static final RegistryObject<Block> TIN_SHELL_REDSTONE_OUT = ModRegistry.BLOCKS.register("tin_shell_redstone_out", BlockTinShellRedstoneOut::new);
	public static final RegistryObject<Block> URANIUM_BLOCK = ModRegistry.BLOCKS.register("uranium_block", BlockUraniumBlock::new);
	public static final RegistryObject<Block> URANIUM_ORE = ModRegistry.BLOCKS.register("uranium_ore", BlockUraniumOre::new);
	public static final RegistryObject<Block> URANIUM_REFINERY = ModRegistry.BLOCKS.register("uranium_refinery", BlockUraniumRefinery::new);
	public static final RegistryObject<Block> VOLCANIC_GLASS = ModRegistry.BLOCKS.register("volcanic_glass", BlockVolcanicGlass::new);
	public static final RegistryObject<Block> VOLCANIC_SAND = ModRegistry.BLOCKS.register("volcanic_sand", BlockVolcanicSand::new);
	public static final RegistryObject<Block> WHITE = ModRegistry.BLOCKS.register("white", BlockWhite::new);
	public static final RegistryObject<Block> WOODEN_BARREL = ModRegistry.BLOCKS.register("wooden_barrel", BlockWoodenBarrel::new);
	public static final RegistryObject<Block> XRAY_BOX = ModRegistry.BLOCKS.register("xray_box", BlockXRayBox::new);
	public static final RegistryObject<Block> XRAY_BOX_ACTIVE = ModRegistry.BLOCKS.register("xray_box_active", BlockXRayBoxActive::new);
	
	public static void register() {};
}
