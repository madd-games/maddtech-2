/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;
import com.madd.maddtech.item.*;

public class ModItems {
	
	// Item Registration
	public static final RegistryObject<Item> AIR_VENT = ModRegistry.ITEMS.register("air_vent", ItemAirVent::new);
	public static final RegistryObject<Item> BATTERY = ModRegistry.ITEMS.register("battery", ItemBattery::new);
	public static final RegistryObject<Item> BRASS_DUST = ModRegistry.ITEMS.register("brass_dust", ItemBrassDust::new);
	public static final RegistryObject<Item> BRASS_INGOT = ModRegistry.ITEMS.register("brass_ingot", ItemBrassIngot::new);
	public static final RegistryObject<Item> BRASS_NUGGET = ModRegistry.ITEMS.register("brass_nugget", ItemBrassNugget::new);
	public static final RegistryObject<Item> BRONZE_DUST = ModRegistry.ITEMS.register("bronze_dust", ItemBronzeDust::new);
	public static final RegistryObject<Item> BRONZE_INGOT = ModRegistry.ITEMS.register("bronze_ingot", ItemBronzeIngot::new);
	public static final RegistryObject<Item> BRONZE_NUGGET = ModRegistry.ITEMS.register("bronze_nugget", ItemBronzeNugget::new);
	public static final RegistryObject<Item> CAPSULE = ModRegistry.ITEMS.register("capsule", ItemCapsule::new);
	public static final RegistryObject<Item> COAL_DUST = ModRegistry.ITEMS.register("coal_dust", ItemCoalDust::new);
	public static final RegistryObject<Item> COIL = ModRegistry.ITEMS.register("coil", ItemCoil::new);
	public static final RegistryObject<Item> COPPER_DUST = ModRegistry.ITEMS.register("copper_dust", ItemCopperDust::new);
	public static final RegistryObject<Item> COPPER_INGOT = ModRegistry.ITEMS.register("copper_ingot", ItemCopperIngot::new);
	public static final RegistryObject<Item> COPPER_NUGGET = ModRegistry.ITEMS.register("copper_nugget", ItemCopperNugget::new);
	public static final RegistryObject<Item> DIAMOND_DUST = ModRegistry.ITEMS.register("diamond_dust", ItemDiamondDust::new);
	public static final RegistryObject<Item> DIAMOND_FRAGMENT = ModRegistry.ITEMS.register("diamond_fragment", ItemDiamondFragment::new);
	public static final RegistryObject<Item> ELECTRIC_DRILL = ModRegistry.ITEMS.register("electric_drill", ItemElectricDrill::new);
	public static final RegistryObject<Item> ELECTRIC_SCREWDRIVER = ModRegistry.ITEMS.register("electric_screwdriver", ItemElectricScrewdriver::new);
	public static final RegistryObject<Item> ELECTRIC_WELDER = ModRegistry.ITEMS.register("electric_welder", ItemElectricWelder::new);
	public static final RegistryObject<Item> EMERALD_DUST = ModRegistry.ITEMS.register("emerald_dust", ItemEmeraldDust::new);
	public static final RegistryObject<Item> EMERALD_FRAGMENT = ModRegistry.ITEMS.register("emerald_fragment", ItemEmeraldFragment::new);
	public static final RegistryObject<Item> ENERGY_CRYSTAL = ModRegistry.ITEMS.register("energy_crystal", ItemEnergyCrystal::new);
	public static final RegistryObject<Item> ENERGY_FILLER = ModRegistry.ITEMS.register("energy_filler", ItemEnergyFiller::new);
	public static final RegistryObject<Item> FAN = ModRegistry.ITEMS.register("fan", ItemFan::new);
	public static final RegistryObject<Item> GAS_WELDER = ModRegistry.ITEMS.register("gas_welder", ItemGasWelder::new);
	public static final RegistryObject<Item> GLOWDUST_BLACK = ModRegistry.ITEMS.register("glowdust_black", ItemGlowdustBlack::new);
	public static final RegistryObject<Item> GLOWDUST_BLUE = ModRegistry.ITEMS.register("glowdust_blue", ItemGlowdustBlue::new);
	public static final RegistryObject<Item> GLOWDUST_BROWN = ModRegistry.ITEMS.register("glowdust_brown", ItemGlowdustBrown::new);
	public static final RegistryObject<Item> GLOWDUST_CYAN = ModRegistry.ITEMS.register("glowdust_cyan", ItemGlowdustCyan::new);
	public static final RegistryObject<Item> GLOWDUST_GRAY = ModRegistry.ITEMS.register("glowdust_gray", ItemGlowdustGray::new);
	public static final RegistryObject<Item> GLOWDUST_GREEN = ModRegistry.ITEMS.register("glowdust_green", ItemGlowdustGreen::new);
	public static final RegistryObject<Item> GLOWDUST_LBLUE = ModRegistry.ITEMS.register("glowdust_l_blue", ItemGlowdustLBlue::new);
	public static final RegistryObject<Item> GLOWDUST_LGRAY = ModRegistry.ITEMS.register("glowdust_l_gray", ItemGlowdustLGray::new);
	public static final RegistryObject<Item> GLOWDUST_LIME = ModRegistry.ITEMS.register("glowdust_lime", ItemGlowdustLime::new);
	public static final RegistryObject<Item> GLOWDUST_MAGENTA = ModRegistry.ITEMS.register("glowdust_magenta", ItemGlowdustMagenta::new);
	public static final RegistryObject<Item> GLOWDUST_ORANGE = ModRegistry.ITEMS.register("glowdust_orange", ItemGlowdustOrange::new);
	public static final RegistryObject<Item> GLOWDUST_PINK = ModRegistry.ITEMS.register("glowdust_pink", ItemGlowdustPink::new);
	public static final RegistryObject<Item> GLOWDUST_PURPLE = ModRegistry.ITEMS.register("glowdust_purple", ItemGlowdustPurple::new);
	public static final RegistryObject<Item> GLOWDUST_RED = ModRegistry.ITEMS.register("glowdust_red", ItemGlowdustRed::new);
	public static final RegistryObject<Item> GLOWDUST_WHITE = ModRegistry.ITEMS.register("glowdust_white", ItemGlowdustWhite::new);
	public static final RegistryObject<Item> GLOWDUST_YELLOW = ModRegistry.ITEMS.register("glowdust_yellow", ItemGlowdustYellow::new);
	public static final RegistryObject<Item> GOLD_DUST = ModRegistry.ITEMS.register("gold_dust", ItemGoldDust::new);
	public static final RegistryObject<Item> IRON_DUST = ModRegistry.ITEMS.register("iron_dust", ItemIronDust::new);
	public static final RegistryObject<Item> LEAD_DUST = ModRegistry.ITEMS.register("lead_dust", ItemLeadDust::new);
	public static final RegistryObject<Item> LEAD_INGOT = ModRegistry.ITEMS.register("lead_ingot", ItemLeadIngot::new);
	public static final RegistryObject<Item> LEAD_NUGGET = ModRegistry.ITEMS.register("lead_nugget", ItemLeadNugget::new);
	public static final RegistryObject<Item> LEAD_ROD = ModRegistry.ITEMS.register("lead_rod", ItemLeadRod::new);
	public static final RegistryObject<Item> LIGNITE = ModRegistry.ITEMS.register("lignite", ItemLignite::new);
	public static final RegistryObject<Item> MICROPROCESSOR = ModRegistry.ITEMS.register("microprocessor", ItemMicroprocessor::new);
	public static final RegistryObject<Item> MINECART_CRATE = ModRegistry.ITEMS.register("minecart_crate", ItemMinecartCrate::new);
	public static final RegistryObject<Item> MINI_RUBY = ModRegistry.ITEMS.register("mini_ruby", ItemMiniRuby::new);
	public static final RegistryObject<Item> NAQ_COOLER = ModRegistry.ITEMS.register("naq_cooler", ItemNaqCooler::new);
	public static final RegistryObject<Item> NETHER_DUST = ModRegistry.ITEMS.register("nether_dust", ItemNetherDust::new);
	public static final RegistryObject<Item> NICKEL_DUST = ModRegistry.ITEMS.register("nickel_dust", ItemNickelDust::new);
	public static final RegistryObject<Item> NICKEL_INGOT = ModRegistry.ITEMS.register("nickel_ingot", ItemNickelIngot::new);
	public static final RegistryObject<Item> NICKEL_NUGGET = ModRegistry.ITEMS.register("nickel_nugget", ItemNickelNugget::new);
	public static final RegistryObject<Item> RED_ALLOY_DUST = ModRegistry.ITEMS.register("red_alloy_dust", ItemRedAlloyDust::new);
	public static final RegistryObject<Item> RED_ALLOY_INGOT = ModRegistry.ITEMS.register("red_alloy_ingot", ItemRedAlloyIngot::new);
	public static final RegistryObject<Item> RED_ALLOY_NUGGET = ModRegistry.ITEMS.register("red_alloy_nugget", ItemRedAlloyNugget::new);
	public static final RegistryObject<Item> RUBY = ModRegistry.ITEMS.register("ruby", ItemRuby::new);
	public static final RegistryObject<Item> SAFETY_SUIT = ModRegistry.ITEMS.register("safety_suit", ItemSafetySuit::new);
	public static final RegistryObject<Item> SCREWDRIVER = ModRegistry.ITEMS.register("screwdriver", ItemScrewdriver::new);
	public static final RegistryObject<Item> SILVER_DUST = ModRegistry.ITEMS.register("silver_dust", ItemSilverDust::new);
	public static final RegistryObject<Item> SILVER_INGOT = ModRegistry.ITEMS.register("silver_ingot", ItemSilverIngot::new);
	public static final RegistryObject<Item> SILVER_NUGGET = ModRegistry.ITEMS.register("silver_nugget", ItemSilverNugget::new);
	public static final RegistryObject<Item> STEEL_DUST = ModRegistry.ITEMS.register("steel_dust", ItemSteelDust::new);
	public static final RegistryObject<Item> STEEL_INGOT = ModRegistry.ITEMS.register("steel_ingot", ItemSteelIngot::new);
	public static final RegistryObject<Item> STEEL_NUGGET = ModRegistry.ITEMS.register("steel_nugget", ItemSteelNugget::new);
	public static final RegistryObject<Item> STEEL_ROD = ModRegistry.ITEMS.register("steel_rod", ItemSteelRod::new);
	public static final RegistryObject<Item> SULFUR = ModRegistry.ITEMS.register("sulfur", ItemSulfur::new);
	public static final RegistryObject<Item> TEMPLATE = ModRegistry.ITEMS.register("template", ItemTemplate::new);
	public static final RegistryObject<Item> THERMAL_INSULATOR = ModRegistry.ITEMS.register("thermal_insulator", ItemThermalInsulator::new);
	public static final RegistryObject<Item> TIN_DUST = ModRegistry.ITEMS.register("tin_dust", ItemTinDust::new);
	public static final RegistryObject<Item> TIN_INGOT = ModRegistry.ITEMS.register("tin_ingot", ItemTinIngot::new);
	public static final RegistryObject<Item> TIN_NUGGET = ModRegistry.ITEMS.register("tin_nugget", ItemTinNugget::new);
	public static final RegistryObject<Item> TURBINE = ModRegistry.ITEMS.register("turbine", ItemTurbine::new);
	public static final RegistryObject<Item> URANIUM_DUST = ModRegistry.ITEMS.register("uranium_dust", ItemUraniumDust::new);
	public static final RegistryObject<Item> URANIUM_INGOT = ModRegistry.ITEMS.register("uranium_ingot", ItemUraniumIngot::new);
	public static final RegistryObject<Item> URANIUM_NUGGET = ModRegistry.ITEMS.register("uranium_nugget", ItemUraniumNugget::new);


	// BlockItem Registration
	public static final RegistryObject<Item> BASALT_BLOCK = ModRegistry.ITEMS.register("basalt_block", () -> new BlockItem(ModBlocks.BASALT_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ACACIA_CHISELED = ModRegistry.ITEMS.register("acacia_chiseled", () -> new BlockItem(ModBlocks.ACACIA_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ACACIA_PANEL = ModRegistry.ITEMS.register("acacia_panel", () -> new BlockItem(ModBlocks.ACACIA_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ACCUMULATOR = ModRegistry.ITEMS.register("accumulator", () -> new BlockItem(ModBlocks.ACCUMULATOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BASALT_ROCK = ModRegistry.ITEMS.register("basalt_rock", () -> new BlockItem(ModBlocks.BASALT_ROCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BASALT_BLOCKS = ModRegistry.ITEMS.register("basalt_blocks", () -> new BlockItem(ModBlocks.BASALT_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BASALT_BRICK_LAVA = ModRegistry.ITEMS.register("basalt_brick_lava", () -> new BlockItem(ModBlocks.BASALT_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BASALT_BRICKS = ModRegistry.ITEMS.register("basalt_bricks", () -> new BlockItem(ModBlocks.BASALT_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BASALT_CHISELED = ModRegistry.ITEMS.register("basalt_chiseled", () -> new BlockItem(ModBlocks.BASALT_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BASALT_COBBLESTONE = ModRegistry.ITEMS.register("basalt_cobblestone", () -> new BlockItem(ModBlocks.BASALT_COBBLESTONE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BIRCH_CHISELED = ModRegistry.ITEMS.register("birch_chiseled", () -> new BlockItem(ModBlocks.BIRCH_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BIRCH_PANEL = ModRegistry.ITEMS.register("birch_panel", () -> new BlockItem(ModBlocks.BIRCH_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BRASS_BLOCK = ModRegistry.ITEMS.register("brass_block", () -> new BlockItem(ModBlocks.BRASS_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BRASS_FIBRE_CABLE = ModRegistry.ITEMS.register("brass_fibre_cable", () -> new BlockItem(ModBlocks.BRASS_FIBRE_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BREAKER = ModRegistry.ITEMS.register("breaker", () -> new BlockItem(ModBlocks.BREAKER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BRICKS_COLORED = ModRegistry.ITEMS.register("bricks_colored", () -> new BlockItem(ModBlocks.BRICKS_COLORED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BRONZE_BLOCK = ModRegistry.ITEMS.register("bronze_block", () -> new BlockItem(ModBlocks.BRONZE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> BRONZE_FIBRE_CABLE = ModRegistry.ITEMS.register("bronze_fibre_cable", () -> new BlockItem(ModBlocks.BRONZE_FIBRE_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> CABINET = ModRegistry.ITEMS.register("cabinet", () -> new BlockItem(ModBlocks.CABINET.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> CENTRIFUGE = ModRegistry.ITEMS.register("centrifuge", () -> new BlockItem(ModBlocks.CENTRIFUGE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> CHEMICAL_REACTOR = ModRegistry.ITEMS.register("chemical_reactor", () -> new BlockItem(ModBlocks.CHEMICAL_REACTOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> COMPRESSOR = ModRegistry.ITEMS.register("compressor", () -> new BlockItem(ModBlocks.COMPRESSOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> COPPER_BLOCK = ModRegistry.ITEMS.register("copper_block", () -> new BlockItem(ModBlocks.COPPER_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> COPPER_ENERGY_CABLE = ModRegistry.ITEMS.register("copper_energy_cable", () -> new BlockItem(ModBlocks.COPPER_ENERGY_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> COPPER_ORE = ModRegistry.ITEMS.register("copper_ore", () -> new BlockItem(ModBlocks.COPPER_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> CRATE = ModRegistry.ITEMS.register("crate", () -> new BlockItem(ModBlocks.CRATE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DARK_OAK_CHISELED = ModRegistry.ITEMS.register("dark_oak_chiseled", () -> new BlockItem(ModBlocks.DARK_OAK_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DARK_OAK_PANEL = ModRegistry.ITEMS.register("dark_oak_panel", () -> new BlockItem(ModBlocks.DARK_OAK_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DENSE_BRICKS = ModRegistry.ITEMS.register("dense_bricks", () -> new BlockItem(ModBlocks.DENSE_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DENSE_BRICKS_REDSTONE = ModRegistry.ITEMS.register("dense_bricks_redstone", () -> new BlockItem(ModBlocks.DENSE_BRICKS_REDSTONE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DEWAR = ModRegistry.ITEMS.register("dewar", () -> new BlockItem(ModBlocks.DEWAR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DOLOMITE_ROCK = ModRegistry.ITEMS.register("dolomite_rock", () -> new BlockItem(ModBlocks.DOLOMITE_ROCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DOLOMITE_BLOCK = ModRegistry.ITEMS.register("dolomite_block", () -> new BlockItem(ModBlocks.DOLOMITE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DOLOMITE_BLOCKS = ModRegistry.ITEMS.register("dolomite_blocks", () -> new BlockItem(ModBlocks.DOLOMITE_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DOLOMITE_BRICK_LAVA = ModRegistry.ITEMS.register("dolomite_brick_lava", () -> new BlockItem(ModBlocks.DOLOMITE_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DOLOMITE_BRICKS = ModRegistry.ITEMS.register("dolomite_bricks", () -> new BlockItem(ModBlocks.DOLOMITE_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DOLOMITE_CHISELED = ModRegistry.ITEMS.register("dolomite_chiseled", () -> new BlockItem(ModBlocks.DOLOMITE_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> DYNAMO = ModRegistry.ITEMS.register("dynamo", () -> new BlockItem(ModBlocks.DYNAMO.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ELECTRIC_FURNACE = ModRegistry.ITEMS.register("electric_furnace", () -> new BlockItem(ModBlocks.ELECTRIC_FURNACE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ENERGY_PROBE = ModRegistry.ITEMS.register("energy_probe", () -> new BlockItem(ModBlocks.ENERGY_PROBE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ENERGY_SHOP = ModRegistry.ITEMS.register("energy_shop", () -> new BlockItem(ModBlocks.ENERGY_SHOP.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ENG_TABLE = ModRegistry.ITEMS.register("eng_table", () -> new BlockItem(ModBlocks.ENG_TABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_CABLE = ModRegistry.ITEMS.register("fluid_cable", () -> new BlockItem(ModBlocks.FLUID_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_DISPENSER = ModRegistry.ITEMS.register("fluid_dispenser", () -> new BlockItem(ModBlocks.FLUID_DISPENSER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_HOLE = ModRegistry.ITEMS.register("fluid_hole", () -> new BlockItem(ModBlocks.FLUID_HOLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_MONITOR = ModRegistry.ITEMS.register("fluid_monitor", () -> new BlockItem(ModBlocks.FLUID_MONITOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_SHOP = ModRegistry.ITEMS.register("fluid_shop", () -> new BlockItem(ModBlocks.FLUID_SHOP.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_TANK = ModRegistry.ITEMS.register("fluid_tank", () -> new BlockItem(ModBlocks.FLUID_TANK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> FLUID_VALVE = ModRegistry.ITEMS.register("fluid_valve", () -> new BlockItem(ModBlocks.FLUID_VALVE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GAS_EXTRACTOR = ModRegistry.ITEMS.register("gas_extractor", () -> new BlockItem(ModBlocks.GAS_EXTRACTOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GLOWNET_CABLE = ModRegistry.ITEMS.register("glownet_cable", () -> new BlockItem(ModBlocks.GLOWNET_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GLOWNET_SWITCH = ModRegistry.ITEMS.register("glownet_switch", () -> new BlockItem(ModBlocks.GLOWNET_SWITCH.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GOLD_ENERGY_CABLE = ModRegistry.ITEMS.register("gold_energy_cable", () -> new BlockItem(ModBlocks.GOLD_ENERGY_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GRANITE_ROCK = ModRegistry.ITEMS.register("granite_rock", () -> new BlockItem(ModBlocks.GRANITE_ROCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GRANITE_BLOCK = ModRegistry.ITEMS.register("granite_block", () -> new BlockItem(ModBlocks.GRANITE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GRANITE_BLOCKS = ModRegistry.ITEMS.register("granite_blocks", () -> new BlockItem(ModBlocks.GRANITE_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GRANITE_BRICK_LAVA = ModRegistry.ITEMS.register("granite_brick_lava", () -> new BlockItem(ModBlocks.GRANITE_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GRANITE_BRICKS = ModRegistry.ITEMS.register("granite_bricks", () -> new BlockItem(ModBlocks.GRANITE_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> GRANITE_CHISELED = ModRegistry.ITEMS.register("granite_chiseled", () -> new BlockItem(ModBlocks.GRANITE_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> IRON_CRATE = ModRegistry.ITEMS.register("iron_crate", () -> new BlockItem(ModBlocks.IRON_CRATE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> IRON_ENERGY_CABLE = ModRegistry.ITEMS.register("iron_energy_cable", () -> new BlockItem(ModBlocks.IRON_ENERGY_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ITEM_CABLE = ModRegistry.ITEMS.register("item_cable", () -> new BlockItem(ModBlocks.ITEM_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ITEM_GATE = ModRegistry.ITEMS.register("item_gate", () -> new BlockItem(ModBlocks.ITEM_GATE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ITEM_MONITOR = ModRegistry.ITEMS.register("item_monitor", () -> new BlockItem(ModBlocks.ITEM_MONITOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ITEM_SORTER = ModRegistry.ITEMS.register("item_sorter", () -> new BlockItem(ModBlocks.ITEM_SORTER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> ITEM_TRANSPORTER = ModRegistry.ITEMS.register("item_transporter", () -> new BlockItem(ModBlocks.ITEM_TRANSPORTER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> JUNGLE_CHISELED = ModRegistry.ITEMS.register("jungle_chiseled", () -> new BlockItem(ModBlocks.JUNGLE_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> JUNGLE_PANEL = ModRegistry.ITEMS.register("jungle_panel", () -> new BlockItem(ModBlocks.JUNGLE_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> KAOLIN_ROCK = ModRegistry.ITEMS.register("kaolin_rock", () -> new BlockItem(ModBlocks.KAOLIN_ROCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> KAOLIN_BLOCK = ModRegistry.ITEMS.register("kaolin_block", () -> new BlockItem(ModBlocks.KAOLIN_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> KAOLIN_BLOCKS = ModRegistry.ITEMS.register("kaolin_blocks", () -> new BlockItem(ModBlocks.KAOLIN_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> KAOLIN_BRICK_LAVA = ModRegistry.ITEMS.register("kaolin_brick_lava", () -> new BlockItem(ModBlocks.KAOLIN_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> KAOLIN_BRICKS = ModRegistry.ITEMS.register("kaolin_bricks", () -> new BlockItem(ModBlocks.KAOLIN_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> KAOLIN_CHISELED = ModRegistry.ITEMS.register("kaolin_chiseled", () -> new BlockItem(ModBlocks.KAOLIN_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_BLACK_OFF = ModRegistry.ITEMS.register("lamp_black_off", () -> new BlockItem(ModBlocks.LAMP_BLACK_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_BLACK_OFF_INV = ModRegistry.ITEMS.register("lamp_black_off_inv", () -> new BlockItem(ModBlocks.LAMP_BLACK_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_BLACK_ON = ModRegistry.ITEMS.register("lamp_black_on", () -> new BlockItem(ModBlocks.LAMP_BLACK_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_BLACK_ON_INV = ModRegistry.ITEMS.register("lamp_black_on_inv", () -> new BlockItem(ModBlocks.LAMP_BLACK_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_BLUE_OFF = ModRegistry.ITEMS.register("lamp_blue_off", () -> new BlockItem(ModBlocks.LAMP_BLUE_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_BLUE_OFF_INV = ModRegistry.ITEMS.register("lamp_blue_off_inv", () -> new BlockItem(ModBlocks.LAMP_BLUE_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_BLUE_ON = ModRegistry.ITEMS.register("lamp_blue_on", () -> new BlockItem(ModBlocks.LAMP_BLUE_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_BLUE_ON_INV = ModRegistry.ITEMS.register("lamp_blue_on_inv", () -> new BlockItem(ModBlocks.LAMP_BLUE_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_BROWN_OFF = ModRegistry.ITEMS.register("lamp_brown_off", () -> new BlockItem(ModBlocks.LAMP_BROWN_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_BROWN_OFF_INV = ModRegistry.ITEMS.register("lamp_brown_off_inv", () -> new BlockItem(ModBlocks.LAMP_BROWN_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_BROWN_ON = ModRegistry.ITEMS.register("lamp_brown_on", () -> new BlockItem(ModBlocks.LAMP_BROWN_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_BROWN_ON_INV = ModRegistry.ITEMS.register("lamp_brown_on_inv", () -> new BlockItem(ModBlocks.LAMP_BROWN_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_CYAN_OFF = ModRegistry.ITEMS.register("lamp_cyan_off", () -> new BlockItem(ModBlocks.LAMP_CYAN_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_CYAN_OFF_INV = ModRegistry.ITEMS.register("lamp_cyan_off_inv", () -> new BlockItem(ModBlocks.LAMP_CYAN_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_CYAN_ON = ModRegistry.ITEMS.register("lamp_cyan_on", () -> new BlockItem(ModBlocks.LAMP_CYAN_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_CYAN_ON_INV = ModRegistry.ITEMS.register("lamp_cyan_on_inv", () -> new BlockItem(ModBlocks.LAMP_CYAN_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_GRAY_OFF = ModRegistry.ITEMS.register("lamp_gray_off", () -> new BlockItem(ModBlocks.LAMP_GRAY_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_GRAY_OFF_INV = ModRegistry.ITEMS.register("lamp_gray_off_inv", () -> new BlockItem(ModBlocks.LAMP_GRAY_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_GRAY_ON = ModRegistry.ITEMS.register("lamp_gray_on", () -> new BlockItem(ModBlocks.LAMP_GRAY_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_GRAY_ON_INV = ModRegistry.ITEMS.register("lamp_gray_on_inv", () -> new BlockItem(ModBlocks.LAMP_GRAY_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_GREEN_OFF = ModRegistry.ITEMS.register("lamp_green_off", () -> new BlockItem(ModBlocks.LAMP_GREEN_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_GREEN_OFF_INV = ModRegistry.ITEMS.register("lamp_green_off_inv", () -> new BlockItem(ModBlocks.LAMP_GREEN_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_GREEN_ON = ModRegistry.ITEMS.register("lamp_green_on", () -> new BlockItem(ModBlocks.LAMP_GREEN_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_GREEN_ON_INV = ModRegistry.ITEMS.register("lamp_green_on_inv", () -> new BlockItem(ModBlocks.LAMP_GREEN_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_LBLUE_OFF = ModRegistry.ITEMS.register("lamp_l_blue_off", () -> new BlockItem(ModBlocks.LAMP_LBLUE_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_LBLUE_OFF_INV = ModRegistry.ITEMS.register("lamp_l_blue_off_inv", () -> new BlockItem(ModBlocks.LAMP_LBLUE_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_LBLUE_ON = ModRegistry.ITEMS.register("lamp_l_blue_on", () -> new BlockItem(ModBlocks.LAMP_LBLUE_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_LBLUE_ON_INV = ModRegistry.ITEMS.register("lamp_l_blue_on_inv", () -> new BlockItem(ModBlocks.LAMP_LBLUE_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_LGRAY_OFF = ModRegistry.ITEMS.register("lamp_l_gray_off", () -> new BlockItem(ModBlocks.LAMP_LGRAY_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_LGRAY_OFF_INV = ModRegistry.ITEMS.register("lamp_l_gray_off_inv", () -> new BlockItem(ModBlocks.LAMP_LGRAY_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_LGRAY_ON = ModRegistry.ITEMS.register("lamp_l_gray_on", () -> new BlockItem(ModBlocks.LAMP_LGRAY_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_LGRAY_ON_INV = ModRegistry.ITEMS.register("lamp_l_gray_on_inv", () -> new BlockItem(ModBlocks.LAMP_LGRAY_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_LIME_OFF = ModRegistry.ITEMS.register("lamp_lime_off", () -> new BlockItem(ModBlocks.LAMP_LIME_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_LIME_OFF_INV = ModRegistry.ITEMS.register("lamp_lime_off_inv", () -> new BlockItem(ModBlocks.LAMP_LIME_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_LIME_ON = ModRegistry.ITEMS.register("lamp_lime_on", () -> new BlockItem(ModBlocks.LAMP_LIME_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_LIME_ON_INV = ModRegistry.ITEMS.register("lamp_lime_on_inv", () -> new BlockItem(ModBlocks.LAMP_LIME_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_MAGENTA_OFF = ModRegistry.ITEMS.register("lamp_magenta_off", () -> new BlockItem(ModBlocks.LAMP_MAGENTA_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_MAGENTA_OFF_INV = ModRegistry.ITEMS.register("lamp_magenta_off_inv", () -> new BlockItem(ModBlocks.LAMP_MAGENTA_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_MAGENTA_ON = ModRegistry.ITEMS.register("lamp_magenta_on", () -> new BlockItem(ModBlocks.LAMP_MAGENTA_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_MAGENTA_ON_INV = ModRegistry.ITEMS.register("lamp_magenta_on_inv", () -> new BlockItem(ModBlocks.LAMP_MAGENTA_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_ORANGE_OFF = ModRegistry.ITEMS.register("lamp_orange_off", () -> new BlockItem(ModBlocks.LAMP_ORANGE_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_ORANGE_OFF_INV = ModRegistry.ITEMS.register("lamp_orange_off_inv", () -> new BlockItem(ModBlocks.LAMP_ORANGE_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_ORANGE_ON = ModRegistry.ITEMS.register("lamp_orange_on", () -> new BlockItem(ModBlocks.LAMP_ORANGE_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_ORANGE_ON_INV = ModRegistry.ITEMS.register("lamp_orange_on_inv", () -> new BlockItem(ModBlocks.LAMP_ORANGE_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_PINK_OFF = ModRegistry.ITEMS.register("lamp_pink_off", () -> new BlockItem(ModBlocks.LAMP_PINK_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_PINK_OFF_INV = ModRegistry.ITEMS.register("lamp_pink_off_inv", () -> new BlockItem(ModBlocks.LAMP_PINK_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_PINK_ON = ModRegistry.ITEMS.register("lamp_pink_on", () -> new BlockItem(ModBlocks.LAMP_PINK_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_PINK_ON_INV = ModRegistry.ITEMS.register("lamp_pink_on_inv", () -> new BlockItem(ModBlocks.LAMP_PINK_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_PURPLE_OFF = ModRegistry.ITEMS.register("lamp_purple_off", () -> new BlockItem(ModBlocks.LAMP_PURPLE_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_PURPLE_OFF_INV = ModRegistry.ITEMS.register("lamp_purple_off_inv", () -> new BlockItem(ModBlocks.LAMP_PURPLE_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_PURPLE_ON = ModRegistry.ITEMS.register("lamp_purple_on", () -> new BlockItem(ModBlocks.LAMP_PURPLE_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_PURPLE_ON_INV = ModRegistry.ITEMS.register("lamp_purple_on_inv", () -> new BlockItem(ModBlocks.LAMP_PURPLE_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_RED_OFF = ModRegistry.ITEMS.register("lamp_red_off", () -> new BlockItem(ModBlocks.LAMP_RED_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_RED_OFF_INV = ModRegistry.ITEMS.register("lamp_red_off_inv", () -> new BlockItem(ModBlocks.LAMP_RED_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_RED_ON = ModRegistry.ITEMS.register("lamp_red_on", () -> new BlockItem(ModBlocks.LAMP_RED_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_RED_ON_INV = ModRegistry.ITEMS.register("lamp_red_on_inv", () -> new BlockItem(ModBlocks.LAMP_RED_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_WHITE_OFF = ModRegistry.ITEMS.register("lamp_white_off", () -> new BlockItem(ModBlocks.LAMP_WHITE_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_WHITE_OFF_INV = ModRegistry.ITEMS.register("lamp_white_off_inv", () -> new BlockItem(ModBlocks.LAMP_WHITE_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_WHITE_ON = ModRegistry.ITEMS.register("lamp_white_on", () -> new BlockItem(ModBlocks.LAMP_WHITE_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_WHITE_ON_INV = ModRegistry.ITEMS.register("lamp_white_on_inv", () -> new BlockItem(ModBlocks.LAMP_WHITE_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_YELLOW_OFF = ModRegistry.ITEMS.register("lamp_yellow_off", () -> new BlockItem(ModBlocks.LAMP_YELLOW_OFF.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_YELLOW_OFF_INV = ModRegistry.ITEMS.register("lamp_yellow_off_inv", () -> new BlockItem(ModBlocks.LAMP_YELLOW_OFF_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LAMP_YELLOW_ON = ModRegistry.ITEMS.register("lamp_yellow_on", () -> new BlockItem(ModBlocks.LAMP_YELLOW_ON.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	//public static final RegistryObject<Item> LAMP_YELLOW_ON_INV = ModRegistry.ITEMS.register("lamp_yellow_on_inv", () -> new BlockItem(ModBlocks.LAMP_YELLOW_ON_INV.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LASER = ModRegistry.ITEMS.register("laser", () -> new BlockItem(ModBlocks.LASER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_BARREL = ModRegistry.ITEMS.register("lead_barrel", () -> new BlockItem(ModBlocks.LEAD_BARREL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_BLOCK = ModRegistry.ITEMS.register("lead_block", () -> new BlockItem(ModBlocks.LEAD_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_DOOR = ModRegistry.ITEMS.register("lead_door", () -> new BlockItem(ModBlocks.LEAD_DOOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_ORE = ModRegistry.ITEMS.register("lead_ore", () -> new BlockItem(ModBlocks.LEAD_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL = ModRegistry.ITEMS.register("lead_shell", () -> new BlockItem(ModBlocks.LEAD_SHELL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_ENERGY_IN = ModRegistry.ITEMS.register("lead_shell_energy_in", () -> new BlockItem(ModBlocks.LEAD_SHELL_ENERGY_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_ENERGY_OUT = ModRegistry.ITEMS.register("lead_shell_energy_out", () -> new BlockItem(ModBlocks.LEAD_SHELL_ENERGY_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_FLUID_IN = ModRegistry.ITEMS.register("lead_shell_fluid_in", () -> new BlockItem(ModBlocks.LEAD_SHELL_FLUID_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_FLUID_OUT = ModRegistry.ITEMS.register("lead_shell_fluid_out", () -> new BlockItem(ModBlocks.LEAD_SHELL_FLUID_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_ITEM_IN = ModRegistry.ITEMS.register("lead_shell_item_in", () -> new BlockItem(ModBlocks.LEAD_SHELL_ITEM_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_ITEM_OUT = ModRegistry.ITEMS.register("lead_shell_item_out", () -> new BlockItem(ModBlocks.LEAD_SHELL_ITEM_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_REDSTONE_IN = ModRegistry.ITEMS.register("lead_shell_redstone_in", () -> new BlockItem(ModBlocks.LEAD_SHELL_REDSTONE_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LEAD_SHELL_REDSTONE_OUT = ModRegistry.ITEMS.register("lead_shell_redstone_out", () -> new BlockItem(ModBlocks.LEAD_SHELL_REDSTONE_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIGNITE_ORE = ModRegistry.ITEMS.register("lignite_ore", () -> new BlockItem(ModBlocks.LIGNITE_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIMESTONE_ROCK = ModRegistry.ITEMS.register("limestone_rock", () -> new BlockItem(ModBlocks.LIMESTONE_ROCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIMESTONE_BLOCK = ModRegistry.ITEMS.register("limestone_block", () -> new BlockItem(ModBlocks.LIMESTONE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIMESTONE_BLOCKS = ModRegistry.ITEMS.register("limestone_blocks", () -> new BlockItem(ModBlocks.LIMESTONE_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIMESTONE_BRICK_LAVA = ModRegistry.ITEMS.register("limestone_brick_lava", () -> new BlockItem(ModBlocks.LIMESTONE_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIMESTONE_BRICKS = ModRegistry.ITEMS.register("limestone_bricks", () -> new BlockItem(ModBlocks.LIMESTONE_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> LIMESTONE_CHISELED = ModRegistry.ITEMS.register("limestone_chiseled", () -> new BlockItem(ModBlocks.LIMESTONE_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MACERATOR = ModRegistry.ITEMS.register("macerator", () -> new BlockItem(ModBlocks.MACERATOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MARBLE_ROCK = ModRegistry.ITEMS.register("marble_rock", () -> new BlockItem(ModBlocks.MARBLE_ROCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MARBLE_BLOCK = ModRegistry.ITEMS.register("marble_block", () -> new BlockItem(ModBlocks.MARBLE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MARBLE_BLOCKS = ModRegistry.ITEMS.register("marble_blocks", () -> new BlockItem(ModBlocks.MARBLE_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MARBLE_BRICK_LAVA = ModRegistry.ITEMS.register("marble_brick_lava", () -> new BlockItem(ModBlocks.MARBLE_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MARBLE_BRICKS = ModRegistry.ITEMS.register("marble_bricks", () -> new BlockItem(ModBlocks.MARBLE_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MARBLE_CHISELED = ModRegistry.ITEMS.register("marble_chiseled", () -> new BlockItem(ModBlocks.MARBLE_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> MOTION_SENSOR = ModRegistry.ITEMS.register("motion_sensor", () -> new BlockItem(ModBlocks.MOTION_SENSOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NETHER_BLOCK = ModRegistry.ITEMS.register("nether_block", () -> new BlockItem(ModBlocks.NETHER_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NETHER_BLOCKS = ModRegistry.ITEMS.register("nether_blocks", () -> new BlockItem(ModBlocks.NETHER_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NETHER_BRICK_LAVA = ModRegistry.ITEMS.register("nether_brick_lava", () -> new BlockItem(ModBlocks.NETHER_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NETHER_BRICKS = ModRegistry.ITEMS.register("nether_bricks", () -> new BlockItem(ModBlocks.NETHER_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NETHER_CHISELED = ModRegistry.ITEMS.register("nether_chiseled", () -> new BlockItem(ModBlocks.NETHER_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NICKEL_BLOCK = ModRegistry.ITEMS.register("nickel_block", () -> new BlockItem(ModBlocks.NICKEL_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NICKEL_ORE = ModRegistry.ITEMS.register("nickel_ore", () -> new BlockItem(ModBlocks.NICKEL_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NICKEL_REDSTONE_CABLE = ModRegistry.ITEMS.register("nickel_redstone_cable", () -> new BlockItem(ModBlocks.NICKEL_REDSTONE_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NUCLEAR_REACTOR = ModRegistry.ITEMS.register("nuclear_reactor", () -> new BlockItem(ModBlocks.NUCLEAR_REACTOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> NUKE = ModRegistry.ITEMS.register("nuke", () -> new BlockItem(ModBlocks.NUKE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> OAK_CHISELED = ModRegistry.ITEMS.register("oak_chiseled", () -> new BlockItem(ModBlocks.OAK_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> OAK_PANEL = ModRegistry.ITEMS.register("oak_panel", () -> new BlockItem(ModBlocks.OAK_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> PEAT = ModRegistry.ITEMS.register("peat", () -> new BlockItem(ModBlocks.PEAT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> PLACER = ModRegistry.ITEMS.register("placer", () -> new BlockItem(ModBlocks.PLACER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> POWER_SWITCH = ModRegistry.ITEMS.register("power_switch", () -> new BlockItem(ModBlocks.POWER_SWITCH.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> PUMP = ModRegistry.ITEMS.register("pump", () -> new BlockItem(ModBlocks.PUMP.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> RED_ALLOY_BLOCK = ModRegistry.ITEMS.register("red_alloy_block", () -> new BlockItem(ModBlocks.RED_ALLOY_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> REDSTONE_BRIDGE = ModRegistry.ITEMS.register("redstone_bridge", () -> new BlockItem(ModBlocks.REDSTONE_BRIDGE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> REDSTONE_CABLE = ModRegistry.ITEMS.register("redstone_cable", () -> new BlockItem(ModBlocks.REDSTONE_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> REDSTONE_EMITTER = ModRegistry.ITEMS.register("redstone_emitter", () -> new BlockItem(ModBlocks.REDSTONE_EMITTER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> REDSTONE_LATCH = ModRegistry.ITEMS.register("redstone_latch", () -> new BlockItem(ModBlocks.REDSTONE_LATCH.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> REDSTONE_NOT = ModRegistry.ITEMS.register("redstone_not", () -> new BlockItem(ModBlocks.REDSTONE_NOT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> REDSTONE_PULSER = ModRegistry.ITEMS.register("redstone_pulser", () -> new BlockItem(ModBlocks.REDSTONE_PULSER.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> RUBY_ORE = ModRegistry.ITEMS.register("ruby_ore", () -> new BlockItem(ModBlocks.RUBY_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SANDSTONE_BRICKS = ModRegistry.ITEMS.register("sandstone_bricks", () -> new BlockItem(ModBlocks.SANDSTONE_BRICKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SANDSTONE_BRICKS_MOSSY = ModRegistry.ITEMS.register("sandstone_bricks_mossy", () -> new BlockItem(ModBlocks.SANDSTONE_BRICKS_MOSSY.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SANDSTONE_HEIROGLYPH = ModRegistry.ITEMS.register("sandstone_heiroglyph", () -> new BlockItem(ModBlocks.SANDSTONE_HEIROGLYPH.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SANDSTONE_HEIROGLYPH2 = ModRegistry.ITEMS.register("sandstone_heiroglyph2", () -> new BlockItem(ModBlocks.SANDSTONE_HEIROGLYPH2.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SHOP = ModRegistry.ITEMS.register("shop", () -> new BlockItem(ModBlocks.SHOP.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SILVER_BLOCK = ModRegistry.ITEMS.register("silver_block", () -> new BlockItem(ModBlocks.SILVER_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SILVER_ENERGY_CABLE = ModRegistry.ITEMS.register("silver_energy_cable", () -> new BlockItem(ModBlocks.SILVER_ENERGY_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SILVER_ORE = ModRegistry.ITEMS.register("silver_ore", () -> new BlockItem(ModBlocks.SILVER_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SOLAR_PANEL = ModRegistry.ITEMS.register("solar_panel", () -> new BlockItem(ModBlocks.SOLAR_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SPOTLIGHT = ModRegistry.ITEMS.register("spotlight", () -> new BlockItem(ModBlocks.SPOTLIGHT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEAM_GENERATOR = ModRegistry.ITEMS.register("steam_generator", () -> new BlockItem(ModBlocks.STEAM_GENERATOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SPRUCE_CHISELED = ModRegistry.ITEMS.register("spruce_chiseled", () -> new BlockItem(ModBlocks.SPRUCE_CHISELED.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SPRUCE_PANEL = ModRegistry.ITEMS.register("spruce_panel", () -> new BlockItem(ModBlocks.SPRUCE_PANEL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_BARREL = ModRegistry.ITEMS.register("steel_barrel", () -> new BlockItem(ModBlocks.STEEL_BARREL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_BLOCK = ModRegistry.ITEMS.register("steel_block", () -> new BlockItem(ModBlocks.STEEL_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_DOOR = ModRegistry.ITEMS.register("steel_door", () -> new BlockItem(ModBlocks.STEEL_DOOR.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL = ModRegistry.ITEMS.register("steel_shell", () -> new BlockItem(ModBlocks.STEEL_SHELL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_ENERGY_IN = ModRegistry.ITEMS.register("steel_shell_energy_in", () -> new BlockItem(ModBlocks.STEEL_SHELL_ENERGY_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_ENERGY_OUT = ModRegistry.ITEMS.register("steel_shell_energy_out", () -> new BlockItem(ModBlocks.STEEL_SHELL_ENERGY_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_FLUID_IN = ModRegistry.ITEMS.register("steel_shell_fluid_in", () -> new BlockItem(ModBlocks.STEEL_SHELL_FLUID_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_FLUID_OUT = ModRegistry.ITEMS.register("steel_shell_fluid_out", () -> new BlockItem(ModBlocks.STEEL_SHELL_FLUID_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_ITEM_IN = ModRegistry.ITEMS.register("steel_shell_item_in", () -> new BlockItem(ModBlocks.STEEL_SHELL_ITEM_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_ITEM_OUT = ModRegistry.ITEMS.register("steel_shell_item_out", () -> new BlockItem(ModBlocks.STEEL_SHELL_ITEM_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_REDSTONE_IN = ModRegistry.ITEMS.register("steel_shell_redstone_in", () -> new BlockItem(ModBlocks.STEEL_SHELL_REDSTONE_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STEEL_SHELL_REDSTONE_OUT = ModRegistry.ITEMS.register("steel_shell_redstone_out", () -> new BlockItem(ModBlocks.STEEL_SHELL_REDSTONE_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STONE_BLOCK = ModRegistry.ITEMS.register("stone_block", () -> new BlockItem(ModBlocks.STONE_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STONE_BLOCKS = ModRegistry.ITEMS.register("stone_blocks", () -> new BlockItem(ModBlocks.STONE_BLOCKS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STONE_BRICK_LAVA = ModRegistry.ITEMS.register("stone_brick_lava", () -> new BlockItem(ModBlocks.STONE_BRICK_LAVA.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> STONE_LAMP = ModRegistry.ITEMS.register("stone_lamp", () -> new BlockItem(ModBlocks.STONE_LAMP.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SUBSTATION = ModRegistry.ITEMS.register("substation", () -> new BlockItem(ModBlocks.SUBSTATION.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SULFUR_ORE = ModRegistry.ITEMS.register("sulfur_ore", () -> new BlockItem(ModBlocks.SULFUR_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> SUPER_TRAMPOLINE = ModRegistry.ITEMS.register("super_trampoline", () -> new BlockItem(ModBlocks.SUPER_TRAMPOLINE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TRAMPOLINE = ModRegistry.ITEMS.register("trampoline", () -> new BlockItem(ModBlocks.TRAMPOLINE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TERMINAL = ModRegistry.ITEMS.register("terminal", () -> new BlockItem(ModBlocks.TERMINAL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_BLOCK = ModRegistry.ITEMS.register("tin_block", () -> new BlockItem(ModBlocks.TIN_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_ORE = ModRegistry.ITEMS.register("tin_ore", () -> new BlockItem(ModBlocks.TIN_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL = ModRegistry.ITEMS.register("tin_shell", () -> new BlockItem(ModBlocks.TIN_SHELL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_ENERGY_CABLE = ModRegistry.ITEMS.register("tin_energy_cable", () -> new BlockItem(ModBlocks.TIN_ENERGY_CABLE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_ENERGY_IN = ModRegistry.ITEMS.register("tin_shell_energy_in", () -> new BlockItem(ModBlocks.TIN_SHELL_ENERGY_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_ENERGY_OUT = ModRegistry.ITEMS.register("tin_shell_energy_out", () -> new BlockItem(ModBlocks.TIN_SHELL_ENERGY_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_FLUID_IN = ModRegistry.ITEMS.register("tin_shell_fluid_in", () -> new BlockItem(ModBlocks.TIN_SHELL_FLUID_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_FLUID_OUT = ModRegistry.ITEMS.register("tin_shell_fluid_out", () -> new BlockItem(ModBlocks.TIN_SHELL_FLUID_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_ITEM_IN = ModRegistry.ITEMS.register("tin_shell_item_in", () -> new BlockItem(ModBlocks.TIN_SHELL_ITEM_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_ITEM_OUT = ModRegistry.ITEMS.register("tin_shell_item_out", () -> new BlockItem(ModBlocks.TIN_SHELL_ITEM_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_REDSTONE_IN = ModRegistry.ITEMS.register("tin_shell_redstone_in", () -> new BlockItem(ModBlocks.TIN_SHELL_REDSTONE_IN.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> TIN_SHELL_REDSTONE_OUT = ModRegistry.ITEMS.register("tin_shell_redstone_out", () -> new BlockItem(ModBlocks.TIN_SHELL_REDSTONE_OUT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> URANIUM_BLOCK = ModRegistry.ITEMS.register("uranium_block", () -> new BlockItem(ModBlocks.URANIUM_BLOCK.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> URANIUM_ORE = ModRegistry.ITEMS.register("uranium_ore", () -> new BlockItem(ModBlocks.URANIUM_ORE.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> URANIUM_REFINERY = ModRegistry.ITEMS.register("uranium_refinery", () -> new BlockItem(ModBlocks.URANIUM_REFINERY.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> VOLCANIC_GLASS = ModRegistry.ITEMS.register("volcanic_glass", () -> new BlockItem(ModBlocks.VOLCANIC_GLASS.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> VOLCANIC_SAND = ModRegistry.ITEMS.register("volcanic_sand", () -> new BlockItem(ModBlocks.VOLCANIC_SAND.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> WOODEN_BARREL = ModRegistry.ITEMS.register("wooden_barrel", () -> new BlockItem(ModBlocks.WOODEN_BARREL.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	public static final RegistryObject<Item> XRAY_BOX = ModRegistry.ITEMS.register("xray_box", () -> new BlockItem(ModBlocks.XRAY_BOX.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	
	//public static final RegistryObject<Item> BASALT = ModRegistry.ITEMS.register("basalt", () -> new BlockItem(ModBlocks.BASALT.get(), new Item.Properties().tab(ItemGroup.TAB_BUILDING_BLOCKS)));
	
	public static void register() {};
}