/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;

import net.minecraft.world.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.util.DamageSource;


/**
 * An explosion with an extra "nuclear" stage that has special effects on blocks.
 */
public class NuclearExplosion extends Explosion
{
	public NuclearExplosion(World p_i231610_1_, Entity p_i231610_2_, DamageSource p_i231610_3_,
			ExplosionContext p_i231610_4_, double p_i231610_5_, double p_i231610_7_, double p_i231610_9_,
			float p_i231610_11_, boolean p_i231610_12_, Mode p_i231610_13_) {
		super(p_i231610_1_, p_i231610_2_, p_i231610_3_, p_i231610_4_, p_i231610_5_, p_i231610_7_, p_i231610_9_, p_i231610_11_,
				p_i231610_12_, p_i231610_13_);
		// TODO Auto-generated constructor stub
	}

	private World world;
	
	
	
//	private Block mapBlock(Random random, Block block)
//	{
//		if (block == Blocks.dirt)
//		{
//			return Blocks.gravel;
//		}
//		else if (block == Blocks.grass)
//		{
//			return Blocks.gravel;
//		}
//		else if (block == Blocks.farmland)
//		{
//			return Blocks.gravel;
//		}
//		else if (block == Blocks.gravel)
//		{
//			return Blocks.air;
//		}
//		else if (block == Blocks.sand)
//		{
//			return Blocks.sand;
//		}
//		else if (block instanceof BlockLiquid)
//		{
//			return Blocks.air;
//		}
//		else if (block == Blocks.stone)
//		{
//			if (random.nextInt(100) < 70)
//			{
//				return Blocks.air;
//			};
//			
//			return Blocks.cobblestone;
//		}
//		else if (block == Blocks.cobblestone)
//		{
//			return Blocks.air;
//		}
//		else if (block.getMaterial() == Material.glass)
//		{
//			return Blocks.air;
//		};
//		
//		return null;
//	};
//	
//	private void hitBlock(Random random, int x, int y, int z)
//	{
//		if (y == 0)
//		{
//			return;
//		};
//		
//		if (random.nextInt(100) < 25)
//		{
//			// 25% chance of simply destroying the block
//			world.setBlockToAir(x, y, z);
//		}
//		else
//		{
//			Block block = world.getBlock(x, y, z);
//			Block newBlock = mapBlock(random, block);
//			
//			if (newBlock != null)
//			{
//				world.setBlock(x, y, z, newBlock);
//			};
//			
//
//		};
//	};
//	
//	private void hitBlockWithFire(Random random, int x, int y, int z)
//	{
//		Block block = world.getBlock(x, y, z);
//		if (block == Blocks.air)
//		{
//			Block blockUnder = world.getBlock(x, y-1, z);
//			if ((blockUnder.getMaterial() == Material.wood) || (blockUnder == Blocks.leaves) || (blockUnder == Blocks.leaves))
//			{
//				if (random.nextInt(100) < 30)
//				{
//					// 30% chance of setting wooden blocks on fire
//					world.setBlock(x, y, z, Blocks.fire);
//				};
//			};
//		};
//	};
//	
//	private void doFireThrows(int radius)
//	{
//		// set entities within the radius on fire.
//		double r = (double) radius;
//		AxisAlignedBB bb = AxisAlignedBB.getBoundingBox(
//			explosionX - r, explosionY - r, explosionZ - r,
//			explosionX + r, explosionY + r, explosionZ + r
//		);
//		
//		List entities = world.getEntitiesWithinAABB(Entity.class, bb);
//		int i;
//		for (i=0; i<entities.size(); i++)
//		{
//			Entity ent = (Entity) entities.get(i);
//			ent.setFire(50);
//		};
//	};
//	
//	public void doExplosionNuclear(double factor)
//	{
//		int radius = (int) (0.5 * explosionY * factor);
//		
//		doFireThrows(radius*2);
//		
//		int startX = (int)explosionX - 2*radius;
//		int startY = (int)explosionY - radius;
//		int startZ = (int)explosionZ - radius;
//		
//		int endX = (int)explosionX + 2*radius;
//		int endY = (int)explosionY + 2*radius;
//		int endZ = (int)explosionZ + 2*radius;
//
//		Random random = new Random();
//		
//		int x, y, z;
//		for (x=startX; x<=endX; x++)
//		{
//			for (y=startY; y<=endY; y++)
//			{
//				for (z=startZ; z<=endZ; z++)
//				{
//					// make sure we are within bounds
//					if ((y > 0) && (y < 256))
//					{
//						//System.out.println("TRYING: (" + x + ", " + y + ", " + z + ")");
//						
//						// furthermore, ensure it is within radius
//						int offX = (int)explosionX - x;
//						int offY = (int)explosionY - y;
//						int offZ = (int)explosionZ - z;
//						
//						//System.out.println("DISTANCE: (" + offX + ", " + offY + ", " + offZ + ")");
//						if (offX*offX + offY*offY + offZ*offZ < radius*radius)
//						{
//							hitBlock(random, x, y, z);
//						};
//						
//						if (offX*offX + offY*offY + offZ*offZ < 4*radius*radius)
//						{
//							hitBlockWithFire(random, x, y, z);
//						};
//					};
//				};
//			};
//		};
//	};
};
