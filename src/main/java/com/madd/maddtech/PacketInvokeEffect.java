/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;

import io.netty.buffer.*;
import net.minecraft.world.*;
import java.io.*;
import java.util.*;

/**
 * This packet is sent to clients to spawn effects like smoke.
 */
public class PacketInvokeEffect
{
	public static final int SMOKE = 0;

	public int dim;
	public int x;
	public int y;
	public int z;
	public int effect;

	public PacketInvokeEffect()
	{
	};

//	public PacketInvokeEffect(World world, int x, int y, int z, int effect)
//	{
//		this.dim = world.provider.dimensionId;
//		this.x = x;
//		this.y = y;
//		this.z = z;
//		this.effect = effect;
//	};
//
//	@Override
//	public void fromBytes(ByteBuf buf)
//	{
//		dim = buf.readInt();
//		x = buf.readInt();
//		y = buf.readInt();
//		z = buf.readInt();
//		effect = buf.readInt();
//	};
//
//	@Override
//	public void toBytes(ByteBuf buf)
//	{
//		buf.writeInt(dim);
//		buf.writeInt(x);
//		buf.writeInt(y);
//		buf.writeInt(z);
//		buf.writeInt(effect);
//	};
//
//	public static class Handler implements IMessageHandler<PacketInvokeEffect, PacketInvokeEffect>
//	{
//		public Handler()
//		{
//		};
//
//		@Override
//		public PacketInvokeEffect onMessage(PacketInvokeEffect packet, MessageContext ctx)
//		{
//			Random random = new Random();
//
//			double offX = 0.1*(random.nextGaussian()-0.5);
//			double offZ = 0.1*(random.nextGaussian()-0.5);
//
//			//World world = Minecraft.getMinecraft().theWorld;
//			World world = MaddTech.proxy.getWorld();
//			if (world != null)
//			{
//				if (world.provider.dimensionId == packet.dim)
//				{
//					switch (packet.effect)
//					{
//					case SMOKE:
//						world.spawnParticle("smoke",
//							(double)packet.x+0.5+offX,
//							(double)packet.y,
//							(double)packet.z+0.5+offZ,
//							0.0, 0.1+0.1*(double)random.nextGaussian(), 0.0);
//					};
//				};
//			};
//
//			return null;
//		};
//	};
};
