/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech;


import io.netty.buffer.*;
import net.minecraft.client.*;
import net.minecraft.world.*;
import java.io.*;
import java.util.*;
import net.minecraft.server.*;
import net.minecraft.tileentity.*;
import java.nio.charset.*;

/**
 * This packet is sent to the server to change Terminal prices.
 */
public class PacketTerminalUpdate
{
	public int dim;
	public int x;
	public int y;
	public int z;
	public int linkno;
	public String target;

	public PacketTerminalUpdate()
	{
	};

//	public PacketTerminalUpdate(World world, int x, int y, int z, int linkno, String target)
//	{
//		this.dim = world.provider.dimensionId;
//		this.x = x;
//		this.y = y;
//		this.z = z;
//		this.linkno = linkno;
//		this.target = target;
//	};
//
//	@Override
//	public void fromBytes(ByteBuf buf)
//	{
//		dim = buf.readInt();
//		x = buf.readInt();
//		y = buf.readInt();
//		z = buf.readInt();
//		linkno = buf.readInt();
//		
//		int strlen = buf.readInt();
//		byte[] bytes = new byte[strlen];
//		buf.readBytes(bytes);
//		
//		target = new String(bytes, StandardCharsets.UTF_8);
//	};
//
//	@Override
//	public void toBytes(ByteBuf buf)
//	{
//		buf.writeInt(dim);
//		buf.writeInt(x);
//		buf.writeInt(y);
//		buf.writeInt(z);
//		buf.writeInt(linkno);
//		buf.writeInt(target.length());
//		buf.writeBytes(target.getBytes(StandardCharsets.UTF_8));
//	};
//
//	public static class Handler implements IMessageHandler<PacketTerminalUpdate, PacketTerminalUpdate>
//	{
//		public Handler()
//		{
//		};
//
//		private World getWorldByDim(int dim)
//		{
//			WorldServer[] worlds = MinecraftServer.getServer().worldServers;
//			int i;
//			for (i=0; i<worlds.length; i++)
//			{
//				World world = worlds[i];
//				if (world.provider.dimensionId == dim)
//				{
//					return world;
//				};
//			};
//			
//			return null;
//		};
//		
//		@Override
//		public PacketTerminalUpdate onMessage(PacketTerminalUpdate packet, MessageContext ctx)
//		{
//			World world = getWorldByDim(packet.dim);
//			if (world != null)
//			{
//				TileEntity te = world.getTileEntity(packet.x, packet.y, packet.z);
//				if (te != null)
//				{
//					if (te instanceof TileEntityTerminal)
//					{
//						TileEntityTerminal term = (TileEntityTerminal) te;
//						term.setTarget(packet.linkno, packet.target);
//					};
//				};
//			}
//			else
//			{
//				System.out.println("[MADDTECH] WARNING: Failed to find dimension for Terminal update!");
//			};
//			
//			return null;
//		};
//	};
};
