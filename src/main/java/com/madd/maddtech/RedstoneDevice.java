/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * This file is here so that we can easily make all the edgy redstone devices!
 * Look at the block JSON files for such devices to understand.
 */

package com.madd.maddtech;

import net.minecraft.world.*;
import net.minecraft.block.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;
import java.util.*;

public abstract class RedstoneDevice
{
	public static final int NONE = 0;
	public static final int INPUT = 1;
	public static final int OUTPUT = 2;

	/**
	 * This is called when the device is right-clicked with a screwdriver. It shall return the
	 * new metadata for the block.
	 */
	public int onScrewdriver(int metadata)
	{
		return metadata;
	};

	/**
	 * Get an array, such that each index is a side number, and indicates whether it is an INPUT, OUTPUT,
	 * or NONE (indicating the side is not used), when the block has the given metadata.
	 */
	public abstract int[] getSideTypes(int metadata);

	/**
	 * Called when the device needs to be updated; i.e. when a neightbor block changes. The array passed as
	 * an argument indicates the strength of each input side; all output and unused sides have their corresponding
	 * value set to 0. This function shall return the new metadata for the block.
	 */
	public int update(int[] sidePower, int metadata)
	{
		return metadata;
	};

	/**
	 * This is called when information about an output is needed. In the sidePowers array, inputs are set to the
	 * right value, and everything else is left at 0. This function shall fill in the outputs.
	 * Return true if the block must be updated.
	 */
	public abstract boolean getOutputs(int[] sidePowers, int metadata);

	/**
	 * This is called to register icons on the client.
	 */
//	public abstract void registerIcons(IIconRegister ir);
//
//	/**
//	 * Return the texture to use for the specified side with the specified metadata.
//	 */
//	public abstract IIcon getIcon(int side, int metadata);
};
