/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * RedstoneDeviceEmitter.java
 * The "redstone emitter" - emits a redstone signal of a specific strength in all directions,
 * and the strength can be changed with a scredriver.
 */

package com.madd.maddtech;

import net.minecraft.world.*;
import net.minecraft.block.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;
import java.util.*;

public class RedstoneDeviceEmitter extends RedstoneDevice
{
//	private IIcon iconSide;
//	private IIcon[] numberIcons = new IIcon[16];

	public RedstoneDeviceEmitter()
	{
	};

	@Override
	public int onScrewdriver(int meta)
	{
		return (meta+1) & 0xF;
	};

	@Override
	public int[] getSideTypes(int meta)
	{
		return new int[] {OUTPUT, OUTPUT, OUTPUT, OUTPUT, OUTPUT, OUTPUT};
	};

	@Override
	public boolean getOutputs(int[] powers, int metadata)
	{
		int i;
		for (i=0; i<6; i++)
		{
			powers[i] = metadata;
		};

		return false;
	};

//	@Override
//	public void registerIcons(IIconRegister ir)
//	{
//		iconSide = ir.registerIcon("maddtech:RedstoneEmitter");
//
//		int i;
//		for (i=0; i<16; i++)
//		{
//			numberIcons[i] = ir.registerIcon("maddtech:RedstoneEmitter_" + i);
//		};
//	};
//
//	@Override
//	public IIcon getIcon(int side, int meta)
//	{
//		if (side != 1)
//		{
//			return iconSide;
//		};
//
//		return numberIcons[meta];
//	};
};
