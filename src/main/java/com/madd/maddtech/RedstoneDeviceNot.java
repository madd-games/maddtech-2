/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * RedstoneDeviceNot.java
 * The "NOT" gate.
 */

package com.madd.maddtech;

import net.minecraft.world.*;
import net.minecraft.block.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;
import java.util.*;
import java.io.*;

public class RedstoneDeviceNot extends RedstoneDevice
{
//	private IIcon iconInput;
//	private IIcon iconOutput;
//	private IIcon iconGate;
//	private IIcon iconOther;

	public RedstoneDeviceNot()
	{
	};

	@Override
	public int onScrewdriver(int meta)
	{
		return (meta+1) % 6;
	};

	private int getInputSide(int meta)
	{
		return meta;
	};

	private int getOutputSide(int meta)
	{
		return meta ^ 1;
	};

	@Override
	public int[] getSideTypes(int meta)
	{
		int sideInput = getInputSide(meta);
		int sideOutput = getOutputSide(meta);

		int[] sides = new int[] {NONE, NONE, NONE, NONE, NONE, NONE};
		sides[sideInput] = INPUT;
		sides[sideOutput] = OUTPUT;

		return sides;
	};

	@Override
	public int update(int sidePower[], int meta)
	{
		return meta;
	};

	@Override
	public boolean getOutputs(int[] powers, int meta)
	{
		int sideInput = getInputSide(meta);
		int sideOutput = getOutputSide(meta);

		if (powers[sideInput] > 0)
		{
			powers[sideOutput] = 0;
		}
		else
		{
			powers[sideOutput] = 15;
		};

		return true;
	};

//	@Override
//	public void registerIcons(IIconRegister ir)
//	{
//		iconInput = ir.registerIcon("maddtech:RedstoneBridgeInput");
//		iconOutput = ir.registerIcon("maddtech:RedstoneBridgeOutput");
//		iconOther = ir.registerIcon("maddtech:RedstoneNot");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int meta)
//	{
//		int sideInput = getInputSide(meta);
//		int sideOutput = getOutputSide(meta);
//
//		if (side == sideInput)
//		{
//			return iconInput;
//		}
//		else if (side == sideOutput)
//		{
//			return iconOutput;
//		}
//		else
//		{
//			return iconOther;
//		}
//	};
};
