/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import com.madd.maddtech.*;
import com.madd.maddtech.item.ItemCapsule;

import net.minecraft.item.*;

/**
 * This class can be used to access capsules.
 */
public class CapsuleAccess implements IFluidMachine
{
	private ItemStack stack;
	private ItemCapsule capsule;

	/**
	 * Constructor. The ItemStack passed in does not have to be an actual capsule - in that
	 * case, the machine will just ignore all requests for push and pull, accepting and returning
	 * no energy.
	 */
	public CapsuleAccess(ItemStack stack)
	{
		if (stack.getItem() == MaddTech.itemCapsule)
		{
			this.stack = stack;
		}
		else
		{
			this.stack = null;
		};

		capsule = (ItemCapsule) MaddTech.itemCapsule;
	};

	@Override
	public boolean canConnectFluid(int side)
	{
		return true;
	}

	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	};

//	@Override
//	public String getFluidType(int side)
//	{
//		if (stack == null)
//		{
//			return "maddtech:empty";
//		}
//		else
//		{
//			return capsule.getContent(stack);
//		}
//	};

//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		if (getFluidType(0).equals("maddtech:empty"))
//		{
//			return 0;
//		}
//		else
//		{
//			return capsule.pullLiquid(stack, max);
//		}
//	};
//
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		if (stack == null)
//		{
//			return 0;
//		}
//		else
//		{
//			return capsule.addLiquid(stack, id, max);
//		}
//	};
};
