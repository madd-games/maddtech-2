/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import com.madd.maddtech.*;
import net.minecraft.item.*;
import net.minecraft.block.*;
import java.util.*;
import java.io.*;

/**
 * Represents a basic engineering table recipe. Supports the ore dictionary too.
 */
public class EngTableRecipe implements IEngTableRecipe
{
	private String name;
	private Object[] inputs;
	private int inWidth;
	private int inHeight;
	
	private ItemStack[] outputs;
	private int outWidth;
	private int outHeight;
	
	private ToolSpec[] tools;
	
	public EngTableRecipe(String name, int inWidth, int inHeight, Object[] inputs, ToolSpec[] tools,
		int outWidth, int outHeight, ItemStack[] outputs)
	{
		this.name = name;
		
		this.inputs = inputs;
		this.inWidth = inWidth;
		this.inHeight = inHeight;
		
		this.outputs = outputs;
		this.outWidth = outWidth;
		this.outHeight = outHeight;
		
		this.tools = tools;
	};
	
//	@Override
//	public String getName()
//	{
//		return name;
//	};
//	
//	private boolean haveTool(TileEntityEngTable table, String type)
//	{
//		int i;
//		for (i=0; i<3; i++)
//		{
//			ItemStack stack = table.getStackInSlot(table.TOOL_SLOT_BASE+i);
//			if (stack != null)
//			{
//				IEngTableTool tool = (IEngTableTool) stack.getItem();
//				if (tool.getToolType().equals(type))
//				{
//					if ((stack.getItem().getMaxDamage() == 0) || (stack.getItemDamage() < stack.getItem().getMaxDamage()))
//					{
//						return true;
//					};
//				};
//			};
//		};
//		
//		return false;
//	};
//	
//	private boolean canMergeStack(ItemStack a, ItemStack b)
//	{
//		if ((a == null) || (b == null))
//		{
//			return true;
//		};
//		
//		if (a.getItem() != b.getItem())
//		{
//			return false;
//		};
//		
//		if (a.getItemDamage() != b.getItemDamage())
//		{
//			return false;
//		};
//		
//		if ((a.stackSize+b.stackSize) > a.getMaxStackSize())
//		{
//			return false;
//		};
//		
//		return true;
//	};
//	
//	private boolean canExecOutputs(TileEntityEngTable table)
//	{
//		int startX = 2 - (outWidth/2);
//		int startY = 2 - (outHeight/2);
//		
//		int x, y;
//		for (x=0; x<outWidth; x++)
//		{
//			for (y=0; y<outHeight; y++)
//			{
//				int slot = table.OUTPUT_SLOT_BASE + (startY+y) * 5 + (startX+x);
//				ItemStack stack = table.getStackInSlot(slot);
//				if (!canMergeStack(stack, outputs[y * outWidth + x]))
//				{
//					return false;
//				};
//			};
//		};
//		
//		return true;
//	};
//	
//	@Override
//	public boolean isMatchingRecipe(TileEntityEngTable table)
//	{
//		// check if we have all the needed tools.
//		int i;
//		for (i=0; i<tools.length; i++)
//		{
//			if (!haveTool(table, tools[i].type))
//			{
//				return false;
//			};
//		};
//		
//		if (!canExecOutputs(table))
//		{
//			return false;
//		};
//		
//		int spareX = 5 - inWidth;
//		int spareY = 5 - inHeight;
//		
//		int x, y;
//		for (x=0; x<=spareX; x++)
//		{
//			for (y=0; y<=spareY; y++)
//			{
//				if (isMatchingRecipeFrom(table, x, y))
//				{
//					return true;
//				};
//			};
//		};
//		
//		return false;
//	};
//	
//	private boolean isMatchingOredictName(String name, ItemStack stack)
//	{
//		int[] ids = OreDictionary.getOreIDs(stack);
//		int i;
//		
//		for (i=0; i<ids.length; i++)
//		{
//			String oreName = OreDictionary.getOreName(ids[i]);
//			if (oreName.equals(name))
//			{
//				return true;
//			};
//		};
//		
//		return false;
//	};
//	
//	private boolean isRestEmpty(TileEntityEngTable table, int left, int top)
//	{
//		int x, y;
//		for (x=0; x<5; x++)
//		{
//			for (y=0; y<5; y++)
//			{
//				int relX = x - left;
//				int relY = y - top;
//				
//				if ((relX < 0) || (relX >= inWidth) || (relY < 0) || (relY >= inHeight))
//				{
//					// outside of the recipe box, must be empty
//					ItemStack stack = table.getStackInSlot(table.INPUT_SLOT_BASE + y * 5 + x);
//					if (stack != null)
//					{
//						return false;
//					};
//				};
//			};
//		};
//		
//		return true;
//	};
//	
//	private boolean isMatchingRecipeFrom(TileEntityEngTable table, int left, int top)
//	{
//		int x, y;
//		for (x=0; x<inWidth; x++)
//		{
//			for (y=0; y<inHeight; y++)
//			{
//				int slot = table.INPUT_SLOT_BASE + (y+top) * 5 + (x+left);
//				ItemStack stackInSlot = table.getStackInSlot(slot);
//				
//				Object inObj = inputs[y * inWidth + x];
//				if (inObj == null)
//				{
//					if (stackInSlot != null)
//					{
//						return false;
//					};
//				}
//				else if (inObj instanceof String)
//				{
//					if (stackInSlot == null)
//					{
//						return false;
//					};
//					
//					String name = (String) inObj;
//					if (!isMatchingOredictName(name, stackInSlot))
//					{
//						return false;
//					};
//				}
//				else if (inObj instanceof Block)
//				{
//					if (stackInSlot == null)
//					{
//						return false;
//					};
//					
//					Block block = (Block) inObj;
//					ItemStack stack = new ItemStack(block, 1);
//					if (stack.getItem() != stackInSlot.getItem())
//					{
//						return false;
//					};
//				}
//				else if (inObj instanceof Item)
//				{
//					if (stackInSlot == null)
//					{
//						return false;
//					};
//					
//					Item item = (Item) inObj;
//					if (item != stackInSlot.getItem())
//					{
//						return false;
//					};
//				}
//				else if (inObj instanceof EngTableFluidInput)
//				{
//					if (stackInSlot == null)
//					{
//						return false;
//					};
//					
//					EngTableFluidInput inFluid = (EngTableFluidInput) inObj;
//					Item item = stackInSlot.getItem();
//					boolean ok = false;
//					if (item instanceof ItemCapsule)
//					{
//						ItemCapsule capsule = (ItemCapsule) item;
//						if (capsule.getContent(stackInSlot).equals(inFluid.type))
//						{
//							if (capsule.getAmount(stackInSlot) >= inFluid.amount)
//							{
//								ok = true;
//							};
//						};
//					};
//					
//					if (!ok) return false;
//				}
//				else
//				{
//					return false;
//				};
//			};
//		};
//		
//		return isRestEmpty(table, left, top);
//	};
//	
//	private void removeInputsFrom(TileEntityEngTable table, int left, int top)
//	{
//		int x, y;
//		for (x=0; x<inWidth; x++)
//		{
//			for (y=0; y<inHeight; y++)
//			{
//				int slot = table.INPUT_SLOT_BASE + (y+top) * 5 + (x+left);
//				ItemStack stackInSlot = table.getStackInSlot(slot);
//				if (stackInSlot != null)
//				{
//					Object inObj = inputs[y * inWidth + x];
//					if (inObj instanceof EngTableFluidInput)
//					{
//						ItemCapsule capsule = (ItemCapsule) stackInSlot.getItem();
//						capsule.pullLiquid(stackInSlot, ((EngTableFluidInput)inObj).amount);
//					}
//					else
//					{
//						stackInSlot.stackSize--;
//						if (stackInSlot.stackSize == 0)
//						{
//							table.setInventorySlotContents(slot, null);
//						};
//					};
//				};
//			};
//		};
//	};
//	
//	private void removeInputs(TileEntityEngTable table)
//	{
//		int spareX = 5 - inWidth;
//		int spareY = 5 - inHeight;
//		
//		int x, y;
//		for (x=0; x<=spareX; x++)
//		{
//			for (y=0; y<=spareY; y++)
//			{
//				if (isMatchingRecipeFrom(table, x, y))
//				{
//					removeInputsFrom(table, x, y);
//					return;
//				};
//			};
//		};
//	};
//	
//	@Override
//	public void execRecipe(TileEntityEngTable table)
//	{
//		removeInputs(table);
//		
//		int startX = 2 - (outWidth/2);
//		int startY = 2 - (outHeight/2);
//		
//		int x, y;
//		for (x=0; x<outWidth; x++)
//		{
//			for (y=0; y<outHeight; y++)
//			{
//				int slot = table.OUTPUT_SLOT_BASE + (y+startY) * 5 + (x+startX);
//				ItemStack stack = table.getStackInSlot(slot);
//				
//				if (stack == null)
//				{
//					table.setInventorySlotContents(slot, outputs[y * outWidth + x].copy());
//				}
//				else
//				{
//					stack.stackSize += outputs[y * outWidth + x].stackSize;
//				};
//			};
//		};
//	};
//	
//	@Override
//	public ToolSpec[] getTools()
//	{
//		return tools;
//	};
//	
//	/**
//	 * Returns an array of ItemStacks, which can be passed to the CraftGuide recipe generator.
//	 * Returns null if there is a problem with the recipe.
//	 */
//	public Object[] getCraftGuide()
//	{
//		ArrayList<ItemStack> guide = new ArrayList<ItemStack>();
//		guide.add(new ItemStack(MaddTech.blockEngTable, 1));
//		
//		int marginX = 5 - inWidth;
//		int marginY = 5 - inHeight;
//		
//		int marginLeft = marginX / 2;
//		int marginRight = marginX / 2 + marginX % 2;
//		int marginTop = marginY / 2;
//		int marginBottom = marginY / 2 + marginY % 2;
//		
//		while ((marginTop--) != 0)
//		{
//			int count = 5;
//			while ((count--) != 0)
//			{
//				guide.add(null);
//			};
//		};
//		
//		int x, y;
//		for (y=0; y<inHeight; y++)
//		{
//			int count = marginLeft;
//			while ((count--) != 0) guide.add(null);
//			
//			for (x=0; x<inWidth; x++)
//			{
//				Object obj = inputs[y * inWidth + x];
//				if (obj == null)
//				{
//					guide.add(null);
//				}
//				else if (obj instanceof String)
//				{
//					ArrayList<ItemStack> possible = OreDictionary.getOres((String) obj);
//					if (possible.size() == 0) return null;
//					guide.add(possible.get(0));
//				}
//				else if (obj instanceof Item)
//				{
//					guide.add(new ItemStack((Item) obj, 1));
//				}
//				else if (obj instanceof Block)
//				{
//					guide.add(new ItemStack((Block) obj, 1));
//				}
//				else if (obj instanceof EngTableFluidInput)
//				{
//					EngTableFluidInput fluid = (EngTableFluidInput) obj;
//					int meta = 2;
//					if (fluid.amount > 1000)
//					{
//						meta = 3;
//					};
//					ItemCapsule capsule = (ItemCapsule) MaddTech.itemCapsule;
//					ItemStack stack = new ItemStack(capsule, 1, meta);
//					
//					capsule.addLiquid(stack, fluid.type, fluid.amount);
//					
//					guide.add(stack);
//				}
//				else
//				{
//					return null;
//				};
//			};
//			
//			count = marginRight;
//			while ((count--) != 0) guide.add(null);
//		};
//		
//		while ((marginBottom--) != 0)
//		{
//			int count = 5;
//			while ((count--) != 0) guide.add(null);
//		};
//		
//		marginX = 5 - outWidth;
//		marginY = 5 - outHeight;
//
//		marginLeft = marginX / 2;
//		marginRight = marginX / 2 + marginX % 2;
//		marginTop = marginY / 2;
//		marginBottom = marginY / 2 + marginY % 2;
//		
//		while ((marginTop--) != 0)
//		{
//			int count = 5;
//			while ((count--) != 0)
//			{
//				guide.add(null);
//			};
//		};
//
//		for (y=0; y<outHeight; y++)
//		{
//			int count = marginLeft;
//			while ((count--) != 0) guide.add(null);
//			
//			for (x=0; x<outWidth; x++)
//			{
//				guide.add(outputs[y * outWidth + x]);
//			};
//			
//			count = marginRight;
//			while ((count--) != 0) guide.add(null);
//		};
//		
//		while ((marginBottom--) != 0)
//		{
//			int count = 5;
//			while ((count--) != 0) guide.add(null);
//		};
//
//		// TODO: tools!
//		int i;
//		for (i=0; i<3; i++)
//		{
//			if (tools.length <= i)
//			{
//				guide.add(null);
//			}
//			else
//			{
//				ToolSpec spec = tools[i];
//				ItemStack stack = null;
//				
//				if (spec.type.equals("screwdriver"))
//				{
//					stack = new ItemStack(MaddTech.itemScrewdriver, spec.count);
//				}
//				else if (spec.type.equals("welder"))
//				{
//					stack = new ItemStack(MaddTech.itemGasWelder, spec.count);
//				};
//				
//				guide.add(stack);
//			};
//		};
//		
//		return guide.toArray();
//	};
};
