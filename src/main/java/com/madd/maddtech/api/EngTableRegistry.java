/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import java.util.*;
import com.madd.maddtech.*;
import com.madd.maddtech.tileentity.TileEntityEngTable;

/**
 * Registry of recipies for the engineering table. Use via MaddRegistry.engTable.
 */
public class EngTableRegistry
{
	private ArrayList<IEngTableRecipe> recipies = new ArrayList<IEngTableRecipe>();
	
	/**
	 * Add a new engineering table recipe.
	 */
	public void add(IEngTableRecipe recipe)
	{
		recipies.add(recipe);
	};
	
	/**
	 * Return a matching IEngTableRecipe for the specified state of an engineering table.
	 * If no recipe matches the table, return null.
	 */
	public IEngTableRecipe get(TileEntityEngTable table)
	{
		int i;
		for (i=0; i<recipies.size(); i++)
		{
			IEngTableRecipe recipe = recipies.get(i);
//			if (recipe.isMatchingRecipe(table))
//			{
//				return recipe;
//			};
		};
		
		return null;
	};
	
	/**
	 * Return an array of recipies.
	 */
	public IEngTableRecipe[] getRecipies()
	{
		return recipies.toArray(new IEngTableRecipe[recipies.size()]);
	};
};
