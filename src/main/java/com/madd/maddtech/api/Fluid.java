/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import net.minecraft.util.*;
import net.minecraft.world.*;

/**
 * A class that represents information about fluids.
 */
public class Fluid
{
	private String id;
	private String name;
	private int barLevel;
	private int capLevel;
	private ResourceLocation texture;

	/**
	 * Constructor for fluids.
	 * @param id ID in the format "modid:xxx", where modid is the mod ID and xxx is a unique fluid ID within the mod.
	 * @param name The display name of this fluid.
	 * @param barLevel The minimum level of barrel required to hold this fluid (see BarrelType class).
	 * @param capLevel The minimum level of capsule required to hold this fluid (see CapsuleType class).
	 */
	public Fluid(String id, String name, int barLevel, int capLevel)
	{
		this.id = id;
		this.name = name;
		this.barLevel = barLevel;
		this.capLevel = capLevel;

		String[] parts = id.split("\\:");
		String modid = parts[0];
		String fname = parts[1];

		texture = new ResourceLocation(modid + ":textures/fluids/" + fname + ".png");
	};

	public String getID()
	{
		return id;
	};

	public String getName()
	{
		String unlocalName = "fluid." + id.replace(':', '.');
		return "";
	};

	public int getBarrelLevel()
	{
		return barLevel;
	};

	public int getCapsuleLevel()
	{
		return capLevel;
	};

	public ResourceLocation getTexture()
	{
		return texture;
	};

	/**
	 * Called when the fluid is to be dispensed.
	 * By default, it does not dispense (ie. just returns 'amount').
	 * Please note that there may be a block at the specified position already!
	 * @param world The world in which this fluid is dispensed.
	 * @param x Position to dispense at.
	 * @param y Position to dispense at.
	 * @param z Position to dispense at.
	 * @param amount Amount of fluid to dispense.
	 * @return The amount of fluid (out of the given amount) that is left (not dispensed).
	 */
	public int onDispense(World world, int x, int y, int z, int amount)
	{
		return amount;
	};

	/**
	 * Called whenever a capsule with this fluid is inside of a running chemical reactor. It is called
	 * once per capsule per iteration.
	 * @param capsule You may use this to pull/push fluids of the capsule. See IFluidMachine.
	 * @param reactor A handle for the chemical reactor, see IChemicalReactor.
	 */
	public void updateCR(IFluidMachine capsule, IChemicalReactor reactor)
	{
	};

	/**
	 * Called whenever a capsule with this fluid is inside of a running nuclear reactor. It is called
	 * once per capsule per iteration.
	 * @param capsule You may use this to pull/push fluids of the capsule. See IFluidMachine.
	 * @param reactor A handle for the nuclear reactor, see INuclearReactor.
	 */
	public void updateNR(IFluidMachine capsule, INuclearReactor reactor)
	{
	};
};
