/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import net.minecraft.item.*;
import com.madd.maddtech.*;

/**
 * Interface implemented by classes which specify engineering table recipies.
 */
public interface IEngTableRecipe
{
	/**
	 * Return the name of this recipe. Recipe names are used by templates.
	 */
	//public String getName();
	
	/**
	 * Given a TileEntityEngTable, return true if it contains the ingredients needed for this
	 * recipe, false otherwise.
	 */
	//public boolean isMatchingRecipe(TileEntityEngTable table);
	
	/**
	 * Given a TileEntityEngTable, remove each item of the recipe from the input area, and store
	 * the product(s) in the output area.
	 */
	//public void execRecipe(TileEntityEngTable table);
	
	/**
	 * Return the list of tool specifications needed by this recipe. Please note that isMatchingRecipe()
	 * must also make sure that all tools are available!
	 */
	//public ToolSpec[] getTools();
};
