/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import net.minecraft.item.*;

/**
 * Interface to the fluid registry. Use via MaddRegistry.fluids.
 */
public interface IFluidRegistry
{
	/**
	 * Register a new fluid. You must do this before you pour it into any capsules or barrels,
	 * else a really weird thing happens. Don't risk it.
	 */
	public void add(Fluid fluid);

	/**
	 * Get a fluid by ID.
	 */
	public Fluid get(String id);
	
	/**
	 * Register a fluid bucket.
	 */
	public void addBucket(Item item, String fluidID);

	/**
	 * Returns a fluid contained in the specified bucket. If the item in the ItemStack is not
	 * recognised as holding any MaddTech-registered fluid, null is returned.
	 */
	public Fluid fromBucket(ItemStack stack);
	
	/**
	 * Given a fluid, returns the ItemStack representing the appropriate bucket to contain it.
	 */
	public ItemStack toBucket(Fluid fluid);
	
	/**
	 * Register a Forge fluid as being mapped to some MaddTech fluid.
	 */
	public void addForgeFluid(int forgeID, String mtID);
	
	/**
	 * Given a Forge fluid ID, return the equivalent MaddTech fluid ID.
	 */
	public String fromForgeID(int forgeID);
	
	/**
	 * Give a MaddTech fluid ID, return the equivalent Forge fluid ID.
	 */
	public int toForgeID(String mtID);
};
