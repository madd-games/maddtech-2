/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import net.minecraft.item.*;

/**
 * The MaddTech common (chemical and nuclear) reactor interface.
 */
public interface IReactor
{
	/**
	 * Returns the temperature of the reactor.
	 */
	public int getTemperature();

	/**
	 * Returns the magnitude of the input redstone signal to the reactor.
	 * (0-15).
	 */
	public int getRedstonePower();

	/**
	 * Heats up the reactor. Negative value for dt causes the temperature to go down.
	 */
	public void heatUp(int dt);

	/**
	 * Puts an item stack into the reactor.
	 */
	public void pushStack(ItemStack stack);

	/**
	 * Try pulling the specified liquid from a capsule in the reactor.
	 */
	public int pullLiquid(String type, int amount);

	/**
	 * Returns true if the reactor is too hot to take items out.
	 */
	public boolean isHot();

	/**
	 * Forces the reactor to update the block on clients.
	 */
	public void updateBlock();

	/**
	 * Returns the number of items in the reactor which match the given item stack.
	 */
	public int countItems(ItemStack searchStack);

	/**
	 * Checks whether removeStack() is currently possible.
	 */
	public boolean canRemoveStack(ItemStack searchStack);

	/**
	 * Deletes items from the reactor. The specified ItemStack tells this function how many
	 * items to remove and of what type. You must manually check, using canRemoveStack(), if
	 * it is possible to remove this stack at the moment.
	 */
	public void removeStack(ItemStack searchStack);

	/**
	 * Tries to pull the specified fluid from the cables connected to the reactor.
	 * Returns the amount of fluid pulled.
	 */
	public int pullFluidFromCables(String id, int amount);

	/**
	 * Tries to push the specified fluid through the cables connected to the reactor.
	 * Returns the amount of fluid left.
	 */
	public int pushFluidToCables(String id, int amount);

	/**
	 * Generate energy and output it from the back.
	 */
	public void generateEnergy(int energy);
};
