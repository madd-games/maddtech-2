/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import net.minecraft.world.*;
import net.minecraft.entity.item.*;
import net.minecraft.block.*;
import net.minecraft.item.*;

import java.util.*;
import com.madd.maddtech.*;
import com.madd.maddtech.util.SideUtil;

/**
 * A class for casting laser beams.
 */
public class LaserBeam
{
	public World world;
	public int x;
	public int y;
	public int z;
	public int dir;
	public int dx;
	public int dy;
	public int dz;
	public int str;

	public LaserBeam(World world, int x, int y, int z, int dir, int str)
	{
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.dir = dir;
		this.str = str;

		dx = SideUtil.getSideX(dir);
		dy = SideUtil.getSideY(dir);
		dz = SideUtil.getSideZ(dir);
	};

//	public void cast()
//	{
//		Block block = world.getBlock(x, y, z);
//		int meta = world.getBlockMetadata(x, y, z);
//
//		if (!block.canCollideCheck(meta, false))
//		{
//			str--;
//			if (str != 0)
//			{
//				x += dx;
//				y += dy;
//				z += dz;
//				cast();
//			}
//		}
//		else if (block instanceof ILaserSensitiveBlock)
//		{
//			ILaserSensitiveBlock lsb = (ILaserSensitiveBlock) block;
//			if (lsb.onHitByLaser(this))
//			{
//				str--;
//				if (str != 0)
//				{
//					x += dx;
//					y += dy;
//					z += dz;
//					cast();
//				};
//			};
//		}
//		else if (block == Blocks.bedrock)
//		{
//			// do nothing with bedrock.
//		}
//		else if (block == Blocks.tnt)
//		{
//			world.createExplosion(null, (double)x, (double)y, (double)z, 4.0f, true);
//		}
//		else
//		{
//			float rx = 0.5F;
//			float ry = 0.5F;
//			float rz = 0.5F;
//
//			EntityItem entityItem = new EntityItem(world,
//				x + rx, y + ry, z + rz,
//				new ItemStack(block, 1, meta));
//
//			entityItem.motionX = 0.0F;
//			entityItem.motionY = 0.0F;
//			entityItem.motionZ = 0.0F;
//			world.spawnEntityInWorld(entityItem);
//			world.setBlockToAir(x, y, z);
//		};
//	};
};
