/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Search.java
 * An abstract class that helps in searching the Minecraft world.
 */

package com.madd.maddtech.api;

import net.minecraft.world.*;

import net.minecraft.block.*;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import java.util.*;
import com.madd.maddtech.*;

public abstract class Search
{
	protected World world;

	private class Position
	{
		public Position(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		};

		public int x;
		public int y;
		public int z;
	};

	private ArrayList<Position> searchedPositions = new ArrayList<Position>();

	public Search()
	{
	};

	public Search(World world)
	{
		this.world = world;
	};

	private boolean haveSearched(int x, int y, int z)
	{
		int i;
		for (i=0; i<searchedPositions.size(); i++)
		{
			Position pos = searchedPositions.get(i);
			if ((pos.x == x) && (pos.y == y) && (pos.z == z))
			{
				return true;
			};
		};

		return false;
	};

	protected void doSearch(int x, int y, int z, int fromSide, int dist)
	{
		if (dist >= 600)
		{
			return;
		};
		
		if (haveSearched(x, y, z))
		{
			return;
		};

		boolean sides[] = new boolean[] {true, true, true, true, true, true};

//		if (canBridge())
//		{
//			if ((fromSide >= 2) && (fromSide <= 5))
//			{
//				TileEntity te = world.getTileEntity(x, y, z);
//				if (te != null)
//				{
//					if (te instanceof TileEntityTerminal)
//					{
//						TileEntityTerminal term = (TileEntityTerminal) te;
//						ArrayList<Coords> peers = term.getPeers(fromSide);
//					
//						int i;
//						for (i=0; i<peers.size(); i++)
//						{
//							Coords coords = peers.get(i);
//							doSearch(coords.x, coords.y, coords.z, coords.side, dist+1);
//						};
//					
//						return;
//					};
//				};
//			};
//		};
		
//		if (handleBlock(x, y, z, fromSide, sides, dist))
//		{
//			searchedPositions.add(new Position(x, y, z));
//			int i;
//			for (i=0; i<6; i++)
//			{
//				if ((i != fromSide) && (i != SideUtil.opposite(fromSide)))
//				{
//					if (sides[i])
//					{
//						int targetX = x + SideUtil.getSideX(i);
//						int targetY = y + SideUtil.getSideY(i);
//						int targetZ = z + SideUtil.getSideZ(i);
//						int op = SideUtil.opposite(i);
//						doSearch(targetX, targetY, targetZ, op, dist+1);
//					};
//				};
//			};
//			
//			// after branch-preffered search, do straight search.
//			if (fromSide != 6)
//			{
//				int forward = SideUtil.opposite(fromSide);
//				if (sides[forward])
//				{
//					int targetX = x + SideUtil.getSideX(forward);
//					int targetY = y + SideUtil.getSideY(forward);
//					int targetZ = z + SideUtil.getSideZ(forward);
//					int op = fromSide;
//					doSearch(targetX, targetY, targetZ, op, dist+1);
//				};
//			};
//		};
	};

	/**
	 * May be overriden by children. Returns true if the corresponding cable can be bridged across
	 * Glownet, false if not. Returns true by default.
	 */
	protected boolean canBridge()
	{
		return true;
	};
	
	/**
	 * Called for every block on the search path(s).
	 * Should return 'true' if the block defines the path, false otherwise.
	 * The 'sides' array indicates whether each side shall be searched; 
	 */
	//protected abstract boolean handleBlock(int x, int y, int z, int side, boolean[] sides, int dist);
};
