/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SearchEnergy.java
 * Search for energy machines across energy cables.
 */

package com.madd.maddtech.api;

import net.minecraft.world.*;
import net.minecraft.block.*;
import net.minecraft.tileentity.*;
import java.util.*;
import java.lang.*;
import com.madd.maddtech.*;
import com.madd.maddtech.block.BlockMaddEnergyCable;

public class SearchEnergy extends Search
{
	private int powerLimit = -1;
	private int distLimit = -1;
	
	private class Machine
	{
		public IEnergyMachine mach;
		public int side;

		public Machine(IEnergyMachine mach, int side)
		{
			this.mach = mach;
			this.side = side;
		};
	};

	private ArrayList<Machine> machines = new ArrayList<Machine>();

	public SearchEnergy(World world)
	{
		super(world);
	};

	public void execute(int x, int y, int z, int side)
	{
		doSearch(x, y, z, side, 1);
	};

	private boolean isEnergyCable(Block block)
	{
		return (block instanceof BlockMaddEnergyCable) || (block instanceof IEnergyRouter);
	}

//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean[] sides, int dist) {
//		// TODO Auto-generated method stub
//		return false;
//	};

//	private int getPowerLimit(Block block, int x, int y, int z)
//	{
//		if (block instanceof BlockMaddEnergyCable)
//		{
//			BlockMaddEnergyCable cable = (BlockMaddEnergyCable) block;
//			return cable.getPowerLimit();
//		}
//		else if (block instanceof IEnergyRouter)
//		{
//			IEnergyRouter router = (IEnergyRouter) block;
//			return router.getPowerLimit(world, x, y, z);
//		}
//		else
//		{
//			return 0;
//		}
//	};
//	
//	public int getDistLimit(Block block, int x, int y, int z)
//	{
//		if (block instanceof BlockMaddEnergyCable)
//		{
//			BlockMaddEnergyCable cable = (BlockMaddEnergyCable) block;
//			return cable.getDistLimit();
//		}
//		else
//		{
//			return -1;
//		}
//	};
//	
//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean sides[], int dist)
//	{
//		Block block = world.getBlock(x, y, z);
//		if (isEnergyCable(block))
//		{
//			int limit = getPowerLimit(block, x, y, z);
//			int dlimit = getDistLimit(block, x, y, z);
//			
//			if (powerLimit == -1)
//			{
//				powerLimit = limit;
//			}
//			else
//			{
//				powerLimit = Math.min(powerLimit, limit);
//			};
//
//			if (distLimit == -1)
//			{
//				distLimit = dlimit;
//			}
//			else
//			{
//				distLimit = Math.min(distLimit, dlimit);
//			};
//			
//			if (block instanceof IEnergyRouter)
//			{
//				IEnergyRouter router = (IEnergyRouter) block;
//				return router.handleEnergyBlock(world, x, y, z, side, sides);
//			};
//
//			return true;
//		}
//		else if (block instanceof BlockShell)
//		{
//			BlockShell shell = (BlockShell) block;
//			if (shell.getShellFunction().startsWith("energy"))
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				if (meta != 0)
//				{
//					sides[meta & 7] = false;
//					Coords coords = new Coords(x, y, z, meta & 7);
//					
//					int count = 0;
//					while (true)
//					{
//						if ((count++) >= 64)
//						{
//							System.out.println("[MADDTECH] SearchEnergy: Casing inconsistent!!!");
//							break;
//						};
//						
//						coords = coords.next();
//						block = world.getBlock(coords.x, coords.y, coords.z);
//						if (block instanceof BlockShell)
//						{
//							coords.side = world.getBlockMetadata(coords.x, coords.y, coords.z) & 7;
//						}
//						else
//						{
//							break;
//						};
//					};
//					
//					TileEntity te = world.getTileEntity(coords.x, coords.y, coords.z);
//					if (te != null)
//					{
//						if (te instanceof IEnergyMachine)
//						{
//							// we use "side 6" to represent the energy-input-on-shell,
//							// and "side 7" for the output
//							int sideidx = 6;
//							if (shell.getShellFunction().equals("energy_out"))
//							{
//								sideidx = 7;
//							};
//
//							machines.add(new Machine((IEnergyMachine)te, sideidx));
//						};
//					};
//				};
//				return true;
//			};
//			
//			return false;
//		}
//		else if ((distLimit != -1) && (distLimit < dist))
//		{
//			return false;
//		}
//		else
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te != null)
//			{
//				if (te instanceof IEnergyMachine)
//				{
//					IEnergyMachine mach = (IEnergyMachine) te;
//					if (mach.canConnectEnergy(side))
//					{
//						machines.add(new Machine(mach, side));
//					};
//				};
//			};
//
//			return false;
//		}
//	};
//
//	/**
//	 * Push energy into machines.
//	 * Returns the amount of energy left (ie. not pushed).
//	 */
//	public int pushEnergy(int energy)
//	{
//		int orgEnergy = energy;
//
//		if (powerLimit != -1)
//		{
//			if (energy > powerLimit)
//			{
//				energy = powerLimit;
//			};
//		};
//
//		int pushed = 0;
//		int i;
//		for (i=0; i<machines.size(); i++)
//		{
//			if (energy == 0)
//			{
//				break;
//			};
//
//			Machine mach = machines.get(i);
//			int pushedNow = mach.mach.pushEnergy(energy, mach.side);
//			pushed += pushedNow;
//			energy -= pushedNow;
//		};
//
//		return orgEnergy - pushed;
//	};
//
//	/**
//	 * Pull energy from machines.
//	 * Returns the amount of energy actually pulled.
//	 */
//	public int pullEnergy(int max)
//	{
//		if (powerLimit != -1)
//		{
//			if (max > powerLimit)
//			{
//				max = powerLimit;
//			};
//		};
//
//		int pulled = 0;
//		int i;
//		for (i=0; i<machines.size(); i++)
//		{
//			if (max == 0)
//			{
//				break;
//			};
//
//			Machine mach = machines.get(i);
//			int pulledNow = mach.mach.pullEnergy(max, mach.side);
//			pulled += pulledNow;
//			max -= pulledNow;
//		};
//
//		return pulled;
//	};
//	
//	@Override
//	protected boolean canBridge()
//	{
//		return false;
//	};
};
