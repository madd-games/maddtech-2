/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SearchFluid.java
 * Searches for fluid machines through fluid cables.
 */

package com.madd.maddtech.api;

import net.minecraft.world.*;

import net.minecraft.tileentity.*;
import net.minecraft.block.*;
import java.util.*;
import com.madd.maddtech.*;
import java.io.*;

public class SearchFluid extends Search
{
	private class Machine
	{
		public IFluidMachine mach;
		public int side;

		public Machine(IFluidMachine mach, int side)
		{
			this.mach = mach;
			this.side = side;
		};
	};

	private class FluidBlock
	{
		int x;
		int y;
		int z;
		Block block;

		FluidBlock(int x, int y, int z, Block block)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.block = block;
		};
	};

	private ArrayList<Machine> fluidMachines = new ArrayList<Machine>();
	private ArrayList<FluidBlock> fluidBlocks = new ArrayList<FluidBlock>();

	public SearchFluid()
	{
	};

	public SearchFluid(World world)
	{
		super(world);
	}

//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean[] sides, int dist) {
//		// TODO Auto-generated method stub
//		return false;
//	};

//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean[] sides, int dist)
//	{
//		Block block = world.getBlock(x, y, z);
//		if (block == MaddTech.blockFluidCable)
//		{
//			return true;
//		}
//		else if (block instanceof BlockShell)
//		{
//			BlockShell shell = (BlockShell) block;
//			if (shell.isShellFluid())
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				if (meta != 0)
//				{
//					sides[meta & 7] = false;
//					Coords coords = new Coords(x, y, z, meta & 7);
//					
//					int count = 0;
//					while (true)
//					{
//						if ((count++) >= 64)
//						{
//							System.out.println("[MADDTECH] SearchFluid: Casing inconsistent!!!");
//							break;
//						};
//						
//						coords = coords.next();
//						block = world.getBlock(coords.x, coords.y, coords.z);
//						if (block instanceof BlockShell)
//						{
//							coords.side = world.getBlockMetadata(coords.x, coords.y, coords.z) & 7;
//						}
//						else
//						{
//							break;
//						};
//					};
//					
//					TileEntity te = world.getTileEntity(coords.x, coords.y, coords.z);
//					if (te != null)
//					{
//						if (te instanceof IFluidMachine)
//						{
//							// we use "side 6" to represent the fluid-input-on-shell,
//							// and "side 7" for the output
//							int sideidx = 6;
//							if (shell.getShellFunction().equals("fluid_out"))
//							{
//								sideidx = 7;
//							};
//
//							IFluidMachine fmach = (IFluidMachine) te;
//							if (fmach.canConnectFluid(sideidx))
//							{
//								fluidMachines.add(new Machine(fmach, sideidx));
//							};
//						};
//					};
//				};
//				return true;
//			};
//			
//			return false;
//		}
//		else if (block instanceof IFluidRouter)
//		{
//			IFluidRouter router = (IFluidRouter) block;
//			return router.handleFluidBlock(world, x, y, z, side, sides);
//		}
//		else
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te != null)
//			{
//				if (te instanceof IFluidMachine)
//				{
//					IFluidMachine fmach = (IFluidMachine) te;
//					if (fmach.canConnectFluid(side))
//					{
//						fluidMachines.add(new Machine(fmach, side));
//					};
//				}
//				else if (te instanceof IFluidHandler)
//				{
//					fluidMachines.add(new Machine(new FluidTranslator((IFluidHandler)te), side));
//				};
//			};
//
//			if ((block == Blocks.water) || (block == Blocks.lava))
//			{
//				if (side == 1)
//				{
//					fluidBlocks.add(new FluidBlock(x, y, z, block));
//				};
//			};
//
//			return false;
//		}
//	};
//
//	/**
//	 * Pushes fluid of the specified type across the cables. Returns the amount of fluid left,
//	 * i.e. the amount of fluid NOT pushed.
//	 */
//	public int pushFluid(String id, int amount)
//	{
//		int i;
//		for (i=0; i<fluidMachines.size(); i++)
//		{
//			if (amount == 0)
//			{
//				return 0;
//			};
//
//			Machine mach = fluidMachines.get(i);
//			amount -= mach.mach.pushFluid(id, amount, mach.side);
//		};
//
//		return amount;
//	};
//
//	/**
//	 * Pulled the specified fluid across the cables. Returns the amount of fluid pulled.
//	 */
//	public int pullFluid(String id, int amount)
//	{
//		int pulled = 0;
//		int i;
//		for (i=0; i<fluidMachines.size(); i++)
//		{
//			if (amount == 0)
//			{
//				break;
//			};
//
//			Machine mach = fluidMachines.get(i);
//			int pulledNow = mach.mach.pullFluid(id, amount, mach.side);
//			amount -= pulledNow;
//			pulled += pulledNow;
//		};
//
//		for (i=0; i<fluidBlocks.size(); i++)
//		{
//			if (amount >= 1000)
//			{
//				FluidBlock fb = fluidBlocks.get(i);
//				if (getFluidByBlock(fb.block).equals(id))
//				{
//					amount -= 1000;
//					pulled += 1000;
//					world.setBlockToAir(fb.x, fb.y, fb.z);
//				};
//			};
//		};
//
//		return pulled;
//	};
//
//	public String getFluidByBlock(Block block)
//	{
//		if ((block == Blocks.water))
//		{
//			return "maddtech:water";
//		}
//		else if ((block == Blocks.lava))
//		{
//			return "maddtech:lava";
//		}
//		else
//		{
//			return "maddtech:empty";
//		}
//	};
//
//	public String getClosestFluidType()
//	{
//		if (fluidMachines.size() == 0)
//		{
//			if (fluidBlocks.size() == 0)
//			{
//				return "maddtech:empty";
//			}
//			else
//			{
//				return getFluidByBlock(fluidBlocks.get(0).block);
//			}
//		}
//		else
//		{
//			return fluidMachines.get(0).mach.getFluidType(fluidMachines.get(0).side);
//		}
//	};
//
//	public void execute(int x, int y, int z, int fromSide)
//	{
//		doSearch(x, y, z, fromSide, 1);
//	};
};
