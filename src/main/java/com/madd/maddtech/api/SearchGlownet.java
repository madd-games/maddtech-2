/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SearchGlownet.java
 * Search for glownet devices across glownet cables.
 */

package com.madd.maddtech.api;

import net.minecraft.world.*;
import net.minecraft.block.*;
import net.minecraft.tileentity.*;
import java.util.*;
import java.lang.*;
import com.madd.maddtech.*;
import com.madd.maddtech.util.Coords;

public class SearchGlownet extends Search
{
	private String peerAddr;
	private String localAddr;
	
	private class Link
	{
		public int x;
		public int y;
		public int z;
		public int side;
		public int dist;
		
		public Link(int x, int y, int z, int side, int dist)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.side = side;
			this.dist = dist;
		};
	};
	
	private ArrayList<Link> peers = new ArrayList<Link>();
	private ArrayList<Coords> termPoints = new ArrayList<Coords>();
	
	public SearchGlownet(World world)
	{
		super(world);
	};

	public void execute(int x, int y, int z, int side, String localAddr, String peerAddr)
	{
		this.localAddr = localAddr;
		this.peerAddr = peerAddr;
		if (!localAddr.equals("*"))
		{
			doSearch(x, y, z, side, 1);
		};
	}

//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean[] sides, int dist) {
//		// TODO Auto-generated method stub
//		return false;
//	};

//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean sides[], int dist)
//	{
//		Block block = world.getBlock(x, y, z);
//		if (block == MaddTech.blockGlownetCable)
//		{
//			return true;
//		}
//		
//		if (block == MaddTech.blockCabinet)
//		{
//			int metadata = world.getBlockMetadata(x, y, z);
//			if (BlockMaddCabinet.isSideInput(side, metadata))
//			{
//				int op = SideUtil.opposite(side);
//				int i;
//				for (i=0; i<6; i++)
//				{
//					sides[i] = (i == op);
//				};
//				
//				return true;
//			};
//			
//			return false;
//		}
//		else if (block == MaddTech.blockGlownetSwitch)
//		{
//			return world.isBlockIndirectlyGettingPowered(x, y, z);
//		}
//		else if (side == 0)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te != null)
//			{
//				if (te instanceof TileEntityTerminal)
//				{
//					if (localAddr.equals("XXX"))
//					{
//						termPoints.add(new Coords(x, y, z));
//					}
//					else
//					{
//						TileEntityTerminal term = (TileEntityTerminal) te;
//						for (side=2; side<6; side++)
//						{
//							if (term.getSideAddr(side).equals(peerAddr))
//							{
//								if (term.canGlownetConnect(localAddr, side))
//								{
//									peers.add(new Link(x, y, z, side, dist));
//								};
//							};
//						};
//					};
//				};
//			};
//		};
//		
//		return false;
//	};
//	
//	/**
//	 * Returns an ArrayList of coordinates to be searched by a Search after bridging over this glownet link.
//	 */
//	public ArrayList<Coords> getPeerCoords()
//	{
//		ArrayList<Coords> output = new ArrayList<Coords>();
//		
//		int i;
//		for (i=0; i<peers.size(); i++)
//		{
//			Link link = peers.get(i);
//			Coords coords = new Coords(link.x, link.y, link.z, link.side);
//			coords = coords.next();
//			coords.side = SideUtil.opposite(link.side);
//			output.add(coords);
//		};
//		
//		return output;
//	};
//	
//	/**
//	 * Return the distance to the furthest peered terminal.
//	 */
//	public int getDistance()
//	{
//		int output = 0;
//		
//		int i;
//		for (i=0; i<peers.size(); i++)
//		{
//			Link link = peers.get(i);
//			if (link.dist > output)
//			{
//				output = link.dist;
//			};
//		};
//		
//		return output;
//	};
//	
//	/**
//	 * Notify all connected terminals of a change in state.
//	 */
//	public void notifyUpdate()
//	{
//		int i;
//		for (i=0; i<termPoints.size(); i++)
//		{
//			Coords coords = termPoints.get(i);
//			world.notifyBlocksOfNeighborChange(coords.x, coords.y, coords.z, MaddTech.blockTerminal);
//		};
//	};
};
