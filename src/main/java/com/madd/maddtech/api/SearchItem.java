/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SearchItem.java
 * Searches for inventories through item cables.
 */

package com.madd.maddtech.api;

import net.minecraft.item.*;

import net.minecraft.world.*;
import net.minecraft.block.*;
import net.minecraft.tileentity.*;
import net.minecraft.inventory.*;
import java.util.*;
import com.madd.maddtech.*;

public class SearchItem extends Search
{
	private class Manager
	{
		TileEntity te;
		ISidedInventory sidedInv = null;
		IInventory inv;
		int side;

		public Manager(TileEntity te, int side)
		{
			this.te = te;
			this.side = side;
			if (te instanceof ISidedInventory)
			{
				sidedInv = (ISidedInventory) te;
			};
			inv = (IInventory) te;
		};

		//public ItemStack pullItem(ItemStack searchStack)
//		public ItemStack pullItem(ItemFilter filter)
//		{
//			int chosenSlot = -1;
//			if (sidedInv != null)
//			{
//				int[] slots = sidedInv.getAccessibleSlotsFromSide(side);
//				int i;
//				for (i=0; i<slots.length; i++)
//				{
//					int slot = slots[i];
//					ItemStack stack = inv.getStackInSlot(slot);
//					if (stack != null)
//					{
//						if (stack.getTagCompound() != null) continue;
//
//						//if ((stack.getItem() == searchStack.getItem())
//						//	&& (stack.getItemDamage() == searchStack.getItemDamage()))
//						if (filter.isAcceptedStack(stack))
//						{
//							if (sidedInv.canExtractItem(slot, stack, side))
//							{
//								chosenSlot = slot;
//								break;
//							};
//						};
//					};
//				};
//			}
//			else
//			{
//				int i;
//				for (i=0; i<inv.getSizeInventory(); i++)
//				{
//					ItemStack stack = inv.getStackInSlot(i);
//					if (stack != null)
//					{
//						if (stack.getTagCompound() != null) continue;
//						
//						//if ((stack.getItem() == searchStack.getItem())
//						//	&& (stack.getItemDamage() == searchStack.getItemDamage()))
//						if (filter.isAcceptedStack(stack))
//						{
//							chosenSlot = i;
//							break;
//						};
//					};
//				};
//			};
//
//			if (chosenSlot == -1)
//			{
//				return null;
//			};
//
//			ItemStack theStack = inv.getStackInSlot(chosenSlot);
//			ItemStack output = new ItemStack(theStack.getItem(), 1, theStack.getItemDamage());
//			theStack.stackSize--;
//			if (theStack.stackSize == 0)
//			{
//				inv.setInventorySlotContents(chosenSlot, null);
//			};
//			inv.markDirty();
//			return output;
//		};
//
//		public void pushStack(ItemStack pstack)
//		{
//			int chosenSlot = -1;
//			if (sidedInv != null)
//			{
//				int[] slots = sidedInv.getAccessibleSlotsFromSide(side);
//				int i;
//				for (i=0; i<slots.length; i++)
//				{
//					int slot = slots[i];
//					ItemStack stack = inv.getStackInSlot(slot);
//					if (stack != null)
//					{
//						if ((stack.getItem() == pstack.getItem())
//							&& (stack.getItemDamage() == pstack.getItemDamage())
//							&& ((stack.stackSize+pstack.stackSize) <= stack.getItem().getItemStackLimit()))
//						{
//							if (sidedInv.canInsertItem(slot, pstack, side))
//							{
//								chosenSlot = slot;
//								break;
//							};
//						};
//					}
//					else
//					{
//						if (sidedInv.canInsertItem(slot, pstack, side))
//						{
//							chosenSlot = slot;
//							break;
//						};
//					};
//				};
//			}
//			else
//			{
//				int i;
//				for (i=0; i<inv.getSizeInventory(); i++)
//				{
//					ItemStack stack = inv.getStackInSlot(i);
//					if (stack != null)
//					{
//						if ((stack.getItem() == pstack.getItem())
//							&& (stack.getItemDamage() == pstack.getItemDamage())
//							&& (stack.stackSize < 64))
//						{
//							chosenSlot = i;
//							break;
//						};
//					}
//					else
//					{
//						chosenSlot = i;
//						break;
//					};
//				};
//			};
//
//			if (chosenSlot == -1)
//			{
//				// Do not modify the pstack.
//				return;
//			};
//
//			ItemStack currentStack = inv.getStackInSlot(chosenSlot);
//			if (currentStack == null)
//			{
//				inv.setInventorySlotContents(chosenSlot, new ItemStack(pstack.getItem(), 1, pstack.getItemDamage()));
//			}
//			else
//			{
//				currentStack.stackSize++;
//			};
//
//			pstack.stackSize--;
//			inv.markDirty();
//		};
//
//		public IInventory get()
//		{
//			return inv;
//		};
//	};
//
//	private ArrayList<Manager> managers = new ArrayList<Manager>();
//
//	public SearchItem()
//	{
//	};
//
//	public SearchItem(World world)
//	{
//		super(world);
//	};
//
//	@Override
//	protected boolean handleBlock(int x, int y, int z, int side, boolean sides[], int dist)
//	{
//		Block block = world.getBlock(x, y, z);
//		if (block == MaddTech.blockItemCable)
//		{
//			return true;
//		}
//		else if (block instanceof BlockShell)
//		{
//			BlockShell shell = (BlockShell) block;
//			if (shell.getShellFunction().startsWith("item"))
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				if (meta != 0)
//				{
//					sides[meta & 7] = false;
//					Coords coords = new Coords(x, y, z, meta & 7);
//					
//					int count = 0;
//					while (true)
//					{
//						if ((count++) >= 64)
//						{
//							System.out.println("[MADDTECH] SearchItem: Casing inconsistent!!!");
//							break;
//						};
//						
//						coords = coords.next();
//						block = world.getBlock(coords.x, coords.y, coords.z);
//						if (block instanceof BlockShell)
//						{
//							coords.side = world.getBlockMetadata(coords.x, coords.y, coords.z) & 7;
//						}
//						else
//						{
//							break;
//						};
//					};
//					
//					TileEntity te = world.getTileEntity(coords.x, coords.y, coords.z);
//					if (te != null)
//					{
//						if (te instanceof IInventory)
//						{
//							// we use "side 6" to represent the item-input-on-shell,
//							// and "side 7" for the output
//							int sideidx = 6;
//							if (shell.getShellFunction().equals("item_out"))
//							{
//								sideidx = 7;
//							};
//
//							managers.add(new Manager(te, sideidx));
//						};
//					};
//				};
//				return true;
//			};
//			
//			return false;
//		}
//		else if (block instanceof IItemRouter)
//		{
//			IItemRouter router = (IItemRouter) block;
//			return router.handleItemBlock(world, x, y, z, side, sides);
//		}
//		else
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te != null)
//			{
//				if (te instanceof IInventory)
//				{
//					managers.add(new Manager(te, side));
//				};
//			};
//
//			return false;
//		}
//	};
//
//	public void execute(int x, int y, int z, int fromSide)
//	{
//		doSearch(x, y, z, fromSide, 1);
//	};
//
//	/**
//	 * Pull an item stack.
//	 * If searchStack is not null, then the pulled stack matches the item and metadata of the search stack.
//	 * If searchStack is null, any available stack is pulled.
//	 * The final ItemStack is returned.
//	 * The stack size returned is 1.
//	 * For more advanced filtering use the other version of pullStack.
//	 */
//	public ItemStack pullStack(ItemStack searchStack)
//	{
//		if (searchStack == null)
//		{
//			return pullStack(new ItemFilterAll());
//		}
//		else
//		{
//			return pullStack(new ItemFilterOnly(new ItemStack[] {searchStack.copy()}));
//		}
//	};
//	
//	public ItemStack pullStack(ItemFilter filter)
//	{
//		int i;
//		for (i=0; i<managers.size(); i++)
//		{
//			Manager man = managers.get(i);
//			ItemStack output = man.pullItem(filter);
//			if (output != null)
//			{
//				return output;
//			};
//		};
//
//		return null;
//	};
//
//	/**
//	 * Tries pushing the specified item stack across the cables.
//	 * Returns a modified version of the item stack if not all items were
//	 * pushed, null if everything was pushed.
//	 */
//	public ItemStack pushStack(ItemStack stack)
//	{
//		int i;
//		for (i=0; i<managers.size(); i++)
//		{
//			Manager man = managers.get(i);
//			int oldSize;
//			do
//			{
//				oldSize = stack.stackSize;
//				man.pushStack(stack);
//			} while ((oldSize > stack.stackSize) && (stack.stackSize > 0));
//
//			if (stack.stackSize == 0)
//			{
//				break;
//			};
//		};
//
//		if (stack.stackSize == 0)
//		{
//			return null;
//		};
//
//		return stack;
//	};
//
//	/**
//	 * Return an ArrayList of inventories that are connected to this network.
//	 * On an unmodified network, the order of inventories returned is consistent.
//	 */
//	public ArrayList<IInventory> getInventories()
//	{
//		ArrayList<IInventory> invs = new ArrayList<IInventory>();
//
//		int i;
//		for (i=0; i<managers.size(); i++)
//		{
//			Manager man = managers.get(i);
//			IInventory inv = man.get();
//			if (!(inv instanceof ISidedInventory))
//			{
//				invs.add(inv);
//			};
//		};
//
//		return invs;
//	};
//};
}
}