/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import java.util.*;
import net.minecraft.item.*;
import net.minecraft.block.*;

/**
 * Thermal resistance registry - allows modders to set the thermal resistance of materials,
 * which helps MaddTech determine how fast machines should be cooled. The current instance
 * of this class is MaddRegistry.thermalResistance.
 */
public class ThermalResistanceRegistry
{
	private Hashtable<Item, Float> trMap = new Hashtable<Item, Float>();

	public ThermalResistanceRegistry()
	{
	};

	/**
	 * Sets the thermal resistance of an item.
	 * @param item The item in question.
	 * @param resist The resistance (0-1).
	 */
	public void set(Item item, float resist)
	{
		trMap.put(item, resist);
	};

	/**
	 * Sets the thermal resistance of a block.
	 * @param item The block in question.
	 * @param resist The resistance (0-1).
	 */
//	public void set(Block block, float resist)
//	{
//		set(Item.getItemFromBlock(block), resist);
//	};

	/**
	 * Returns the thermal resistance of an item.
	 */
	public float get(Item item)
	{
		if (trMap.containsKey(item))
		{
			return trMap.get(item);
		}
		else
		{
			return 0.1F;
		}
	};

	/**
	 * Returns the thermal resistance of a block.
	 */
//	public float get(Block block)
//	{
//		return get(Item.getItemFromBlock(block));
//	};
};
