/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.api;

import com.madd.maddtech.*;
import net.minecraft.world.*;
import java.util.*;

/**
 * This class is used to define new wiring rules as well as register them with MaddTech.
 */
public class WiringRules
{
	private static ArrayList<WiringRules> rules = new ArrayList<WiringRules>();

	/**
	 * Register a new set of wiring rules.
	 */
	public static void add(WiringRules newRules)
	{
		rules.add(newRules);
	};

	public static final int CABLE_ENERGY = 0;
	public static final int CABLE_FLUID = 1;
	public static final int CABLE_ITEM = 2;
	public static final int CABLE_REDSTONE = 3;
	public static final int CABLE_GLASS = 4;

	/**
	 * Returns true if any of the wiring rules allow the specified cable to connect to the specified side of the
	 * given block.
	 */
//	public static boolean canConnectCable(int type, IBlockAccess world, int x, int y, int z, int side)
//	{
//		int i;
//		for (i=0; i<rules.size(); i++)
//		{
//			WiringRules rule = rules.get(i);
//			switch (type)
//			{
//			case CABLE_ENERGY:
//				if (rule.canConnectEnergy(world, x, y, z, side)) return true;
//				break;
//			case CABLE_FLUID:
//				if (rule.canConnectFluid(world, x, y, z, side)) return true;
//				break;
//			case CABLE_ITEM:
//				if (rule.canConnectItem(world, x, y, z, side)) return true;
//				break;
//			case CABLE_REDSTONE:
//				if (rule.canConnectRedstone(world, x, y, z, side)) return true;
//				break;
//			case CABLE_GLASS:
//				if (rule.canConnectGlass(world, x, y, z, side)) return true;
//				break;
//			};
//		};
//
//		return false;
//	};
//
//	/**
//	 * Return true if the fluid cable can be connected to the specified side of the specified block.
//	 */
//	public boolean canConnectFluid(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return false;
//	};
//
//	/**
//	 * Return true if the energy cable can be connected to the specified side of the specified block.
//	 */
//	public boolean canConnectEnergy(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return false;
//	};
//
//	/**
//	 * Return true if the item cable can be connected to the specified side of the specified block.
//	 */
//	public boolean canConnectItem(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return false;
//	};
//
//	/**
//	 * Return true if the redstone cable can be connected to the specified side of the specified block.
//	 */
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return false;
//	};
//
//	/**
//	 * Return true if the glass cable can be connected to the specified side of the specified block.
//	 */
//	public boolean canConnectGlass(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return false;
//	};
};
