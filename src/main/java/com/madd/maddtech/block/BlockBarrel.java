/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockBarrel.java
 * Base class for barrels in MaddTech.
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.ContainerBlock;
import net.minecraft.block.material.Material;
import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import java.util.*;
import com.madd.maddtech.api.*;

public class BlockBarrel extends ContainerBlock
{
	private String typeName;
	private int level;
	private int physLevel;

//	private IIcon iconTop;
//	private IIcon iconSide;
//	private IIcon iconBottom;

	public BlockBarrel(Properties properties)
	{
		super(properties);
	};

	protected void mtSetup(String typeName, int level, int physLevel)
	{
		this.typeName = typeName;
		this.level = level;
		this.physLevel = physLevel;
		
		if (physLevel > 0)
		{
			//setBlockBounds(2.0F/16.0F, 0.0F, 2.0F/16.0F, 1.0F-(2.0F/16.0F), 1.0F, 1.0F-(2.0F/16.0F));
		};
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconTop = ir.registerIcon("maddtech:" + typeName + "Top");
//		iconSide = ir.registerIcon("maddtech:" + typeName + "Side");
//		iconBottom = ir.registerIcon("maddtech:" + typeName + "Bottom");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		switch (side)
//		{
//		case 0:
//			return iconBottom;
//		case 1:
//			return iconTop;
//		default:
//			return iconSide;
//		}
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntityBarrel(typeName, level, physLevel);
//	};
//	
//	
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public void breakBlock(World world, int x, int y, int z, Block wtf, int lol)
//	{
//		dropItems(world, x, y, z);
//		super.breakBlock(world, x, y, z, wtf, lol);
//	};
//
//	private void dropItems(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		TileEntity tileEntity = world.getTileEntity(x, y, z);
//		if (!(tileEntity instanceof IInventory))
//		{
//			return;
//		}
//        
//		IInventory inventory = (IInventory) tileEntity;
//
//		int i;
//		for (i = 0; i < inventory.getSizeInventory(); i++)
//		{
//        		ItemStack item = inventory.getStackInSlot(i);
//
//			if (item != null && item.stackSize > 0)
//			{
//				float rx = rand.nextFloat() * 0.8F + 0.1F;
//				float ry = rand.nextFloat() * 0.8F + 0.1F;
//				float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//				EntityItem entityItem = new EntityItem(world,
//					x + rx, y + ry, z + rz,
//					new ItemStack(item.getItem(), item.stackSize, item.getItemDamage()));
//        		
//				if (item.hasTagCompound())
//				{
//					entityItem.getEntityItem().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
//				}
//
//				float factor = 0.05F;
//				entityItem.motionX = rand.nextGaussian() * factor;
//				entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//				entityItem.motionZ = rand.nextGaussian() * factor;
//				world.spawnEntityInWorld(entityItem);
//				item.stackSize = 0;
//			};
//		};
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//		
//		player.openGui(MaddTech.instance, GuiHandler.BARREL, world, x, y, z);
//		return true;
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
