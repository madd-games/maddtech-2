/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockCable.java
 * Base class for all cables.
 */

package com.madd.maddtech.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FenceBlock;
import net.minecraft.block.SixWayBlock;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraftforge.common.extensions.IForgeBlockState;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.*;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.Property;
import net.minecraft.state.StateContainer;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.*;
import com.madd.maddtech.api.*;

import java.awt.Shape;
import java.io.*;

public abstract class BlockCable extends SixWayBlock
{
	   private static final Direction[] DIRECTIONS = Direction.values();
	   public static final BooleanProperty UP = BooleanProperty.create("up");
	   public static final BooleanProperty DOWN = BooleanProperty.create("down");
	   public static final BooleanProperty NORTH = BooleanProperty.create("north");
	   public static final BooleanProperty EAST = BooleanProperty.create("east");
	   public static final BooleanProperty SOUTH = BooleanProperty.create("south");
	   public static final BooleanProperty WEST = BooleanProperty.create("west");
	   final VoxelShape BASE;
	   final VoxelShape[] SHAPES;
	
	public BlockCable(Properties properties)
	{
		super(0, properties);
		this.SHAPES = this.makeShapes(0.2f, 0.2f);
		this.BASE = Block.box(5.0D, 5.0D, 5.0D, 11.0D, 11.0D, 11.0D);
		
	}
	
	@Override
	protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
		// allows the directional variables used in the block state files to be recognised and used
		      builder.add(UP).add(DOWN).add(NORTH).add(EAST).add(SOUTH).add(WEST);
	}
	@Override
	public VoxelShape getShape(final BlockState state, final IBlockReader worldIn, final BlockPos pos, final ISelectionContext context) {
		// Uses the shape list from makeShapes() called in the constructor
		if (state.getValue(NORTH) == false && state.getValue(SOUTH) == false && state.getValue(EAST) == false && state.getValue(WEST) == false && state.getValue(UP) == false && state.getValue(DOWN) == false)
		{
			return BASE;
		}
		return this.SHAPES[this.getAABBIndex(state)];
	}
	
	protected boolean canConnect(BlockState blockState)
	{
		// Checks if the other block is suitable to connect to (same cable/pipe type etc)
		Block nblock = blockState.getBlock();
		
		if (nblock instanceof BlockMaddEnergyCable && this.getBlock() instanceof BlockMaddEnergyCable) 
		{
			return true;
		}
		else if (nblock instanceof BlockMaddFluidCable && this.getBlock() instanceof BlockMaddFluidCable)
		{
			return true;
		}
		else if (nblock instanceof BlockMaddGlownetCable && this.getBlock() instanceof BlockMaddGlownetCable)
		{
			return true;
		}
		else if (nblock instanceof BlockMaddItemCable && this.getBlock() instanceof BlockMaddItemCable)
		{
			return true;
		}
		else if (nblock instanceof BlockMaddRedstoneCable && this.getBlock() instanceof BlockMaddRedstoneCable)
		{
			return true;
		}
		
		
		return false;
		
	}
	
	public BlockState getStateForPlacement(BlockItemUseContext bContext) 
	{
		// When placed, we need to check each side incase we can connect to it (its another cable)
		IBlockReader bReader = bContext.getLevel();
		BlockPos blockpos = bContext.getClickedPos();
		BlockPos blockposN = blockpos.north();
		BlockPos blockposS = blockpos.south();
		BlockPos blockposE = blockpos.east();
		BlockPos blockposW = blockpos.west();
		BlockPos blockposU = blockpos.above();
		BlockPos blockposD = blockpos.below();
		BlockState bstateN = bReader.getBlockState(blockposN);
		BlockState bstateS = bReader.getBlockState(blockposS);
		BlockState bstateE = bReader.getBlockState(blockposE);
		BlockState bstateW = bReader.getBlockState(blockposW);
		BlockState bstateU = bReader.getBlockState(blockposU);
		BlockState bstateD = bReader.getBlockState(blockposD);
		BlockState result = defaultBlockState();
		result = result.setValue(NORTH, Boolean.valueOf(canConnect(bstateN)));
		result = result.setValue(EAST, Boolean.valueOf(canConnect(bstateE)));
		result = result.setValue(SOUTH, Boolean.valueOf(canConnect(bstateS)));
		result = result.setValue(WEST, Boolean.valueOf(canConnect(bstateW)));
		result = result.setValue(UP, Boolean.valueOf(canConnect(bstateU)));
		result = result.setValue(DOWN, Boolean.valueOf(canConnect(bstateD)));
		return result;
	}
	
	
	@Override
	public BlockState updateShape(BlockState bState, Direction dir, BlockState oBlockState, IWorld world, BlockPos bPos, BlockPos bPos2)
	{
		return bState.setValue(PROPERTY_BY_DIRECTION.get(dir), Boolean.valueOf(canConnect(oBlockState)));
		
	}
	
	private VoxelShape[] makeShapes(float coreSize, float extensionWidth)
	{
		float cMin = 8 - coreSize;
		float cMax = 8 + coreSize;
		
		// Storing base cable
		final VoxelShape baseS = box(cMin, cMin, cMin, cMax, cMax, cMax);

		
		VoxelShape[] shapeDirection = new VoxelShape[DIRECTIONS.length];
		
		// Makes a list of voxel shapes based on direction (to combine later)
	      for(int i = 0; i < DIRECTIONS.length; ++i) {
	         Direction direction = DIRECTIONS[i];
	         shapeDirection[i] = VoxelShapes.box(0.5D + Math.min((double)(-extensionWidth), (double)direction.getStepX() * 0.5D), 0.5D + Math.min((double)(-extensionWidth), (double)direction.getStepY() * 0.5D), 0.5D + Math.min((double)(-extensionWidth), (double)direction.getStepZ() * 0.5D), 0.5D + Math.max((double)extensionWidth, (double)direction.getStepX() * 0.5D), 0.5D + Math.max((double)extensionWidth, (double)direction.getStepY() * 0.5D), 0.5D + Math.max((double)extensionWidth, (double)direction.getStepZ() * 0.5D));
	      }

	      // Our list of shapes to populate with all possbile pipe/cable shapes
	      VoxelShape[] shapeList = new VoxelShape[64];
	      
	      // Combines the base pipe/cable shape with another direction or directions shape. Also includes no aditional directions at all 
	      for(int k = 0; k < 64; ++k) {
	         VoxelShape workingShape = baseS;

	         for(int j = 0; j < DIRECTIONS.length; ++j) {
	            if ((k & 1 << j) != 0) {
	            	workingShape = VoxelShapes.or(workingShape, shapeDirection[j]);
	            }
	         }

	         shapeList[k] = workingShape;
	      }
	      
	      return shapeList;
		
	}
	
	
	
//	@Override
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderCableID;
//	};
//
//	@Override
//	public boolean isBlockNormalCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderCable.renderPass = pass;
//		return true;
//	};
//
//	@Override
//	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
//	{
//		float x1 = (5.0F/16.0F);
//		float x2 = 1.0F - x1;
//		float y1 = x1;
//		float y2 = x2;
//		float z1 = x1;
//		float z2 = x2;
//
//		if (canConnectTo(world, x-1, y, z, 5))
//		{
//			x1 = 0.0F;
//		};
//
//		if (canConnectTo(world, x+1, y, z, 4))
//		{
//			x2 = 1.0F;
//		};
//
//		if (canConnectTo(world, x, y-1, z, 1))
//		{
//			y1 = 0.0F;
//		};
//
//		if (canConnectTo(world, x, y+1, z, 0))
//		{
//			y2 = 1.0F;
//		};
//
//		if (canConnectTo(world, x, y, z-1, 3))
//		{
//			z1 = 0.0F;
//		};
//
//		if (canConnectTo(world, x, y, z+1, 2))
//		{
//			z2 = 1.0F;
//		};
//
//		setBlockBounds(x1, y1, z1, x2, y2, z2);
//	};
//
//	@Override
//	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
//	{
//		setBlockBoundsBasedOnState(world, x, y, z);
//		return super.getCollisionBoundingBoxFromPool(world, x, y, z);
//	};
};
