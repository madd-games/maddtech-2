/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockCompareExact.java
 * A class which only matches the exact block.
 */

package com.madd.maddtech.block;

import com.madd.maddtech.util.SideUtil;

import net.minecraft.block.*;
import net.minecraft.world.*;

public class BlockCompareExact extends BlockCompare
{
	private Block targetBlock;
	private int meta;
	
	public BlockCompareExact(Block block)
	{
		targetBlock = block;
		meta = 0;
	};
	
	public BlockCompareExact(Block block, int pathSide)
	{
		targetBlock = block;
		meta = pathSide | 8;
	};

//	@Override
//	public boolean matches(Block block)
//	{
//		if (block instanceof BlockShell)
//		{
//			BlockShell shell = (BlockShell) block;
//			if (targetBlock instanceof BlockShell)
//			{
//				BlockShell targetShell = (BlockShell) targetBlock;
//				if (targetShell.getShellMaterial().equals(shell.getShellMaterial()))
//				{
//					return true;
//				};
//			};
//		};
//		
//		return block == targetBlock;
//	};
	
	@Override
	public void config(World world, int x, int y, int z)
	{
		if (meta != 0)
		{
			//world.setBlockMetadataWithNotify(x, y, z, meta, 3);
		};
	};
	
	@Override
	public BlockCompare spin(int newFrontSide)
	{
		if ((meta == 0) || (meta == 8) || (meta == 9))
		{
			// neutral, top and bottom do not spin
			return this;
		};
		
		int side = meta & 7;
		
		int newRightSide = 4;
		switch (newFrontSide)
		{
		case 2:
			newRightSide = 5;
			break;
		case 3:
			newRightSide =  4;
			break;
		case 4:
			newRightSide = 2;
			break;
		case 5:
			newRightSide = 3;
			break;
		};
		
		int newBackSide = SideUtil.opposite(newFrontSide);
		int newLeftSide = SideUtil.opposite(newRightSide);
		
		int newSide = 2;
		if (side == 3)				/* "old front side" */
		{
			newSide = newFrontSide;
		}
		else if (side == 4)			/* "old right side" */
		{
			newSide = newRightSide;
		}
		else if (side == 5)			/* "old left side" */
		{
			newSide = newLeftSide;
		}
		else
		{
			newSide = newBackSide;
		};
		
		return new BlockCompareExact(targetBlock, newSide);
	}

	@Override
	public boolean matches(Block block) {
		// TODO Auto-generated method stub
		return false;
	};
};
