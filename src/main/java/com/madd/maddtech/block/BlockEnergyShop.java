/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockEnergyShop.java
 * The energy shop block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;






import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;

import java.util.*;

import net.minecraft.item.*;
import net.minecraft.entity.*;

public class BlockEnergyShop extends ContainerBlock
{
//	private IIcon iconInput;
//	private IIcon iconOutput;
//	private IIcon iconRuby;
//	private IIcon iconSide;
//	private IIcon iconInputLM;
//	private IIcon iconOutputLM;
//	private IIcon iconRubyLM;
//	private IIcon iconSideLM;

	public BlockEnergyShop()
	{
		super(Block.Properties.of(Material.STONE));
		
		
		
		
		
	};

	protected void mtSetup()
	{
	};

//	public int getOutputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return 2 + (world.getBlockMetadata(x, y, z) % 4);
//	};
//	
//	public int getInputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return getOutputSide(world, x, y, z) ^ 1;
//	};
	
	public int getRubySide()
	{
		return 1;
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconInput = ir.registerIcon("maddtech:AccumulatorInput");
//		iconOutput = ir.registerIcon("maddtech:AccumulatorOutput");
//		iconInputLM = ir.registerIcon("maddtech:AccumulatorInput_lm");
//		iconOutputLM = ir.registerIcon("maddtech:AccumulatorOutput_lm");
//		iconSide = ir.registerIcon("maddtech:Shop");
//		iconSideLM = ir.registerIcon("maddtech:AutoShop_lm");
//		iconRuby = ir.registerIcon("maddtech:RubyInput");
//		iconRubyLM = ir.registerIcon("maddtech:RubyInput_lm");
//	};
//
//	@Override
//	public IIcon getIcon(int sideAndType, int metadata)
//	{
//		int type = sideAndType & 0xF0;
//		int side = sideAndType & 0x0F;
//		
//		int outputSide = 2 + (metadata % 4);
//		int inputSide = outputSide ^ 1;
//		int rubySide = 1;
//		
//		if (type == 0)
//		{
//			if (side == outputSide)
//			{
//				return iconOutput;
//			}
//			else if (side == inputSide)
//			{
//				return iconInput;
//			}
//			else if (side == rubySide)
//			{
//				return iconRuby;
//			}
//			else
//			{
//				return iconSide;
//			}
//		}
//		else
//		{
//			if (side == outputSide)
//			{
//				return iconOutputLM;
//			}
//			else if (side == inputSide)
//			{
//				return iconInputLM;
//			}
//			else if (side == rubySide)
//			{
//				return iconRubyLM;
//			}
//			else
//			{
//				return iconSideLM;
//			}
//		}
//	};
//
//	@Override
//	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase placer, ItemStack stack)
//	{
//		if (!world.isRemote)
//		{
//			if (placer instanceof EntityPlayer)
//			{
//				EntityPlayer player = (EntityPlayer) placer;
//				TileEntityEnergyShop te = (TileEntityEnergyShop) world.getTileEntity(x, y, z);
//				te.setOwner(player.getCommandSenderName());
//				player.openGui(MaddTech.instance, GuiHandler.ENERGY_SHOP, world, x, y, z);
//			};
//		};
//	};
//	
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				world.setBlockMetadataWithNotify(x, y, z, (meta+1) % 4, 3);
//				return true;
//			};
//		};
//
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		TileEntityEnergyShop shop = (TileEntityEnergyShop) world.getTileEntity(x, y, z);
//		if (shop.getOwner().equals(player.getCommandSenderName()))
//		{
//			player.openGui(MaddTech.instance, GuiHandler.ENERGY_SHOP, world, x, y, z);
//		};
//		
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntityEnergyShop();
//	};
//	
//	@Override
//	public void breakBlock(World world, int x, int y, int z, Block wtf, int lol)
//	{
//		dropItems(world, x, y, z);
//		super.breakBlock(world, x, y, z, wtf, lol);
//	}
//	
//	private void dropItems(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		TileEntity tileEntity = world.getTileEntity(x, y, z);
//		if (!(tileEntity instanceof IInventory))
//		{
//			return;
//		}
//        
//		IInventory inventory = (IInventory) tileEntity;
//
//		int i;
//		for (i = 0; i < inventory.getSizeInventory(); i++)
//		{
//        		ItemStack item = inventory.getStackInSlot(i);
//
//			if (item != null && item.stackSize > 0)
//			{
//				float rx = rand.nextFloat() * 0.8F + 0.1F;
//				float ry = rand.nextFloat() * 0.8F + 0.1F;
//				float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//				EntityItem entityItem = new EntityItem(world,
//					x + rx, y + ry, z + rz,
//					new ItemStack(item.getItem(), item.stackSize, item.getItemDamage()));
//        		
//				if (item.hasTagCompound())
//				{
//					entityItem.getEntityItem().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
//				}
//
//				float factor = 0.05F;
//				entityItem.motionX = rand.nextGaussian() * factor;
//				entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//				entityItem.motionZ = rand.nextGaussian() * factor;
//				world.spawnEntityInWorld(entityItem);
//				item.stackSize = 0;
//			};
//		};
//	};
//
//	// LIGHTMAP
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderLightmapID;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
