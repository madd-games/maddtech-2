/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockLampOn.java
 * Base class for lamps that are on in MaddTech.
 */

package com.madd.maddtech.block;

import java.util.*;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.item.*;
import net.minecraft.entity.player.*;

public class BlockLampOn extends Block implements IGlowingLamp
{
	private Block blockOff;
	private float red;
	private float green;
	private float blue;
	private boolean phonyMode;

	public BlockLampOn(Properties properties)
	{
		super(properties);
	};

	protected void mtSetup(float red, float green, float blue)
	{
		//setCreativeTab(null);
		this.red = red;
		this.green = green;
		this.blue = blue;
	};

	public void setBlockOff(Block block)
	{
		blockOff = block;
	};

//	@Override
//	public void onBlockAdded(World world, int x, int y, int z)
//	{
//		updateState(world, x, y, z);
//	};
//
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
//	{
//		updateState(world, x, y, z);
//	};
//
//	public void updateState(World world, int x, int y, int z)
//	{
//		if (!world.isBlockIndirectlyGettingPowered(x, y, z))
//		{
//			world.setBlock(x, y, z, blockOff);
//		};
//	};
//
//	@Override
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		if (phonyMode)
//		{
//			return 0;
//		}
//		else
//		{
//			return LibTech.lampGlowRenderID;
//		}
//	};
//
//	@Override
//	public boolean isBlockNormalCube()
//	{
//		return false;
//	};
//
//	@Override
//	public Item getItemDropped(int w, Random t, int f)
//	{
//		return Item.getItemFromBlock(blockOff);
//	};
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		LampGlowRender.renderPass = pass;
//		return true;
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};

	// IGlowingLamp
	@Override
	public float getRed()
	{
		return red;
	};

	@Override
	public float getGreen()
	{
		return green;
	};

	@Override
	public float getBlue()
	{
		return blue;
	};

	@Override
	public void setPhonyMode(boolean flag)
	{
		phonyMode = flag;
	};

//	@Override
//	public boolean canHarvestBlock(EntityPlayer player, int meta)
//	{
//		return true;
//	};
};
