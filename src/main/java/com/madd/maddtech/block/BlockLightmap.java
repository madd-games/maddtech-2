/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockDynamo.java
 * The dynamo.
 */

package com.madd.maddtech.block;

import java.util.*;

import org.spongepowered.asm.mixin.MixinEnvironment.Side;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.*;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraft.item.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.entity.player.*;
import com.madd.maddtech.api.*;

public class BlockLightmap extends Block
{
	private boolean phonyMode;
	//public IIcon iconLightmap;
	private String mapName;
	
	public BlockLightmap(Properties properties)
	{
		super(properties);
	};

	protected String getTextureName()
	{
		return "";
	};
	
	protected void mtSetup()
	{
		mapName = getTextureName() + "_lm";
	};
	
	protected void mtSetup(String name)
	{
		mapName = "maddtech:" + name;
	};

	//	@Override
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		if (phonyMode)
//		{
//			return 0;
//		}
//		else
//		{
//			return LibTech.renderLightmapID;
//		}
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	};
//
//	public void setPhonyMode(boolean flag)
//	{
//		phonyMode = flag;
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		super.registerBlockIcons(ir);
//		iconLightmap = ir.registerIcon(mapName);
//	};
//	
//	@Override
//	public IIcon getIcon(int side, int meta)
//	{
//		if ((side & 0xF0) == 0)
//		{
//			return super.getIcon(side, meta);
//		}
//		else
//		{
//			return iconLightmap;
//		}
//	};
};
