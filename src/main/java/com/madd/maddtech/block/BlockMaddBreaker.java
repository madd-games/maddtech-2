/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddBreaker.java
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import com.madd.maddtech.api.*;

/**
 * Metadata:
 * 3 2 1 0
 * A|SIDE |
 *
 * A - if set, the "on" animation frame shall be displayed.
 * SIDE - which side is the back
 */
public class BlockMaddBreaker extends Block implements IItemRouter
{
//	public IIcon iconFrontOff;
//	public IIcon iconFrontOn;
//	public IIcon iconSideOn;
//	public IIcon iconSideOff;
//	public IIcon iconBack;
//	
//	public IIcon iconFrontLM;
//	public IIcon iconSideLM;
//	public IIcon iconBackLM;
	
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconFrontOff = ir.registerIcon("maddtech:BlockBreakerFrontOff");
//		iconFrontOn = ir.registerIcon("maddtech:BlockBreakerFrontOn");
//		iconSideOff = ir.registerIcon("maddtech:BlockBreakerSideOff");
//		iconSideOn = ir.registerIcon("maddtech:BlockBreakerSideOn");
//		iconBack = ir.registerIcon("maddtech:BlockBreakerOut");
//		iconBackLM = ir.registerIcon("maddtech:BlockBreakerOut_lm");
//		iconFrontLM = ir.registerIcon("maddtech:BlockBreakerFront_lm");
//		iconSideLM = ir.registerIcon("maddtech:BlockBreakerSide_lm");
//	};
//	
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		boolean a = (metadata & 8) != 0;
//		
//		if (side == 0)
//		{
//			return iconBack;
//		};
//		
//		if (side == 1)
//		{
//			if (a) return iconFrontOn;
//			else return iconFrontOff;
//		};
//		
//		if (a) return iconSideOn;
//		else return iconSideOff;
//	};
//
//	public BlockMaddBreaker(final Block.Properties properties)
//	{
//		super(Block.Properties.of(Material.STONE));
//	};
//
	protected void mtSetup()
	{
	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				int mask = meta & 8;
//				world.setBlockMetadataWithNotify(x, y, z, mask | (((meta&7)+1) % 6), 3);
//				return true;
//			};
//		};
//
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		return (pass == 0);
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderRotatingID;
//	};
//	
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block ignoreThis)
//	{
//		int ourMeta = world.getBlockMetadata(x, y, z);
//		int flag = 0;
//		if (world.isBlockIndirectlyGettingPowered(x, y, z))
//		{
//			flag = 8;
//			if ((ourMeta & 8) != 0) return;
//			
//			int backSide = world.getBlockMetadata(x, y, z) & 7;
//			
//			int bx = x - SideUtil.getSideX(backSide);
//			int by = y - SideUtil.getSideY(backSide);
//			int bz = z - SideUtil.getSideZ(backSide);
//			
//			Block block = world.getBlock(bx, by, bz);
//			int meta = world.getBlockMetadata(bx, by, bz);
//			ArrayList<ItemStack> drops = block.getDrops(world, bx, by, bz, meta, 0);
//			world.setBlockToAir(bx, by, bz);
//			
//			SearchItem si = new SearchItem(world);
//			si.execute(x+SideUtil.getSideX(backSide), y+SideUtil.getSideY(backSide), z+SideUtil.getSideZ(backSide), SideUtil.opposite(backSide));
//			
//			Random rand = new Random();
//			for (ItemStack stack : drops)
//			{
//				ItemStack throwOut = si.pushStack(stack);
//				if (throwOut != null)
//				{
//
//					float rx = rand.nextFloat() * 0.8F + 0.1F;
//					float ry = rand.nextFloat() * 0.8F + 0.1F;
//					float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//					EntityItem entityItem = new EntityItem(world,
//						x + SideUtil.getSideX(backSide) + rx,
//						y + SideUtil.getSideY(backSide) + ry,
//						z + SideUtil.getSideZ(backSide) + rz,
//						throwOut.copy());
//
//					float factor = 0.05F;
//					float fac2 = 0.02F;
//					entityItem.motionX = SideUtil.getSideX(backSide) * fac2 + rand.nextGaussian() * factor;
//					entityItem.motionY = SideUtil.getSideY(backSide) * fac2 + rand.nextGaussian() * factor;
//					entityItem.motionZ = SideUtil.getSideZ(backSide) * fac2 + rand.nextGaussian() * factor;
//					world.spawnEntityInWorld(entityItem);
//				};
//			};
//		};
//		
//		world.setBlockMetadataWithNotify(x, y, z, (ourMeta&7) | flag, 2);
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//	
//	@Override
//	public boolean canConnectItem(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return side == (world.getBlockMetadata(x, y, z) & 7);
//	};
	
	public BlockMaddBreaker(Properties properties) {
		super(properties);
		// TODO Auto-generated constructor stub
	}

//	@Override
//	public boolean handleItemBlock(World world, int x, int y, int z, int side, boolean[] sides)
//	{
//		return false;
//	}

};
