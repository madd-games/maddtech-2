/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.block;



import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class BlockMaddBricksColored extends BlockNormal
{
//	private IIcon[] icons;
	private static String[] colorNames = new String[] {
		"Black", "Red", "Green", "Brown", "Blue", "Purple", "Cyan", "LightGray", "Gray",
		"Pink", "Lime", "Yellow", "LightBlue", "Magenta", "Orange", "White"
	};
	
	public BlockMaddBricksColored(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	}

//	@Override
//	@SideOnly(Side.CLIENT)
//	public IIcon getIcon(int side, int meta)
//	{
//		return icons[meta];
//	};
//
//	@Override
//	public int damageDropped(int p_149692_1_)
//	{
//		return p_149692_1_;
//	}
//
//	@Override
//	@SideOnly(Side.CLIENT)
//	public void getSubBlocks(Item item, CreativeTabs tabs, List list)
//	{	
//		int i;
//		for (i=0; i<16; i++)
//		{
//			list.add(new ItemStack(item, 1, i));
//		};
//	}
//
//	@Override
//	@SideOnly(Side.CLIENT)
//	public void registerBlockIcons(IIconRegister ir)
//	{	
//		icons = new IIcon[16];
//		int i;
//		
//		for (i=0; i<16; i++)
//		{
//			icons[i] = ir.registerIcon("maddtech:Bricks" + colorNames[i]);
//		};
//	}
}
