/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddCabinet.java
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;

public class BlockMaddCabinet extends BlockNormal
{
//	private IIcon iconIn;
//	private IIcon iconOut;
//	private IIcon iconInLM;
//	private IIcon iconOutLM;

	public BlockMaddCabinet(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

	public static boolean isSideInput(int side, int metadata)
	{
		int axis = (side >> 1) & 3;
		int setting = (metadata >> axis) & 1;
		return ((side & 1) ^ setting) == 0;
	};
	
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconIn = ir.registerIcon("maddtech:CabinetIn");
//		iconInLM = ir.registerIcon("maddtech:CabinetIn_lm");
//		iconOut = ir.registerIcon("maddtech:CabinetOut");
//		iconOutLM = ir.registerIcon("maddtech:CabinetOut_lm");
//	};
//
//	@Override
//	public IIcon getIcon(int sideAndType, int metadata)
//	{
//		int type = sideAndType & 0xF0;
//		int side = sideAndType & 0x0F;
//		
//		if (type != 0)
//		{
//			// lightmaps
//			if (isSideInput(side, metadata)) return iconInLM;
//			else return iconOutLM;
//		}
//		else
//		{
//			if (isSideInput(side, metadata)) return iconIn;
//			else return iconOut;
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				int axis = (side >> 1) & 3;
//				int mask = (1 << axis);
//				meta ^= mask;
//				world.setBlockMetadataWithNotify(x, y, z, meta, 3);
//				return true;
//			};
//		};
//
//		return false;
//	};
//
//	// LIGHTMAP
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderLightmapID;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	};
};
