/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.util.*;

import net.minecraft.client.renderer.texture.*;
import net.minecraft.inventory.*;
import net.minecraft.tileentity.*;
import net.minecraft.entity.player.*;
import com.madd.maddtech.api.*;
import java.io.*;

public class BlockMaddCentrifuge extends Block
{
//	public static IIcon iconHandle;
	
	public BlockMaddCentrifuge(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconHandle = ir.registerIcon("maddtech:DynamoHandle");
//	};
//
	protected void mtSetup()
	{
	};
//	
//	@Override
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderCentrifugeID;
//	};
//
//	@Override
//	public boolean isBlockNormalCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderCentrifuge.renderPass = pass;
//		return true;
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		int meta = world.getBlockMetadata(x, y, z);
//		world.setBlockMetadataWithNotify(x, y, z, (meta+1) % 16, 2);
//
//		return true;
//	};
};
