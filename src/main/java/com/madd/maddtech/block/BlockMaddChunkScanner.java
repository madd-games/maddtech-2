/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddChunkScanner.java
 * The "chunk scanner" block. When placed, prints out some information about 
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.*;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import java.io.*;


public class BlockMaddChunkScanner extends Block
{	
	public BlockMaddChunkScanner(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int gowno)
//	{
//		int startX = x & ~0xF;
//		int startZ = z & ~0xF;
//		
//		int numUraniumOre = 0;
//		int numStone = 0;
//		int numRubyOre = 0;
//		
//		for (x=startX; x<startX+16; x++)
//		{
//			for (y=0; y<256; y++)
//			{
//				for (z=startZ; z<startZ+16; z++)
//				{
//					Block block = world.getBlock(x, y, z);
//					if (block == Blocks.stone)
//					{
//						numStone++;
//					}
//					else if (block == MaddTech.blockRubyOre)
//					{
//						numRubyOre++;
//					}
//					else if (block == MaddTech.blockUraniumOre)
//					{
//						numUraniumOre++;
//					};
//				};
//			};
//		};
//		
//		System.out.println("[MADDTECH] Uranium ore: " + numUraniumOre);
//		System.out.println("[MADDTECH] Stone: " + numStone);
//		System.out.println("[MADDTECH] Ruby ore: " + numRubyOre);
//		
//		return 0;
//	};
};
