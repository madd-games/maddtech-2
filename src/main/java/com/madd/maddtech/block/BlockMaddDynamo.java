/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockDynamo.java
 * The dynamo.
 */

package com.madd.maddtech.block;

import java.util.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.item.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.entity.player.*;
import com.madd.maddtech.api.*;

public class BlockMaddDynamo extends Block
{
	public boolean phonyMode = false;

//	private IIcon iconSide;
//	private IIcon iconBottom;
//	public IIcon iconHandle;

	public BlockMaddDynamo(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{

	};

//	@Override
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		if (phonyMode)
//		{
//			return 0;
//		}
//		else
//		{
//			return LibTech.renderDynamoID;
//		}
//	};
//
//	@Override
//	public boolean isBlockNormalCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderDynamo.renderPass = pass;
//		return true;
//	};
//
//	public void setPhonyMode(boolean flag)
//	{
//		phonyMode = flag;
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconSide = ir.registerIcon("maddtech:TinShell");
//		iconBottom = ir.registerIcon("maddtech:AccumulatorOutput");
//		iconHandle = ir.registerIcon("maddtech:DynamoHandle");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		switch (side)
//		{
//		case 0:
//			return iconBottom;
//		default:
//			return iconSide;
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		int meta = world.getBlockMetadata(x, y, z);
//		world.setBlockMetadataWithNotify(x, y, z, (meta+1) % 16, 2);
//
//		boolean haveBattery = false;
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemBattery)
//			{
//				haveBattery = true;
//			};
//		};
//
//		if (!world.isRemote)
//		{
//			if (haveBattery)
//			{
//				ItemBattery battery = (ItemBattery) MaddTech.itemBattery;
//				return battery.pushEnergy(stack, 5) > 0;
//			}
//			else
//			{
//				SearchEnergy search = new SearchEnergy(world);
//				search.execute(x, y-1, z, 1);
//				search.pushEnergy(5);
//			};
//		};
//
//		return true;
//	};
};
