/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddEnergyProbe.java
 * Base class for 'redstone cable' blocks in MaddTech.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.*;
import net.minecraft.world.*;
import net.minecraft.util.*;

import net.minecraft.client.renderer.texture.*;
import java.io.*;
import java.util.*;
import net.minecraft.item.*;
import net.minecraft.entity.item.*;

public class BlockMaddEnergyProbe extends BlockLightmap
{
//	private IIcon iconTop;
//	private IIcon iconSide;
//	private IIcon iconSideLM;
//	private IIcon iconTopLM;

	public BlockMaddEnergyProbe(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return side == 1;		// top
//	};
//
//	@Override
//	public boolean canProvidePower()
//	{
//		return true;
//	};
//
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (side != 0)		// opposite to the top, because minecraft fucks logic.
//		{
//			return 0;
//		};
//
//		if (world.getBlockMetadata(x, y, z) == 1)
//		{
//			return 15;
//		}
//		else
//		{
//			return 0;
//		}
//	};
//
//	@Override
//	public void onBlockAdded(World world, int x, int y, int z)
//	{
//		updateState(world, x, y, z);
//	};
//
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
//	{
//		updateState(world, x, y, z);
//	};
//
//	public void updateState(World world, int x, int y, int z)
//	{
//		if (world.getBlock(x, y-1, z) != MaddTech.blockAccumulator)
//		{
//			pop(world, x, y, z);
//		};
//	};
//
//	private void pop(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		float rx = rand.nextFloat() * 0.8F + 0.1F;
//		float ry = rand.nextFloat() * 0.8F + 0.1F;
//		float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//		EntityItem entityItem = new EntityItem(world,
//			x + rx, y + ry, z + rz,
//			new ItemStack(MaddTech.blockEnergyProbe, 1));
//
//		float factor = 0.05F;
//		entityItem.motionX = rand.nextGaussian() * factor;
//		entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//		entityItem.motionZ = rand.nextGaussian() * factor;
//		world.spawnEntityInWorld(entityItem);
//		world.setBlock(x, y, z, Blocks.air);
//		world.setTileEntity(x, y, z, null);
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconSide = ir.registerIcon("maddtech:EnergyProbe");
//		iconTop = ir.registerIcon("maddtech:EnergyProbeTop");
//		iconTopLM = ir.registerIcon("maddtech:EnergyProbeTop_lm");
//		iconSideLM = ir.registerIcon("maddtech:EnergyProbe_lm");
//	};
//
//	@Override
//	public IIcon getIcon(int sideAndType, int metadata)
//	{
//		int type = sideAndType & 0xF0;
//		int side = sideAndType & 0x0F;
//		
//		if (side == 1)
//		{
//			if (type == 0) return iconTop;
//			else return iconTopLM;
//		}
//		else
//		{
//			if (type == 0) return iconSide;
//			else return iconSideLM;
//		}
//	};
};
