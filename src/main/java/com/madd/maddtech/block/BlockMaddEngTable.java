/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddEngTable.java
 * Base class for 'normal' blocks in MaddTech.
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.ContainerBlock;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;

public class BlockMaddEngTable extends ContainerBlock
{
//	private IIcon iconTop;
//	private IIcon iconSide;
//	private IIcon iconInput;
//	
//	private IIcon iconTopLM;
//	private IIcon iconSideLM;
//	private IIcon iconInputLM;

	public BlockMaddEngTable(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconTop = ir.registerIcon("maddtech:EngTableTop");
//		iconSide = ir.registerIcon("maddtech:EngTableSide");
//		iconInput = ir.registerIcon("maddtech:EngTableInput");
//		
//		iconTopLM = ir.registerIcon("maddtech:EngTableTop_lm");
//		iconSideLM = ir.registerIcon("maddtech:EngTableSide_lm");
//		iconInputLM = ir.registerIcon("maddtech:EngTableInput_lm");
//	};
//
//	public int getInputSideByMetadata(int metadata)
//	{
//		int side = 2 + (metadata % 4);
//		return side;
//	};
//	
//	@Override
//	public IIcon getIcon(int typeAndSide, int metadata)
//	{
//		int type = typeAndSide & 0xF0;
//		int side = typeAndSide & 0x0F;
//		
//		int inputSide = getInputSideByMetadata(metadata);
//		
//		if (side == inputSide)
//		{
//			if (type == 0) return iconInput;
//			else return iconInputLM;
//		};
//		
//		switch (side)
//		{
//		case 0:
//		case 1:
//			if (type == 0) return iconTop;
//			else return iconTopLM;
//		default:
//			if (type == 0) return iconSide;
//			else return iconSideLM;
//		}
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntityEngTable();
//	};
//
//	@Override
//	public void breakBlock(World world, int x, int y, int z, Block wtf, int lol)
//	{
//		dropItems(world, x, y, z);
//		super.breakBlock(world, x, y, z, wtf, lol);
//	};
//
//	private void dropItems(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		TileEntity tileEntity = world.getTileEntity(x, y, z);
//		if (!(tileEntity instanceof IInventory))
//		{
//			return;
//		}
//        
//		IInventory inventory = (IInventory) tileEntity;
//
//		int i;
//		for (i = 0; i < inventory.getSizeInventory(); i++)
//		{
//        		ItemStack item = inventory.getStackInSlot(i);
//
//			if (item != null && item.stackSize > 0)
//			{
//				float rx = rand.nextFloat() * 0.8F + 0.1F;
//				float ry = rand.nextFloat() * 0.8F + 0.1F;
//				float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//				EntityItem entityItem = new EntityItem(world,
//					x + rx, y + ry, z + rz,
//					new ItemStack(item.getItem(), item.stackSize, item.getItemDamage()));
//        		
//				if (item.hasTagCompound())
//				{
//					entityItem.getEntityItem().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
//				}
//
//				float factor = 0.05F;
//				entityItem.motionX = rand.nextGaussian() * factor;
//				entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//				entityItem.motionZ = rand.nextGaussian() * factor;
//				world.spawnEntityInWorld(entityItem);
//				item.stackSize = 0;
//			};
//		};
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				world.setBlockMetadataWithNotify(x, y, z, (meta+1) % 4, 3);
//				return true;
//			};
//		};
//		
//		if (player.isSneaking())
//		{
//			return false;
//		};
//		
//		player.openGui(MaddTech.instance, GuiHandler.ENG_TABLE, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//
//	// LIGHTMAP
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderLightmapID;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
