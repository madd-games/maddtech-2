/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddFluidCable.java
 * Base class for 'Fluid cable' blocks in MaddTech.
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.util.*;

import net.minecraft.tileentity.*;
import net.minecraft.client.renderer.texture.*;
import java.io.*;
import com.madd.maddtech.api.*;

public class BlockMaddFluidCable extends BlockCable
{
//	private IIcon connectorIcon;

	public BlockMaddFluidCable(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		super.registerBlockIcons(ir);
//		connectorIcon = ir.registerIcon("maddtech:FluidCableCC");
//	};
//
//	// ICable
//	@Override
//	public boolean canConnectTo(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (WiringRules.canConnectCable(WiringRules.CABLE_FLUID, world, x, y, z, side)) return true;
//
//		Block block = world.getBlock(x, y, z);
//		if (block == this)
//		{
//			return true;
//		}
//		else if ((block == MaddTech.blockCentrifuge) && (side == 1))
//		{
//			return true;
//		}
//		else if (block == MaddTech.blockUraniumRefinery)
//		{
//			return true;
//		}
//		else if (block instanceof BlockShell)
//		{
//			BlockShell shell = (BlockShell) block;
//			return shell.isShellFluid();
//		}
//		else if (block instanceof IFluidRouter)
//		{
//			IFluidRouter router = (IFluidRouter) block;
//			return router.canConnectFluid(world, x, y, z, side);
//		}
//		else if ((block == Blocks.water) || (block == Blocks.flowing_water) || (block == Blocks.lava) ||
//			(block == Blocks.flowing_lava))
//		{
//			return side == 1;
//		}
//		else if (block instanceof BlockMaddTerminal)
//		{
//			return side >= 2;
//		}
//		else
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent != null)
//			{
//				if (ent instanceof IFluidMachine)
//				{
//					IFluidMachine mach = (IFluidMachine) ent;
//					return mach.canConnectFluid(side);
//				}
//				else if (ent instanceof IFluidHandler)
//				{
//					return true;
//				}
//				else
//				{
//					return false;
//				}
//			}
//			else
//			{
//				return false;
//			}
//		}
//	};
//
//	@Override
//	public IIcon getConnectorTexture()
//	{
//		return connectorIcon;
//	};
};
