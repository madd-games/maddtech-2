/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.AbstractBlock.Properties;
import net.minecraft.block.material.Material;
import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import net.minecraft.item.*;

/**
 * METADATA:
 * 3 2 1 0
 * S|ZERO
 *
 * S - set to 1 if a redstone signal is being emitted via the output sides.
 * ZERO - zero.
 */
public class BlockMaddFluidMonitor extends ContainerBlock
{
//	private IIcon iconIn;
//	private IIcon iconOut;
//	
//	private IIcon iconInLM;
//	private IIcon iconOutLM;
	
	public BlockMaddFluidMonitor()
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

	// LIGHTMAP
	public int getRenderBlockPass()
	{
		return 1;
	};

//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderLightmapID;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntityFluidMonitor();
//	};
//	
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconIn = ir.registerIcon("maddtech:FluidMonitorIn");
//		iconOut = ir.registerIcon("maddtech:FluidMonitorOut");
//		
//		iconInLM = ir.registerIcon("maddtech:FluidMonitorIn_lm");
//		iconOutLM = ir.registerIcon("maddtech:FluidMonitorOut_lm");
//	};
//	
//	@Override
//	public IIcon getIcon(int side, int meta)
//	{
//		if (side == 0x11)
//		{
//			return iconInLM;
//		}
//		else if (side == 1)
//		{
//			return iconIn;
//		}
//		else if ((side & 0xF0) == 0)
//		{
//			return iconOut;
//		}
//		else
//		{
//			return iconOutLM;
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		player.openGui(MaddTech.instance, GuiHandler.FLUID_MONITOR, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (side == 0)
//		{
//			return 0;
//		};
//		
//		int meta = world.getBlockMetadata(x, y, z);
//		if ((meta & 8) != 0)
//		{
//			return 15;
//		};
//		
//		return 0;
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return side != 1;
//	};
//
//	@Override
//	public boolean canProvidePower()
//	{
//		return true;
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
