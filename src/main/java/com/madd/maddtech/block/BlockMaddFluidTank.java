/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddFluidTank.java
 * The pump block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;

public class BlockMaddFluidTank extends BlockMaddGlass implements ITileEntityProvider
{
//	private IIcon iconTop;
//	private IIcon iconRest;
	private static boolean isPhonyMode = false;

	public BlockMaddFluidTank()
	{
		super(Block.Properties.of(Material.STONE));
		//setBlockBounds(-1.0F, -10.0F, -1.0F, 2.0F, 1.0F, 2.0F);
	};

	protected void mtSetup()
	{
	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconTop = ir.registerIcon("maddtech:FluidTankTop");
//		iconRest = ir.registerIcon("maddtech:VolcanicGlass");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		if (side == 1)
//		{
//			return iconTop;
//		}
//		else
//		{
//			return iconRest;
//		}
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntityFluidTank();
//	};
//
//	@Override
//	public boolean canPlaceBlockAt(World world, int x, int y, int z)
//	{
//		return canTankBePlacedAt(world, x, y, z);
//	};
//
//	@Override
//	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta)
//	{
//		int ry;
//		for (ry=1; ry<10; ry++)
//		{
//			world.setBlock(x, y-ry, z, MaddTech.blockFakeGlass);
//		};
//
//		return meta;
//	};
//
//	public static boolean canTankBePlacedAt(World world, int x, int y, int z)
//	{
//		int rx, ry, rz;
//		for (rx=-1; rx<2; rx++)
//		{
//			for (rz=-1; rz<2; rz++)
//			{
//				if (world.getBlock(x+rx, y-10, z+rz) != MaddTech.blockLeadShell)
//				{
//					System.out.println("[MADDTECH] No lead shell at: " + (x+rx) + ", " + (y-10) + ", " + (z+rz));
//					return false;
//				};
//			};
//		};
//
//		for (ry=0; ry<10; ry++)
//		{
//			for (rx=-1; rx<2; rx++)
//			{
//				for (rz=-1; rz<2; rz++)
//				{
//					if ((rx != 0) || (rz != 0))
//					{
//						if (world.getBlock(x+rx, y-ry, z+rz) != MaddTech.blockVolcanicGlass)
//						{
//							System.out.println("[MADDTECH] No glass at: " + (x+rx) + ", " + (y-ry) + ", " + (z+rz));
//							return false;
//						};
//					};
//				};
//			};
//		};
//
//		return true;
//	};
//
//	private static void destroyFakeGlassBlocks(World world, int x, int y, int z)
//	{
//		int ry;
//		for (ry=1; ry<10; ry++)
//		{
//			world.setBlock(x, y-ry, z, Blocks.air);
//		};
//	};
//
//	public static void pop(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		float rx = rand.nextFloat() * 0.8F + 0.1F;
//		float ry = rand.nextFloat() * 0.8F + 0.1F;
//		float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//		EntityItem entityItem = new EntityItem(world,
//			x + rx, y + ry, z + rz,
//			new ItemStack(MaddTech.blockFluidTank, 1));
//
//		float factor = 0.05F;
//		entityItem.motionX = rand.nextGaussian() * factor;
//		entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//		entityItem.motionZ = rand.nextGaussian() * factor;
//		world.spawnEntityInWorld(entityItem);
//		world.setBlock(x, y, z, Blocks.air);
//		world.setTileEntity(x, y, z, null);
//		destroyFakeGlassBlocks(world, x, y, z);
//	};
//
//	@Override
//	public void breakBlock(World world, int x, int y, int z, Block w, int tf)
//	{
//		destroyFakeGlassBlocks(world, x, y, z);
//		super.breakBlock(world, x, y, z, w, tf);
//	};
//
//	public static void setPhonyMode(boolean m)
//	{
//		isPhonyMode = m;
//	};
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderFluidTank.renderPass = pass;
//		return true;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		if (isPhonyMode)
//		{
//			return 0;
//		}
//		else
//		{
//			return LibTech.renderFluidTankID;
//		}
//	};
//
//	@Override
//	public boolean isBlockNormalCube()
//	{
//		return false;
//	};
//
//	public void setBlockBoundsToNormal()
//	{
//		setBlockBounds(-1.0F, -10.0F, -1.0F, 2.0F, 1.0F, 2.0F);
//	};
//
//	public void setBlockBoundsToPhony()
//	{
//		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
//	};
};
