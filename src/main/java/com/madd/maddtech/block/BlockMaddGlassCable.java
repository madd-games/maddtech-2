/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddGlassCable.java
 * Glass cables, for supplying stuff to engines.
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.util.*;

import net.minecraft.tileentity.*;
import net.minecraft.client.renderer.texture.*;
import java.io.*;
import com.madd.maddtech.api.*;

public class BlockMaddGlassCable extends BlockCable
{
//	private IIcon connectorIcon;

	public BlockMaddGlassCable(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		super.registerBlockIcons(ir);
//		connectorIcon = ir.registerIcon("maddtech:GlassCableCC");
//	};
//
//	// ICable
//	@Override
//	public boolean canConnectTo(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (WiringRules.canConnectCable(WiringRules.CABLE_FLUID, world, x, y, z, side)) return true;
//
//		Block block = world.getBlock(x, y, z);
//		if (block == this)
//		{
//			return true;
//		};
//
//		return false;
//	};
//
//	@Override
//	public IIcon getConnectorTexture()
//	{
//		return connectorIcon;
//	};
};
