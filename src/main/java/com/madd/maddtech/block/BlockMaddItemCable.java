/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddItemCable.java
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.util.*;

import net.minecraft.client.renderer.texture.*;
import net.minecraft.inventory.*;
import net.minecraft.tileentity.*;
import com.madd.maddtech.api.*;
import java.io.*;

public class BlockMaddItemCable extends BlockCable
{
//	private IIcon connectorIcon;

	public BlockMaddItemCable(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		super.registerBlockIcons(ir);
//		connectorIcon = ir.registerIcon("maddtech:ItemCableCC");
//	};
//
//	// ICable
//	@Override
//	public boolean canConnectTo(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (WiringRules.canConnectCable(WiringRules.CABLE_ITEM, world, x, y, z, side)) return true;
//
//		if ((side == 1) && (world.getBlock(x, y-1, z) == MaddTech.blockUraniumRefinery))
//		{
//			return true;
//		};
//
//		Block block = world.getBlock(x, y, z);
//		if (block == this)
//		{
//			return true;
//		}
//		else if (block instanceof IItemRouter)
//		{
//			IItemRouter router = (IItemRouter) block;
//			return router.canConnectItem(world, x, y, z, side);
//		}
//		else if (block instanceof BlockShell)
//		{
//			BlockShell shell = (BlockShell) block;
//			return shell.getShellFunction().startsWith("item");
//		}
//		else if ((block == MaddTech.blockItemSorter) && (side == 1))
//		{
//			return true;
//		}
//		else if (block instanceof BlockMaddTerminal)
//		{
//			return side >= 2;
//		}
//		else
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te != null)
//			{
//				if (te instanceof IItemMachine)
//				{
//					IItemMachine mach = (IItemMachine) te;
//					return mach.canConnectItem(side);
//				}
//				else if (te instanceof ISidedInventory)
//				{
//					ISidedInventory inv = (ISidedInventory) te;
//					int[] slots = inv.getAccessibleSlotsFromSide(side);
//					if (slots.length > 0)
//					{
//						return true;
//					};
//				}
//				else if (te instanceof IInventory)
//				{
//					return true;
//				};
//			};
//
//			return false;
//		}
//	};
//
//	@Override
//	public IIcon getConnectorTexture()
//	{
//		return connectorIcon;
//	};
};
