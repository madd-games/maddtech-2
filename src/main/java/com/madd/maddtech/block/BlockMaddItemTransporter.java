/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddItemTransporter.java
 * The pump block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;






import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import com.madd.maddtech.api.*;

public class BlockMaddItemTransporter extends BlockLightmap implements ITileEntityProvider, IItemRouter
{
//	private IIcon iconSide;
//	private IIcon iconInput;
//	private IIcon iconOutput;
//	
//	private IIcon iconSideLM;
//	private IIcon iconInputLM;
//	private IIcon iconOutputLM;

	public BlockMaddItemTransporter(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	public int getInputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return world.getBlockMetadata(x, y, z);
//	};
//
//	public int getOutputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return SideUtil.opposite(getInputSide(world, x, y, z));
//	};
//	
//	@Override
//	public void breakBlock(World world, int x, int y, int z, Block wtf, int lol)
//	{
//		dropItems(world, x, y, z);
//		super.breakBlock(world, x, y, z, wtf, lol);
//	}
//	
//	private void dropItems(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		TileEntity tileEntity = world.getTileEntity(x, y, z);
//		if (!(tileEntity instanceof IInventory))
//		{
//			return;
//		}
//        
//		IInventory inventory = (IInventory) tileEntity;
//
//		int i;
//		for (i = 0; i < inventory.getSizeInventory(); i++)
//		{
//        		ItemStack item = inventory.getStackInSlot(i);
//
//			if (item != null && item.stackSize > 0)
//			{
//				float rx = rand.nextFloat() * 0.8F + 0.1F;
//				float ry = rand.nextFloat() * 0.8F + 0.1F;
//				float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//				EntityItem entityItem = new EntityItem(world,
//					x + rx, y + ry, z + rz,
//					new ItemStack(item.getItem(), item.stackSize, item.getItemDamage()));
//        		
//				if (item.hasTagCompound())
//				{
//					entityItem.getEntityItem().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
//				}
//
//				float factor = 0.05F;
//				entityItem.motionX = rand.nextGaussian() * factor;
//				entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//				entityItem.motionZ = rand.nextGaussian() * factor;
//				world.spawnEntityInWorld(entityItem);
//				item.stackSize = 0;
//			};
//		};
//	};
//	
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return (side != getInputSide(world, x, y, z)) && (side != getOutputSide(world, x, y, z));
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconSide = ir.registerIcon("maddtech:ItemTransporterSide");
//		iconInput = ir.registerIcon("maddtech:ItemTransporterInput");
//		iconOutput = ir.registerIcon("maddtech:ItemTransporterOutput");
//		
//		iconSideLM = ir.registerIcon("maddtech:ItemTransporterSide_lm");
//		iconInputLM = ir.registerIcon("maddtech:ItemTransporterInput_lm");
//		iconOutputLM = ir.registerIcon("maddtech:ItemTransporterOutput_lm");
//	};
//
//	@Override
//	public IIcon getIcon(int sideAndType, int metadata)
//	{
//		int type = sideAndType & 0xF0;
//		int side = sideAndType & 0x0F;
//		
//		if (side == metadata)
//		{
//			if (type == 0) return iconInput;
//			return iconInputLM;
//		}
//		else if (side == SideUtil.opposite(metadata))
//		{
//			if (type == 0) return iconOutput;
//			else return iconOutputLM;
//		}
//		else
//		{
//			if (type == 0) return iconSide;
//			else return iconSideLM;
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				world.setBlockMetadataWithNotify(x, y, z, (meta+1) % 6, 3);
//				return true;
//			};
//		};
//
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		player.openGui(MaddTech.instance, GuiHandler.ITEM_TRANSPORTER, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int par)
//	{
//		return new TileEntityItemTransporter();
//	};
//	
//	@Override
//	public boolean canConnectItem(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//	
//	@Override
//	public boolean handleItemBlock(World world, int x, int y, int z, int side, boolean[] sides)
//	{
//		return true;
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
