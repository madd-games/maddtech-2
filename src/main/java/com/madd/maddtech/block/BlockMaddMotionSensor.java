/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * METADATA:
 * 00DM
 *
 * M - 1 for master, 0 for slave.
 * D - 0 for door on X-axis, 1 for door on Z-axis.
 *
 * Master sensor drives the door and stores door state; slaves are used merely as structure
 * enforcement. Master vs slave is decided upon placing a sensor. When 8 sensors are placed
 * in a line, the sensor on one end becomes a master.
 */
package com.madd.maddtech.block;

import java.util.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.item.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.entity.player.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.*;
import net.minecraft.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.*;
import com.madd.maddtech.api.*;
import java.util.*;

public class BlockMaddMotionSensor extends BlockLightmap implements ITileEntityProvider
{
	public BlockMaddMotionSensor(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
		super.mtSetup();
	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};

//	@Override
//	public TileEntity createNewTileEntity(World world, int par)
//	{
//		return new TileEntityMotionSensor();
//	};
//
//	private int scanSensorsX(World world, int x, int y, int z)
//	{
//		int myX = x;
//		while (world.getBlock(x-1, y, z) == MaddTech.blockMotionSensor)
//		{
//			x--;
//		};
//		
//		int scan;
//		boolean ok = true;
//		for (scan=0; scan<8; scan++)
//		{
//			if (world.getBlock(x+scan, y, z) != MaddTech.blockMotionSensor)
//			{
//				ok = false;
//				break;
//			};
//		};
//		
//		if (!ok)
//		{
//			return -1;
//		};
//		
//		int myMeta = 0;
//		if (myX == x)
//		{
//			myMeta = 1;
//		};
//		
//		for (scan=0; scan<8; scan++)
//		{
//			int meta = 0;
//			if (scan == 0)
//			{
//				meta = 1;
//			};
//			
//			world.setBlockMetadataWithNotify(x+scan, y, z, meta, 0);
//		};
//		
//		return myMeta;
//	};
//
//	private int scanSensorsZ(World world, int x, int y, int z)
//	{
//		int myZ = z;
//		while (world.getBlock(x, y, z-1) == MaddTech.blockMotionSensor)
//		{
//			z--;
//		};
//		
//		int scan;
//		boolean ok = true;
//		for (scan=0; scan<8; scan++)
//		{
//			if (world.getBlock(x, y, z+scan) != MaddTech.blockMotionSensor)
//			{
//				ok = false;
//				break;
//			};
//		};
//		
//		if (!ok)
//		{
//			return -1;
//		};
//		
//		int myMeta = 2;
//		if (myZ == z)
//		{
//			myMeta = 3;
//		};
//		
//		for (scan=0; scan<8; scan++)
//		{
//			int meta = 2;
//			if (scan == 0)
//			{
//				meta = 3;
//			};
//			
//			world.setBlockMetadataWithNotify(x, y, z+scan, meta, 0);
//		};
//		
//		return myMeta;
//	};
//	
//	@Override
//	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase placer, ItemStack stack)
//	{
//		int meta;
//		if (!world.isRemote)
//		{
//			meta = scanSensorsX(world, x, y, z);
//			if (meta == -1)
//			{
//				meta = scanSensorsZ(world, x, y, z);
//				if (meta == -1)
//				{
//					meta = 0;
//				};
//			};
//		};
//	};
};
