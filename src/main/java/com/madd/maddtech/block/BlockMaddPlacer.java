/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddPlacer.java
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import com.madd.maddtech.api.*;

/**
 * Metadata:
 * 4 3 2 1
 * A|SIDE |
 *
 * A - if set, the "on" animation frame shall be displayed.
 * SIDE - which side is the back
 */
public class BlockMaddPlacer extends ContainerBlock
{

	protected BlockMaddPlacer(Properties properties) {
		super(Block.Properties.of(Material.STONE));
		// TODO Auto-generated constructor stub
	}
//	public IIcon iconFrontOff;
//	public IIcon iconFrontOn;
//	public IIcon iconSideOn;
//	public IIcon iconSideOff;
//	public IIcon iconBack;
//	public IIcon iconBackLM;
//	
//	public IIcon iconFrontOffLM;
//	public IIcon iconSideOffLM;
//	
//	public IIcon iconFrontOnLM;
//	public IIcon iconSideOnLM;

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconFrontOff = ir.registerIcon("maddtech:BlockPlacerFrontOff");
//		iconFrontOn = ir.registerIcon("maddtech:BlockPlacerFrontOn");
//		iconSideOff = ir.registerIcon("maddtech:BlockPlacerSideOff");
//		iconSideOn = ir.registerIcon("maddtech:BlockPlacerSideOn");
//		iconBack = ir.registerIcon("maddtech:BlockPlacerIn");
//		iconBackLM = ir.registerIcon("maddtech:BlockPlacerIn_lm");
//		iconFrontOffLM = ir.registerIcon("maddtech:BlockPlacerFront_lm");
//		iconSideOffLM = ir.registerIcon("maddtech:BlockPlacerSideOff_lm");
//		iconFrontOnLM = ir.registerIcon("maddtech:BlockPlacerOn_lm");
//		iconSideOnLM = ir.registerIcon("maddtech:BlockPlacerSide_lm");
//	};
//	
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		boolean a = (metadata & 8) != 0;
//		
//		if (side == 0)
//		{
//			return iconBack;
//		};
//		
//		if (side == 1)
//		{
//			if (a) return iconFrontOn;
//			else return iconFrontOff;
//		};
//		
//		if (a) return iconSideOn;
//		else return iconSideOff;
//	};
//
//	public BlockMaddPlacer(final Block.Properties properties)
//	{
//		super(Block.Properties.of(Material.STONE));
//	};
//
	protected void mtSetup()
	{
	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				int mask = meta & 8;
//				world.setBlockMetadataWithNotify(x, y, z, mask | (((meta&7)+1) % 6), 3);
//				return true;
//			};
//		};
//		
//		player.openGui(MaddTech.instance, GuiHandler.BLOCK_PLACER, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		return (pass == 0);
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderRotatingID;
//	};
//	
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block ignoreThis)
//	{
//		int ourMeta = world.getBlockMetadata(x, y, z);
//		int flag = 0;
//		if (world.isBlockIndirectlyGettingPowered(x, y, z))
//		{
//			flag = 8;
//			TileEntityPlacer placer = (TileEntityPlacer) world.getTileEntity(x, y, z);
//			
//			int placeSide = SideUtil.opposite(ourMeta & 7);
//			int bx = x + SideUtil.getSideX(placeSide);
//			int by = y + SideUtil.getSideY(placeSide);
//			int bz = z + SideUtil.getSideZ(placeSide);
//			
//			int i;
//			for (i=0; i<9; i++)
//			{
//				ItemStack stack = placer.getStackInSlot(i);
//				if (stack != null)
//				{
//					Item item = stack.getItem();
//					if (item instanceof ItemBlock)
//					{
//						Block block = ((ItemBlock)item).field_150939_a;
//						if (block.canPlaceBlockOnSide(world, bx, by, bz, 0))
//						{
//							int dmg = stack.getItemDamage();
//							stack.stackSize--;
//							if (stack.stackSize == 0)
//							{
//								placer.setInventorySlotContents(i, null);
//							};
//						
//							world.setBlock(bx, by, bz, block, dmg, 3);
//							block.onBlockAdded(world, bx, by, bz);
//							break;
//						};
//					};
//				};
//			};
//		};
//		
//		world.setBlockMetadataWithNotify(x, y, z, (ourMeta&7) | flag, 2);
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int par)
//	{
//		return new TileEntityPlacer();
//	};
};
