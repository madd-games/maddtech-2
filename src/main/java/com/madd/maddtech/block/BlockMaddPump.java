/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddPump.java
 * The pump block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.*;
import com.madd.maddtech.api.*;
import java.util.*;

public class BlockMaddPump extends BlockLightmap implements ITileEntityProvider, IFluidRouter
{
//	private IIcon iconSide;
//	private IIcon iconInput;
//	private IIcon iconOutput;
//	private IIcon iconPumpLM;

	public BlockMaddPump(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};

//	public int getInputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return world.getBlockMetadata(x, y, z);
//	};
//
//	public int getOutputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return SideUtil.opposite(getInputSide(world, x, y, z));
//	};
//	
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return (side != getInputSide(world, x, y, z)) && (side != getOutputSide(world, x, y, z));
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconSide = ir.registerIcon("maddtech:PumpSide");
//		iconInput = ir.registerIcon("maddtech:PumpInput");
//		iconOutput = ir.registerIcon("maddtech:PumpOutput");
//		iconPumpLM = ir.registerIcon("maddtech:PumpSide_lm");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		if ((side & 0xF0) == 0x10)
//		{
//			return iconPumpLM;
//		};
//		
//		if (side == metadata)
//		{
//			return iconInput;
//		}
//		else if (side == SideUtil.opposite(metadata))
//		{
//			return iconOutput;
//		}
//		else
//		{
//			return iconSide;
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				world.setBlockMetadataWithNotify(x, y, z, (meta+1) % 6, 3);
//				return true;
//			};
//		};
//
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		player.openGui(MaddTech.instance, GuiHandler.PUMP, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int par)
//	{
//		return new TileEntityPump();
//	};
//
//	@Override
//	public boolean canConnectFluid(IBlockAccess world, int x, int y, int z, int side)
//	{
//		int meta = world.getBlockMetadata(x, y, z);
//		return (meta & 0xE) == (side & 0xE);
//	};
//
//	@Override
//	public boolean handleFluidBlock(World world, int x, int y, int z, int side, boolean[] sides)
//	{
//		int meta = world.getBlockMetadata(x, y, z);
//		if ((meta & 0xE) != (side & 0xE))
//		{
//			return false;
//		};
//
//		int i;
//		for (i=0; i<6; i++)
//		{
//			sides[i] = ((i & 0xE) == (meta & 0xE));
//		};
//
//		return true;
//	};
//
//	@Override
//	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
//	{
//		// if a pump is next to this one, we copy it's metadata to be equally aligned and
//		// form a series automatically by default.
//		int i;
//		for (i=0; i<6; i++)
//		{
//			int bx = x + SideUtil.getSideX(i);
//			int by = y + SideUtil.getSideY(i);
//			int bz = z + SideUtil.getSideZ(i);
//
//			Block otherBlock = world.getBlock(bx, by, bz);
//			int otherMeta = world.getBlockMetadata(bx, by, bz);
//
//			if (otherBlock == MaddTech.blockPump)
//			{
//				world.setBlockMetadataWithNotify(x, y, z, otherMeta, 2);
//				return;
//			};
//		};
//	};
};
