/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddRedstoneCable.java
 * Base class for 'redstone cable' blocks in MaddTech.
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.util.*;

import net.minecraft.client.renderer.texture.*;
import com.madd.maddtech.api.*;
import java.io.*;
import java.util.*;

public class BlockMaddRedstoneCable extends BlockCable
{
//	private IIcon connectorIcon;
	private int resistance;

	private class Position
	{
		public int x;
		public int y;
		public int z;

		public Position(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		};
	};

	private static ArrayList<Position> searchedPositions = new ArrayList<Position>();
	private static ArrayList<Position> updatedPositions = new ArrayList<Position>();

	public BlockMaddRedstoneCable(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup(int resistance)
	{
		this.resistance = resistance;
	};

//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//
//	@Override
//	public boolean canProvidePower()
//	{
//		return true;
//	};
//
//	private boolean haveSearched(int x, int y, int z)
//	{
//		int i;
//		for (i=0; i<searchedPositions.size(); i++)
//		{
//			Position pos = searchedPositions.get(i);
//			if ((pos.x == x) && (pos.y == y) && (pos.z == z))
//			{
//				return true;
//			};
//		};
//
//		return false;
//	};
//
//	private boolean haveUpdated(int x, int y, int z)
//	{
//		int i;
//		for (i=0; i<updatedPositions.size(); i++)
//		{
//			Position pos = updatedPositions.get(i);
//			if ((pos.x == x) && (pos.y == y) && (pos.z == z))
//			{
//				return true;
//			};
//		};
//
//		return false;
//	};
//
//	private void stopUpdating(int x, int y, int z)
//	{
//		int i;
//		for (i=0; i<updatedPositions.size(); i++)
//		{
//			Position pos = updatedPositions.get(i);
//			if ((pos.x == x) && (pos.y == y) && (pos.z == z))
//			{
//				updatedPositions.remove(i);
//				break;
//			};
//		};
//	};
//
//	public void unsearch(int x, int y, int z)
//	{
//		int i;
//		for (i=0; i<searchedPositions.size(); i++)
//		{
//			Position pos = searchedPositions.get(i);
//			if ((pos.x == x) && (pos.y == y) && (pos.z == z))
//			{
//				searchedPositions.remove(i);
//				break;
//			};
//		};
//	};
//
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (haveSearched(x, y, z)) return 0;
//
//		searchedPositions.add(new Position(x, y, z));
//
//		side = SideUtil.opposite(side);
//		int meta = world.getBlockMetadata(x, y, z);
//		if (meta == 0)
//		{
//			// no input.
//			unsearch(x, y, z);
//			return 0;
//		}
//		else
//		{
//			int inputSide = meta - 1;
//			if (side == inputSide)
//			{
//				// We don't provide power to our input.
//				unsearch(x, y, z);
//				return 0;
//			}
//			else
//			{
//				// Otherwise, we return the amount of power provided by
//				// our input.
//				int result = Math.max(getPowerFrom(world, x, y, z, inputSide) - resistance, 0);
//				unsearch(x, y, z);
//				return result;
//			}
//		}
//	};
//
//	@Override
//	public void onBlockAdded(World world, int x, int y, int z)
//	{
//		updateState(world, x, y, z);
//	};
//
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
//	{
//		updateState(world, x, y, z);
//	};
//
//	public int getPowerFrom(IBlockAccess world, int x, int y, int z, int side)
//	{
//		int bx = x + SideUtil.getSideX(side);
//		int by = y + SideUtil.getSideY(side);
//		int bz = z + SideUtil.getSideZ(side);
//
//		Block block = world.getBlock(bx, by, bz);
//		return block.isProvidingWeakPower(world, bx, by, bz, side);
//	};
//
//	public void updateState(World world, int x, int y, int z)
//	{
//		if (haveUpdated(x, y, z)) return;
//		updatedPositions.add(new Position(x, y, z));
//
//		int meta = 0;
//		int max = 0;
//		int i;
//		for (i=0; i<6; i++)
//		{
//			int pow = getPowerFrom(world, x, y, z, i);
//			if (pow > max)
//			{
//				meta = i+1;
//				max = pow;
//			};
//		};
//
//		int oldMeta = world.getBlockMetadata(x, y, z);
//		if (/*(meta != oldMeta) || (resistance != 0)*/true)
//		{
//			world.setBlockMetadataWithNotify(x, y, z, meta, 2);
//
//			// Tell all surrounding blocks except the input
//			if ((meta == oldMeta) && (meta == 0))
//			{
//				stopUpdating(x, y, z);
//				return;
//			};
//
//			for (i=0; i<6; i++)
//			{
//				if (i != (meta-1))
//				{
//					int bx = x + SideUtil.getSideX(i);
//					int by = y + SideUtil.getSideY(i);
//					int bz = z + SideUtil.getSideZ(i);
//
//					Block block = world.getBlock(bx, by, bz);
//					block.onNeighborBlockChange(world, bx, by, bz, this);
//				};
//			};
//		};
//
//		stopUpdating(x, y, z);
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		super.registerBlockIcons(ir);
//		connectorIcon = ir.registerIcon("maddtech:RedstoneCableCC");
//	};
//
//	// ICable
//	@Override
//	public boolean canConnectTo(IBlockAccess world, int x, int y, int z, int side)
//	{
//		Block block = world.getBlock(x, y, z);
//		if (block.canConnectRedstone(world, x, y, z, side))
//		{
//			return true;
//		}
//		else if (block instanceof BlockShell)
//		{
//			return ((BlockShell)block).isShellRedstone();
//		}
//		else if (block == Blocks.redstone_lamp)
//		{
//			return true;
//		}
//		else if (block == Blocks.lit_redstone_lamp)
//		{
//			return true;
//		}
//		else if (block == Blocks.tnt)
//		{
//			return true;
//		}
//		else
//		{
//			return WiringRules.canConnectCable(WiringRules.CABLE_REDSTONE, world, x, y, z, side);
//		}
//	};
//
//	@Override
//	public IIcon getConnectorTexture()
//	{
//		return connectorIcon;
//	};
};
