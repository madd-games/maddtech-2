/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockRedstonePulser.java
 * The RedstonePulser.
 *
 * METADATA:
 * 3 2 1 0
 * SD |0|A
 *
 * SD - the angle of the pointer, mapped onto the scale 0-3, used to decide the emission side.
 * 0 - this is always zero.
 * A - alternating bit, constantly alternated to force redraw.
 */

package com.madd.maddtech.block;

import java.util.*;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.item.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.entity.player.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.*;
import com.madd.maddtech.api.*;
import java.util.*;

public class BlockMaddRedstonePulser extends Block implements ITileEntityProvider
{
//	public IIcon iconHandle;
//	private IIcon iconBase;

	public BlockMaddRedstonePulser(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
		//setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, (2.0F/3.0F), 1.0F);
	};

	protected void mtSetup()
	{

	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};

//	@Override
//	public int getRenderBlockPass()
//	{
//		return 0;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderRedstonePulserID;
//	};
//
//	@Override
//	public boolean isBlockNormalCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		return pass == 0;
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconHandle = ir.registerIcon("maddtech:RedstonePulser");
//		iconBase = ir.registerIcon("maddtech:RedstonePulserBase");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		return iconBase;
//	};
//	
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int par)
//	{
//		return new TileEntityPulser();
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//		
//		player.openGui(MaddTech.instance, GuiHandler.REDSTONE_PULSER, world, x, y, z);
//		return true;
//	};
//	
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		int emitSide = 2 + ((world.getBlockMetadata(x, y, z) >> 2) & 3);
//		
//		/**
//		 * The "emit side" taken from geometry is incompatible with Minecraft sides - we must swap 2 & 5
//		 * to make it compatible.
//		 */
//		if (emitSide == 2)
//		{
//			emitSide = 5;
//		}
//		else if (emitSide == 5)
//		{
//			emitSide = 2;
//		};
//		
//		if (side == emitSide)
//		{
//			return 15;
//		};
//		
//		return 0;
//	};
//
//	@Override
//	public boolean canProvidePower()
//	{
//		return true;
//	};
};
