/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddSideConfig.java
 * The "side configurator" block, used in designing structures.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.*;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;

public class BlockMaddSideConfig extends Block
{
//	private IIcon iconWhite;
//	private IIcon iconRed;
	
	public BlockMaddSideConfig(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
		//setBlockBounds(0.25f, 0.25f, 0.25f, 0.75f, 0.75f, 0.75f);
	};

//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				int newMeta;
//				if (meta == 0)
//				{
//					newMeta = 8;
//				}
//				else if (meta == (8 | 5))
//				{
//					newMeta = 0;
//				}
//				else
//				{
//					newMeta = meta + 1;
//				};
//				
//				world.setBlockMetadataWithNotify(x, y, z, newMeta, 3);
//				return true;
//			};
//		};
//
//		return false;
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconWhite = ir.registerIcon("maddtech:SideConfigWhite");
//		iconRed = ir.registerIcon("maddtech:SideConfigRed");
//	};
//	
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		if ((side | 8) == metadata)
//		{
//			return iconRed;
//		}
//		else
//		{
//			return iconWhite;
//		}
//	};
//	
//	@Override
//	public boolean isOpaqueCube()
//	{
//		return false;
//	};
};
