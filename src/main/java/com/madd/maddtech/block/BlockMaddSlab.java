/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddSlab.java
 * Base class for 'slab' blocks in MaddTech.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;

import net.minecraft.item.*;
import java.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.*;

public class BlockMaddSlab extends SlabBlock
{
	private Block drops;
	public Block doubleBlock;
	public Block singleBlock;
	public BlockMaddSlab(boolean isDouble, Material mat, Block drops, Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
		this.drops = drops;
		//this.useNeighborBrightness = true;
		
		if (isDouble)
		{
			//setCreativeTab(null);
		}
		else
		{
			//setCreativeTab(MaddTech.tabDeco);
		};
	};

	protected void mtSetup()
	{
	};
	
//	@Override
//	public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
//	{
//		if (drops == null)
//		{
//			return Item.getItemFromBlock(this);
//		};
//		
//		return Item.getItemFromBlock(drops);
//	};
//	
//	@Override
//	public String func_150002_b(int p_150002_1_)
//	{
//		return getUnlocalizedName();
//	};
//
//	public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLiving par5EntityLiving)
//	{
//		if(par1World.getBlock(par2, par3 - 1, par4) == singleBlock)
//		{
//			par1World.setBlock(par2, par3 - 1, par4, doubleBlock);
//		};
//		
//		if(par1World.getBlock(par2, par3 + 1, par4) == singleBlock)
//		{
//			par1World.setBlock(par2, par3 + 1, par4, doubleBlock);
//		};
//	};
};
