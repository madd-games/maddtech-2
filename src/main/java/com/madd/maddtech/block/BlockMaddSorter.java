/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddSorter.java
 */

package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.item.*;
import net.minecraft.inventory.*;
import com.madd.maddtech.api.*;
import java.util.*;

public class BlockMaddSorter extends BlockLightmap
{
	private Block blockOn;

	public BlockMaddSorter(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
		super.mtSetup();
	};

//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
//	{
//		if (world.isBlockIndirectlyGettingPowered(x, y, z))
//		{
//			SearchItem si = new SearchItem(world);
//			si.execute(x, y+1, z, 0);
//
//			ArrayList<IInventory> invs = si.getInventories();
//			if (invs.size() == 0) return;
//
//			HashMap<Item, Integer> counts = new HashMap<Item, Integer>();
//			ArrayList<ItemStack> unsortables = new ArrayList<ItemStack>();
//			ArrayList<ItemStack> finalSorted = new ArrayList<ItemStack>();
//
//			// unpack all the stacks
//			int i, j;
//			for (i=0; i<invs.size(); i++)
//			{
//				IInventory inv = invs.get(i);
//				for (j=0; j<inv.getSizeInventory(); j++)
//				{
//					ItemStack stack = inv.getStackInSlot(j);
//					inv.setInventorySlotContents(j, null);
//
//					if (stack != null)
//					{
//						if (!stack.isStackable() || stack.hasTagCompound() || stack.getHasSubtypes())
//						{
//							unsortables.add(stack);
//						}
//						else
//						{
//							Item item = stack.getItem();
//							if (!counts.containsKey(item))
//							{
//								counts.put(item, stack.stackSize);
//							}
//							else
//							{
//								counts.put(item, counts.get(item)+stack.stackSize);
//							};
//						};
//					};
//				};
//			};
//
//			// OK, now unload all buffered items into stacks.
//			for (Map.Entry<Item, Integer> entry : counts.entrySet())
//			{
//				Item item = entry.getKey();
//				int count = entry.getValue();
//
//				while (count > item.getItemStackLimit())
//				{
//					finalSorted.add(new ItemStack(item, item.getItemStackLimit()));
//					count -= item.getItemStackLimit();
//				};
//
//				if (count > 0)
//				{
//					finalSorted.add(new ItemStack(item, count));
//				};
//			};
//
//			// push the unsortables to the end of the sorted list
//			for (i=0; i<unsortables.size(); i++)
//			{
//				finalSorted.add(unsortables.get(i));
//			};
//
//			// unload into inventories
//			IInventory current = invs.get(0);
//			int currentIndex = 0;
//			int currentSlot = 0;
//
//			for (i=0; i<finalSorted.size(); i++)
//			{
//				ItemStack stack = finalSorted.get(i);
//				if (currentSlot == current.getSizeInventory())
//				{
//					current = invs.get(++currentIndex);
//					currentSlot = 0;
//				};
//				current.setInventorySlotContents(currentSlot++, stack);
//			};
//
//			// mark all inventories dirty
//			for (i=0; i<invs.size(); i++)
//			{
//				invs.get(i).markDirty();
//			};
//		};
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return (side != 1);
//	};
};
