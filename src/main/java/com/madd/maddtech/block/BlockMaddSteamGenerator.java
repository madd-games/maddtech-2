/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddSteamGenerator.java
 * The pump block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;

public class BlockMaddSteamGenerator extends ContainerBlock
{
	//private IIcon iconFront;
//	private IIcon[] iconFront = new IIcon[4];
//	private IIcon iconOutput;
//	private IIcon iconSide;
//	private IIcon iconTop;

	public BlockMaddSteamGenerator(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	public int getOutputSide(IBlockAccess world, int x, int y, int z)
//	{
//		return world.getBlockMetadata(x, y, z);
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		//iconFront = ir.registerIcon("maddtech:SteamGeneratorFront");
//		int i;
//		for (i=0; i<4; i++)
//		{
//			iconFront[i] = ir.registerIcon("maddtech:SteamGeneratorFront" + i);
//		};
//		
//		iconOutput = ir.registerIcon("maddtech:SteamGeneratorBack");
//		iconSide = ir.registerIcon("maddtech:SteamGeneratorSide");
//		iconTop = ir.registerIcon("maddtech:SteamGeneratorTop");
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		if (side == ((metadata&3)+2))
//		{
//			return iconOutput;
//		}
//		else if (side < 2)
//		{
//			return iconTop;
//		}
//		else if (SideUtil.opposite(side) == ((metadata&3)+2))
//		{
//			int index = (metadata >> 2) & 3;
//			return iconFront[index];
//		}
//		else
//		{
//			return iconSide;
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				world.setBlockMetadataWithNotify(x, y, z, ((meta&3)+1) % 4, 3);
//				return true;
//			};
//		};
//
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		player.openGui(MaddTech.instance, GuiHandler.STEAM_GENERATOR, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntitySteamGenerator();
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
