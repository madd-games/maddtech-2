/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddStructDef.java
 * The "structure definition" block. When placed, asks a few questions in the console and outputs
 * a structure definition file to be later used in defining multiblock structures in the code.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.*;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import java.io.*;

public class BlockMaddStructDef extends Block
{
	private int kind;
	
	public BlockMaddStructDef(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup(int kind)
	{
		this.kind = kind;
	};

//	@Override
//	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int gowno)
//	{
//		int dx, dy, dz;
//		
//		try
//		{
//			PrintWriter out = new PrintWriter("struct_output");
//		
//			for (dx=-10; dx<=10; dx++)
//			{
//				for (dy=-10; dy<=10; dy++)
//				{
//					for (dz=-10; dz<=10; dz++)
//					{
//						Block block = world.getBlock(x+dx, y+dy, z+dz);
//						if (kind == 1)
//						{
//							// path definition
//							if (block != null)
//							{
//								if (block instanceof BlockMaddSideConfig)
//								{
//									int meta = world.getBlockMetadata(x+dx, y+dy, z+dz) & 7;
//									out.println("PATH " + dx + "," + dy + "," + dz + ":" + meta);
//								};
//							};
//						}
//						else
//						{
//							// block layout definition
//							if ((dx != 0) || (dy != 0) || (dz != 0))
//							{
//								if (block != null)
//								{
//									String name = block.getUnlocalizedName();
//									out.println("BLOCK " + dx + "," + dy + "," + dz + ":" + name);
//								}
//								else
//								{
//									out.println("BLOCK " + dx + "," + dy + "," + dz + ":air");
//								};
//							};
//						};
//					};
//				};
//			};
//		
//			out.close();
//		}
//		catch (Exception e)
//		{
//			return 1;
//		};
//		
//		return 0;
//	};

};
