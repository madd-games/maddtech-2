/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import java.util.*;

import net.minecraft.item.*;

public class BlockMaddSubstation extends ContainerBlock
{
	public static boolean shouldDropItems = true;
//	public IIcon iconNormal[] = new IIcon[4];
//	public IIcon iconLightmap[] = new IIcon[4];
//	
	public BlockMaddSubstation(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntitySubstation();
//	};
//
//	public void pop(World world, int x, int y, int z)
//	{
//		if (!shouldDropItems) return;
//
//		Random rand = new Random();
//
//		float rx = rand.nextFloat() * 0.8F + 0.1F;
//		float ry = rand.nextFloat() * 0.8F + 0.1F;
//		float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//		EntityItem entityItem = new EntityItem(world,
//			x + rx, y + ry, z + rz,
//			new ItemStack(MaddTech.blockSubstation, 1));
//
//		float factor = 0.05F;
//		entityItem.motionX = rand.nextGaussian() * factor;
//		entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//		entityItem.motionZ = rand.nextGaussian() * factor;
//		world.spawnEntityInWorld(entityItem);
//		world.setBlock(x, y, z, Blocks.air);
//		world.setTileEntity(x, y, z, null);
//	};
//
//	@Override
//	public void breakBlock(World world, int x, int y, int z, Block wtf, int lol)
//	{
//		super.breakBlock(world, x, y, z, wtf, lol);
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//		
//		player.openGui(MaddTech.instance, GuiHandler.SUBSTATION, world, x, y, z);
//		return true;
//	};
//
//	@Override
//	public boolean canPlaceBlockAt(World world, int x, int y, int z)
//	{
//		MachineStruct struct = LibTech.fitStruct(world, x, y, z, LibTech.machSubstation);
//		if (struct == null)
//		{
//			return false;
//		};
//		
//		struct.config(world, x, y, z);
//		return true;
//	};
//
//	// LIGHTMAP
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderLightmapID;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	};
//	
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		int i;
//		for (i=0; i<4; i++)
//		{
//			iconNormal[i] = ir.registerIcon("maddtech:Substation" + i);
//			iconLightmap[i] = ir.registerIcon("maddtech:Substation" + i + "_lm");
//		};
//	};
//	
//	@Override
//	public IIcon getIcon(int side, int meta)
//	{
//		if ((side & 0xF0) == 0)
//		{
//			return iconNormal[meta & 3];
//		}
//		else
//		{
//			return iconLightmap[meta & 3];
//		}
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
