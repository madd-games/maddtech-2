/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddTerminal.java
 * The pump block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;
import java.io.*;
import com.madd.maddtech.api.*;

public class BlockMaddTerminal extends ContainerBlock
{
//	private IIcon iconEnergyIn;
//	private IIcon iconGlownetLink;
//	private IIcon iconSideLM;
//	private IIcon iconEnergyInLM;
//	private IIcon iconGlownetLinkLM;
//	private IIcon[] iconSides;

	public BlockMaddTerminal(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};

//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		iconEnergyIn = ir.registerIcon("maddtech:TerminalEnergyIn");
//		iconGlownetLink = ir.registerIcon("maddtech:TerminalGlownetLink");
//		iconSideLM = ir.registerIcon("maddtech:TerminalSide_lm");
//		iconEnergyInLM = ir.registerIcon("maddtech:TerminalEnergyIn_lm");
//		iconGlownetLinkLM = ir.registerIcon("maddtech:TerminalGlownetLink_lm");
//		iconSides = new IIcon[4];
//		iconSides[0] = ir.registerIcon("maddtech:TerminalSideRed");
//		iconSides[1] = ir.registerIcon("maddtech:TerminalSideGreen");
//		iconSides[2] = ir.registerIcon("maddtech:TerminalSideBlue");
//		iconSides[3] = ir.registerIcon("maddtech:TerminalSideWhite");
//	};
//
//	@Override
//	public IIcon getIcon(int sideAndType, int metadata)
//	{
//		int type = sideAndType & 0xF0;
//		int side = sideAndType & 0x0F;
//		
//		if (type != 0)
//		{
//			// lightmaps
//			switch (side)
//			{
//			case 0:
//				return iconGlownetLinkLM;
//			case 1:
//				return iconEnergyInLM;
//			default:
//				return iconSideLM;
//			}
//		}
//		else
//		{
//			switch (side)
//			{
//			case 0:
//				return iconGlownetLink;
//			case 1:
//				return iconEnergyIn;
//			default:
//				return iconSides[side-2];
//			}
//		}
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				player.openGui(MaddTech.instance, GuiHandler.TERMINAL, world, x, y, z);
//				return true;
//			};
//		};
//
//		return false;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int wtf)
//	{
//		return new TileEntityTerminal();
//	};
//
//	// LIGHTMAP
//	public int getRenderBlockPass()
//	{
//		return 1;
//	};
//
//	@Override
//	public int getRenderType()
//	{
//		return LibTech.renderLightmapID;
//	};
//
//	@Override
//	public boolean canRenderInPass(int pass)
//	{
//		RenderLightmap.renderPass = pass;
//		return true;
//	};
//
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return side > 1;
//	};
//
//	@Override
//	public boolean canProvidePower()
//	{
//		return true;
//	};
//
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		side = SideUtil.opposite(side);
//		
//		TileEntity te = world.getTileEntity(x, y, z);
//		if (te != null)
//		{
//			TileEntityTerminal term = (TileEntityTerminal) te;
//			ArrayList<Coords> peers = term.getPeers(side);
//			
//			int power = 0;
//			int i;
//			
//			for (i=0; i<peers.size(); i++)
//			{
//				Coords coords = peers.get(i);
//				Block block = world.getBlock(coords.x, coords.y, coords.z);
//				
//				int thisOne = block.isProvidingWeakPower(world, coords.x, coords.y, coords.z, coords.side^1);
//				if (thisOne > power)
//				{
//					power = thisOne;
//				};
//			};
//			
//			return power;
//		}
//		else
//		{
//			System.out.println("[MADDTECH] Terminal TileEntity is null during redstone bridging");
//		};
//		
//		return 0;
//	};
//
//	@Override
//	public int isProvidingStrongPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return isProvidingWeakPower(world, x, y, z, side);
//	};
//	
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
//	{
//		if (!world.isRemote)
//		{
//			SearchGlownet sg = new SearchGlownet(world);
//			sg.execute(x, y-1, z, 1, "XXX", "XXX");
//			sg.notifyUpdate();
//		};
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
