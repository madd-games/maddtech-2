/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockMaddUraniumRefinery.java
 * The uranium refinery.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;

import java.util.*;

public class BlockMaddUraniumRefinery extends Block implements ITileEntityProvider
{
	public BlockMaddUraniumRefinery(final Block.Properties properties)
	{
		super(Block.Properties.of(Material.STONE));
	};

	protected void mtSetup()
	{
	};
	
//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		return true;
//	};
//
//	@Override
//	public TileEntity createNewTileEntity(World world, int par)
//	{
//		return new TileEntityUraniumRefinery();
//	};
//
//	public void pop(World world, int x, int y, int z)
//	{
//		Random rand = new Random();
//
//		float rx = rand.nextFloat() * 0.8F + 0.1F;
//		float ry = rand.nextFloat() * 0.8F + 0.1F;
//		float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//		EntityItem entityItem = new EntityItem(world,
//			x + rx, y + ry, z + rz,
//			new ItemStack(MaddTech.blockUraniumRefinery, 1));
//
//		float factor = 0.05F;
//		entityItem.motionX = rand.nextGaussian() * factor;
//		entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//		entityItem.motionZ = rand.nextGaussian() * factor;
//		world.spawnEntityInWorld(entityItem);
//		world.setBlock(x, y, z, Blocks.air);
//		world.setTileEntity(x, y, z, null);
//	};
//
//	@Override
//	public boolean canPlaceBlockAt(World world, int x, int y, int z)
//	{
//		MachineStruct struct = LibTech.fitStruct(world, x, y, z, LibTech.machUraniumRefinery);
//		if (struct == null)
//		{
//			return false;
//		};
//		
//		struct.config(world, x, y, z);
//		return true;
//	}

	@Override
	public TileEntity newBlockEntity(IBlockReader p_196283_1_) {
		// TODO Auto-generated method stub
		return null;
	};
};
