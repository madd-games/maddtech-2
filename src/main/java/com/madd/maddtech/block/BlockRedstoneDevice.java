/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockRedstoneDevice.java
 * Common block definition for redstone devices.
 */
package com.madd.maddtech.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;







import net.minecraft.*;
import net.minecraft.client.renderer.texture.*;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.*;
import java.util.*;

import com.madd.maddtech.RedstoneDevice;
import com.madd.maddtech.api.*;

public class BlockRedstoneDevice extends Block
{
	private RedstoneDevice device;

	public BlockRedstoneDevice(Properties properties)
	{
		super(properties);
	};

	protected void mtSetup(RedstoneDevice dev)
	{
		device = dev;
	};

//	@Override
//	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
//	{
//		int meta = world.getBlockMetadata(x, y, z);
//		int[] types = device.getSideTypes(meta);
//		return types[side] != RedstoneDevice.NONE;
//	};
//
//	@Override
//	public boolean canProvidePower()
//	{
//		return true;
//	};
//
//	private int getPowerFrom(IBlockAccess world, int x, int y, int z, int side)
//	{
//		int bx = x + SideUtil.getSideX(side);
//		int by = y + SideUtil.getSideY(side);
//		int bz = z + SideUtil.getSideZ(side);
//
//		Block block = world.getBlock(bx, by, bz);
//		return block.isProvidingWeakPower(world, bx, by, bz, side);
//	};
//
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		side = SideUtil.opposite(side);
//		int meta = world.getBlockMetadata(x, y, z);
//		int[] types = device.getSideTypes(meta);
//
//		if (types[side] != RedstoneDevice.OUTPUT) return 0;
//		int[] powers = new int[] {0, 0, 0, 0, 0, 0};
//		int i;
//
//		for (i=0; i<6; i++)
//		{
//			if (types[i] == RedstoneDevice.INPUT)
//			{
//				powers[i] = getPowerFrom(world, x, y, z, i);
//			};
//		};
//
//		device.getOutputs(powers, meta);
//		return powers[side];
//	};
//
//	@Override
//	public void onBlockAdded(World world, int x, int y, int z)
//	{
//		updateState(world, x, y, z);
//	};
//
//	@Override
//	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
//	{
//		updateState(world, x, y, z);
//	};
//
//	private void updateState(World world, int x, int y, int z)
//	{
//		int meta = world.getBlockMetadata(x, y, z);
//		int[] types = device.getSideTypes(meta);
//		int[] powers = new int[] {0, 0, 0, 0, 0, 0};
//		int i;
//
//		for (i=0; i<6; i++)
//		{
//			if (types[i] == device.INPUT)
//			{
//				powers[i] = getPowerFrom(world, x, y, z, i);
//			};
//		};
//
//		int newMeta = device.update(powers, meta);
//		if (newMeta == meta)
//		{
//			world.setBlockMetadataWithNotify(x, y, z, 0, 3);
//			world.setBlockMetadataWithNotify(x, y, z, meta, 3);
//		}
//		else
//		{
//			world.setBlockMetadataWithNotify(x, y, z, newMeta, 3);
//		};
//	};
//
//	@Override
//	public void registerBlockIcons(IIconRegister ir)
//	{
//		device.registerIcons(ir);
//	};
//
//	@Override
//	public IIcon getIcon(int side, int metadata)
//	{
//		return device.getIcon(side, metadata);
//	};
//
//	@Override
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int wtf, float w, float t, float f)
//	{
//		ItemStack stack = player.getHeldItem();
//		if (stack != null)
//		{
//			if (stack.getItem() == MaddTech.itemScrewdriver)
//			{
//				int meta = world.getBlockMetadata(x, y, z);
//				int newMeta = device.onScrewdriver(meta);
//				if (newMeta != meta)
//				{
//					world.setBlockMetadataWithNotify(x, y, z, newMeta, 3);
//					return true;
//				};
//			};
//		};
//
//		return false;
//	};
};
