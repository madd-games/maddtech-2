/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * BlockShell.java
 * Base class for shell blocks, which include path-tracing to interface block.
 */
package com.madd.maddtech.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import net.minecraft.world.*;
import net.minecraft.entity.player.*;
import net.minecraft.tileentity.*;
import java.io.*;

import com.madd.maddtech.api.*;

public class BlockShell extends BlockLightmap
{
	private String shellMaterial;
	private String shellFunction;
	
	public BlockShell(Properties properties)
	{
		super(properties);
	};
	
	protected void mtSetup(String mat)
	{
		super.mtSetup();
		shellMaterial = mat;
		shellFunction = "none";
	};
	
	protected void mtSetup(String mat, String func)
	{
		super.mtSetup();
		shellMaterial = mat;
		shellFunction = func;
	};
	
//	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int w, float t, float f, float wtf)
//	{
//		int meta = world.getBlockMetadata(x, y, z);
//		if ((meta & 8) == 0)
//		{
//			return false;
//		};
//		
//		// bit 3 is set, so the bottom 3 bits identify the side to trace to
//		int side = meta & 7;
//		
//		int tx = x + SideUtil.getSideX(side);
//		int ty = y + SideUtil.getSideY(side);
//		int tz = z + SideUtil.getSideZ(side);
//		
//		Block targetBlock = world.getBlock(tx, ty, tz);
//		if (targetBlock == null)
//		{
//			return false;
//		};
//		return targetBlock.onBlockActivated(world, tx, ty, tz, player, w, t, f, wtf);
//	};
//	
//	public String getShellMaterial()
//	{
//		return shellMaterial;
//	};
//	
//	public String getShellFunction()
//	{
//		return shellFunction;
//	};
//	
//	public boolean isShellFluid()
//	{
//		return shellFunction.startsWith("fluid");
//	};
//	
//	public boolean isShellEnergy()
//	{
//		return shellFunction.startsWith("energy");
//	};
//	
//	public boolean isShellRedstone()
//	{
//		return shellFunction.startsWith("redstone");
//	};
//	
//	@Override
//	public boolean canProvidePower()
//	{
//		return shellFunction.equals("redstone_out");
//	};
//	
//	@Override
//	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side)
//	{
//		if (!shellFunction.equals("redstone_out"))
//		{
//			return 0;
//		};
//		
//		// traverse to the interface block
//		while (world.getBlock(x, y, z) instanceof BlockShell)
//		{
//			int meta = world.getBlockMetadata(x, y, z);
//			if (meta == 0)
//			{
//				return 0;
//			};
//			
//			int dir = meta & 7;
//			x += SideUtil.getSideX(dir);
//			y += SideUtil.getSideY(dir);
//			z += SideUtil.getSideZ(dir);
//		};
//		
//		TileEntity te = world.getTileEntity(x, y, z);
//		if (te == null)
//		{
//			return 0;
//		};
//		
//		if (!(te instanceof IRedstoneStateMachine))
//		{
//			return 0;
//		};
//		
//		IRedstoneStateMachine mach = (IRedstoneStateMachine) te;
//		return mach.getRedstoneState();
//	};
};
