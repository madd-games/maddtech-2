/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.container;

import com.madd.maddtech.tileentity.TileEntityAccumulator;
import com.madd.maddtech.tileentity.TileEntityNuclearReactor;

import net.minecraft.entity.player.*;
import net.minecraft.inventory.*;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.*;

public class ContainerNuclearReactor extends Container
{
	protected TileEntityNuclearReactor tileEntity;
	
	public ContainerNuclearReactor(final int windowID, final PlayerInventory playerInventory, final TileEntityAccumulator tileEntity) {
		super(null, windowID);

		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 9; j++)
			{
	    			//addSlotToContainer(new SlotNuclearReactor(tileEntity, (j + i * 9),
	                                    //8 + j * 18, 17 + i * 18));

			};
		};

		//bindPlayerInventory(inventoryPlayer);
	}

	@Override
	public boolean stillValid(PlayerEntity p_75145_1_) {
		// TODO Auto-generated method stub
		return false;
	};

//	@Override
//	public boolean canInteractWith(EntityPlayer player)
//	{
//		return tileEntity.isUseableByPlayer(player);
//	};
//
//	protected void bindPlayerInventory(InventoryPlayer inventoryPlayer)
//	{
//		for (int i = 0; i < 3; i++) {
//			for (int j = 0; j < 9; j++) {
//				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
//	                                   8 + j * 18, 174 + i * 18));
//	        };
//	    };
//
//	    for (int i = 0; i < 9; i++) {
//	    	addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 232));
//	    };
//	};
//	
//	@Override
//	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
//	{
//		ItemStack stack = null;
//		Slot slotObject = (Slot) inventorySlots.get(slot);
//
//		if (slotObject != null && slotObject.getHasStack())
//		{
//			ItemStack stackInSlot = slotObject.getStack();
//			stack = stackInSlot.copy();
//
//			//merges the item into player inventory since its in the tileEntity
//			if (slot < (9*8))
//			{
//				if (!this.mergeItemStack(stackInSlot, 9*8, inventorySlots.size(), true))
//				{
//					return null;
//				}
//			}
//                        //places it into the tileEntity is possible since its in the player inventory
//			else if (!this.mergeItemStack(stackInSlot, 0, 9*8, false))
//			{
//				return null;
//			}
//
//			if (stackInSlot.stackSize == 0)
//			{
//				slotObject.putStack(null);
//                        }
//			else
//			{
//				slotObject.onSlotChanged();
//			}
//		};
//                return stack;
//	};
};
