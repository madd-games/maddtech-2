/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.container;

import net.minecraft.entity.player.*;
import net.minecraft.inventory.*;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityAccumulator;
import com.madd.maddtech.tileentity.TileEntityProc;

public class ContainerProc extends Container
{
	protected TileEntityProc tileEntity;
	
	public ContainerProc(final int windowID, final PlayerInventory playerInventory, final TileEntityAccumulator tileEntity) {
		super(null, windowID);

		//addSlotToContainer(new Slot(tileEntity, 0, 56, 36));
		//addSlotToContainer(new SlotReadOnly(tileEntity, 1, 100, 36));

		int i;
		for (i=0; i<4; i++)
		{
			//addSlotToContainer(new SlotMachineUpgrade(tileEntity, i+2, 152, 9+18*i));
		};

		//bindPlayerInventory(inventoryPlayer);
	}

	@Override
	public boolean stillValid(PlayerEntity p_75145_1_) {
		// TODO Auto-generated method stub
		return false;
	};

//	@Override
//	public boolean canInteractWith(EntityPlayer player)
//	{
//		return tileEntity.isUseableByPlayer(player);
//	};
//
//	protected void bindPlayerInventory(InventoryPlayer inventoryPlayer)
//	{
//		for (int i = 0; i < 3; i++) {
//			for (int j = 0; j < 9; j++) {
//				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
//	                                   8 + j * 18, 84 + i * 18));
//	        };
//	    };
//
//	    for (int i = 0; i < 9; i++) {
//	    	addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
//	    };
//	};
//	
//	private int findSlotForUpgrade(Item item)
//	{
//		int i;
//		for (i=2; i<6; i++)
//		{
//			Slot slot = (Slot) inventorySlots.get(i);
//			if (slot != null)
//			{
//				if (slot.getHasStack())
//				{
//					ItemStack stack = slot.getStack();
//					if (stack.getItem() == item)
//					{
//						return i;
//					};
//				};
//			};
//		};
//		
//		return -1;
//	};
//	
//	@Override
//	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
//	{
//		ItemStack stack = null;
//		Slot slotObject = (Slot) inventorySlots.get(slot);
//		
//		if (slotObject != null && slotObject.getHasStack())
//		{
//			ItemStack stackInSlot = slotObject.getStack();
//			stack = stackInSlot.copy();
//			
//			if (slot < 6)
//			{
//				// the item is currently in the machine so we merge it into the player's inventory.
//				if (!mergeItemStack(stackInSlot, 6, inventorySlots.size(), true))
//				{
//					return null;
//				};
//			}
//			else
//			{
//				// the item is in the player's inventory.
//				if (stack.getItem() instanceof IMachineUpgrade)
//				{
//					// it's an upgrade so we try to push it into the upgrade slots;
//					// if there is already a slot with that upgrade, we push into that one,
//					// else we just push into whatever slot is free
//					int upidx = findSlotForUpgrade(stack.getItem());
//					if (upidx == -1)
//					{
//						if (!mergeItemStack(stackInSlot, 2, 6, false))
//						{
//							return null;
//						};
//					}
//					else
//					{
//						if (!mergeItemStack(stackInSlot, upidx, upidx+1, false))
//						{
//							return null;
//						};
//					};
//				}
//				else
//				{
//					// it is not an upgrade, so we try to push it into the input slot
//					// (never the output)
//					if (!mergeItemStack(stackInSlot, 0, 1, false))
//					{
//						return null;
//					};
//				};
//			};
//			
//			if (stackInSlot.stackSize == 0)
//			{
//				slotObject.putStack(null);
//			}
//			else
//			{
//				slotObject.onSlotChanged();
//			};
//		};
//		
//		return stack;
//	};
};
