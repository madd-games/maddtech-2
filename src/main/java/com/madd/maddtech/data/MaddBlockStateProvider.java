/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.data;

import com.madd.maddtech.MaddTech;
import com.madd.maddtech.ModBlocks;
import com.madd.maddtech.ModRegistry;

import net.minecraft.data.DataGenerator;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.fml.RegistryObject;

public class MaddBlockStateProvider extends BlockStateProvider {
    public MaddBlockStateProvider(DataGenerator gen, ExistingFileHelper exFileHelper) {
        super(gen, MaddTech.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {
    	simpleBlock(ModBlocks.BASALT_BLOCK.get());
    	simpleBlock(ModBlocks.ACACIA_CHISELED.get());
    	simpleBlock(ModBlocks.ACACIA_PANEL.get());
    	//(ModBlocks.ACCUMULATOR.get());
    	simpleBlock(ModBlocks.BASALT_ROCK.get());
    	simpleBlock(ModBlocks.BASALT_BLOCKS.get());
    	simpleBlock(ModBlocks.BASALT_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.BASALT_BRICKS.get());
    	simpleBlock(ModBlocks.BASALT_CHISELED.get());
    	simpleBlock(ModBlocks.BASALT_COBBLESTONE.get());
    	simpleBlock(ModBlocks.BIRCH_CHISELED.get());
    	simpleBlock(ModBlocks.BIRCH_PANEL.get());
    	simpleBlock(ModBlocks.BRASS_BLOCK.get());
    	//simpleBlock(ModBlocks.BRASS_FIBRE_CABLE.get());
    	//simpleBlock(ModBlocks.BREAKER.get());
    	//simpleBlock(ModBlocks.BRICKS_COLORED.get());
    	simpleBlock(ModBlocks.BRONZE_BLOCK.get());
    	//simpleBlock(ModBlocks.BRONZE_FIBRE_CABLE.get());
    	//simpleBlock(ModBlocks.CABINET.get());
    	//simpleBlock(ModBlocks.CENTRIFUGE.get());
    	//simpleBlock(ModBlocks.CHEMICAL_REACTOR.get());
    	//simpleBlock(ModBlocks.COMPRESSOR.get());
    	simpleBlock(ModBlocks.COPPER_BLOCK.get());
    	simpleBlock(ModBlocks.COPPER_ORE.get());
    	//simpleBlock(ModBlocks.CRATE.get());
    	simpleBlock(ModBlocks.DARK_OAK_CHISELED.get());
    	simpleBlock(ModBlocks.DARK_OAK_PANEL.get());
    	simpleBlock(ModBlocks.DENSE_BRICKS.get());
    	simpleBlock(ModBlocks.DENSE_BRICKS_REDSTONE.get());
    	//simpleBlock(ModBlocks.DEWAR.get());
    	simpleBlock(ModBlocks.DOLOMITE_ROCK.get());
    	simpleBlock(ModBlocks.DOLOMITE_BLOCK.get());
    	simpleBlock(ModBlocks.DOLOMITE_BLOCKS.get());
    	simpleBlock(ModBlocks.DOLOMITE_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.DOLOMITE_BRICKS.get());
    	simpleBlock(ModBlocks.DOLOMITE_CHISELED.get());
    	//simpleBlock(ModBlocks.DYNAMO.get());
    	//simpleBlock(ModBlocks.ELECTRIC_FURNACE.get());
    	//simpleBlock(ModBlocks.ENERGY_PROBE.get());
    	//simpleBlock(ModBlocks.ENERGY_SHOP.get());
    	//simpleBlock(ModBlocks.ENG_TABLE.get());
    	//simpleBlock(ModBlocks.FLUID_CABLE.get());
    	//simpleBlock(ModBlocks.FLUID_DISPENSER.get());
    	//simpleBlock(ModBlocks.FLUID_HOLE.get());
    	//simpleBlock(ModBlocks.FLUID_MONITOR.get());
    	//simpleBlock(ModBlocks.FLUID_SHOP.get());
    	//simpleBlock(ModBlocks.FLUID_TANK.get());
    	//simpleBlock(ModBlocks.FLUID_VALVE.get());
    	//simpleBlock(ModBlocks.GAS_EXTRACTOR.get());
    	//simpleBlock(ModBlocks.GLOWNET_CABLE.get());
    	//simpleBlock(ModBlocks.GLOWNET_SWITCH.get());
    	//simpleBlock(ModBlocks.GOLD_ENERGY_CABLE.get());
    	simpleBlock(ModBlocks.GRANITE_ROCK.get());
    	simpleBlock(ModBlocks.GRANITE_BLOCK.get());
    	simpleBlock(ModBlocks.GRANITE_BLOCKS.get());
    	simpleBlock(ModBlocks.GRANITE_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.GRANITE_BRICKS.get());
    	simpleBlock(ModBlocks.GRANITE_CHISELED.get());
    	//simpleBlock(ModBlocks.IRON_CRATE.get());
    	//simpleBlock(ModBlocks.IRON_ENERGY_CABLE.get());
    	//simpleBlock(ModBlocks.ITEM_CABLE.get());
    	//simpleBlock(ModBlocks.ITEM_GATE.get());
    	//simpleBlock(ModBlocks.ITEM_MONITOR.get());
    	//simpleBlock(ModBlocks.ITEM_SORTER.get());
    	//simpleBlock(ModBlocks.ITEM_TRANSPORTER.get());
    	simpleBlock(ModBlocks.JUNGLE_CHISELED.get());
    	simpleBlock(ModBlocks.JUNGLE_PANEL.get());
    	simpleBlock(ModBlocks.KAOLIN_ROCK.get());
    	simpleBlock(ModBlocks.KAOLIN_BLOCK.get());
    	simpleBlock(ModBlocks.KAOLIN_BLOCKS.get());
    	simpleBlock(ModBlocks.KAOLIN_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.KAOLIN_BRICKS.get());
    	simpleBlock(ModBlocks.KAOLIN_CHISELED.get());
    	simpleBlock(ModBlocks.LAMP_BLACK_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_BLACK_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_BLACK_ON.get());
    	//simpleBlock(ModBlocks.LAMP_BLACK_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_BLUE_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_BLUE_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_BLUE_ON.get());
    	//simpleBlock(ModBlocks.LAMP_BLUE_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_BROWN_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_BROWN_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_BROWN_ON.get());
    	//simpleBlock(ModBlocks.LAMP_BROWN_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_CYAN_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_CYAN_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_CYAN_ON.get());
    	//simpleBlock(ModBlocks.LAMP_CYAN_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_GRAY_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_GRAY_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_GRAY_ON.get());
    	//simpleBlock(ModBlocks.LAMP_GRAY_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_GREEN_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_GREEN_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_GREEN_ON.get());
    	//simpleBlock(ModBlocks.LAMP_GREEN_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_LBLUE_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_LBLUE_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_LBLUE_ON.get());
    	//simpleBlock(ModBlocks.LAMP_LBLUE_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_LGRAY_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_LGRAY_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_LGRAY_ON.get());
    	//simpleBlock(ModBlocks.LAMP_LGRAY_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_LIME_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_LIME_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_LIME_ON.get());
    	//simpleBlock(ModBlocks.LAMP_LIME_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_MAGENTA_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_MAGENTA_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_MAGENTA_ON.get());
    	//simpleBlock(ModBlocks.LAMP_MAGENTA_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_ORANGE_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_ORANGE_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_ORANGE_ON.get());
    	//simpleBlock(ModBlocks.LAMP_ORANGE_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_PINK_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_PINK_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_PINK_ON.get());
    	//simpleBlock(ModBlocks.LAMP_PINK_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_PURPLE_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_PURPLE_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_PURPLE_ON.get());
    	//simpleBlock(ModBlocks.LAMP_PURPLE_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_RED_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_RED_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_RED_ON.get());
    	//simpleBlock(ModBlocks.LAMP_RED_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_WHITE_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_WHITE_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_WHITE_ON.get());
    	//simpleBlock(ModBlocks.LAMP_WHITE_ON_INV.get());
    	simpleBlock(ModBlocks.LAMP_YELLOW_OFF.get());
    	//simpleBlock(ModBlocks.LAMP_YELLOW_OFF_INV.get());
    	simpleBlock(ModBlocks.LAMP_YELLOW_ON.get());
    	//simpleBlock(ModBlocks.LAMP_YELLOW_ON_INV.get());
    	simpleBlock(ModBlocks.LASER.get());
    	//simpleBlock(ModBlocks.LEAD_BARREL.get());
    	simpleBlock(ModBlocks.LEAD_BLOCK.get());
    	//simpleBlock(ModBlocks.LEAD_DOOR.get());
    	simpleBlock(ModBlocks.LEAD_ORE.get());
    	simpleBlock(ModBlocks.LEAD_SHELL.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_ENERGY_IN.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_ENERGY_OUT.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_FLUID_IN.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_FLUID_OUT.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_ITEM_IN.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_ITEM_OUT.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_REDSTONE_IN.get());
    	simpleBlock(ModBlocks.LEAD_SHELL_REDSTONE_OUT.get());
    	simpleBlock(ModBlocks.LIGNITE_ORE.get());
    	simpleBlock(ModBlocks.LIMESTONE_ROCK.get());
    	simpleBlock(ModBlocks.LIMESTONE_BLOCK.get());
    	simpleBlock(ModBlocks.LIMESTONE_BLOCKS.get());
    	simpleBlock(ModBlocks.LIMESTONE_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.LIMESTONE_BRICKS.get());
    	simpleBlock(ModBlocks.LIMESTONE_CHISELED.get());
    	//simpleBlock(ModBlocks.MACERATOR.get());
    	simpleBlock(ModBlocks.MARBLE_ROCK.get());
    	simpleBlock(ModBlocks.MARBLE_BLOCK.get());
    	simpleBlock(ModBlocks.MARBLE_BLOCKS.get());
    	simpleBlock(ModBlocks.MARBLE_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.MARBLE_BRICKS.get());
    	simpleBlock(ModBlocks.MARBLE_CHISELED.get());
    	//simpleBlock(ModBlocks.MOTION_SENSOR.get());
    	simpleBlock(ModBlocks.NETHER_BLOCK.get());
    	simpleBlock(ModBlocks.NETHER_BLOCKS.get());
    	simpleBlock(ModBlocks.NETHER_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.NETHER_BRICKS.get());
    	simpleBlock(ModBlocks.NETHER_CHISELED.get());
    	simpleBlock(ModBlocks.NICKEL_BLOCK.get());
    	simpleBlock(ModBlocks.NICKEL_ORE.get());
    	//simpleBlock(ModBlocks.NICKEL_REDSTONE_CABLE.get());
    	//simpleBlock(ModBlocks.NUCLEAR_REACTOR.get());
    	//simpleBlock(ModBlocks.NUKE.get());
    	simpleBlock(ModBlocks.OAK_CHISELED.get());
    	simpleBlock(ModBlocks.OAK_PANEL.get());
    	simpleBlock(ModBlocks.PEAT.get());
    	//simpleBlock(ModBlocks.PLACER.get());
    	//simpleBlock(ModBlocks.POWER_SWITCH.get());
    	//simpleBlock(ModBlocks.PUMP.get());
    	simpleBlock(ModBlocks.RED_ALLOY_BLOCK.get());
    	//simpleBlock(ModBlocks.REDSTONE_BRIDGE.get());
    	//simpleBlock(ModBlocks.REDSTONE_CABLE.get());
    	//simpleBlock(ModBlocks.REDSTONE_EMITTER.get());
    	//simpleBlock(ModBlocks.REDSTONE_LATCH.get());
    	//simpleBlock(ModBlocks.REDSTONE_NOT.get());
    	//simpleBlock(ModBlocks.REDSTONE_PULSER.get());
    	simpleBlock(ModBlocks.RUBY_ORE.get());
    	simpleBlock(ModBlocks.SANDSTONE_BRICKS.get());
    	simpleBlock(ModBlocks.SANDSTONE_BRICKS_MOSSY.get());
    	simpleBlock(ModBlocks.SANDSTONE_HEIROGLYPH.get());
    	simpleBlock(ModBlocks.SANDSTONE_HEIROGLYPH2.get());
    	//simpleBlock(ModBlocks.SHOP.get());
    	simpleBlock(ModBlocks.SILVER_BLOCK.get());
    	simpleBlock(ModBlocks.SILVER_ENERGY_CABLE.get());
    	simpleBlock(ModBlocks.SILVER_ORE.get());
    	simpleBlock(ModBlocks.SOLAR_PANEL.get());
    	simpleBlock(ModBlocks.SPOTLIGHT.get());
    	//simpleBlock(ModBlocks.STEAM_GENERATOR.get());
    	simpleBlock(ModBlocks.SPRUCE_CHISELED.get());
    	simpleBlock(ModBlocks.SPRUCE_PANEL.get());
    	//simpleBlock(ModBlocks.STEEL_BARREL.get());
    	simpleBlock(ModBlocks.STEEL_BLOCK.get());
    	//simpleBlock(ModBlocks.STEEL_DOOR.get());
    	simpleBlock(ModBlocks.STEEL_SHELL.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_ENERGY_IN.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_ENERGY_OUT.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_FLUID_IN.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_FLUID_OUT.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_ITEM_IN.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_ITEM_OUT.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_REDSTONE_IN.get());
    	simpleBlock(ModBlocks.STEEL_SHELL_REDSTONE_OUT.get());
    	simpleBlock(ModBlocks.STONE_BLOCK.get());
    	simpleBlock(ModBlocks.STONE_BLOCKS.get());
    	simpleBlock(ModBlocks.STONE_BRICK_LAVA.get());
    	simpleBlock(ModBlocks.STONE_LAMP.get());
    	simpleBlock(ModBlocks.SUBSTATION.get());
    	simpleBlock(ModBlocks.SULFUR_ORE.get());
    	//simpleBlock(ModBlocks.SUPER_TRAMPOLINE.get());
    	//simpleBlock(ModBlocks.TRAMPOLINE.get());
    	//simpleBlock(ModBlocks.TERMINAL.get());
    	simpleBlock(ModBlocks.TIN_BLOCK.get());
    	simpleBlock(ModBlocks.TIN_ORE.get());
    	//simpleBlock(ModBlocks.TIN_ENERGY_CABLE.get());
    	simpleBlock(ModBlocks.TIN_SHELL.get());
    	simpleBlock(ModBlocks.TIN_SHELL_ENERGY_IN.get());
    	simpleBlock(ModBlocks.TIN_SHELL_ENERGY_OUT.get());
    	simpleBlock(ModBlocks.TIN_SHELL_FLUID_IN.get());
    	simpleBlock(ModBlocks.TIN_SHELL_FLUID_OUT.get());
    	simpleBlock(ModBlocks.TIN_SHELL_ITEM_IN.get());
    	simpleBlock(ModBlocks.TIN_SHELL_ITEM_OUT.get());
    	simpleBlock(ModBlocks.TIN_SHELL_REDSTONE_IN.get());
    	simpleBlock(ModBlocks.TIN_SHELL_REDSTONE_OUT.get());
    	simpleBlock(ModBlocks.URANIUM_BLOCK.get());
    	simpleBlock(ModBlocks.URANIUM_ORE.get());
    	//simpleBlock(ModBlocks.URANIUM_REFINERY.get());
    	simpleBlock(ModBlocks.VOLCANIC_GLASS.get());
    	simpleBlock(ModBlocks.VOLCANIC_SAND.get());
    	//simpleBlock(ModBlocks.WOODEN_BARREL.get());
    	simpleBlock(ModBlocks.XRAY_BOX.get());

    }
}
