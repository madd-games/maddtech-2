/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.data;

import com.madd.maddtech.MaddTech;
import com.madd.maddtech.ModBlocks;
import com.madd.maddtech.ModRegistry;
import com.madd.maddtech.item.ItemAirVent;
import com.madd.maddtech.item.ItemBattery;
import com.madd.maddtech.item.ItemBrassDust;
import com.madd.maddtech.item.ItemBrassIngot;
import com.madd.maddtech.item.ItemBrassNugget;
import com.madd.maddtech.item.ItemBronzeDust;
import com.madd.maddtech.item.ItemBronzeIngot;
import com.madd.maddtech.item.ItemBronzeNugget;
import com.madd.maddtech.item.ItemCapsule;
import com.madd.maddtech.item.ItemCoalDust;
import com.madd.maddtech.item.ItemCoil;
import com.madd.maddtech.item.ItemCopperDust;
import com.madd.maddtech.item.ItemCopperIngot;
import com.madd.maddtech.item.ItemCopperNugget;
import com.madd.maddtech.item.ItemDiamondDust;
import com.madd.maddtech.item.ItemDiamondFragment;
import com.madd.maddtech.item.ItemElectricDrill;
import com.madd.maddtech.item.ItemElectricScrewdriver;
import com.madd.maddtech.item.ItemElectricWelder;
import com.madd.maddtech.item.ItemEmeraldDust;
import com.madd.maddtech.item.ItemEmeraldFragment;
import com.madd.maddtech.item.ItemEnergyCrystal;
import com.madd.maddtech.item.ItemEnergyFiller;
import com.madd.maddtech.item.ItemFan;
import com.madd.maddtech.item.ItemGasWelder;
import com.madd.maddtech.item.ItemGlowdustBlack;
import com.madd.maddtech.item.ItemGlowdustBlue;
import com.madd.maddtech.item.ItemGlowdustBrown;
import com.madd.maddtech.item.ItemGlowdustCyan;
import com.madd.maddtech.item.ItemGlowdustGray;
import com.madd.maddtech.item.ItemGlowdustGreen;
import com.madd.maddtech.item.ItemGlowdustLBlue;
import com.madd.maddtech.item.ItemGlowdustLGray;
import com.madd.maddtech.item.ItemGlowdustLime;
import com.madd.maddtech.item.ItemGlowdustMagenta;
import com.madd.maddtech.item.ItemGlowdustOrange;
import com.madd.maddtech.item.ItemGlowdustPink;
import com.madd.maddtech.item.ItemGlowdustPurple;
import com.madd.maddtech.item.ItemGlowdustRed;
import com.madd.maddtech.item.ItemGlowdustWhite;
import com.madd.maddtech.item.ItemGlowdustYellow;
import com.madd.maddtech.item.ItemGoldDust;
import com.madd.maddtech.item.ItemIronDust;
import com.madd.maddtech.item.ItemLeadDust;
import com.madd.maddtech.item.ItemLeadIngot;
import com.madd.maddtech.item.ItemLeadNugget;
import com.madd.maddtech.item.ItemLeadRod;
import com.madd.maddtech.item.ItemLignite;
import com.madd.maddtech.item.ItemMicroprocessor;
import com.madd.maddtech.item.ItemMinecartCrate;
import com.madd.maddtech.item.ItemMiniRuby;
import com.madd.maddtech.item.ItemNaqCooler;
import com.madd.maddtech.item.ItemNetherDust;
import com.madd.maddtech.item.ItemNickelDust;
import com.madd.maddtech.item.ItemNickelIngot;
import com.madd.maddtech.item.ItemNickelNugget;
import com.madd.maddtech.item.ItemRedAlloyDust;
import com.madd.maddtech.item.ItemRedAlloyIngot;
import com.madd.maddtech.item.ItemRedAlloyNugget;
import com.madd.maddtech.item.ItemRuby;
import com.madd.maddtech.item.ItemSafetySuit;
import com.madd.maddtech.item.ItemScrewdriver;
import com.madd.maddtech.item.ItemSilverDust;
import com.madd.maddtech.item.ItemSilverIngot;
import com.madd.maddtech.item.ItemSilverNugget;
import com.madd.maddtech.item.ItemSteelDust;
import com.madd.maddtech.item.ItemSteelIngot;
import com.madd.maddtech.item.ItemSteelNugget;
import com.madd.maddtech.item.ItemSteelRod;
import com.madd.maddtech.item.ItemSulfur;
import com.madd.maddtech.item.ItemTemplate;
import com.madd.maddtech.item.ItemThermalInsulator;
import com.madd.maddtech.item.ItemTinDust;
import com.madd.maddtech.item.ItemTinIngot;
import com.madd.maddtech.item.ItemTinNugget;
import com.madd.maddtech.item.ItemTurbine;
import com.madd.maddtech.item.ItemUraniumDust;
import com.madd.maddtech.item.ItemUraniumIngot;
import com.madd.maddtech.item.ItemUraniumNugget;

import net.minecraft.data.DataGenerator;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.fml.RegistryObject;

public class MaddItemModelProvider extends ItemModelProvider {

	public MaddItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, MaddTech.MODID, existingFileHelper);
		// TODO Auto-generated constructor stub
	}
	
	

	@Override
	protected void registerModels() {
		withExistingParent("basalt_block", modLoc("block/basalt_block"));
		withExistingParent("acacia_chiseled", modLoc("block/acacia_chiseled"));
		withExistingParent("acacia_panel", modLoc("block/acacia_panel"));
		//withExistingParent("accumulator", modLoc("block/accumulator"));
		withExistingParent("basalt_rock", modLoc("block/basalt_rock"));
		withExistingParent("basalt_blocks", modLoc("block/basalt_blocks"));
		withExistingParent("basalt_brick_lava", modLoc("block/basalt_brick_lava"));
		withExistingParent("basalt_bricks", modLoc("block/basalt_bricks"));
		withExistingParent("basalt_chiseled", modLoc("block/basalt_chiseled"));
		withExistingParent("basalt_cobblestone", modLoc("block/basalt_cobblestone"));
		withExistingParent("birch_chiseled", modLoc("block/birch_chiseled"));
		withExistingParent("birch_panel", modLoc("block/birch_panel"));
		withExistingParent("brass_block", modLoc("block/brass_block"));
		//withExistingParent("brass_fibre_cable", modLoc("block/brass_fibre_cable"));
		//withExistingParent("breaker", modLoc("block/breaker"));
		//withExistingParent("bricks_colored", modLoc("block/bricks_colored"));
		withExistingParent("bronze_block", modLoc("block/bronze_block"));
		//withExistingParent("bronze_fibre_cable", modLoc("block/bronze_fibre_cable"));
		//withExistingParent("cabinet", modLoc("block/cabinet"));
		//withExistingParent("centrifuge", modLoc("block/centrifuge"));
		//withExistingParent("chemical_reactor", modLoc("block/chemical_reactor"));
		//withExistingParent("compressor", modLoc("block/compressor"));
		withExistingParent("copper_block", modLoc("block/copper_block"));
		withExistingParent("copper_ore", modLoc("block/copper_ore"));
		//withExistingParent("crate", modLoc("block/crate"));
		withExistingParent("dark_oak_chiseled", modLoc("block/dark_oak_chiseled"));
		withExistingParent("dark_oak_panel", modLoc("block/dark_oak_panel"));
		withExistingParent("dense_bricks", modLoc("block/dense_bricks"));
		withExistingParent("dense_bricks_redstone", modLoc("block/dense_bricks_redstone"));
		//withExistingParent("dewar", modLoc("block/dewar"));
		withExistingParent("dolomite_rock", modLoc("block/dolomite_rock"));
		withExistingParent("dolomite_block", modLoc("block/dolomite_block"));
		withExistingParent("dolomite_blocks", modLoc("block/dolomite_blocks"));
		withExistingParent("dolomite_brick_lava", modLoc("block/dolomite_brick_lava"));
		withExistingParent("dolomite_bricks", modLoc("block/dolomite_bricks"));
		withExistingParent("dolomite_chiseled", modLoc("block/dolomite_chiseled"));
		//withExistingParent("dynamo", modLoc("block/dynamo"));
		//withExistingParent("electric_furnace", modLoc("block/electric_furnace"));
		//withExistingParent("energy_probe", modLoc("block/energy_probe"));
		//withExistingParent("energy_shop", modLoc("block/energy_shop"));
		//withExistingParent("eng_table", modLoc("block/eng_table"));
		//withExistingParent("fluid_cable", modLoc("block/fluid_cable"));
		//withExistingParent("fluid_dispenser", modLoc("block/fluid_dispenser"));
		//withExistingParent("fluid_hole", modLoc("block/fluid_hole"));
		//withExistingParent("fluid_monitor", modLoc("block/fluid_monitor"));
		//withExistingParent("fluid_shop", modLoc("block/fluid_shop"));
		//withExistingParent("fluid_tank", modLoc("block/fluid_tank"));
		//withExistingParent("fluid_valve", modLoc("block/fluid_valve"));
		//withExistingParent("gas_extractor", modLoc("block/gas_extractor"));
		//withExistingParent("glownet_cable", modLoc("block/glownet_cable"));
		//withExistingParent("glownet_switch", modLoc("block/glownet_switch"));
		//withExistingParent("gold_energy_cable", modLoc("block/gold_energy_cable"));
		withExistingParent("granite_rock", modLoc("block/granite_rock"));
		withExistingParent("granite_block", modLoc("block/granite_block"));
		withExistingParent("granite_blocks", modLoc("block/granite_blocks"));
		withExistingParent("granite_brick_lava", modLoc("block/granite_brick_lava"));
		withExistingParent("granite_bricks", modLoc("block/granite_bricks"));
		withExistingParent("granite_chiseled", modLoc("block/granite_chiseled"));
		//withExistingParent("iron_crate", modLoc("block/iron_crate"));
		//withExistingParent("iron_energy_cable", modLoc("block/iron_energy_cable"));
		//withExistingParent("item_cable", modLoc("block/item_cable"));
		//withExistingParent("item_gate", modLoc("block/item_gate"));
		//withExistingParent("item_monitor", modLoc("block/item_monitor"));
		//withExistingParent("item_sorter", modLoc("block/item_sorter"));
		//withExistingParent("item_transporter", modLoc("block/item_transporter"));
		withExistingParent("jungle_chiseled", modLoc("block/jungle_chiseled"));
		withExistingParent("jungle_panel", modLoc("block/jungle_panel"));
		withExistingParent("kaolin_rock", modLoc("block/kaolin_rock"));
		withExistingParent("kaolin_block", modLoc("block/kaolin_block"));
		withExistingParent("kaolin_blocks", modLoc("block/kaolin_blocks"));
		withExistingParent("kaolin_brick_lava", modLoc("block/kaolin_brick_lava"));
		withExistingParent("kaolin_bricks", modLoc("block/kaolin_bricks"));
		withExistingParent("kaolin_chiseled", modLoc("block/kaolin_chiseled"));
		withExistingParent("lamp_black_off", modLoc("block/lamp_black_off"));
		//withExistingParent("lamp_black_off_inv", modLoc("block/lamp_black_off_inv"));
		withExistingParent("lamp_black_on", modLoc("block/lamp_black_on"));
		//withExistingParent("lamp_black_on_inv", modLoc("block/lamp_black_on_inv"));
		withExistingParent("lamp_blue_off", modLoc("block/lamp_blue_off"));
		//withExistingParent("lamp_blue_off_inv", modLoc("block/lamp_blue_off_inv"));
		withExistingParent("lamp_blue_on", modLoc("block/lamp_blue_on"));
		//withExistingParent("lamp_blue_on_inv", modLoc("block/lamp_blue_on_inv"));
		withExistingParent("lamp_brown_off", modLoc("block/lamp_brown_off"));
		//withExistingParent("lamp_brown_off_inv", modLoc("block/lamp_brown_off_inv"));
		withExistingParent("lamp_brown_on", modLoc("block/lamp_brown_on"));
		//withExistingParent("lamp_brown_on_inv", modLoc("block/lamp_brown_on_inv"));
		withExistingParent("lamp_cyan_off", modLoc("block/lamp_cyan_off"));
		//withExistingParent("lamp_cyan_off_inv", modLoc("block/lamp_cyan_off_inv"));
		withExistingParent("lamp_cyan_on", modLoc("block/lamp_cyan_on"));
		//withExistingParent("lamp_cyan_on_inv", modLoc("block/lamp_cyan_on_inv"));
		withExistingParent("lamp_gray_off", modLoc("block/lamp_gray_off"));
		//withExistingParent("lamp_gray_off_inv", modLoc("block/lamp_gray_off_inv"));
		withExistingParent("lamp_gray_on", modLoc("block/lamp_gray_on"));
		//withExistingParent("lamp_gray_on_inv", modLoc("block/lamp_gray_on_inv"));
		withExistingParent("lamp_green_off", modLoc("block/lamp_green_off"));
		//withExistingParent("lamp_green_off_inv", modLoc("block/lamp_green_off_inv"));
		withExistingParent("lamp_green_on", modLoc("block/lamp_green_on"));
		//withExistingParent("lamp_green_on_inv", modLoc("block/lamp_green_on_inv"));
		withExistingParent("lamp_l_blue_off", modLoc("block/lamp_l_blue_off"));
		//withExistingParent("lamp_l_blue_off_inv", modLoc("block/lamp_l_blue_off_inv"));
		withExistingParent("lamp_l_blue_on", modLoc("block/lamp_l_blue_on"));
		//withExistingParent("lamp_l_blue_on_inv", modLoc("block/lamp_l_blue_on_inv"));
		withExistingParent("lamp_l_gray_off", modLoc("block/lamp_l_gray_off"));
		//withExistingParent("lamp_l_gray_off_inv", modLoc("block/lamp_l_gray_off_inv"));
		withExistingParent("lamp_l_gray_on", modLoc("block/lamp_l_gray_on"));
		//withExistingParent("lamp_l_gray_on_inv", modLoc("block/lamp_l_gray_on_inv"));
		withExistingParent("lamp_lime_off", modLoc("block/lamp_lime_off"));
		//withExistingParent("lamp_lime_off_inv", modLoc("block/lamp_lime_off_inv"));
		withExistingParent("lamp_lime_on", modLoc("block/lamp_lime_on"));
		//withExistingParent("lamp_lime_on_inv", modLoc("block/lamp_lime_on_inv"));
		withExistingParent("lamp_magenta_off", modLoc("block/lamp_magenta_off"));
		//withExistingParent("lamp_magenta_off_inv", modLoc("block/lamp_magenta_off_inv"));
		withExistingParent("lamp_magenta_on", modLoc("block/lamp_magenta_on"));
		//withExistingParent("lamp_magenta_on_inv", modLoc("block/lamp_magenta_on_inv"));
		withExistingParent("lamp_orange_off", modLoc("block/lamp_orange_off"));
		//withExistingParent("lamp_orange_off_inv", modLoc("block/lamp_orange_off_inv"));
		withExistingParent("lamp_orange_on", modLoc("block/lamp_orange_on"));
		//withExistingParent("lamp_orange_on_inv", modLoc("block/lamp_orange_on_inv"));
		withExistingParent("lamp_pink_off", modLoc("block/lamp_pink_off"));
		//withExistingParent("lamp_pink_off_inv", modLoc("block/lamp_pink_off_inv"));
		withExistingParent("lamp_pink_on", modLoc("block/lamp_pink_on"));
		//withExistingParent("lamp_pink_on_inv", modLoc("block/lamp_pink_on_inv"));
		withExistingParent("lamp_purple_off", modLoc("block/lamp_purple_off"));
		//withExistingParent("lamp_purple_off_inv", modLoc("block/lamp_purple_off_inv"));
		withExistingParent("lamp_purple_on", modLoc("block/lamp_purple_on"));
		//withExistingParent("lamp_purple_on_inv", modLoc("block/lamp_purple_on_inv"));
		withExistingParent("lamp_red_off", modLoc("block/lamp_red_off"));
		//withExistingParent("lamp_red_off_inv", modLoc("block/lamp_red_off_inv"));
		withExistingParent("lamp_red_on", modLoc("block/lamp_red_on"));
		//withExistingParent("lamp_red_on_inv", modLoc("block/lamp_red_on_inv"));
		withExistingParent("lamp_white_off", modLoc("block/lamp_white_off"));
		//withExistingParent("lamp_white_off_inv", modLoc("block/lamp_white_off_inv"));
		withExistingParent("lamp_white_on", modLoc("block/lamp_white_on"));
		//withExistingParent("lamp_white_on_inv", modLoc("block/lamp_white_on_inv"));
		withExistingParent("lamp_yellow_off", modLoc("block/lamp_yellow_off"));
		//withExistingParent("lamp_yellow_off_inv", modLoc("block/lamp_yellow_off_inv"));
		withExistingParent("lamp_yellow_on", modLoc("block/lamp_yellow_on"));
		//withExistingParent("lamp_yellow_on_inv", modLoc("block/lamp_yellow_on_inv"));
		withExistingParent("laser", modLoc("block/laser"));
		//withExistingParent("lead_barrel", modLoc("block/lead_barrel"));
		withExistingParent("lead_block", modLoc("block/lead_block"));
		//withExistingParent("lead_door", modLoc("block/lead_door"));
		withExistingParent("lead_ore", modLoc("block/lead_ore"));
		withExistingParent("lead_shell", modLoc("block/lead_shell"));
		withExistingParent("lead_shell_energy_in", modLoc("block/lead_shell_energy_in"));
		withExistingParent("lead_shell_energy_out", modLoc("block/lead_shell_energy_out"));
		withExistingParent("lead_shell_fluid_in", modLoc("block/lead_shell_fluid_in"));
		withExistingParent("lead_shell_fluid_out", modLoc("block/lead_shell_fluid_out"));
		withExistingParent("lead_shell_item_in", modLoc("block/lead_shell_item_in"));
		withExistingParent("lead_shell_item_out", modLoc("block/lead_shell_item_out"));
		withExistingParent("lead_shell_redstone_in", modLoc("block/lead_shell_redstone_in"));
		withExistingParent("lead_shell_redstone_out", modLoc("block/lead_shell_redstone_out"));
		withExistingParent("lignite_ore", modLoc("block/lignite_ore"));
		withExistingParent("limestone_rock", modLoc("block/limestone_rock"));
		withExistingParent("limestone_block", modLoc("block/limestone_block"));
		withExistingParent("limestone_blocks", modLoc("block/limestone_blocks"));
		withExistingParent("limestone_brick_lava", modLoc("block/limestone_brick_lava"));
		withExistingParent("limestone_bricks", modLoc("block/limestone_bricks"));
		withExistingParent("limestone_chiseled", modLoc("block/limestone_chiseled"));
		//withExistingParent("macerator", modLoc("block/macerator"));
		withExistingParent("marble_rock", modLoc("block/marble_rock"));
		withExistingParent("marble_block", modLoc("block/marble_block"));
		withExistingParent("marble_blocks", modLoc("block/marble_blocks"));
		withExistingParent("marble_brick_lava", modLoc("block/marble_brick_lava"));
		withExistingParent("marble_bricks", modLoc("block/marble_bricks"));
		withExistingParent("marble_chiseled", modLoc("block/marble_chiseled"));
		//withExistingParent("motion_sensor", modLoc("block/motion_sensor"));
		withExistingParent("nether_block", modLoc("block/nether_block"));
		withExistingParent("nether_blocks", modLoc("block/nether_blocks"));
		withExistingParent("nether_brick_lava", modLoc("block/nether_brick_lava"));
		withExistingParent("nether_bricks", modLoc("block/nether_bricks"));
		withExistingParent("nether_chiseled", modLoc("block/nether_chiseled"));
		withExistingParent("nickel_block", modLoc("block/nickel_block"));
		withExistingParent("nickel_ore", modLoc("block/nickel_ore"));
		//withExistingParent("nickel_redstone_cable", modLoc("block/nickel_redstone_cable"));
		//withExistingParent("nuclear_reactor", modLoc("block/nuclear_reactor"));
		//withExistingParent("nuke", modLoc("block/nuke"));
		withExistingParent("oak_chiseled", modLoc("block/oak_chiseled"));
		withExistingParent("oak_panel", modLoc("block/oak_panel"));
		withExistingParent("peat", modLoc("block/peat"));
		//withExistingParent("placer", modLoc("block/placer"));
		//withExistingParent("power_switch", modLoc("block/power_switch"));
		//withExistingParent("pump", modLoc("block/pump"));
		withExistingParent("red_alloy_block", modLoc("block/red_alloy_block"));
		//withExistingParent("redstone_bridge", modLoc("block/redstone_bridge"));
		//withExistingParent("redstone_cable", modLoc("block/redstone_cable"));
		//withExistingParent("redstone_emitter", modLoc("block/redstone_emitter"));
		//withExistingParent("redstone_latch", modLoc("block/redstone_latch"));
		//withExistingParent("redstone_not", modLoc("block/redstone_not"));
		//withExistingParent("redstone_pulser", modLoc("block/redstone_pulser"));
		withExistingParent("ruby_ore", modLoc("block/ruby_ore"));
		withExistingParent("sandstone_bricks", modLoc("block/sandstone_bricks"));
		withExistingParent("sandstone_bricks_mossy", modLoc("block/sandstone_bricks_mossy"));
		withExistingParent("sandstone_heiroglyph", modLoc("block/sandstone_heiroglyph"));
		withExistingParent("sandstone_heiroglyph2", modLoc("block/sandstone_heiroglyph2"));
		//withExistingParent("shop", modLoc("block/shop"));
		withExistingParent("silver_block", modLoc("block/silver_block"));
		//withExistingParent("silver_energy_cable", modLoc("block/silver_energy_cable"));
		withExistingParent("silver_ore", modLoc("block/silver_ore"));
		withExistingParent("solar_panel", modLoc("block/solar_panel"));
		withExistingParent("spotlight", modLoc("block/spotlight"));
		//withExistingParent("steam_generator", modLoc("block/steam_generator"));
		withExistingParent("spruce_chiseled", modLoc("block/spruce_chiseled"));
		withExistingParent("spruce_panel", modLoc("block/spruce_panel"));
		//withExistingParent("steel_barrel", modLoc("block/steel_barrel"));
		withExistingParent("steel_block", modLoc("block/steel_block"));
		//withExistingParent("steel_door", modLoc("block/steel_door"));
		withExistingParent("steel_shell", modLoc("block/steel_shell"));
		withExistingParent("steel_shell_energy_in", modLoc("block/steel_shell_energy_in"));
		withExistingParent("steel_shell_energy_out", modLoc("block/steel_shell_energy_out"));
		withExistingParent("steel_shell_fluid_in", modLoc("block/steel_shell_fluid_in"));
		withExistingParent("steel_shell_fluid_out", modLoc("block/steel_shell_fluid_out"));
		withExistingParent("steel_shell_item_in", modLoc("block/steel_shell_item_in"));
		withExistingParent("steel_shell_item_out", modLoc("block/steel_shell_item_out"));
		withExistingParent("steel_shell_redstone_in", modLoc("block/steel_shell_redstone_in"));
		withExistingParent("steel_shell_redstone_out", modLoc("block/steel_shell_redstone_out"));
		withExistingParent("stone_block", modLoc("block/stone_block"));
		withExistingParent("stone_blocks", modLoc("block/stone_blocks"));
		withExistingParent("stone_brick_lava", modLoc("block/stone_brick_lava"));
		withExistingParent("stone_lamp", modLoc("block/stone_lamp"));
		//withExistingParent("substation", modLoc("block/substation"));
		withExistingParent("sulfur_ore", modLoc("block/sulfur_ore"));
		//withExistingParent("super_trampoline", modLoc("block/super_trampoline"));
		//withExistingParent("trampoline", modLoc("block/trampoline"));
		//withExistingParent("terminal", modLoc("block/terminal"));
		withExistingParent("tin_block", modLoc("block/tin_block"));
		withExistingParent("tin_ore", modLoc("block/tin_ore"));
		//withExistingParent("tin_energy_cable", modLoc("block/tin_energy_cable"));
		withExistingParent("tin_shell", modLoc("block/tin_shell"));
		withExistingParent("tin_shell_energy_in", modLoc("block/tin_shell_energy_in"));
		withExistingParent("tin_shell_energy_out", modLoc("block/tin_shell_energy_out"));
		withExistingParent("tin_shell_fluid_in", modLoc("block/tin_shell_fluid_in"));
		withExistingParent("tin_shell_fluid_out", modLoc("block/tin_shell_fluid_out"));
		withExistingParent("tin_shell_item_in", modLoc("block/tin_shell_item_in"));
		withExistingParent("tin_shell_item_out", modLoc("block/tin_shell_item_out"));
		withExistingParent("tin_shell_redstone_in", modLoc("block/tin_shell_redstone_in"));
		withExistingParent("tin_shell_redstone_out", modLoc("block/tin_shell_redstone_out"));
		withExistingParent("uranium_block", modLoc("block/uranium_block"));
		withExistingParent("uranium_ore", modLoc("block/uranium_ore"));
		//withExistingParent("uranium_refinery", modLoc("block/uranium_refinery"));
		withExistingParent("volcanic_glass", modLoc("block/volcanic_glass"));
		withExistingParent("volcanic_sand", modLoc("block/volcanic_sand"));
		//withExistingParent("wooden_barrel", modLoc("block/wooden_barrel"));
		withExistingParent("xray_box", modLoc("block/xray_box"));
		
		ModelFile itemGenerated = getExistingFile(mcLoc("item/generated"));
		
		//builder(itemGenerated, "basalt_block");
		builder(itemGenerated, "air_vent");
		builder(itemGenerated, "battery");
		builder(itemGenerated, "brass_dust");
		builder(itemGenerated, "brass_ingot");
		builder(itemGenerated, "brass_nugget");
		builder(itemGenerated, "bronze_dust");
		builder(itemGenerated, "bronze_ingot");
		builder(itemGenerated, "bronze_nugget");
		builder(itemGenerated, "capsule");
		builder(itemGenerated, "coal_dust");
		builder(itemGenerated, "coil");
		builder(itemGenerated, "copper_dust");
		builder(itemGenerated, "copper_ingot");
		builder(itemGenerated, "copper_nugget");
		builder(itemGenerated, "diamond_dust");
		builder(itemGenerated, "diamond_fragment");
		builder(itemGenerated, "electric_drill");
		builder(itemGenerated, "electric_screwdriver");
		builder(itemGenerated, "electric_welder");
		builder(itemGenerated, "emerald_dust");
		builder(itemGenerated, "emerald_fragment");
		builder(itemGenerated, "energy_crystal");
		builder(itemGenerated, "energy_filler");
		//builder(itemGenerated, "fan");
		builder(itemGenerated, "gas_welder");
		builder(itemGenerated, "glowdust_black");
		builder(itemGenerated, "glowdust_blue");
		builder(itemGenerated, "glowdust_brown");
		builder(itemGenerated, "glowdust_cyan");
		builder(itemGenerated, "glowdust_gray");
		builder(itemGenerated, "glowdust_green");
		builder(itemGenerated, "glowdust_l_blue");
		builder(itemGenerated, "glowdust_l_gray");
		builder(itemGenerated, "glowdust_lime");
		builder(itemGenerated, "glowdust_magenta");
		builder(itemGenerated, "glowdust_orange");
		builder(itemGenerated, "glowdust_pink");
		builder(itemGenerated, "glowdust_purple");
		builder(itemGenerated, "glowdust_red");
		builder(itemGenerated, "glowdust_white");
		builder(itemGenerated, "glowdust_yellow");
		builder(itemGenerated, "gold_dust");
		builder(itemGenerated, "iron_dust");
		builder(itemGenerated, "lead_dust");
		builder(itemGenerated, "lead_ingot");
		builder(itemGenerated, "lead_nugget");
		builder(itemGenerated, "lead_rod");
		builder(itemGenerated, "lignite");
		builder(itemGenerated, "microprocessor");
		//builder(itemGenerated, "minecart_crate");
		builder(itemGenerated, "mini_ruby");
		builder(itemGenerated, "naq_cooler");
		builder(itemGenerated, "nether_dust");
		builder(itemGenerated, "nickel_dust");
		builder(itemGenerated, "nickel_ingot");
		builder(itemGenerated, "nickel_nugget");
		builder(itemGenerated, "red_alloy_dust");
		builder(itemGenerated, "red_alloy_ingot");
		builder(itemGenerated, "red_alloy_nugget");
		builder(itemGenerated, "ruby");
		builder(itemGenerated, "safety_suit");
		builder(itemGenerated, "screwdriver");
		builder(itemGenerated, "silver_dust");
		builder(itemGenerated, "silver_ingot");
		builder(itemGenerated, "silver_nugget");
		builder(itemGenerated, "steel_dust");
		builder(itemGenerated, "steel_ingot");
		builder(itemGenerated, "steel_nugget");
		builder(itemGenerated, "steel_rod");
		builder(itemGenerated, "sulfur");
		builder(itemGenerated, "template");
		builder(itemGenerated, "thermal_insulator");
		builder(itemGenerated, "tin_dust");
		builder(itemGenerated, "tin_ingot");
		builder(itemGenerated, "tin_nugget");
		builder(itemGenerated, "turbine");
		builder(itemGenerated, "uranium_dust");
		builder(itemGenerated, "uranium_ingot");
		builder(itemGenerated, "uranium_nugget");
		
	}
	
	private ItemModelBuilder builder(ModelFile itemGenerated, String name)
	{
		return getBuilder(name).parent(itemGenerated).texture("layer0", "item/" + name);
		
	}
	
	
	
}
