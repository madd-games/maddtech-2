/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.entity;



import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraft.entity.item.*;
import net.minecraft.block.*;


public class EntityNukePrimed extends EntityTNTPrimed
{
	public EntityNukePrimed(World world, double x, double y, double z)
	{
		super(world, x, y, z);
		//this.fuse = 200;			// nukes explode after 10 seconds.
	};

//	public EntityNukePrimed(World world)
//	{
//		super(world);
//	};

//	public void onUpdate()
//	{
//		this.prevPosX = this.posX;
//		this.prevPosY = this.posY;
//		this.prevPosZ = this.posZ;
//		this.motionY -= 0.03999999910593033D;
//		this.moveEntity(this.motionX, this.motionY, this.motionZ);
//		this.motionX *= 0.9800000190734863D;
//		this.motionY *= 0.9800000190734863D;
//		this.motionZ *= 0.9800000190734863D;
//
//		if (this.onGround)
//		{
//			this.motionX *= 0.699999988079071D;
//			this.motionZ *= 0.699999988079071D;
//			this.motionY *= -0.5D;
//		}
//
//		if (this.fuse-- <= 0)
//		{
//			this.setDead();
//
//			if (!this.worldObj.isRemote)
//			{
//				this.nuke();
//			}
//		}
//		else
//		{
//			this.worldObj.spawnParticle("smoke", this.posX, this.posY + 0.5D, this.posZ, 0.0D, 0.0D, 0.0D);
//		}
//	}
//
//	private void nuke()
//	{
//		// explode with the strength of a just-overheated nuclear reactor, unless there is uranium around, in which case
//		// it decays too.
//		float f = 50.0F;
//
//		int x1 = (int) (this.posX - 5.0);
//		int x2 = x1 + 10;
//		int y1 = (int) (this.posY - 5.0);
//		int y2 = y1 + 10;
//		int z1 = (int) (this.posZ - 5.0);
//		int z2 = z1 + 10;
//
//		int x, y, z;
//		for (x=x1; x<=x2; x++)
//		{
//			for (y=y1; y<=y2; y++)
//			{
//				for (z=z1; z<=z2; z++)
//				{
//					Block block = worldObj.getBlock(x, y, z);
//					if (block == MaddTech.blockUraniumOre)
//					{
//						f += 20.0F;
//					}
//					else if (block == MaddTech.blockUraniumBlock)
//					{
//						f += (50.0F * 4.5F);
//					}
//					else if (block == MaddTech.blockNuke)
//					{
//						f += 50.0F;
//					};
//				};
//			};
//		};
//
//		if (f > 200.0F)
//		{
//			f = 200.0F;
//		};
//
//		//this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, f, true);
//		NuclearExplosion explosion = new NuclearExplosion(worldObj, this, posX, posY, posZ, f);
//		explosion.isFlaming = false;
//		explosion.isSmoking = true;
//		explosion.doExplosionA();
//		explosion.doExplosionB(true);
//		explosion.doExplosionNuclear(f/50.0);
//	};
};
