/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.fluid;

import com.madd.maddtech.api.*;
import net.minecraftforge.fluids.*;
import net.minecraftforge.common.util.*;

/**
 * This class is used to implement a MaddTech IFluidMachine on top of a Forge IFluidHandler
 * to enable some inter-API translation. The MaddTech interface is superior anyway though.
 */
public class FluidTranslator implements IFluidMachine
{
	//private IFluidHandler handler;
	
	public FluidTranslator()
	{
		//this.handler = handler;
	};
	
	@Override
	public boolean canConnectFluid(int side)
	{
		// can we tell any better?
		return true;
	};
	
//	@Override
//	public String getFluidType(int side)
//	{
		// simulate the draining of a fluid and attempt to map said fluid
		// into a MaddTech one. If not possible, we return "maddtech:empty"
		// and make it impossible to pull any fluid.
		
//		FluidStack stack = handler.drain(ForgeDirection.getOrientation(side), 1, false);
//		if (stack == null)
//		{
//			return "maddtech:empty";
//		};
//		
//		int id = stack.getFluid().getID();
//		return MaddRegistry.fluids.fromForgeID(id);
//	};
	
	@Override
	public int pullFluid(String id, int max, int side)
	{
		if (!getFluidType(side).equals(id))
		{
			return 0;
		};
		
		if (id.equals("maddtech:empty"))
		{
			// just in case
			return 0;
		};
		
		//FluidStack stack = handler.drain(ForgeDirection.getOrientation(side), max, true);
		return 1;
	};
	
	@Override
	public int pushFluid(String id, int max, int side)
	{
		int forgeID = MaddRegistry.fluids.toForgeID(id);
		if (forgeID == -1)
		{
			// not mapped, do not push
			return 0;
		};
		
		//FluidStack stack = new FluidStack(forgeID, max);
		return 1;
	}

	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	};
};
