/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.fluid;

import com.madd.maddtech.MaddTech;
import com.madd.maddtech.api.*;
import net.minecraft.world.*;

import net.minecraft.block.*;
import net.minecraft.item.*;
import java.util.*;

public class FluidUranium extends Fluid
{
	public FluidUranium()
	{
		super("maddtech:refu", "Uranium", BarrelType.LEAD, CapsuleType.LEAD);
	};

	@Override
	public void updateNR(IFluidMachine capsule, INuclearReactor reactor)
	{
		Random rand = new Random();
		
		if (reactor.getRedstonePower() > 5)
		{
			int pulled = capsule.pullFluid("maddtech:refu", 1, 0);
			int dt = pulled * 505;
			reactor.heatUp(dt);
			reactor.generateEnergy(dt*100/505);

			if ((rand.nextInt(100) < 1) && (pulled > 0))
			{
				reactor.pushStack(new ItemStack(MaddTech.itemLeadNugget, 1));
			};

			int leadRods = reactor.countItems(new ItemStack(MaddTech.itemLeadRod, 1));
			int plutonium = pulled * leadRods * 5;
			if (plutonium > 50)
			{
				plutonium = 50;
			};
			if (plutonium > 0)
			{
				reactor.pushFluidToCables("maddtech:plutonium", rand.nextInt(plutonium));
			};
		};
	};
};
