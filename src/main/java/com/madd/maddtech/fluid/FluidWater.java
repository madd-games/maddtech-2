/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.fluid;

import com.madd.maddtech.api.*;
import net.minecraft.world.*;

import net.minecraft.item.*;

public class FluidWater extends Fluid
{
	public FluidWater()
	{
		super("maddtech:water", "Water", BarrelType.WOODEN, CapsuleType.TIN);
	};

	@Override
	public int onDispense(World world, int x, int y, int z, int amount)
	{
//		if ((world.getBlock(x, y, z) == Blocks.air) && (amount >= 1000))
//		{
//			world.setBlock(x, y, z, Blocks.flowing_water);
//			return amount-1000;
//		}
//		else
//		{
//			return amount;
//		}
		return 1;
	};
	
	@Override
	public void updateCR(IFluidMachine capsule, IChemicalReactor reactor)
	{
		if (reactor.getRedstonePower() > 0)
		{
			int dt = capsule.pullFluid("maddtech:water", 5, 0) / 2;
			if (reactor.getTemperature() > 250)
			{
				reactor.heatUp(-dt);
			};

			// Clay production
			//ItemStack dirtIn = new ItemStack(Blocks.dirt, 1);
			//ItemStack gravelIn = new ItemStack(Blocks.gravel, 1);
//			if (reactor.canRemoveStack(dirtIn) && reactor.canRemoveStack(gravelIn))
//			{
//				if (capsule.pullFluid("maddtech:water", 1000, 0) == 1000)
//				{
//					reactor.removeStack(dirtIn);
//					reactor.removeStack(gravelIn);
//					reactor.pushStack(new ItemStack(Blocks.clay, 1));
//				};
//			};
			
			// Freezing
//			if (reactor.getTemperature() <= 200)
//			{
//				if (capsule.pullFluid("maddtech:water", 1000, 0) == 1000)
//				{
//					reactor.pushStack(new ItemStack(Blocks.ice, 1));
//				};
//			};
		};
	};
	
	@Override
	public void updateNR(IFluidMachine capsule, INuclearReactor reactor)
	{
		if (reactor.getRedstonePower() > 0)
		{
			int dt = capsule.pullFluid("maddtech:water", 5, 0) / 2;
			if (reactor.getTemperature() > 250)
			{
				reactor.heatUp(-dt);
			};
		};
	};
};
