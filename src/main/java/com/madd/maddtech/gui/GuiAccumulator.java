/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityAccumulator;

import net.minecraft.tileentity.*;

public class GuiAccumulator
{
	private static ResourceLocation texBackground = null;
	private TileEntityAccumulator tileEntity;

	public GuiAccumulator(PlayerInventory invPlayer, TileEntityAccumulator tileEntity)
	{
		super();
		//ySize = 256;
		this.tileEntity = tileEntity;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/accumulator.png");
		};
	};

//	private void reloadEntity()
//	{
//		// reload entity if needed
//		TileEntity ent = tileEntity.getWorld().getTileEntity(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
//		if (ent != null)
//		{
//			if (ent instanceof TileEntityAccumulator)
//			{
//				tileEntity = (TileEntityAccumulator) ent;
//			};
//		};
//	};
//	
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		reloadEntity();
//		
//		int energy = tileEntity.getEnergy();
//		int powerIn = tileEntity.getPowerIn();
//		int powerOut = tileEntity.getPowerOut();
//
//		fontRendererObj.drawString("Accumulator", 8, 6, 0x404040);
//		fontRendererObj.drawString(energy + "/30000 J", 8, 14, 0x404040);
//		fontRendererObj.drawString("IN: " + powerIn + " J/t", 8, 22, 0x404040);
//		fontRendererObj.drawString("OUT: " + powerOut + " J/t", 8, 30, 0x404040);
//
//		fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 0x404040);
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		reloadEntity();
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//
//		int height = tileEntity.getEnergy() * 99 / 30000;
//		this.drawTexturedModalRect(x+78, y+150-height, 178, 101-height, 20, height);
//	};
};
