/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityFluidShop;

import net.minecraft.client.gui.*;

public class GuiFluidShop
{
	private static ResourceLocation texBackground = null;
	private TileEntityFluidShop shop;
//	private GuiTextField txtFluidPerRuby;
//	private GuiButton btnFluidID;
	
	public GuiFluidShop(PlayerInventory invPlayer, TileEntityFluidShop tileEntity)
	{
		super();
		shop = tileEntity;
		//ySize = 246;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/FluidShop.png");
		};
	};
	
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		fontRendererObj.drawString("My fluid shop", 8, 6, 0x404040);
//		fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 0x404040);
//
//		fontRendererObj.drawString("Fluid units per mini ruby:", 8, 21, 0x404040);
//		txtFluidPerRuby.drawTextBox();
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//	};
//
//	private String getFluidName(String fluidID)
//	{
//		Fluid fluid = MaddRegistry.fluids.get(fluidID);
//		if (fluid == null) return "???";
//		return fluid.getName();
//	};
//	
//	@Override
//	public void initGui()
//	{
//		super.initGui();
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//
//		txtFluidPerRuby = new GuiTextField(fontRendererObj, 20, 41, 100, 20);
//		txtFluidPerRuby.setFocused(true);
//		txtFluidPerRuby.setCanLoseFocus(false);
//		txtFluidPerRuby.setText("" + shop.getFluidPerRuby());
//		
//		btnFluidID = new GuiButton(0, x+20, y+62, 100, 20, getFluidName(shop.getFluidID()));
//		buttonList.add(btnFluidID);
//	};
//
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		FluidRegistry freg = (FluidRegistry) MaddRegistry.fluids;
//		String[] names = freg.getFluids();
//		
//		int i;
//		for (i=0; i<names.length; i++)
//		{
//			if (names[i].equals(shop.getFluidID()))
//			{
//				break;
//			};
//		};
//		
//		i = (i+1) % names.length;
//		String newFluidID = names[i];
//		shop.setProps(newFluidID, shop.getFluidPerRuby());
//		btnFluidID.displayString = getFluidName(newFluidID);
//	};
//
//	@Override
//	protected void keyTyped(char p_73869_1_, int p_73869_2_)
//	{
//		if (p_73869_2_ == 1 || p_73869_2_ == this.mc.gameSettings.keyBindInventory.getKeyCode())
//		{
//			this.mc.thePlayer.closeScreen();
//		}
//		else
//		{
//			txtFluidPerRuby.textboxKeyTyped(p_73869_1_, p_73869_2_);
//			try
//			{
//				int fluidAmount = Integer.parseInt(txtFluidPerRuby.getText());
//				if (fluidAmount >= 0)
//				{
//					shop.setProps(shop.getFluidID(), fluidAmount);
//				};
//			}
//			catch (NumberFormatException e)
//			{
//				// NOP
//			};
//		};
//	};
};
