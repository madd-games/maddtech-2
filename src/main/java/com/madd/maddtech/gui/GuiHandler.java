/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler
{
	public static final int CRATE = 0;
	public static final int CHEMICAL_REACTOR = 1;
	public static final int BARREL = 2;
	public static final int ITEM_TRANSPORTER = 3;
	public static final int ACCUMULATOR = 4;
	public static final int PROC = 5;
	public static final int NUCLEAR_REACTOR = 6;
	public static final int STEAM_GENERATOR = 7;
	public static final int ENG_TABLE = 8;
	public static final int COMPRESSOR = 9;
	public static final int BLOCK_PLACER = 10;
	public static final int REDSTONE_PULSER = 11;
	public static final int SHOP_OWNER = 12;
	public static final int SHOP_CLIENT = 13;
	public static final int TERMINAL = 14;
	public static final int PUMP = 15;
	public static final int ITEM_MONITOR = 16;
	public static final int REDSTONE_LATCH = 17;
	public static final int FLUID_MONITOR = 18;
	public static final int ELECTRIC_ENGINE = 19;		// BuildCraft integrator only.
	public static final int SUBSTATION = 20;
	public static final int ENERGY_SHOP = 21;
	public static final int FLUID_SHOP = 22;
	
//	@Override
//	public Object getServerGuiElement(int id, PlayerEntity player, World world, int x, int y, int z)
//	{
//		if (id == CRATE)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			return new ContainerCrate(player.inventory, ent);
//		}
//		else if (id == CHEMICAL_REACTOR)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityChemicalReactor)
//			{
//				return new ContainerChemicalReactor(player.inventory, (TileEntityChemicalReactor) ent);
//			};
//		}
//		else if (id == BARREL)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityBarrel)
//			{
//				return new ContainerBarrel(player.inventory, (TileEntityBarrel) ent);
//			};
//		}
//		else if (id == ITEM_TRANSPORTER)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityItemTransporter)
//			{
//				return new ContainerItemTransporter(player.inventory, (TileEntityItemTransporter) ent);
//			};
//		}
//		else if (id == ACCUMULATOR)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityAccumulator)
//			{
//				return new ContainerAccumulator(player.inventory, (TileEntityAccumulator) ent);
//			};
//		}
//		else if (id == PROC)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityProc)
//			{
//				return new ContainerProc(player.inventory, (TileEntityProc) ent);
//			};
//		}
//		else if (id == NUCLEAR_REACTOR)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityNuclearReactor)
//			{
//				return new ContainerNuclearReactor(player.inventory, (TileEntityNuclearReactor) ent);
//			};
//		}
//		else if (id == STEAM_GENERATOR)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntitySteamGenerator)
//			{
//				return new ContainerSteamGenerator(player.inventory, (TileEntitySteamGenerator) ent);
//			};
//		}
//		else if (id == ENG_TABLE)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityEngTable)
//			{
//				return new ContainerEngTable(player.inventory, (TileEntityEngTable) ent);
//			};
//		}
//		else if (id == COMPRESSOR)
//		{
//			return new ContainerCompressor(player.inventory);
//		}
//		else if (id == BLOCK_PLACER)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityPlacer)
//			{
//				return new ContainerPlacer(player.inventory, (TileEntityPlacer) ent);
//			};
//		}
//		else if (id == REDSTONE_PULSER)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityPulser)
//			{
//				return new ContainerPulser(player.inventory);
//			};
//		}
//		else if (id == SHOP_OWNER)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityShop)
//			{
//				return new ContainerShopOwner(player.inventory, (TileEntityShop) ent);
//			};
//		}
//		else if (id == SHOP_CLIENT)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityShop)
//			{
//				return new ContainerShopClient(player.inventory, (TileEntityShop) ent);
//			};
//		}
//		else if (id == TERMINAL)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityTerminal)
//			{
//				return new ContainerTerminal(player.inventory);
//			};
//		}
//		/* no server-side for PUMP */
//		else if (id == ITEM_MONITOR)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityItemMonitor)
//			{
//				return new ContainerItemMonitor(player.inventory, (TileEntityItemMonitor)ent);
//			};
//		}
//		else if (id == REDSTONE_LATCH)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityRedstoneLatch)
//			{
//				return new ContainerRedstoneLatch(player.inventory);
//			};
//		}
//		else if (id == FLUID_MONITOR)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityFluidMonitor)
//			{
//				return new ContainerFluidMonitor(player.inventory, (TileEntityFluidMonitor)ent);
//			};
//		}
//		else if (id == SUBSTATION)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntitySubstation)
//			{
//				return new ContainerSubstation(player.inventory, (TileEntitySubstation)ent);
//			};
//		}
//		else if (id == ENERGY_SHOP)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityEnergyShop)
//			{
//				return new ContainerEnergyShop(player.inventory, (TileEntityEnergyShop)ent);
//			};
//		}
//		else if (id == FLUID_SHOP)
//		{
//			TileEntity ent = world.getTileEntity(x, y, z);
//			if (ent instanceof TileEntityFluidShop)
//			{
//				return new ContainerFluidShop(player.inventory, (TileEntityFluidShop)ent);
//			};
//		};
//		
//		return Integrator.getServerGuiElement(id, player, world, x, y, z);
//	};
//
//	@Override
//	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
//	{
//		if (id == CRATE)
//		{
//			TileEntity tileEntity = world.getTileEntity(x, y, z);
//			return new GuiCrate(player.inventory, tileEntity);
//		}
//		else if (id == CHEMICAL_REACTOR)
//		{
//			TileEntity tileEntity = world.getTileEntity(x, y, z);
//			if (tileEntity instanceof TileEntityChemicalReactor)
//			{
//				return new GuiChemicalReactor(player.inventory, (TileEntityChemicalReactor) tileEntity);
//			};
//		}
//		else if (id == BARREL)
//		{
//			TileEntity tileEntity = world.getTileEntity(x, y, z);
//			if (tileEntity instanceof TileEntityBarrel)
//			{
//				return new GuiBarrel(player.inventory, (TileEntityBarrel) tileEntity);
//			};
//		}
//		else if (id == ITEM_TRANSPORTER)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityItemTransporter)
//			{
//				return new GuiItemTransporter(player.inventory, (TileEntityItemTransporter) te);
//			};
//		}
//		else if (id == ACCUMULATOR)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityAccumulator)
//			{
//				return new GuiAccumulator(player.inventory, (TileEntityAccumulator) te);
//			};
//		}
//		else if (id == PROC)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityProc)
//			{
//				return new GuiProc(player.inventory, (TileEntityProc) te);
//			};
//		}
//		else if (id == NUCLEAR_REACTOR)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityNuclearReactor)
//			{
//				return new GuiNuclearReactor(player.inventory, (TileEntityNuclearReactor) te);
//			};
//		}
//		else if (id == STEAM_GENERATOR)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntitySteamGenerator)
//			{
//				return new GuiSteamGenerator(player.inventory, (TileEntitySteamGenerator) te);
//			};
//		}
//		else if (id == ENG_TABLE)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityEngTable)
//			{
//				return new GuiEngTable(player.inventory, (TileEntityEngTable) te);
//			};
//		}
//		else if (id == COMPRESSOR)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityCompressor)
//			{
//				return new GuiCompressor(player.inventory, (TileEntityCompressor) te);
//			};
//		}
//		else if (id == BLOCK_PLACER)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityPlacer)
//			{
//				return new GuiPlacer(player.inventory, (TileEntityPlacer) te);
//			};
//		}
//		else if (id == REDSTONE_PULSER)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityPulser)
//			{
//				return new GuiPulser(player.inventory, (TileEntityPulser) te);
//			};
//		}
//		else if (id == SHOP_OWNER)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityShop)
//			{
//				return new GuiShopOwner(player.inventory, (TileEntityShop) te);
//			};
//		}
//		else if (id == SHOP_CLIENT)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityShop)
//			{
//				return new GuiShopClient(player.inventory, (TileEntityShop) te);
//			};
//		}
//		else if (id == TERMINAL)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityTerminal)
//			{
//				return new GuiTerminal(player.inventory, (TileEntityTerminal) te);
//			};
//		}
//		else if (id == PUMP)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityPump)
//			{
//				return new GuiPump((TileEntityPump)te);
//			};
//		}
//		else if (id == ITEM_MONITOR)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityItemMonitor)
//			{
//				return new GuiItemMonitor(player.inventory, (TileEntityItemMonitor)te);
//			};
//		}
//		else if (id == REDSTONE_LATCH)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityRedstoneLatch)
//			{
//				return new GuiRedstoneLatch(player.inventory, (TileEntityRedstoneLatch) te);
//			};
//		}
//		else if (id == FLUID_MONITOR)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityFluidMonitor)
//			{
//				return new GuiFluidMonitor(player.inventory, (TileEntityFluidMonitor)te);
//			};
//		}
//		else if (id == SUBSTATION)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntitySubstation)
//			{
//				return new GuiSubstation(player.inventory, (TileEntitySubstation)te);
//			};
//		}
//		else if (id == ENERGY_SHOP)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityEnergyShop)
//			{
//				return new GuiEnergyShop(player.inventory, (TileEntityEnergyShop)te);
//			};
//		}
//		else if (id == FLUID_SHOP)
//		{
//			TileEntity te = world.getTileEntity(x, y, z);
//			if (te instanceof TileEntityFluidShop)
//			{
//				return new GuiFluidShop(player.inventory, (TileEntityFluidShop)te);
//			};
//		};
//		
//		return Integrator.getClientGuiElement(id, player, world, x, y, z);
//	};
};
