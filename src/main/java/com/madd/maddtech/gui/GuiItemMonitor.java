/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;

import com.madd.maddtech.tileentity.TileEntityItemMonitor;

public class GuiItemMonitor
{
	private static ResourceLocation texBackground = null;
//	private GuiButton btnOp;
//	private GuiTextField txtParam;
	private TileEntityItemMonitor monitor;
	
	public GuiItemMonitor(PlayerInventory invPlayer, TileEntityItemMonitor tileEntity)
	{
		super();
		//ySize = 130;
		monitor = tileEntity;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/ItemMonitor.png");
		};
	};
	
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		fontRendererObj.drawString("Item Monitor", 8, 6, 0x404040);
//		fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 0x404040);
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//		txtParam.drawTextBox();
//	};
//
//	@Override
//	protected void keyTyped(char p_73869_1_, int p_73869_2_)
//	{
//		if (p_73869_2_ == 1 || p_73869_2_ == this.mc.gameSettings.keyBindInventory.getKeyCode())
//		{
//			super.keyTyped(p_73869_1_, p_73869_2_);
//		}
//		else
//		{
//			txtParam.textboxKeyTyped(p_73869_1_, p_73869_2_);
//			try
//			{
//				monitor.setParam(Integer.parseInt(txtParam.getText()));
//			}
//			catch (NumberFormatException e)
//			{
//				// NOP
//			};
//		};
//	};
//
//	@Override
//	public void initGui()
//	{
//		super.initGui();
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		btnOp = new GuiButton(1, x+25, y+15, 40, 20, monitor.getOpName());
//		buttonList.add(btnOp);
//
//		txtParam = new GuiTextField(fontRendererObj, x+66, y+15, 100, 20);
//		txtParam.setFocused(true);
//		txtParam.setCanLoseFocus(false);
//		txtParam.setText("" + monitor.getParam());
//	};
//
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		if (button.id == 1)
//		{
//			btnOp.displayString = monitor.nextOp();
//		};
//	};
};
