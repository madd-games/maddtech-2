/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityPump;

public class GuiPump
{
	private TileEntityPump pump;
	private static ResourceLocation texBackground = null;
	
	//private GuiButton btnFilter;
	//private GuiTextField txtRate;
	
	private static int ySize = 133;
	private static int xSize = 175;
	
	public GuiPump(TileEntityPump pump)
	{
		this.pump = pump;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/Dialog.png");
		};
	};
	
	private String getFilterName(String filter)
	{
		if (filter.equals("*"))
		{
			return "ANY FLUID";
		};
		
		Fluid fluid = MaddRegistry.fluids.get(filter);
		return fluid.getName();
	};
	
//	@Override
//	public void drawScreen(int mouseX, int mouseY, float partialTicks)
//	{
//		drawDefaultBackground();
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//
//		fontRendererObj.drawString("Pump", x+8, y+6, 0x404040);
//		fontRendererObj.drawString("Filter:", x+8, y+23, 0x404040);
//		fontRendererObj.drawString("Rate:", x+8, y+43, 0x404040);
//		
//		fontRendererObj.drawString("" + pump.getRate(), x+50, y+34, 0x404040);
//		txtRate.drawTextBox();
//		
//		super.drawScreen(mouseX, mouseY, partialTicks);
//	};
//	
//	@Override
//	public boolean doesGuiPauseGame()
//	{
//		return false;
//	};
//
//	@Override
//	protected void keyTyped(char p_73869_1_, int p_73869_2_)
//	{
//		if (p_73869_2_ == 1 || p_73869_2_ == this.mc.gameSettings.keyBindInventory.getKeyCode())
//		{
//			this.mc.thePlayer.closeScreen();
//		}
//		else
//		{
//			txtRate.textboxKeyTyped(p_73869_1_, p_73869_2_);
//			try
//			{
//				pump.setRate(Integer.parseInt(txtRate.getText()));
//			}
//			catch (NumberFormatException e)
//			{
//				// NOP
//			};
//		};
//	};
//	
//	@Override
//	public void initGui()
//	{
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		btnFilter = new GuiButton(1, x+50, y+14, 100, 20, getFilterName(pump.getFilter()));
//		buttonList.add(btnFilter);
//		
//		txtRate = new GuiTextField(fontRendererObj, x+50, y+34, 100, 20);
//		txtRate.setFocused(true);
//		txtRate.setCanLoseFocus(false);
//		txtRate.setText("" + pump.getRate());
//	};
//	
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		if (button.id == 1)
//		{
//			btnFilter.displayString = getFilterName(pump.nextFilter());
//		};
//	};
};
