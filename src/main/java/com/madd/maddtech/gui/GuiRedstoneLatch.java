/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityRedstoneLatch;

import net.minecraft.client.gui.*;

public class GuiRedstoneLatch
{
	private static ResourceLocation texBackground = null;
	private TileEntityRedstoneLatch tileEntity;

	private static final int BTN_SUB = 1;
	private static final int BTN_ADD = 2;
	
	public GuiRedstoneLatch(PlayerInventory invPlayer, TileEntityRedstoneLatch tileEntity)
	{
		super();
//		ySize = 133;
//		xSize = 175;
		this.tileEntity = tileEntity;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/Dialog.png");
		};
	};
	
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		fontRendererObj.drawString("Redstone Latch", 8, 6, 0x404040);
//		fontRendererObj.drawString("Latch for: ", 8, 14, 0x404040);
//		fontRendererObj.drawString(tileEntity.getResetValue() + " ticks", 50, 26, 0x404040);
//	};
//	
//	@Override
//	public void initGui()
//	{
//		super.initGui();
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		buttonList.add(new GuiButton(BTN_SUB, x+8, y+24, 20, 20, "-"));
//		buttonList.add(new GuiButton(BTN_ADD, x+111, y+24, 20, 20, "+"));
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//	};
//	
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		boolean update = false;
//		int newResetValue = tileEntity.getResetValue();
//		switch (button.id)
//		{
//		case BTN_SUB:
//			update = true;
//			if (newResetValue > 0) newResetValue--;
//			break;
//		case BTN_ADD:
//			update = true;
//			if (newResetValue < 20) newResetValue++;
//			break;
//		};
//		
//		if (update) tileEntity.setResetValue(newResetValue);
//	};
};
