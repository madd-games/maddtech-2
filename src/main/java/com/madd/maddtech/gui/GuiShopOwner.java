/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityShop;

import net.minecraft.client.gui.*;

public class GuiShopOwner
{
	private static ResourceLocation texBackground = null;
	private TileEntityShop shop;
	
	public GuiShopOwner(PlayerInventory invPlayer, TileEntityShop tileEntity)
	{
		super();
		shop = tileEntity;
		//ySize = 246;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/ShopOwner.png");
		};
	};
	
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		fontRendererObj.drawString("My shop", 8, 6, 0x404040);
//		fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 0x404040);
//
//		int i;
//		for (i=0; i<4; i++)
//		{
//			int price = shop.prices[i];
//			fontRendererObj.drawString("Cost: " + price + " MR", 50, 21 + i * 20, 0x404040);
//		};
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//	};
//
//	@Override
//	public void initGui()
//	{
//		super.initGui();
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			buttonList.add(new GuiButton((i << 1), x+27, y+(15+i*20), 20, 20, "-"));
//			buttonList.add(new GuiButton((i << 1) | 1, x+132, y+(15+i*20), 20, 20, "+"));
//		};
//	};
//
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		int type = button.id & 1;
//		int index = button.id >> 1;
//		
//		if (type == 0)
//		{
//			if (shop.prices[index] > 0) shop.prices[index]--;
//		}
//		else
//		{
//			shop.prices[index]++;
//		};
//		
//		shop.sendUpdateToServer();
//	};
};
