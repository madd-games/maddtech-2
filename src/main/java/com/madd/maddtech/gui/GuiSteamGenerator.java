/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntitySteamGenerator;

import net.minecraft.tileentity.*;

public class GuiSteamGenerator
{
	private static ResourceLocation texBackground = null;
	private TileEntitySteamGenerator tileEntity;

	public GuiSteamGenerator(PlayerInventory invPlayer, TileEntitySteamGenerator tileEntity)
	{
		super();
		//ySize = 256;
		this.tileEntity = tileEntity;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/SteamGenerator.png");
		};
	};
	
//	private void reloadEntity()
//	{
//		// reload entity if needed
//		TileEntity ent = tileEntity.getWorld().getTileEntity(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
//		if (ent != null)
//		{
//			if (ent instanceof TileEntitySteamGenerator)
//			{
//				tileEntity = (TileEntitySteamGenerator) ent;
//			};
//		};
//	};
//
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		reloadEntity();
//		int energy = tileEntity.getEnergy();
//
//		fontRendererObj.drawString("Steam Generator [" + tileEntity.getTemperature() + " C, " + (5*(tileEntity.getTemperature()/100)) + " J/t]", 8, 6, 0x404040);
//		fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 0x404040);
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		reloadEntity();
//		
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		this.mc.getTextureManager().bindTexture(texBackground);
//		int k = (this.width - this.xSize) / 2;
//		int l = (this.height - this.ySize) / 2;
//		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
//
//		int i1 = tileEntity.getBurnTimeRemainingScaled(13);
//		this.drawTexturedModalRect(k + 56, l + 36 + 12 - i1, 176, 12 - i1, 14, i1 + 1);
//	};
};
