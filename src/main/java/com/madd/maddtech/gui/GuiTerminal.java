/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.gui;

import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.tileentity.TileEntityTerminal;

import net.minecraft.client.gui.*;

public class GuiTerminal
{
	private static ResourceLocation texBackground = null;
	private static String[] sideColors = new String[] {"Red", "Green", "Blue", "White"};
	private TileEntityTerminal tileEntity;
	//private GuiTextField text;
	
	public GuiTerminal(PlayerInventory invPlayer, TileEntityTerminal tileEntity)
	{
		super();
//		ySize = 133;
//		xSize = 256;
		this.tileEntity = tileEntity;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/Terminal.png");
		};
	};

//	@Override
//	public void initGui()
//	{
//		super.initGui();
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			buttonList.add(new GuiButton(8 | i, x+214, y+(22+i*20), 21, 20, "SET"));
//			buttonList.add(new GuiButton(8 | i | 4, x+233, y+(22+i*20), 21, 20, "PUB"));
//		};
//		
//		text = new GuiTextField(fontRendererObj, 108, 108, 130, 20);
//		text.setFocused(true);
//		text.setCanLoseFocus(false);
//	};
//
//	@Override
//	protected void drawGuiContainerForegroundLayer(int wtf, int lol)
//	{
//		fontRendererObj.drawString("Glownet Terminal Configuration", 8, 6, 0x404040);
//		
//		fontRendererObj.drawString("SIDE", 8, 14, 0x000066);
//		fontRendererObj.drawString("ADDRESS", 48, 14, 0x000066);
//		fontRendererObj.drawString("TARGET", 148, 14, 0x000066);
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			fontRendererObj.drawString(sideColors[i], 8, 28 + 20 * i, 0x404040);
//			fontRendererObj.drawString(tileEntity.getSideAddr(i+2), 48, 28 + 20 * i, 0x404040);
//			fontRendererObj.drawString(tileEntity.getPeerAddr(i+2), 148, 28 + 20 * i, 0x404040);
//		};
//		
//		fontRendererObj.drawString("Enter value to set: ", 8, 114, 0x404040);
//		text.drawTextBox();
//	};
//	
//	@Override
//	protected void drawGuiContainerBackgroundLayer(float wtf, int lol, int stefan)
//	{
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//	};
//	
//	@Override
//	protected void keyTyped(char par1, int par2)
//	{
//		if (par2 == 1 || par2 == this.mc.gameSettings.keyBindInventory.getKeyCode()) super.keyTyped(par1, par2);
//		else text.textboxKeyTyped(par1, par2);
//	};
//	
//	@Override
//	protected void mouseClicked(int x, int y, int btn)
//	{
//		super.mouseClicked(x, y, btn);
//		text.mouseClicked(x, y, btn);
//	};
//	
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		int btn = button.id;
//		if ((btn & 8) != 0)
//		{
//			int linkno = btn & 3;
//			String target = "*";
//			
//			if ((btn & 4) == 0)
//			{
//				target = text.getText();
//			};
//			
//			tileEntity.setTarget(linkno, target);
//		};
//	};
//	
//	@Override
//	public void updateScreen()
//	{
//		super.updateScreen();
//		text.updateCursorCounter();
//	};
};
