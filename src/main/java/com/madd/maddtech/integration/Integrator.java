/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.integration;

import java.io.*;
import java.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.player.*;

/**
 * Subclasses of this abstract class are responsible for integrating MaddTech with other mods.
 * LibTech will attempt to load a class from a specific package to create the integrator, and
 * if it works (the other mod is present), that integrator is added to the list.
 */
public abstract class Integrator
{
	private static ArrayList<Integrator> intlist = new ArrayList<Integrator>();
	private static String[] integratorNames = new String[] {
		"com.madd.maddtech.intbc.BuildCraftIntegrator"
	};
	
	public Integrator()
	{
	};
	
	/**
	 * Loads all possible integrators.
	 */
	public static void loadAll()
	{
		 int i;
		 for (i=0; i<integratorNames.length; i++)
		 {
		 	String className = integratorNames[i];
		 	try
		 	{
		 		System.out.println("[MADDTECH] Attempting to use integrator: " + className);
		 		Integrator integrator = (Integrator) Class.forName(className).newInstance();
		 		intlist.add(integrator);
		 	}
		 	catch (Throwable e)
		 	{
		 		System.out.println("[MADDTECH] Not integrating using " + className);
		 		e.printStackTrace();
		 	};
		 };
	};
	
	/**
	 * Called to register mod-specific fluids.
	 */
	public void registerModFluids() {};
	
	/**
	 * Called by LibTech to ask all integrators to initialize fluids.
	 */
	public static void registerAllFluids()
	{
		for (Integrator inter : intlist)
		{
			inter.registerModFluids();
		};
	};
	
	/**
	 * Called to register mod-specific items.
	 */
	public void registerModItems() {};
	
	/**
	 * Called by LibTech to ask all integrators to register items.
	 */
	public static void registerAllItems()
	{
		for (Integrator inter : intlist)
		{
			inter.registerModItems();
		};
	};
	
	/**
	 * Called to register mod-specific blocks.
	 */
	public void registerModBlocks() {};
	
	/**
	 * Called by LibTech to ask all integrators to register blocks.
	 */
	public static void registerAllBlocks()
	{
		for (Integrator inter : intlist)
		{
			inter.registerModBlocks();
		};
	};
	
	/**
	 * Called to register mod-specific tile entities.
	 */
	public void registerModTileEntities() {};
	
	/**
	 * Called by LibTech to ask all integrators to register blocks.
	 */
	public static void registerAllTileEntities()
	{
		for (Integrator inter : intlist)
		{
			inter.registerModTileEntities();
		};
	};
	
	/**
	 * Called to register rendering stuff on the client.
	 */
//	@SideOnly(Side.CLIENT)
	public void registerModRendering() {};
	
	/**
	 * Called by ClientProxy to ask all integrators to register rendering.
	 */
//	@SideOnly(Side.CLIENT)
	public static void registerAllRendering()
	{
		for (Integrator inter : intlist)
		{
			inter.registerModRendering();
		};
	};
	
	/**
	 * Called by the GuiHandler for each integrator.
	 */
//	public Object getModServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {return null;};
//	public Object getModClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {return null;};
	
//	public static Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
//	{
//		for (Integrator inter : intlist)
//		{
//			Object obj = inter.getModServerGuiElement(id, player, world, x, y, z);
//			if (obj != null)
//			{
//				return obj;
//			};
//		};
//		
//		return null;
//	};

//	public static Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
//	{
//		for (Integrator inter : intlist)
//		{
//			Object obj = inter.getModClientGuiElement(id, player, world, x, y, z);
//			if (obj != null)
//			{
//				return obj;
//			};
//		};
//		
//		return null;
//	};
//	
//	/**
//	 * Called to register mod-specific packets.
//	 */
//	public void registerModMessages() {};
//	
//	/**
//	 * Called by LibTech to ask all integrators to register packets.
//	 */
//	public static void registerAllMessages()
//	{
//		for (Integrator inter : intlist)
//		{
//			inter.registerModMessages();
//		};
//	};
//	
//	/**
//	 * Called to regsiter mod-specific recipies.
//	 */
//	public void registerModRecipies() {};
//	
//	public static void registerAllRecipies()
//	{
//		for (Integrator inter : intlist)
//		{
//			inter.registerModRecipies();
//		};
//	};
};
