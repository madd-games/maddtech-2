/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.integration.intbc;

import com.madd.maddtech.*;
import java.io.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;
import com.madd.maddtech.integration.Integrator;

import net.minecraft.block.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Integrator for BuildCraft.
 */
public class BuildCraftIntegrator extends Integrator
{
	private Item itemFillerOil;
	private Item itemFillerFuel;
	
	private Block blockElectricEngine;
	private Block blockAlternator;
	
	public BuildCraftIntegrator()
	{
		super();
		System.out.println("[MADDTECH] BuildCraft integration active!");
	};
	
//	@Override
//	public void registerModFluids()
//	{
//		MaddRegistry.fluids.add(new FluidOil());
//		MaddRegistry.fluids.add(new FluidFuel());
//		
//		Item oilBucket = GameRegistry.findItem("BuildCraft|Energy", "bucketOil");
//		Item fuelBucket = GameRegistry.findItem("BuildCraft|Energy", "bucketFuel");
//		
//		MaddRegistry.fluids.addBucket(oilBucket, "maddtech:bcoil");
//		MaddRegistry.fluids.addBucket(fuelBucket, "maddtech:bcfuel");
//		
//		MaddRegistry.fluids.addForgeFluid(BuildCraftEnergy.fluidOil.getID(), "maddtech:bcoil");
//		MaddRegistry.fluids.addForgeFluid(BuildCraftEnergy.fluidFuel.getID(), "maddtech:bcfuel");
//	};
//	
//	@Override
//	public void registerModItems()
//	{
//		itemFillerOil = new ItemFiller("OilFiller", "maddtech:bcoil");
//		GameRegistry.registerItem(itemFillerOil, itemFillerOil.getUnlocalizedName().substring(4));
//		itemFillerFuel = new ItemFiller("FuelFiller", "maddtech:bcfuel");
//		GameRegistry.registerItem(itemFillerFuel, itemFillerFuel.getUnlocalizedName().substring(4));
//	};
//	
//	@SideOnly(Side.CLIENT)
//	@Override
//	public void registerModRendering()
//	{
//		TileEntityElectricEngine engineTile = new TileEntityElectricEngine();
//		engineTile.blockType = blockElectricEngine;
//		engineTile.blockMetadata = 0;
//		
//		RenderingEntityBlocks.blockByEntityRenders.put(new RenderingEntityBlocks.EntityRenderIndex(blockElectricEngine, 0), new RenderEngine(engineTile));
//	};
//	
//	@Override
//	public void registerModBlocks()
//	{
//		blockElectricEngine = new BlockElectricEngine();
//		GameRegistry.registerBlock(blockElectricEngine, blockElectricEngine.getUnlocalizedName().substring(5));
//		blockAlternator = new BlockAlternator();
//		GameRegistry.registerBlock(blockAlternator, blockAlternator.getUnlocalizedName().substring(5));
//	};
//	
//	@Override
//	public void registerModTileEntities()
//	{
//		GameRegistry.registerTileEntity(TileEntityElectricEngine.class, "MT_ElectricEngine");
//		GameRegistry.registerTileEntity(TileEntityAlternator.class, "MT_Alternator");
//	};
//	
//	@Override
//	public Object getModClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
//	{
//		TileEntity ent = world.getTileEntity(x, y, z);
//		if (ent == null)
//		{
//			return null;
//		};
//		
//		switch (id)
//		{
//		case GuiHandler.ELECTRIC_ENGINE:
//			return new GuiElectricEngine((TileEntityElectricEngine) ent);
//		};
//		
//		return null;
//	};
//	
//	@Override
//	public void registerModMessages()
//	{
//		/* IDs 8-10 are reserved for BuildCraft integration, as specified in LibTech.java */
//		LibTech.network.registerMessage(PacketElectricEngineUpdate.Handler.class, PacketElectricEngineUpdate.class, 8, Side.SERVER);
//	};
//	
//	@Override
//	public void registerModRecipies()
//	{
//		MaddRegistry.engTable.add(
//		new EngTableRecipe(
//			"Electrical Engine",
//			3, 4,
//			new Object[] {
//				"ingotSteel", "ingotSteel", "ingotSteel",
//				null, Blocks.glass, null,
//				MaddTech.itemCoil, Blocks.piston, MaddTech.itemCoil,
//				MaddTech.blockSilverEnergyCable, null, MaddTech.blockSilverEnergyCable
//			},
//			new ToolSpec[] {
//				new ToolSpec("welder", 3),
//				new ToolSpec("screwdriver", 1)
//			},
//			1, 1,
//			new ItemStack[] { new ItemStack(blockElectricEngine, 1) }
//		));
//		
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(blockAlternator, 1),
//			"SSS",
//			" C ",
//			" P ",
//			'S', "ingotSteel", 'C', MaddTech.itemCoil, 'P', "ingotSilver"
//		));
//	};
};
