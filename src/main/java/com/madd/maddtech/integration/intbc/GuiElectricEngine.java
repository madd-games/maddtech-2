/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.integration.intbc;

import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;

public class GuiElectricEngine
{
	private TileEntityElectricEngine engine;
	private static ResourceLocation texBackground = null;
	
//	private GuiButton btnSub;
//	private GuiButton btnAdd;
	
	private static int ySize = 133;
	private static int xSize = 175;
	
	public GuiElectricEngine(TileEntityElectricEngine engine)
	{
		this.engine = engine;
		
		if (texBackground == null)
		{
			texBackground = new ResourceLocation("maddtech:textures/gui/Dialog.png");
		};
	};
	
//	@Override
//	public void drawScreen(int mouseX, int mouseY, float partialTicks)
//	{
//		drawDefaultBackground();
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//		
//		this.mc.getTextureManager().bindTexture(texBackground);
//		
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		this.drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
//
//		fontRendererObj.drawString("Electrical Engine", x+8, y+6, 0x404040);
//		fontRendererObj.drawString("INPUT: " + 3 * engine.getMultiplier() + " J", x+30, y+26, 0x404040);
//		fontRendererObj.drawString("OUTPUT: " + 10 * engine.getMultiplier() + " RF", x+30, y+40, 0x404040);
//		
//		super.drawScreen(mouseX, mouseY, partialTicks);
//	};
//	
//	@Override
//	public boolean doesGuiPauseGame()
//	{
//		return false;
//	};
//
//	@Override
//	protected void keyTyped(char p_73869_1_, int p_73869_2_)
//	{
//		if (p_73869_2_ == 1 || p_73869_2_ == this.mc.gameSettings.keyBindInventory.getKeyCode())
//		{
//			this.mc.thePlayer.closeScreen();
//		};
//	};
//	
//	@Override
//	public void initGui()
//	{
//		int x = (this.width - this.xSize) / 2;
//		int y = (this.height - this.ySize) / 2;
//		
//		btnSub = new GuiButton(1, x+8, y+27, 20, 20, "-");
//		btnAdd = new GuiButton(2, x+150, y+27, 20, 20, "+");
//		
//		buttonList.add(btnSub);
//		buttonList.add(btnAdd);
//	};
//	
//	@Override
//	protected void actionPerformed(GuiButton button)
//	{
//		if (button.id == 1)
//		{
//			engine.setMultiplier(engine.getMultiplier()-1);
//		};
//		
//		if (button.id == 2)
//		{
//			engine.setMultiplier(engine.getMultiplier()+1);
//		};
//	};
};
