/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.integration.intbc;

import com.madd.maddtech.*;
import com.madd.maddtech.api.*;
import net.minecraft.nbt.*;
import net.minecraftforge.common.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.player.*;
import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;

public class TileEntityAlternator extends TileEntity implements IEnergyMachine
{
	public TileEntityAlternator(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private static final int MAX_RF_STORAGE = 1000;
	private static final int MAX_J_STORAGE = 300;
	
	// we have an RF buffer and a joule buffer, and we convert when necessary.
	private int rfBuffer = 0;
	private int jouleBuffer = 0;
	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	// *welp*
	// both MaddTech and BuildCraft have the same function name for determining which side energy can connect
	// to. the argument is different though. but the argument is "int" vs "ForgeDirection", and ForgeDirection
	// is an enum, do they count as the same thing? I hope I can overload cause there is some difference.
	
//	@Override
//	public boolean canConnectEnergy(ForgeDirection side)
//	{
//		// BUILDCRAFT: engines connect to sides (not top and bottom)
//		return side.ordinal() >= 2;
//	};
//	
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		// MADDTECH: energy cables connect to bottom side
//		return side == 0;
//	};
//	
//	// receiving buildcraft energy
//	@Override
//	public int receiveEnergy(ForgeDirection from, int energy, boolean simulate)
//	{
//		if (!canConnectEnergy(from))
//		{
//			return 0;
//		};
//		
//		int max = MAX_RF_STORAGE - rfBuffer;
//		if (energy > max) energy = max;
//		
//		if (!simulate)
//		{
//			rfBuffer += energy;
//		};
//		
//		return energy;
//	};
//	
//	@Override
//	public int getEnergyStored(ForgeDirection from)
//	{
//		return rfBuffer;
//	};
//	
//	@Override
//	public int getMaxEnergyStored(ForgeDirection from)
//	{
//		return MAX_RF_STORAGE;
//	};
//	
//	// emitting maddtech energy
//	@Override
//	public int pullEnergy(int energy, int side)
//	{
//		if (side != 0)
//		{
//			return 0;
//		};
//		
//		if (energy > jouleBuffer)
//		{
//			energy = jouleBuffer;
//		};
//		
//		jouleBuffer -= energy;
//		return energy;
//	};
//	
//	@Override
//	public int pushEnergy(int energy, int side)
//	{
//		// do not accept any energy into the alternator.
//		return 0;
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			int delta = 0;
//			while (rfBuffer > 10)
//			{
//				if ((jouleBuffer+3) > MAX_J_STORAGE)
//				{
//					break;
//				};
//				
//				jouleBuffer += 3;
//				rfBuffer -= 10;
//				delta++;
//			};
//			
//			int meta = (worldObj.getBlockMetadata(xCoord, yCoord, zCoord) + delta) % 16;
//			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
//		};
//	};
};
