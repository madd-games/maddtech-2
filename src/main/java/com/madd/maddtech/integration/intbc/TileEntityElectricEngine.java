/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.integration.intbc;

import com.madd.maddtech.*;
import com.madd.maddtech.api.*;
import net.minecraft.nbt.*;
import net.minecraftforge.common.util.*;
import net.minecraft.world.*;
import net.minecraft.entity.player.*;
import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;

public class TileEntityElectricEngine implements IEnergyMachine
{
	private int mtEnergyBuffer = 0;
	private int conversionMultiplier = 1;
	
	public TileEntityElectricEngine()
	{
		super();
	}

	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	};
	
//	@Override
//	public boolean isBurning()
//	{
//		return (mtEnergyBuffer >= 3 * conversionMultiplier) && (isRedstonePowered);
//	};
//	
//	@Override
//	public int getMaxEnergy()
//	{
//		return 500;
//	};
//	
//	@Override
//	public int getIdealOutput()
//	{
//		return 10 * conversionMultiplier;
//	};
//	
//	@Override
//	protected void burn()
//	{
//		if (isRedstonePowered)
//		{
//			if (mtEnergyBuffer < 3 * (conversionMultiplier+2))
//			{
//				SearchEnergy se = new SearchEnergy(worldObj);
//				se.execute(
//					xCoord - orientation.offsetX,
//					yCoord - orientation.offsetY,
//					zCoord - orientation.offsetZ,
//				orientation.ordinal());
//				
//				int toPull = 3 * (conversionMultiplier+2) - mtEnergyBuffer;
//				mtEnergyBuffer += se.pullEnergy(toPull);
//			};
//			
//			if (mtEnergyBuffer < 3*conversionMultiplier)
//			{
//				currentOutput = 0;
//				return;
//			};
//			
//			mtEnergyBuffer -= 3*conversionMultiplier;
//			currentOutput = 10*conversionMultiplier;
//			addEnergy(currentOutput);
//		}
//		else
//		{
//			currentOutput = 0;
//		};
//	};
//	
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		ForgeDirection expectedEmitDirection = ForgeDirection.getOrientation(side).getOpposite();
//		return expectedEmitDirection == orientation;
//	};
//	
//	@Override
//	public int pushEnergy(int energy, int side)
//	{
//		if (canConnectEnergy(side))
//		{
//			mtEnergyBuffer += energy;
//			return energy;
//		};
//		
//		return 0;
//	};
//	
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		// never release energy from the buffer.
//		return 0;
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		mtEnergyBuffer = comp.getInteger("mtEnergyBuffer");
//		conversionMultiplier = comp.getInteger("mtConversionMultiplier");
//	};
//
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setInteger("mtEnergyBuffer", mtEnergyBuffer);
//		comp.setInteger("mtConversionMultiplier", conversionMultiplier);
//	};
//	
//	public int getMultiplier()
//	{
//		return conversionMultiplier;
//	};
//	
//	@Override
//	public boolean onBlockActivated(EntityPlayer player, ForgeDirection side)
//	{
//		if (player.isSneaking())
//		{
//			return false;
//		};
//
//		player.openGui(MaddTech.instance, GuiHandler.ELECTRIC_ENGINE, worldObj, xCoord, yCoord, zCoord);
//		return true;
//	};
//	
//	public void setMultiplier(int value)
//	{
//		if (value < 1)
//		{
//			value = 1;
//		};
//		
//		if (value > 50)
//		{
//			value = 50;
//		};
//		
//		this.conversionMultiplier = value;
//		
//		if (worldObj.isRemote)
//		{
//			// client side; send update to server
//			PacketElectricEngineUpdate update = new PacketElectricEngineUpdate(worldObj, xCoord, yCoord, zCoord, value);
//			LibTech.network.sendToServer(update);
//		}
//		else
//		{
//			// make sure all clients get the update.
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("mtConversionMultiplier", conversionMultiplier);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		conversionMultiplier = comp.getInteger("mtConversionMultiplier");
//	};
//	
//	@Override
//	public void updateHeat()
//	{
//		// do nothing
//	};
};
