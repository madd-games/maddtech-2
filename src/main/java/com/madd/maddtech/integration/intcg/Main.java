/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.integration.intcg;

import com.madd.maddtech.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;
import net.minecraft.item.*;
import java.io.*;
import java.util.*;

/**
 * Main class of the MaddTech-CraftGuide integration package.
 */
public class Main
{
//	@Override
//	public void generateRecipes(RecipeGenerator generator)
//	{
//		//try
//		//{
//			generateMaceratorRecipies(generator);
//			generateEngTableRecipies(generator);
//			generateChemicalReactorRecipies(generator);
//		//}
//		//catch (Exception e)
//		//{
//		//	System.out.println("[MADDTECH] CraftGuide recipe registration failed:");
//		//	e.printStackTrace();
//		//};
//	};
//	
//	private ItemStack fluidStack(String type, int amount)
//	{
//		ItemCapsule capsule = (ItemCapsule) MaddTech.itemCapsule;
//		int meta = 2;
//		if (amount > 1000)
//		{
//			meta = 3;
//		};
//		
////		ItemStack stack = new ItemStack(MaddTech.itemCapsule, 1, meta);
////		capsule.addLiquid(stack, type, amount);
//		return fluidStack(null, 0);
//	};
//	
//	public void generateChemicalReactorRecipies(RecipeGenerator generator)
//	{
//		ItemStack reactor = new ItemStack(MaddTech.blockChemicalReactor, 1);
//		
//		ArrayList<Slot> slots = new ArrayList<Slot>();
//		slots.add(new ItemSlot(75, 21, 16, 16, true).setSlotType(SlotType.MACHINE_SLOT).drawOwnBackground(false));
//		slots.add(new InfoSlot(93, 21, new ItemStack(MaddTech.itemInfo, 1), new String[] {"Temperature: %1$d K", "Delay: %2$d ticks"}).setSlotType(SlotType.MACHINE_SLOT).drawOwnBackground(false));
//		
//		int i;
//		for (i=0; i<10; i++)
//		{
//			slots.add(new ItemSlot(3+18*i, 3, 16, 16, true).drawOwnBackground(true));
//		};
//		
//		for (i=0; i<10; i++)
//		{
//			slots.add(new ItemSlot(3+18*i, 39, 16, 16, true).setSlotType(SlotType.OUTPUT_SLOT).drawOwnBackground(true));
//		};
//		
//		Slot[] slotArray = slots.toArray(new Slot[slots.size()]);
//		RecipeTemplate templateReactor = generator.createRecipeTemplate(slotArray, reactor).setSize(186, 60);
//		
//		for (IChemicalReactorRecipe irecipe : MaddRegistry.chemicalReactor.getRecipies())
//		{
//			if (irecipe instanceof ChemicalReactorRecipe)
//			{
//				ChemicalReactorRecipe recipe = (ChemicalReactorRecipe) irecipe;
//				Object[] items = recipe.getCraftGuide();
//				
//				if (items != null)
//				{
//					generator.addRecipe(templateReactor, items);
//				};
//			};
//		};
//		
////		// non-explicit recipies
////		generator.addRecipe(templateReactor, new Object [] {
////			reactor,
////			new Object[] {0, 0},
////			fluidStack("maddtech:lava", 50), fluidStack("maddtech:water", 20), null, null, null, null, null, null, null, null,
////			new ItemStack(Blocks.obsidian, 1), null, null, null, null, null, null, null, null, null
////		});
////
////		generator.addRecipe(templateReactor, new Object [] {
////			reactor,
////			new Object[] {0, 0},
////			fluidStack("maddtech:lava", 50), null, null, null, null, null, null, null, null, null,
////			new ItemStack(Blocks.cobblestone, 1), null, null, null, null, null, null, null, null, null
////		});
////
////		generator.addRecipe(templateReactor, new Object [] {
////			reactor,
////			new Object[] {0, 0},
////			fluidStack("maddtech:water", 1000), new ItemStack(Blocks.dirt, 1), new ItemStack(Blocks.gravel), null, null, null, null, null, null, null,
////			new ItemStack(Blocks.clay, 1), null, null, null, null, null, null, null, null, null
////		});
//    };
//	
//	public void generateEngTableRecipies(RecipeGenerator generator)
//	{
//		ItemStack engTable = new ItemStack(MaddTech.blockEngTable, 1);
//		
//		ArrayList<Slot> slots = new ArrayList<Slot>();
//		slots.add(new ItemSlot(113, 3, 16, 16, true).setSlotType(SlotType.MACHINE_SLOT).drawOwnBackground(false));
//
//		int x, y;
//		for (y=0; y<5; y++)
//		{
//			for (x=0; x<5; x++)
//			{
//				slots.add(new ItemSlot(3+18*x, 3+18*y, 16, 16, true).drawOwnBackground(true));
//			};
//		};
//		
//		for (y=0; y<5; y++)
//		{
//			for (x=0; x<5; x++)
//			{
//				slots.add(new ItemSlot(151+18*x, 3+18*y, 16, 16, true).setSlotType(SlotType.OUTPUT_SLOT).drawOwnBackground(true));
//			};
//		};
//		
//		int i;
//		for (i=0; i<3; i++)
//		{
//			slots.add(new ItemSlot(95+18*i, 37, 16, 16, true).drawOwnBackground(true));
//		};
//		
//		Slot[] slotArray = slots.toArray(new Slot[slots.size()]);
//		RecipeTemplate templateEngTable = generator.createRecipeTemplate(slotArray, engTable).setSize(243, 95);
//		
//		for (IEngTableRecipe irecipe : MaddRegistry.engTable.getRecipies())
//		{
//			if (irecipe instanceof EngTableRecipe)
//			{
//				try
//				{
//					EngTableRecipe recipe = (EngTableRecipe) irecipe;
//					//Object[] items = recipe.getCraftGuide();
////					if (items != null)
////					{
////						generator.addRecipe(templateEngTable, items);
////					};
//				}
//				catch (Exception e)
//				{
//					System.out.println("RECIPE FAIL:");
//					e.printStackTrace();
//				};
//			};
//		};
//	};
//	
//	public void generateMaceratorRecipies(RecipeGenerator generator)
//	{
//		ItemStack macerator = new ItemStack(MaddTech.blockMaceratorOff, 1);
//
//		RecipeTemplate templateMacerator = generator.createRecipeTemplate(
//			new Slot[]{
//				new ItemSlot(12, 21, 16, 16, true).drawOwnBackground(true),
//				new ItemSlot(50, 21, 16, 16, true).setSlotType(SlotType.OUTPUT_SLOT).drawOwnBackground(true),
//				new ItemSlot(31, 21, 16, 16, true).setSlotType(SlotType.MACHINE_SLOT).drawOwnBackground(false),
//			}, macerator);
//
//		// first register the explicit recipies
//		for(MaceratorRegistry.Recipe recipe: MaceratorRegistry.instance().getRecipies())
//		{
//			generator.addRecipe(templateMacerator,
//				new Object[] {
//					recipe.input,
//					recipe.output,
//					macerator
//				}
//			);
//		};
//		
//		// now the implicit oredict recipies converting ores and ingots into dust
////		for (String oreName : OreDictionary.getOreNames())
////		{
////			if (oreName.startsWith("ingot"))
////			{
////				// an ingot becomes 1 dust, if a dust exists.
////				ArrayList<ItemStack> ingots = OreDictionary.getOres(oreName);
////				ArrayList<ItemStack> dusts = OreDictionary.getOres("dust" + oreName.substring(5));
////				if ((dusts.size() != 0) && (ingots.size() != 0))
////				{
////					generator.addRecipe(templateMacerator, new Object[] {ingots.get(0), dusts.get(0), macerator});
////				};
////			}
////			else if (oreName.startsWith("ore"))
////			{
////				// an ore becomes 2 dusts, if a dust exists.
////				ArrayList<ItemStack> ores = OreDictionary.getOres(oreName);
////				ArrayList<ItemStack> dusts = OreDictionary.getOres("dust" + oreName.substring(3));
////				if ((dusts.size() != 0) && (ores.size() != 0))
////				{
////					generator.addRecipe(templateMacerator, new Object[]
////						{ores.get(0), new ItemStack(dusts.get(0).getItem(), 2), macerator});
////				};
////			};
////		};
//	};
};
