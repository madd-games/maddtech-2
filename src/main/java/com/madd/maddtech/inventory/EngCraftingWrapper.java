/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * A wrapper for InventoryCrafting which makes it work with the engineering table
 * using black magic oh fuck off nobody will ever read these comments why am i writing
 * this fucking hell.
 */

package com.madd.maddtech.inventory;


import net.minecraft.item.ItemStack;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.madd.maddtech.tileentity.TileEntityEngTable;

import net.minecraft.block.Block;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EngCraftingWrapper extends CraftingInventory
{
	/**
	 * The engineering table.
	 */
	private TileEntityEngTable engTable;

	public EngCraftingWrapper(TileEntityEngTable engTable)
	{
		super(null, 1, 1);
		this.engTable = engTable;
	}

	/**
	 * Returns the number of slots in the inventory.
	 */
//	@Override
//	public int getSizeInventory()
//	{
//		return 3*3;
//	}
//
//	/**
//	 * Returns the stack in slot i
//	 */
//	@Override
//	public ItemStack getStackInSlot(int p_70301_1_)
//	{
//		return p_70301_1_ >= 3*3 ? null : getStackInRowAndColumn(p_70301_1_ % 3, p_70301_1_ / 3);
//	}
//
//	/**
//	 * Returns the itemstack in the slot specified (Top left is 0, 0). Args: row, column
//	 */
//	@Override
//	public ItemStack getStackInRowAndColumn(int x, int y)
//	{
//		if (x >= 0 && x < 3 && y >= 0 && y < 3)
//		{
//			// we emulate the crafting matrix being the center of the 5x5 engtable matrix
//			x += 1;
//			y += 1;
//			
//			int actualIndex = y * 5 + x;
//			return engTable.getStackInSlot(actualIndex);
//		}
//		else
//		{
//			return null;
//		}
//	}
//
//	/**
//	 * When some containers are closed they call this on each slot, then drop whatever it returns as an EntityItem -
//	 * like when you close a workbench GUI.
//	 */
//	@Override
//	public ItemStack getStackInSlotOnClosing(int p_70304_1_)
//	{
//		return null;
//	}
//
//	/**
//	 * Removes from an inventory slot (first arg) up to a specified number (second arg) of items and returns them in a
//	 * new stack.
//	 */
//	@Override
//	public ItemStack decrStackSize(int p_70298_1_, int p_70298_2_)
//	{
//		return null;
//	}
//
//	/**
//	 * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
//	 */
//	@Override
//	public void setInventorySlotContents(int p_70299_1_, ItemStack p_70299_2_)
//	{
//	}
};
