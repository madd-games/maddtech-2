/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.item;

import net.minecraft.item.*;
import com.madd.maddtech.api.*;
import java.util.*;
import net.minecraft.entity.player.*;

public class ItemBasicUpgrade extends Item implements IMachineUpgrade
{
	private float timeIncrease;
	private float powerIncrease;
	private String upgradeClass;
	static private final int MAX_STACK = 64;

	protected ItemBasicUpgrade(String upgradeClass, float timeIncrease, float powerIncrease)
	{
		super(new Item.Properties().stacksTo(MAX_STACK).tab(ItemGroup.TAB_MISC));
		this.timeIncrease = timeIncrease;
		this.powerIncrease = powerIncrease;
		this.upgradeClass = upgradeClass;
	};

	@Override
	public float getTimeIncrease()
	{
		return timeIncrease;
	};

	@Override
	public float getPowerIncrease()
	{
		return powerIncrease;
	};

	private String toPercentage(float x)
	{
		String prefix = "";
		if (x >= 0)
		{
			prefix = "+";
		};

		return prefix + (x * 100.0F) + "%";
	};

//	@Override
//	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean wtf)
//	{
//		lines.add(toPercentage(timeIncrease) + " operation time");
//		lines.add(toPercentage(powerIncrease) + " power usage");
//	};

	@Override
	public String getUpgradeClass()
	{
		return upgradeClass;
	};
}
