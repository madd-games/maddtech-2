/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.item;

import net.minecraft.item.*;
import net.minecraft.nbt.*;
import java.util.*;
import net.minecraft.entity.player.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.block.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import java.io.*;

/**
 * Batteries store 3000 J. We use item damage to store the value
 * because it is simpler than the advanced NBT required for capsules.
 */
// Updated some easy stuff (for build) not sure if it will work
public class ItemBattery extends Item
{
	//public IIcon iconBatteryFull;
	static private final int MAX_STACK = 64;

	public ItemBattery()
	{
		super(new Item.Properties().stacksTo(MAX_STACK).tab(ItemGroup.TAB_MISC));
	};

	/**
	 * Returns the energy stored by the item stack.
	 */
	public int getEnergy(ItemStack stack)
	{
		return stack.getItem().getDamage(stack);
	};

	/**
	 * Pushes energy into the battery. Returns the amount of energy actually pushed.
	 */
	public int pushEnergy(ItemStack stack, int energy)
	{
		if (stack.getItem().getDamage(stack)+energy > 3000)
		{
			energy = 3000 - stack.getItem().getDamage(stack);
		};

		stack.setDamageValue(stack.getItem().getDamage(stack) + energy);
		return energy;
	};

	/**
	 * Pulls energy from the battery. Returns the amount of energy actually pulled.
	 */
	public int pullEnergy(ItemStack stack, int energy)
	{
		if (energy > stack.getItem().getDamage(stack))
		{
			energy = stack.getItem().getDamage(stack);
		};

		stack.setDamageValue(stack.getItem().getDamage(stack)-energy);
		return energy;
	};

//	@Override
//	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean wtf)
//	{
//		lines.add(stack.getItem().getDamage(stack) + "/3000 J");
//	};
//
//	@Override
//	public void registerIcons(IIconRegister ir)
//	{
//		super.registerIcons(ir);
//		iconBatteryFull = ir.registerIcon("maddtech:BatteryFull");
//	};
};
