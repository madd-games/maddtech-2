/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Metadata for capsules:
 * Bit 0 (mask 0x1) is 1 for large capsules (5l) and 0 for small capsules (1l).
 * Bit 1 (mask 0x2) is 1 for lead capsules, or 0 for tin capsules.
 * Other bits are undefined and must be ignored.
 */

package com.madd.maddtech.item;

import net.minecraft.item.*;
import net.minecraft.nbt.*;
import java.util.*;
import net.minecraft.entity.player.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.block.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import java.io.*;

public class ItemCapsule extends Item implements IChemicalReactorComponent, INuclearReactorComponent
{
	//private IIcon[] capsuleIcons = new IIcon[4];
	static private final int MAX_STACK = 1;

	public ItemCapsule()
	{
		super(new Item.Properties().stacksTo(MAX_STACK).tab(ItemGroup.TAB_MISC));
	}

//	@Override
//	public void registerIcons(IIconRegister ir)
//	{
//		capsuleIcons[0] = ir.registerIcon("maddtech:Capsule");
//		capsuleIcons[1] = ir.registerIcon("maddtech:CapsuleLarge");
//		capsuleIcons[2] = ir.registerIcon("maddtech:CapsuleLead");
//		capsuleIcons[3] = ir.registerIcon("maddtech:CapsuleLeadLarge");
//	};

//	@Override
//	public IIcon getIconFromDamage(int dmg)
//	{
//		return capsuleIcons[dmg & 3];
//	};

//	@Override
//	public void getSubItems(Item me,  tabs, List list)
//	{
//		int i;
//		for (i=0; i<4; i++)
//		{
//			list.add(new ItemStack(me, 1, i));
//		};
//	};

	public int getLimit(ItemStack stack)
	{
		int dmg = stack.getDamageValue();
		int limit;
		if ((dmg & 0x1) != 0)
		{
			limit = 5000;
		}
		else
		{
			limit = 1000;
		};

		return limit;
	}

	@Override
	public void updateNR(ItemStack stack, INuclearReactor reactor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCR(ItemStack stack, IChemicalReactor reactor) {
		// TODO Auto-generated method stub
		
	};

//	public String getContent(ItemStack stack)
//	{
//		if (stack.hasTagCompound())
//		{
//			NBTTagCompound comp = stack.getTagCompound();
//			return comp.getString("CapsuleContentType");
//		}
//		else
//		{
//			return "maddtech:empty";
//		}
//	};
//
//	public int getAmount(ItemStack stack)
//	{
//		if (stack.hasTagCompound())
//		{
//			NBTTagCompound comp = stack.getTagCompound();
//			return comp.getInteger("CapsuleContentAmount");
//		}
//		else
//		{
//			return 0;
//		}
//	};

//	@Override
//	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean wtf)
//	{
//		int dmg = stack.getDamageValue();
//		String material;
//		if ((dmg & 0x2) != 0)
//		{
//			material = "Lead";
//		}
//		else
//		{
//			material = "Tin";
//		};
//
//		int limit;
//		if ((dmg & 0x1) != 0)
//		{
//			limit = 5000;
//		}
//		else
//		{
//			limit = 1000;
//		};
//
//		lines.add(material + " Coating");
//		lines.add(getAmount(stack) + "/" + limit + " " + MaddRegistry.fluids.get(getContent(stack)).getName());
//	};

//	private void setContent(ItemStack stack, String cont)
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setString("CapsuleContentType", cont);
//		comp.setInteger("CapsuleContentAmount", getAmount(stack));
//		stack.setTagCompound(comp);
//	};
//
//	/**
//	 * Returns the amount of liquid actually added to the capsule.
//	 */
//	public int addLiquid(ItemStack stack, String type, int amount)
//	{
//		int dmg = stack.getItemDamage();
//		int limit;
//		if ((dmg & 0x1) != 0)
//		{
//			limit = 5000;
//		}
//		else
//		{
//			limit = 1000;
//		};
//
//		Fluid fluid = MaddRegistry.fluids.get(type);
//		if (fluid.getCapsuleLevel() > ((dmg >> 1) & 1))
//		{
//			// This liquid cannot be stored in this capsule type.
//			return 0;
//		};
//
//		int current = getAmount(stack);
//		if ((current+amount) > limit)
//		{
//			amount = limit-current;
//		};
//
//		NBTTagCompound comp = new NBTTagCompound();
//		String cont = getContent(stack);
//		if (cont.equals("maddtech:empty"))
//		{
//			cont = type;
//		};
//
//		if (!cont.equals(type))
//		{
//			return 0;		// can't mix liquids.
//		};
//
//		current += amount;
//		comp.setString("CapsuleContentType", cont);
//		comp.setInteger("CapsuleContentAmount", current);
//		stack.setTagCompound(comp);
//
//		return amount;
//	};
//
//	/**
//	 * Returns the amount of liquid actually pulled.
//	 */
//	public int pullLiquid(ItemStack stack, int amount)
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setString("CapsuleContentType", getContent(stack));
//		int current = getAmount(stack);
//		if (amount > current)
//		{
//			amount = current;
//		};
//		current -= amount;
//		comp.setInteger("CapsuleContentAmount", current);
//		stack.setTagCompound(comp);
//
//		return amount;
//	};
//
//	@Override
//	public boolean onItemUse(ItemStack stack, EntityPlayer q, World world, int x, int y, int z, int side, float w, float t, float f)
//	{
//		int bx = x + SideUtil.getSideX(side);
//		int by = y + SideUtil.getSideY(side);
//		int bz = z + SideUtil.getSideZ(side);
//		Block block = world.getBlock(bx, by, bz);
//
//		if (getAmount(stack) == 0)
//		{
//			setContent(stack, "maddtech:empty");
//		};
//
//		if (block == Blocks.water)
//		{
//			if (addLiquid(stack, "maddtech:water", 1000) > 0)
//			{
//				world.setBlockToAir(bx, by, bz);
//				return true;
//			};
//		}
//		else if (block == Blocks.lava)
//		{
//			if (addLiquid(stack, "maddtech:lava", 1000) > 0)
//			{
//				world.setBlockToAir(bx, by, bz);
//				return true;
//			};
//		}
//		else if (block == Blocks.air)
//		{
//			String cont = getContent(stack);
//			if (getAmount(stack) >= 1000)
//			{
//				if (cont.equals("maddtech:water"))
//				{
//					pullLiquid(stack, 1000);
//					world.setBlock(bx, by, bz, Blocks.flowing_water);
//					return true;
//				}
//				else if (cont.equals("maddtech:lava"))
//				{
//					pullLiquid(stack, 1000);
//					world.setBlock(bx, by, bz, Blocks.flowing_lava);
//					return true;
//				};
//			};
//		};
//
//		return false;
//	};
//
//	@Override
//	public void updateCR(ItemStack stack, IChemicalReactor reactor)
//	{
//		Random rand = new Random();
//
//		String cont = getContent(stack);
//		int dmg = stack.getItemDamage();
//		int limit;
//		if ((dmg & 0x1) != 0)
//		{
//			limit = 5000;
//		}
//		else
//		{
//			limit = 1000;
//		};
//		int maxCanPull = limit - getAmount(stack);
//		addLiquid(stack, cont, reactor.pullFluidFromCables(cont, maxCanPull));
//
//		Fluid fluid = MaddRegistry.fluids.get(cont);
//		fluid.updateCR(new CapsuleAccess(stack), reactor);
//	};
//
//	@Override
//	public void updateNR(ItemStack stack, INuclearReactor reactor)
//	{
//		Random rand = new Random();
//
//		String cont = getContent(stack);
//		int dmg = stack.getItemDamage();
//		int limit;
//		if ((dmg & 0x1) != 0)
//		{
//			limit = 5000;
//		}
//		else
//		{
//			limit = 1000;
//		};
//		int maxCanPull = limit - getAmount(stack);
//		addLiquid(stack, cont, reactor.pullFluidFromCables(cont, maxCanPull));
//		
//		Fluid fluid = MaddRegistry.fluids.get(cont);
//		fluid.updateNR(new CapsuleAccess(stack), reactor);
//	};
}
