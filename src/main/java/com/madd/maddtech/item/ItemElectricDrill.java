/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * The electric drill item.
 */

package com.madd.maddtech.item;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import java.util.*;
import net.minecraft.block.Block;
import net.minecraft.entity.*;
import net.minecraft.entity.player.*;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;
import net.minecraft.nbt.*;

public class ItemElectricDrill extends Item implements IEnergyItem
{
	static private final int MAX_STACK = 1;
	/**
	 * Maximum energy which can be stored by the electric drill.
	 */
	public static final int MAX_ENERGY = 3500;
	
	private boolean unknownBlock = false;
	
	public ItemElectricDrill()
	{
		super(new Item.Properties().stacksTo(MAX_STACK).tab(ItemGroup.TAB_MISC));
	}

	@Override
	public ItemStack onTopEnergySlot(ItemStack stack, IEnergyMachine accumulator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack onBottomEnergySlot(ItemStack stack, IEnergyMachine accumulator) {
		// TODO Auto-generated method stub
		return null;
	};
	
	/**
	 * Get current energy stored.
	 */
//	public int getEnergy(ItemStack stack)
//	{
//		if (!stack.hasTagCompound())
//		{
//			return 0;
//		}
//		else
//		{
//			NBTTagCompound comp = stack.getTagCompound();
//			return comp.getInteger("Energy");
//		}
//	};
//	
//	/**
//	 * Set the energy stored.
//	 */
//	public void setEnergy(ItemStack stack, int energy)
//	{
//		NBTTagCompound comp;
//		if (!stack.hasTagCompound())
//		{
//			comp = new NBTTagCompound();
//			stack.setTagCompound(comp);
//		}
//		else
//		{
//			comp = stack.getTagCompound();
//		};
//		
//		comp.setInteger("Energy", energy);
//	};
//	
//	/**
//	 * Translation to human: this returns the strength this item should be digging at. If we have
//	 * enough energy to dig it, then we dig at a constant speed, so the strength depends on the
//	 * hardness of the block itself.
//	 */
//	@Override
//	public float func_150893_a(ItemStack stack, Block block)
//	{
//		// block hardness depends on the world and coordinates apparently, but none of the minecraft
//		// code seems to take those into account. so let's do a funny trick where if there is any
//		// block that even cares, we catch the exceptions and use 100000.0F as the digging speed.
//		float hardness;
//		try
//		{
//			hardness = block.getBlockHardness(null, 0, 0, 0);
//		}
//		catch (Exception e)
//		{
//			unknownBlock = true;
//			return 100000.0F;
//		};
//		
//		// don't dig bedrock and stuff, also blocks that have zero hardness can just be
//		// dug with anything so use normal strength, otherwise werid things happen
//		if (hardness < 0.01F)
//		{
//			return 1.0F;
//		};
//		
//		// check if we have enough energy to mine this
//		int energyNeeded = (int) Math.ceil(hardness / 1.5F);
//		if (getEnergy(stack) < energyNeeded && unknownBlock == false)
//		{
//			return 1.0F;
//		}
//		else if (getEnergy(stack) < 1 && unknownBlock == true)
//		{
//			return 1.0F;
//		}
//		
//		// we have everything we need, dig
//		return hardness * 100.0F;
//	};
//	
//	/**
//	 * When we destroy a block, use up energy.
//	 */
//	@Override
//	public boolean onBlockDestroyed(ItemStack stack, World w, Block block, int x, int y, int z, EntityLivingBase e)
//	{
//		// subtract energy
//		int energyNeeded = (int) Math.ceil(block.getBlockHardness(w, x, y, z) / 1.5F);
//		int currentEnergy = getEnergy(stack);
//		if (currentEnergy >= energyNeeded && unknownBlock == false)
//		{
//			setEnergy(stack, currentEnergy - energyNeeded);
//		}
//		else if (currentEnergy >= 1 && unknownBlock == true)
//		{
//			unknownBlock = false;
//			setEnergy(stack, currentEnergy - 1);
//		}
//		
//		return true;
//	};
//	
//	/**
//	 * Make it harvest everything that diamond pickaxe can.
//	 */
//	@Override
//	public int getHarvestLevel(ItemStack stack, String toolClass)
//	{
//		return 3;
//	};
//	
//	/**
//	 * The durability to be displayed is the amount of energy we have left.
//	 */
//	@Override
//	public double getDurabilityForDisplay(ItemStack stack)
//	{
//		return 1.0D - ((double) getEnergy(stack) / (double) MAX_ENERGY);
//	};
//	
//	/**
//	 * Display the energy content in the additional information.
//	 */
//	@Override
//	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean wtf)
//	{
//		lines.add("" + getEnergy(stack) + "/" + MAX_ENERGY + " J");
//	};
//	
//	/**
//	 * Called when on top of accumulator GUI.
//	 */
//	@Override
//	public ItemStack onTopEnergySlot(ItemStack stack, IEnergyMachine accumulator)
//	{
//		int max = getEnergy(stack);
//		int done = accumulator.pushEnergy(max, 6);
//		setEnergy(stack, max-done);
//		return stack;
//	};
//	
//	/**
//	 * Called when on the bottom of accumulator GUI.
//	 */
//	@Override
//	public ItemStack onBottomEnergySlot(ItemStack stack, IEnergyMachine accumulator)
//	{
//		int max = MAX_ENERGY - getEnergy(stack);
//		int done = accumulator.pullEnergy(max, 6);
//		setEnergy(stack, getEnergy(stack) + done);
//		return stack;
//	};
//	
//	@Override
//	public boolean showDurabilityBar(ItemStack stack)
//	{
//		return true;
//	};
//	
//	@Override
//	public boolean func_150897_b(Block p_150897_1_)
//	{
//		return true;
//	};
};
