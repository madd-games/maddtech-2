/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.item;

import net.minecraft.item.*;
import com.madd.maddtech.api.*;
import net.minecraft.nbt.*;
import java.util.*;
import net.minecraft.entity.player.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.block.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

public class ItemFan extends Item implements IChemicalReactorComponent, INuclearReactorComponent
{
	static private final int MAX_STACK = 1;
	private int coolRate;

	protected ItemFan(String material, int coolRate)
	{
		super(new Item.Properties().stacksTo(MAX_STACK).tab(ItemGroup.TAB_MISC));
		this.coolRate = coolRate;
	};
	
	public ItemFan()
	{
		super(new Item.Properties());
	}

	@Override
	public void updateCR(ItemStack stack, IChemicalReactor reactor)
	{
		updateReactor(stack, reactor);
	};

	@Override
	public void updateNR(ItemStack stack, INuclearReactor reactor)
	{
		updateReactor(stack, reactor);
	};

	private void updateReactor(ItemStack stack, IReactor reactor)
	{
		int effectiveCoolRate = coolRate;
		if (reactor instanceof INuclearReactor)
		{
			effectiveCoolRate = (int) (coolRate * ((INuclearReactor)reactor).getAirflow());
		};
		
		int cool = effectiveCoolRate - reactor.pushFluidToCables("maddtech:hotair", effectiveCoolRate);
		int temp = reactor.getTemperature();
		if ((temp-cool) < 300)
		{
			cool = 300 - temp;
		};
		if (cool < 0)
		{
			cool = 0;
		};
		reactor.heatUp(-cool);
	};
};
