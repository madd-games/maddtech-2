/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.item;

import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public abstract class ItemGroupCMenu extends ItemGroup {
	public static ItemGroupCMenu[] CM_TABS = new ItemGroupCMenu[10];
	
	public static final ItemGroupCMenu CM_DECO_TAB = new ItemGroupCMenu(0, "decotab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public static final ItemGroupCMenu CM_FLUID_TAB = new ItemGroupCMenu(1, "fluidtab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Blocks.IRON_ORE);
		}
	};
	
	public static final ItemGroupCMenu CM_GLOWNET_TAB = new ItemGroupCMenu(2, "glownettab") 
	{
		
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public static final ItemGroupCMenu CM_ITEM_TAB = new ItemGroupCMenu(0, "itemtab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public static final ItemGroupCMenu CM_LIGHT_TAB = new ItemGroupCMenu(1, "lighttab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Blocks.IRON_ORE);
		}
	};
	public static final ItemGroupCMenu CM_MACH_TAB = new ItemGroupCMenu(2, "machtab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public static final ItemGroupCMenu CM_METALS_TAB = new ItemGroupCMenu(0, "metalstab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public static final ItemGroupCMenu CM_REDSTONE_TAB = new ItemGroupCMenu(1, "redstonetab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Blocks.IRON_ORE);
		}
	};
	public static final ItemGroupCMenu CM_TOOLS_TAB = new ItemGroupCMenu(2, "toolstab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public static final ItemGroupCMenu CM_UPGRADE_TAB = new ItemGroupCMenu(2, "upgradetab") 
	{
		public ItemStack makeIcon() 
		{
			return new ItemStack(Items.DRAGON_EGG);
		}
	};
	public ItemGroupCMenu(int i, String label) 
	{
		super(label);
	}
	
}