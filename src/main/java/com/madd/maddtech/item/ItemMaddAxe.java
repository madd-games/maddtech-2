/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.item;

import net.minecraft.item.*;
import java.util.*;

public class ItemMaddAxe extends Item
{
	static private final int MAX_STACK = 1;
	private String mName;
	protected ItemMaddAxe(String matName)
	{
		super(new Item.Properties().stacksTo(MAX_STACK).tab(ItemGroup.TAB_MISC));
	}
	
//	@Override
//	public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
//	{
//		if (mName == "Copper")
//		{
//			if (repair.getItem().equals(MaddTech.itemCopperIngot)) return true;
//		}
//		else if (mName == "Lead")
//		{
//			if (repair.getItem().equals(MaddTech.itemLeadIngot)) return true;
//		}
//		else if (mName == "Tin")
//		{
//			if (repair.getItem().equals(MaddTech.itemTinIngot)) return true;
//		}
//		else if (mName == "Silver")
//		{
//			if (repair.getItem().equals(MaddTech.itemSilverIngot)) return true;
//		}
//		else if (mName == "Uranium")
//		{
//			if (repair.getItem().equals(MaddTech.itemUraniumIngot)) return true;
//		}
//		else if (mName == "Steel")
//		{
//			if (repair.getItem().equals(MaddTech.itemSteelIngot)) return true;
//		}
//		else if (mName == "Bronze")
//		{
//			if (repair.getItem().equals(MaddTech.itemBronzeIngot)) return true;
//		}
//		else if (mName == "Brass")
//		{
//			if (repair.getItem().equals(MaddTech.itemBrassIngot)) return true;
//		}
//		else if (mName == "RedAlloy")
//		{
//			if (repair.getItem().equals(MaddTech.itemRedAlloyIngot)) return true;
//		}
//		else
//		{
//			return false;
//		}
//		return false;
//	}
}
