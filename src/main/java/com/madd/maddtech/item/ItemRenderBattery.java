/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * ItemRenderBattery.java
 * Rendering Batterys.
 */

package com.madd.maddtech.item;

import net.minecraftforge.client.*;
import net.minecraft.item.*;
import net.minecraft.client.renderer.*;
import net.minecraft.client.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import org.lwjgl.opengl.*;
import com.madd.maddtech.api.*;

//public class ItemRenderBattery implements IItemRenderer
//{
//	public ItemRenderBattery()
//	{
//	};
//
//	@Override
//	public boolean handleRenderType(ItemStack item, ItemRenderType type)
//	{
//		if (type == ItemRenderType.INVENTORY)
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	};
//
//	@Override
//	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper)
//	{
//		return false;
//	};
//
//	private void renderIcon(Tessellator tess, int x, int y, int width, int height, float u1, float v1, float u2, float v2)
//	{
//		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
//
//		tess.startDrawingQuads();
//		//tess.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
//		//tess.setNormal(0.0F, 0.0F, -1.0F);
//
//		double x1 = (double)x;
//		double x2 = (double)x + (double)width;
//		double y1 = (double)y;
//		double y2 = (double)y + (double)height;
//
//		tess.addVertexWithUV(x1, y2, 0.0, u1, v2);
//		tess.addVertexWithUV(x2, y2, 0.0, u2, v2);
//		tess.addVertexWithUV(x2, y1, 0.0, u2, v1);
//		tess.addVertexWithUV(x1, y1, 0.0, u1, v1);
//
//		tess.draw();
//	};
//
//	@Override
//	public void renderItem(ItemRenderType type, ItemStack stack, Object... data)
//	{
//		GL11.glEnable(GL11.GL_ALPHA_TEST);
//		GL11.glEnable(GL11.GL_BLEND);
//
//		Tessellator tess = Tessellator.instance;
//		tess.setBrightness(0xf000f0);
//		ItemBattery battery = (ItemBattery) MaddTech.itemBattery;
//
//		IIcon icon = stack.getItem().getIconFromDamage(stack.getItemDamage());
//		float u1 = icon.getMinU();
//		float u2 = icon.getMaxU();
//		float v1 = icon.getMinV();
//		float v2 = icon.getMaxV();
//
//		renderIcon(tess, 0, 0, 16, 16, u1, v1, u2, v2);
//
//		float factor = (float) battery.getEnergy(stack) / 3000.0F;
//		icon = battery.iconBatteryFull;
//		u1 = icon.getMinU();
//		u2 = icon.getMaxU();
//		v1 = icon.getInterpolatedV(16.0F-(16.0*factor));
//		v2 = icon.getMaxV();
//
//		int height = (int) (factor * 16.0F);
//		renderIcon(tess, 0, 16-height, 16, height, u1, v1, u2, v2);
//
//		GL11.glDisable(GL11.GL_BLEND);
//		GL11.glDisable(GL11.GL_ALPHA_TEST);
//	};
//};
