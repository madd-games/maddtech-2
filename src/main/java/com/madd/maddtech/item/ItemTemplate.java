/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Templates
 */

package com.madd.maddtech.item;

import net.minecraft.item.*;
import net.minecraft.nbt.*;
import java.util.*;
import net.minecraft.entity.player.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.util.*;
import net.minecraft.block.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import java.io.*;

public class ItemTemplate extends Item
{

	public ItemTemplate() {
		super(new Item.Properties());
		// TODO Auto-generated constructor stub
	}
	//private IIcon icon;
	
//	public ItemTemplate()
//	{
//		setCreativeTab(MaddTech.tabMach);
//		setUnlocalizedName("itemTemplate");
//		setMaxStackSize(1);
//	}
//
//	@Override
//	public void registerIcons(IIconRegister ir)
//	{
//		icon = ir.registerIcon("maddtech:template");
//	};
//
//	@Override
//	public IIcon getIconFromDamage(int dmg)
//	{
//		return icon;
//	};
//	
//	/**
//	 * Set the layout of this template based on the specified TileEntityEngTable.
//	 */
//	public void setLayout(ItemStack stack, TileEntityEngTable table)
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		NBTTagList list = new NBTTagList();
//		
//		int i;
//		for (i=0; i<5*5; i++)
//		{
//			ItemStack istack = table.getStackInSlot(i + TileEntityEngTable.INPUT_SLOT_BASE);
//			if (istack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				istack.writeToNBT(tag);
//				tag.setByte("Count", (byte) 1);
//				list.appendTag(tag);
//			};
//		};
//		
//		comp.setTag("Inputs", list);
//		stack.setTagCompound(comp);
//	};
//	
//	@Override
//	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean wtf)
//	{
//		if (!stack.hasTagCompound())
//		{
//			lines.add("Undefined");
//		}
//		else
//		{
//			NBTTagCompound comp = stack.getTagCompound();
//			NBTTagList items = comp.getTagList("Inputs", comp.getId());
//			
//			int i;
//			for (i=0; i<items.tagCount(); i++)
//			{
//				NBTTagCompound tag = (NBTTagCompound) items.getCompoundTagAt(i);
//				ItemStack istack = ItemStack.loadItemStackFromNBT(tag);
//				String unlocName = istack.getItem().getUnlocalizedName();
//				if (unlocName == null) unlocName = "";
//				
//				String name = StatCollector.translateToLocal(unlocName + ".name");
//				lines.add(name);
//			};
//		};
//	};
}
