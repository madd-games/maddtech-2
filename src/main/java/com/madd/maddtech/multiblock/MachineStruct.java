/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * MachineStruct.java
 * A class for managing machine structures.
 */

package com.madd.maddtech.multiblock;

import net.minecraft.block.*;

import net.minecraft.world.*;
import java.util.*;

import com.madd.maddtech.block.BlockCompare;
import com.madd.maddtech.block.BlockCompareExact;
import com.madd.maddtech.block.BlockCompareIgnore;
import com.madd.maddtech.util.Coords;
import com.madd.maddtech.util.SideUtil;

public class MachineStruct
{
	private class BlockInfo
	{
		public BlockInfo() {};
		int x;
		int y;
		int z;
		BlockCompare comp;
	};

	private ArrayList<BlockInfo> blocks;
	private HashMap<String, Coords> functions = new HashMap<String, Coords>();
	
	public boolean hasSpecial = false;
	public int sideFacing;

	public MachineStruct()
	{
		blocks = new ArrayList<BlockInfo>();
	};

	/**
	 * Returns the index into the blocks array for the specified coordinates.
	 * -1 is returned if this class does not know about this position.
	 */
	private int getIndexTo(int x, int y, int z)
	{
		int i;
		for (i=0; i<blocks.size(); i++)
		{
			BlockInfo info = blocks.get(i);
			if ((info.x == x) && (info.y == y) && (info.z == z))
			{
				return i;
			};
		};

		return -1;
	};

	/**
	 * Sets the comparer at the specified coordinates.
	 */
	public void set(int x, int y, int z, BlockCompare comp)
	{
		int index = getIndexTo(x, y, z);
		if (index != -1)
		{
			BlockInfo info = blocks.get(index);
			info.comp = comp;
			blocks.set(index, info);
		}
		else
		{
			BlockInfo info = new BlockInfo();
			info.x = x;
			info.y = y;
			info.z = z;
			info.comp = comp;
			blocks.add(info);
		}
	};

	/**
	 * Sets the comparer at the specified coordinates to a specific block.
	 */
	public void set(int x, int y, int z, Block block)
	{
		set(x, y, z, new BlockCompareExact(block));
	};
	
	public void set(int x, int y, int z, Block block, int meta)
	{
		set(x, y, z, new BlockCompareExact(block, meta));
	};

	/**
	 * "Unsets" the specified coordinates - by setting them to BlockCompareIgnore.
	 */
	public void unset(int x, int y, int z)
	{
		set(x, y, z, new BlockCompareIgnore());
	};

	/**
	 * Check if this structure (without spinning), is located at the specified position in the specified world.
	 */
//	public boolean isFoundAt(World world, int x, int y, int z)
//	{
//		int i;
//		for (i=0; i<blocks.size(); i++)
//		{
//			BlockInfo info = blocks.get(i);
//			int bx = x + info.x;
//			int by = y + info.y;
//			int bz = z + info.z;
//			//Block block = world.getBlock(bx, by, bz);
//			if (!info.comp.matches(block))
//			{
//				return false;
//			};
//			
//			/*
//			if ((block == MaddTech.blockTinShellRedstone) || (block == MaddTech.blockLeadShellRedstone))
//			{
//				specialX = info.x;
//				specialY = info.y;
//				specialZ = info.z;
//				hasSpecial = true;
//			};*/
//			if (block instanceof BlockShell)
//			{
//				BlockShell shell = (BlockShell) block;
//				functions.put(shell.getShellFunction(), new Coords(info.x, info.y, info.z));
//			};
//		};
//
//		return true;
//	};

	/**
	 * Defines a box in the structure.
	 */
	public void setBox(int x, int y, int z, int width, int height, int length, BlockCompare comp)
	{
		int offX, offY, offZ;
		for (offX=0; offX<width; offX++)
		{
			for (offY=0; offY<height; offY++)
			{
				for (offZ=0; offZ<length; offZ++)
				{
					set(x+offX, y+offY, z+offZ, comp);
				};
			};
		};
	};

	/**
	 * Defines a box in the structure.
	 */
	public void setBox(int x, int y, int z, int width, int height, int length, Block block)
	{
		setBox(x, y, z, width, height, length, new BlockCompareExact(block));
	};
	
	public void setBox(int x, int y, int z, int width, int height, int length, Block block, int pathSide)
	{
		setBox(x, y, z, width, height, length, new BlockCompareExact(block, pathSide));
	};

	/**
	 * Spins the structure so that the specified side is the front.
	 */
	public MachineStruct spin(int frontSide)
	{
		// Make the rotation matrix.
		// A' = A Aa + B Ab
		// etc.
		int Xx, Xy, Xz;
		int Yx, Yy, Yz;
		int Zx, Zy, Zz;
		
		Zx = SideUtil.getSideX(frontSide);
		Zy = SideUtil.getSideY(frontSide);
		Zz = SideUtil.getSideZ(frontSide);
		
		Yx = 0;
		Yy = 1;
		Yz = 0;
		
		int rightSide = 4;
		switch (frontSide)
		{
		case 2:
			rightSide = 5;
			break;
		case 3:
			rightSide = 4;
			break;
		case 4:
			rightSide = 2;
			break;
		case 5:
			rightSide = 3;
			break;
		};
		
		Xx = SideUtil.getSideX(rightSide);
		Xy = SideUtil.getSideY(rightSide);
		Xz = SideUtil.getSideZ(rightSide);
		
		MachineStruct out = new MachineStruct();
		out.sideFacing = SideUtil.opposite(frontSide);
		int i;
		for (i=0; i<blocks.size(); i++)
		{
			BlockInfo binfo = blocks.get(i);
			
			int x = Xx * binfo.x + Xy * binfo.y + Xz * binfo.z;
			int y = Yx * binfo.x + Yy * binfo.y + Yz * binfo.z;
			int z = Zx * binfo.x + Zy * binfo.y + Zz * binfo.z;
			
			out.set(x, y, z, binfo.comp.spin(frontSide));
		};
		
		return out;
	};
	
	/**
	 * Configure the structure (for interface path tracing and all that mumbojumbo).
	 */
	public void config(World world, int x, int y, int z)
	{
		int i;
		for (i=0; i<blocks.size(); i++)
		{
			BlockInfo info = blocks.get(i);
			int bx = x + info.x;
			int by = y + info.y;
			int bz = z + info.z;
			info.comp.config(world, bx, by, bz);
		};
	};
	
	/**
	 * Return the coordinates of the block with the given functions, or null if it's not there.
	 */
	public Coords getFunctionBlock(String func)
	{
		if (!functions.containsKey(func))
		{
			return null;
		};
		
		return functions.get(func);
	};
};
