/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * ChemicalReactorRecipe.java
 * A chemical reactor recipe.
 */

package com.madd.maddtech.registry;

import com.madd.maddtech.MaddTech;
import com.madd.maddtech.api.*;
import net.minecraft.item.*;
import java.util.*;

public class ChemicalReactorRecipe implements IChemicalReactorRecipe
{
	private ItemStack[] inputs;
	private ItemStack[] outputs;
	private int minTemp;
	private int time;
	private int tickCounter = 0;

	public ChemicalReactorRecipe(ItemStack[] inputs, ItemStack[] outputs, int minTemp, int time)
	{
		this.inputs = inputs;
		this.outputs = outputs;
		this.minTemp = minTemp;
		this.time = time;
	};

	private boolean canReact(IChemicalReactor reactor)
	{
		int i;
		for (i=0; i<inputs.length; i++)
		{
			ItemStack stack = inputs[i];
			if (!reactor.canRemoveStack(stack)) return false;
		};

		return true;
	};

	@Override
	public void updateCR(IChemicalReactor reactor)
	{
		tickCounter++;

		if (tickCounter >= time)
		{
			tickCounter = 0;

			if (reactor.getTemperature() >= minTemp)
			{
				if (canReact(reactor))
				{
					int i;
					for (i=0; i<inputs.length; i++)
					{
						ItemStack stack = inputs[i];
						reactor.removeStack(stack);
					};
					for (i=0; i<outputs.length; i++)
					{
						ItemStack stack = outputs[i].copy();
						reactor.pushStack(stack);
					};
				};
			};
		};
	};
	
	/**
	 * Returns a list of ItemStacks for CraftGuide.
	 */
	public Object[] getCraftGuide()
	{
		ArrayList<Object> guide = new ArrayList<Object>();
		guide.add(new ItemStack(MaddTech.blockChemicalReactor, 1));
		guide.add(new Object[] {minTemp, time});
		
		if (inputs.length > 10)
		{
			return null;
		};
		
		if (outputs.length > 10)
		{
			return null;
		};
		
		for (ItemStack stack : inputs)
		{
			guide.add(stack);
		};
		
		int i;
		for (i=0; i<(10-inputs.length); i++)
		{
			guide.add(null);
		};
		
		for (ItemStack stack : outputs)
		{
			guide.add(stack);
		};
		
		for (i=0; i<(10-outputs.length); i++)
		{
			guide.add(null);
		};
		
		return guide.toArray();
	};
};
