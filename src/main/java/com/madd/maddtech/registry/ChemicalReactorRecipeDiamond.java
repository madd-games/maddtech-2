/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.registry;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;

import java.io.*;
import net.minecraft.entity.item.*;
import java.util.*;

public class ChemicalReactorRecipeDiamond implements IChemicalReactorRecipe
{
	@Override
	public void updateCR(IChemicalReactor reactor)
	{
		// you need a whole stack of coal blocks to do this, but only 7 blocks at a time get
		// converted to one diamond. You know why? Because fuck you, that's why.
		// Have fun wasting 57 blocks of coal.
		//ItemStack neededStack = new ItemStack(Blocks.coal_block, 64);

//		if (!reactor.canRemoveStack(neededStack))
//		{
//			return;
//		};

		if (reactor.getTemperature() < 10000)
		{
			return;
		};

		if (reactor.getPressure() < 50.0f)
		{
			return;
		};

		//reactor.removeStack(new ItemStack(Blocks.coal_block, 7));
		//reactor.pushStack(new ItemStack(Items.diamond, 1));
	};
};
