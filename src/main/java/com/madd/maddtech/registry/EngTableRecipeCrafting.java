/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * An engineering table "recipe" which actually carries over crafting recipies.
 */

package com.madd.maddtech.registry;

import com.madd.maddtech.api.*;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EngTableRecipeCrafting implements IEngTableRecipe
{
	public EngTableRecipeCrafting()
	{
		
	};
	
//	@Override
//	public String getName()
//	{
//		// XXX don't even need this shit
//		return "Crafting Recipe";
//	};

//	private boolean haveTool(TileEntityEngTable table, String type)
//	{
//		int i;
//		for (i=0; i<3; i++)
//		{
//			//ItemStack stack = table.getStackInSlot(table.TOOL_SLOT_BASE+i);
////			if (stack != null)
////			{
////				IEngTableTool tool = (IEngTableTool) stack.getItem();
////				if (tool.getToolType().equals(type))
////				{
////					if ((stack.getItem().getMaxDamage() == 0) || (stack.getItemDamage() < stack.getItem().getMaxDamage()))
////					{
////						return true;
////					};
////				};
////			};
//		};
//		
//		return false;
//	};

//	private boolean canMergeStack(ItemStack a, ItemStack b)
//	{
//		if ((a == null) || (b == null))
//		{
//			return true;
//		};
//		
//		if (a.getItem() != b.getItem())
//		{
//			return false;
//		};
//		
//		if (a.getItemDamage() != b.getItemDamage())
//		{
//			return false;
//		};
//		
//		if ((a.stackSize+b.stackSize) > a.getMaxStackSize())
//		{
//			return false;
//		};
//		
//		return true;
//	};

//	@Override
//	public boolean isMatchingRecipe(TileEntityEngTable table)
//	{
//		// make sure the "outside" is clear
//		int i;
//		for (i=0; i<5*5; i++)
//		{
//			int x = i % 5;
//			int y = i / 5;
//			
//			if ((x == 0 || x == 4) && (y == 0 || y == 4))
//			{
//				if (table.getStackInSlot(i + TileEntityEngTable.INPUT_SLOT_BASE) != null)
//				{
//					return false;
//				};
//			};
//		};
//		
//		// wrap the table into something we can use as an InventoryCrafting
//		EngCraftingWrapper wrapper = new EngCraftingWrapper(table);
//		
//		// figure out which item was crafted
//		ItemStack result = CraftingManager.getInstance().findMatchingRecipe(wrapper, table.getWorld());
//		
//		// if there is no result, declare no matching recipe
//		if (result == null)
//		{
//			return false;
//		};
//		
//		// otherwise, make sure there is a screwdriver in the engineering table
//		if (!haveTool(table, "screwdriver"))
//		{
//			return false;
//		};
//		
//		// finally, check if the output can be stored in the output area
//		return canMergeStack(result, table.getStackInSlot(TileEntityEngTable.OUTPUT_SLOT_BASE + 12));
//	};
//	
//	@Override
//	public ToolSpec[] getTools()
//	{
//		return new ToolSpec[] { new ToolSpec("screwdriver", 1) };
//	};
//	
//	@Override
//	public void execRecipe(TileEntityEngTable table)
//	{
//		// wrap the table into something we can use as an InventoryCrafting
//		EngCraftingWrapper wrapper = new EngCraftingWrapper(table);
//		
//		// figure out which item was crafted
//		ItemStack result = CraftingManager.getInstance().findMatchingRecipe(wrapper, table.getWorld());
//		
//		// idk what to do if no result, so just do nothing
//		if (result == null) return;
//		
//		// also if we can't merge...
//		if (!canMergeStack(result, table.getStackInSlot(TileEntityEngTable.OUTPUT_SLOT_BASE + 12))) return;
//		
//		// remove the input items
//		int slot;
//		for (slot=TileEntityEngTable.INPUT_SLOT_BASE; slot<TileEntityEngTable.INPUT_SLOT_BASE+5*5; slot++)
//		{
//			ItemStack stackInSlot = table.getStackInSlot(slot);
//			if (stackInSlot != null)
//			{
//				stackInSlot.stackSize--;
//				if (stackInSlot.stackSize == 0)
//				{
//					table.setInventorySlotContents(slot, null);
//				};
//			};
//		};
//		
//		// merge the output
//		ItemStack outStack = table.getStackInSlot(TileEntityEngTable.OUTPUT_SLOT_BASE + 12);
//		if (outStack == null)
//		{
//			table.setInventorySlotContents(TileEntityEngTable.OUTPUT_SLOT_BASE + 12, result.copy());
//		}
//		else
//		{
//			outStack.stackSize += result.stackSize;
//		};
//	};
};
