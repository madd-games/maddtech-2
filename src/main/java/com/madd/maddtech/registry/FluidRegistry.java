/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * FluidRegistry.java
 * The fluid registry.
 */

package com.madd.maddtech.registry;

import com.madd.maddtech.api.*;
import java.util.*;
import net.minecraft.item.*;

public class FluidRegistry implements IFluidRegistry
{
	private Hashtable<String, Fluid> fluids = new Hashtable<String, Fluid>();
	private Hashtable<Item, Fluid> buckets = new Hashtable<Item, Fluid>();
	private Hashtable<Fluid, Item> fluidItems = new Hashtable<Fluid, Item>();
	private Hashtable<Integer, String> mapForgeToMadd = new Hashtable<Integer, String>();
	private Hashtable<String, Integer> mapMaddToForge = new Hashtable<String, Integer>();
	
	public FluidRegistry()
	{
	};

	@Override
	public void add(Fluid fluid)
	{
		fluids.put(fluid.getID(), fluid);
	};

	public Fluid get(String id)
	{
		return fluids.get(id);
	};
	
	/**
	 * Returns the list of fluid IDs, replacing "maddtech:empty" with "*".
	 * Used by pumps.
	 */
	public String[] getFluids()
	{
		String[] list = new String[fluids.size()];
		int i = 0;
		
		for (String key : fluids.keySet())
		{
			if (key.equals("maddtech:empty"))
			{
				list[i++] = "*";
			}
			else
			{
				list[i++] = key;
			};
		};
		
		return list;
	};
	
	public Fluid fromBucket(ItemStack stack)
	{
		Item item = stack.getItem();
		if (buckets.containsKey(item))
		{
			return buckets.get(item);
		};
		
		return null;
	};
	
	public void addBucket(Item item, String fluidID)
	{
		buckets.put(item, fluids.get(fluidID));
		fluidItems.put(fluids.get(fluidID), item);
	};
	
	public ItemStack toBucket(Fluid fluid)
	{
		if (!fluidItems.containsKey(fluid))
		{
			return null;
		};
		
		Item item = fluidItems.get(fluid);
		return new ItemStack(item, 1);
	};
	
	public void addForgeFluid(int forgeID, String mtID)
	{
		mapForgeToMadd.put(forgeID, mtID);
		mapMaddToForge.put(mtID, forgeID);
	};
	
	public String fromForgeID(int forgeID)
	{
		if (!mapForgeToMadd.containsKey(forgeID))
		{
			return "maddtech:empty";
		};
		
		return mapForgeToMadd.get(forgeID);
	};
	
	public int toForgeID(String mtID)
	{
		if (!mapMaddToForge.containsKey(mtID))
		{
			return -1;
		};
		
		return mapMaddToForge.get(mtID);
	};
};
