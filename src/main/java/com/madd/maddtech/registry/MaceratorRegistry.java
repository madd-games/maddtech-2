/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.registry;

import com.madd.maddtech.api.*;
import net.minecraft.item.*;
import net.minecraft.block.*;
import java.util.*;

public class MaceratorRegistry implements IMaceratorRegistry
{
	public class Recipe
	{
		public ItemStack input;
		public ItemStack output;

		public Recipe(ItemStack input, ItemStack output)
		{
			this.input = input;
			this.output = output;
		};
	};

	private static MaceratorRegistry staticInstance = new MaceratorRegistry();
	private ArrayList<Recipe> recipies = new ArrayList<Recipe>();

	public MaceratorRegistry()
	{
	};

	public static MaceratorRegistry instance()
	{
		return staticInstance;
	};

	@Override
	public void addRecipe(ItemStack input, ItemStack output)
	{
		recipies.add(new Recipe(input, output));
	};

//	@Override
//	public ItemStack getResult(ItemStack input)
//	{
//		int i;
//		for (i=0; i<recipies.size(); i++)
//		{
//			Recipe recipe = recipies.get(i);
//			if (recipe.input.isItemEqual(input))
//			{
//				return recipe.output;
//			};
//		};
//
//		// also use the ore/ingot/dust rule from OreDictionary.
//		int[] ids = OreDictionary.getOreIDs(input);
//		for (i=0; i<ids.length; i++)
//		{
//			String name = OreDictionary.getOreName(ids[i]);
//			if (name.startsWith("ore"))
//			{
//				String dustname = "dust" + name.substring(3);
//				ArrayList<ItemStack> stacks = OreDictionary.getOres(dustname);
//				if (stacks.size() > 0)
//				{
//					ItemStack output = stacks.get(0).copy();
//					output.stackSize = 2;
//					return output;
//				};
//			}
//			else if (name.startsWith("ingot"))
//			{
//				String dustname = "dust" + name.substring(5);
//				ArrayList<ItemStack> stacks = OreDictionary.getOres(dustname);
//				if (stacks.size() > 0)
//				{
//					ItemStack output = stacks.get(0).copy();
//					output.stackSize = 1;
//					return output;
//				};
//			};
//		};
//
//		return null;
//	};
	
	public Recipe[] getRecipies()
	{
		return recipies.toArray(new Recipe[recipies.size()]);
	}

	@Override
	public ItemStack getResult(ItemStack input) {
		// TODO Auto-generated method stub
		return null;
	};
};
