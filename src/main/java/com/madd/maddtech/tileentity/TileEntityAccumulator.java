/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityAccumulator extends TileEntity implements ISidedInventory, IEnergyMachine
{
	public TileEntityAccumulator(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private ItemStack[] inv = new ItemStack[2];
	private int energy = 0;

	private int currentPowerIn = 0;
	private int currentPowerOut = 0;
	private int powerIn = 0;
	private int powerOut = 0;

//	@Override
//	public int getSizeInventory()
//	{
//		return 2;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "Accumulator";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setInteger("AccumulatorEnergy", energy);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//
//		energy = comp.getInteger("AccumulatorEnergy");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("AccumulatorEnergy", energy);
//		comp.setInteger("PowerIn", powerIn);
//		comp.setInteger("PowerOut", powerOut);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		energy = comp.getInteger("AccumulatorEnergy");
//		powerIn = comp.getInteger("PowerIn");
//		powerOut = comp.getInteger("PowerOut");
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//		if (!worldObj.isRemote)
//		{
//			int outside = getOutputSide();
//			int i;
//
//			if (energy < 30000)
//			{
//				SearchEnergy se = new SearchEnergy(worldObj);
//				for (i=0; i<6; i++)
//				{
//					if (i != outside)
//					{
//						se.execute(xCoord+SideUtil.getSideX(i), yCoord+SideUtil.getSideY(i),
//								zCoord+SideUtil.getSideZ(i), SideUtil.opposite(i));
//					};
//				};
//
//				int toPull = 30000 - energy;
//				int pulled = se.pullEnergy(toPull);
//				energy += pulled;
//				currentPowerIn += pulled;
//				
//				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//			};
//
//			powerIn = currentPowerIn;
//			powerOut = currentPowerOut;
//			currentPowerIn = 0;
//			currentPowerOut = 0;
//
//			ItemStack pullStack = inv[0];
//			if (pullStack != null)
//			{
//				Item item = pullStack.getItem();
//				if (item instanceof ItemBattery)
//				{
//					ItemBattery battery = (ItemBattery) item;
//					int energyNeeded = 30000 - energy;
//					energy += battery.pullEnergy(pullStack, energyNeeded);
//				}
//				else if (item == MaddTech.itemEnergyFiller)
//				{
//					energy = 30000;
//				}
//				else if (item instanceof IEnergyItem)
//				{
//					IEnergyItem ei = (IEnergyItem) item;
//					inv[0] = ei.onTopEnergySlot(pullStack, this);
//				};
//			};
//
//			ItemStack pushStack = inv[1];
//			if (pushStack != null)
//			{
//				Item item = pushStack.getItem();
//				if (item instanceof ItemBattery)
//				{
//					ItemBattery battery = (ItemBattery) item;
//					energy -= battery.pushEnergy(pushStack, energy);
//				}
//				else if (item instanceof IEnergyItem)
//				{
//					IEnergyItem ei = (IEnergyItem) item;
//					inv[1] = ei.onBottomEnergySlot(pushStack, this);
//				};
//			};
//
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//
//	public int getEnergy()
//	{
//		return energy;
//	};
//
//	public int getOutputSide()
//	{
//		return worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//	};
//
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return true;
//	};
//
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		if ((side != getOutputSide()) && (side != 6))
//		{
//			return 0;
//		}
//		else
//		{
//			if (max > energy)
//			{
//				max = energy;
//			};
//
//			energy -= max;
//			currentPowerOut += max;
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//			return max;
//		}
//	};
//
//	@Override
//	public int pushEnergy(int de, int side)
//	{
//		if (side == getOutputSide())
//		{
//			return 0;
//		};
//
//		// input power is regardless of whether the energy is stored or not.
//		currentPowerIn += de;
//
//		if ((energy+de) > 30000)
//		{
//			de = 30000 - energy;
//		};
//
//		energy += de;
//		markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		return de;
//	};
//
//	@Override
//	public void markDirty()
//	{
//		super.markDirty();
//		if (worldObj.getBlock(xCoord, yCoord+1, zCoord) == MaddTech.blockEnergyProbe)
//		{
//			int metadata;
//			if (energy == 0)
//			{
//				metadata = 1;
//			}
//			else
//			{
//				metadata = 0;
//			};
//			worldObj.setBlockMetadataWithNotify(xCoord, yCoord+1, zCoord, metadata, 3);
//		};
//	};
//
//	public int getPowerIn()
//	{
//		return powerIn;
//	};
//
//	public int getPowerOut()
//	{
//		return powerOut;
//	};
//	
//	public World getWorld()
//	{
//		return worldObj;
//	};
//	
//	@Override
//	public int[] getAccessibleSlotsFromSide(int side)
//	{
//		if (side == 0)
//		{
//			return new int[] {1};
//		}
//		else if (side == 1)
//		{
//			return new int[] {0};
//		};
//		
//		return new int[0];
//	};
//	
//	@Override
//	public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int side)
//	{
//		return side < 2;
//	};
//	
//	@Override
//	public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int side)
//	{
//		return side < 2;
//	}

	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	};
};
