/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityBarrel extends TileEntity implements ISidedInventory, IFluidMachine
{
	public TileEntityBarrel(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private ItemStack[] inv = new ItemStack[2];
	private String typeName;
	private int level;
	private int physLevel;
	private String fluidID = "maddtech:empty";
	private int fluidAmount = 0;

	private int currentFluidIn = 0;
	private int currentFluidOut = 0;
	private int fluidIn = 0;
	private int fluidOut = 0;
	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean canConnectFluid(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}



//	@Override
//	public boolean canConnectFluid(int side)
//	{
//		return side == 1;
//	};
//
//	@Override
//	public int getSizeInventory()
//	{
//		return 2;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "Barrel";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setString("BarrelType", typeName);
//		comp.setInteger("BarrelLevel", level);
//		comp.setInteger("BarrelPhysLevel", physLevel);
//		comp.setString("BarrelFluidID", fluidID);
//		comp.setInteger("BarrelFluidAmount", fluidAmount);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//
//		typeName = comp.getString("BarrelType");
//		level = comp.getInteger("BarrelLevel");
//		physLevel = comp.getInteger("BarrelPhysLevel");
//		fluidID = comp.getString("BarrelFluidID");
//		fluidAmount = comp.getInteger("BarrelFluidAmount");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		//this.writeToNBT(tag);
//		comp.setString("BarrelType", typeName);
//		comp.setInteger("BarrelLevel", level);
//		comp.setInteger("BarrelPhysLevel", physLevel);
//		comp.setString("BarrelFluidID", fluidID);
//		comp.setInteger("BarrelFluidAmount", fluidAmount);
//		comp.setInteger("BarrelFluidIn", fluidIn);
//		comp.setInteger("BarrelFluidOut", fluidOut);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		typeName = comp.getString("BarrelType");
//		level = comp.getInteger("BarrelLevel");
//		physLevel = comp.getInteger("BarrelPhysLevel");
//		fluidID = comp.getString("BarrelFluidID");
//		fluidAmount = comp.getInteger("BarrelFluidAmount");
//		fluidIn = comp.getInteger("BarrelFluidIn");
//		fluidOut = comp.getInteger("BarrelFluidOut");
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	public boolean canStoreFluid(String type)
//	{
//		Fluid fluid = MaddRegistry.fluids.get(type);
//		return ((fluid.getBarrelLevel() & 0xFF) <= level) && ((fluid.getBarrelLevel() & 0xFF00) <= physLevel);
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//		if (!worldObj.isRemote)
//		{
//			// like what what what
//			boolean haveUpdatedInventory = true;
//
//			fluidIn = currentFluidIn;
//			fluidOut = currentFluidOut;
//			currentFluidIn = 0;
//			currentFluidOut = 0;
//			
//			ItemStack pullStack = getStackInSlot(0);
//			if (pullStack != null)
//			{
//				Item item = pullStack.getItem();
//				if (item instanceof ItemCapsule)
//				{
//					ItemCapsule capsule = (ItemCapsule) item;
//					String cont = capsule.getContent(pullStack);
//					int amount = capsule.getAmount(pullStack);
//					if (amount > 0)
//					{
//						if (fluidID.equals("maddtech:empty"))
//						{
//							if (canStoreFluid(cont))
//							{
//								fluidID = cont;
//							};
//						};
//
//						if (fluidID.equals(cont))
//						{
//							int toPut = amount;
//							int maxPut = 100000 - fluidAmount;
//							if (toPut > maxPut)
//							{
//								toPut = maxPut;
//							};
//							int pulled = capsule.pullLiquid(pullStack, toPut);
//							fluidAmount += pulled;
//							setInventorySlotContents(0, pullStack);
//							haveUpdatedInventory = true;
//						};
//					};
//				}
//				else if (MaddRegistry.fluids.fromBucket(pullStack) != null)
//				{
//					int maxFill = 100000 - fluidAmount;
//					if (maxFill >= 1000)
//					{
//						Fluid fluid = MaddRegistry.fluids.fromBucket(pullStack);
//						if (canStoreFluid(fluid.getID()))
//						{
//							pushFluid(fluid.getID(), 1000, 1);
//							setInventorySlotContents(0, new ItemStack(Items.bucket, 1, 0));
//							haveUpdatedInventory = true;
//						};
//					};
//				}
//				else if (item instanceof IFiller)
//				{
//					if (getStackInSlot(1) == null)
//					{
//						IFiller filler = (IFiller) item;
//						String fluidToFill = filler.getFluidToFill();
//						if (canStoreFluid(fluidToFill))
//						{
//							fluidID = fluidToFill;
//							fluidAmount = 0;
//							pushFluid(fluidToFill, 100000, 1);
//							haveUpdatedInventory = true;
//						};
//					};
//				}
//				else if (item instanceof IFluidItem)
//				{
//					IFluidItem fi = (IFluidItem) item;
//					inv[0] = fi.onTopFluidSlot(pullStack, this);
//				};
//			};
//
//			ItemStack pushStack = getStackInSlot(1);
//			if (pushStack != null)
//			{
//				Item item = pushStack.getItem();
//				if (item instanceof ItemCapsule)
//				{
//					ItemCapsule capsule = (ItemCapsule) item;
//					int sub = capsule.addLiquid(pushStack, fluidID, fluidAmount);
//					if (sub > 0)
//					{
//						fluidAmount -= sub;
//					};
//					setInventorySlotContents(1, pushStack);
//					haveUpdatedInventory = true;
//				}
//				else if (MaddRegistry.fluids.toBucket(MaddRegistry.fluids.get(fluidID)) != null)
//				{
//					if (pushStack.getItem() == Items.bucket && pushStack.stackSize == 1)
//					{
//						if (fluidAmount >= 1000)
//						{
//							fluidAmount -= 1000;
//							setInventorySlotContents(1, MaddRegistry.fluids.toBucket(MaddRegistry.fluids.get(fluidID)));
//							haveUpdatedInventory = true;
//						};
//					};
//				}
//				else if (item == MaddTech.itemGasWelder)
//				{
//					int toPull = pushStack.getItemDamage();
//					if (toPull > fluidAmount)
//					{
//						toPull = fluidAmount;
//					};
//					pushStack.setItemDamage(pushStack.getItemDamage()-toPull);
//					fluidAmount -= toPull;
//					haveUpdatedInventory = true;
//				}
//				else if (item instanceof IFluidItem)
//				{
//					IFluidItem fi = (IFluidItem) item;
//					inv[1] = fi.onTopFluidSlot(pushStack, this);
//				};
//			};
//
//			if (haveUpdatedInventory)
//			{
//				markDirty();
//				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//			};
//		};
//	};
//
//	public String getType()
//	{
//		return typeName;
//	};
//
//	@Override
//	public String getFluidType(int side)
//	{
//		return getFluidID();
//	};
//
//	public String getFluidID()
//	{
//		return fluidID;
//	};
//
//	public int getFluidAmount()
//	{
//		return fluidAmount;
//	};
//
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		if (!id.equals(fluidID))
//		{
//			return 0;
//		};
//
//		if (max > fluidAmount)
//		{
//			max = fluidAmount;
//		};
//
//		fluidAmount -= max;
//		currentFluidOut += max;
//		if (max > 0)
//		{
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//
//		return max;
//	};
//
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		if (!canStoreFluid(id))
//		{
//			return 0;
//		};
//
//		if (fluidID.equals("maddtech:empty"))
//		{
//			fluidID = id;
//		};
//
//		if (!fluidID.equals(id))
//		{
//			return 0;
//		};
//
//		currentFluidIn += max;
//		int spaceLeft = 100000 - fluidAmount;
//		if (max > spaceLeft)
//		{
//			max = spaceLeft;
//		};
//
//		fluidAmount += max;
//		if (max > 0)
//		{
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//
//		return max;
//	};
//
//	public int getFluidIn()
//	{
//		return fluidIn;
//	};
//
//	public int getFluidOut()
//	{
//		return fluidOut;
//	};
//
//	@Override
//	public int[] getAccessibleSlotsFromSide(int side)
//	{
//		if (side == 0)
//		{
//			return new int[] {1};
//		}
//		else if (side == 1)
//		{
//			return new int[] {0};
//		};
//		
//		return new int[0];
//	};
//	
//	@Override
//	public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int side)
//	{
//		return side < 2;
//	};
//	
//	@Override
//	public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int side)
//	{
//		return side < 2;
//	};
};
