/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;

import com.madd.maddtech.api.*;
import com.madd.maddtech.multiblock.MachineStruct;

import java.io.*;
import net.minecraft.entity.item.*;
import net.minecraft.entity.player.PlayerEntity;

import java.util.*;

public class TileEntityChemicalReactor extends TileEntity implements IInventory, IChemicalReactor
{
	public TileEntityChemicalReactor(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private ItemStack[] inv = new ItemStack[9*8];
	private int temperature = 300;
	private MachineStruct struct = null;
	private int sideFacing = 0;
	private int signalStr = 0;
	private SearchFluid currentSearchFluidIn;
	private SearchFluid currentSearchFluidOut;
	private SearchEnergy currentSearchEnergyIn;
	private SearchEnergy currentSearchEnergyOut;
	private int redstonePowerIn = 0;
	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int getTemperature() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getRedstonePower() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void heatUp(int dt) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void pushStack(ItemStack stack) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int pullLiquid(String type, int amount) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isHot() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void updateBlock() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int countItems(ItemStack searchStack) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean canRemoveStack(ItemStack searchStack) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void removeStack(ItemStack searchStack) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int pullFluidFromCables(String id, int amount) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pushFluidToCables(String id, int amount) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void generateEnergy(int energy) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public float getPressure() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pullEnergyFromCables(int max) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
//	@Override
//	public int getSizeInventory()
//	{
//		return 9*8;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//		updateBlock();
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "Crate";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setInteger("Temperature", temperature);
//		comp.setInteger("ReactorRedstone", signalStr);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//
//		temperature = comp.getInteger("Temperature");
//		signalStr = comp.getInteger("ReactorRedstone");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("Temperature", temperature);
//		comp.setInteger("ReactorRedstone", signalStr);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		temperature = comp.getInteger("Temperature");
//		signalStr = comp.getInteger("ReactorRedstone");
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//
//		if (this.isInvalid())
//		{
//			return;
//		};
//
//		if (!worldObj.isRemote)
//		{
//			if (struct == null)
//			{
//				struct = LibTech.fitStruct(worldObj, xCoord, yCoord, zCoord, LibTech.machChemicalReactor);
//				if (struct == null)
//				{
//					BlockMaddChemicalReactor block = (BlockMaddChemicalReactor) worldObj.getBlock(xCoord, yCoord, zCoord);
//					block.pop(worldObj, xCoord, yCoord, zCoord);
//					invalidate();
//					return;
//				};
//				sideFacing = struct.sideFacing;
//			};
//
//			if (!struct.isFoundAt(worldObj, xCoord, yCoord, zCoord))
//			{
//				BlockMaddChemicalReactor block = (BlockMaddChemicalReactor) worldObj.getBlock(xCoord, yCoord, zCoord);
//				block.pop(worldObj, xCoord, yCoord, zCoord);
//				if (!worldObj.isRemote) invalidate();
//				return;
//			};
//
//			readRedstonePower();
//			float resist = 0.0F;
//			if (getRedstonePower() > 0)
//			{
//				/*
//				currentSearchFluid = new SearchFluid(worldObj);
//				currentSearchFluid.execute(xCoord, yCoord+3, zCoord, 0);
//				currentSearchEnergy = new SearchEnergy(worldObj);
//				currentSearchEnergy.execute(xCoord-5*SideUtil.getSideX(sideFacing), yCoord-5*SideUtil.getSideY(sideFacing),
//								zCoord-5*SideUtil.getSideZ(sideFacing), sideFacing);
//				*/
//				
//				currentSearchFluidIn = new SearchFluid(worldObj);
//				currentSearchFluidOut = new SearchFluid(worldObj);
//				currentSearchEnergyIn = new SearchEnergy(worldObj);
//				currentSearchEnergyOut = new SearchEnergy(worldObj);
//				
//				Coords coords = struct.getFunctionBlock("fluid_in");
//				if (coords != null)
//				{
//					currentSearchFluidIn.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 6);
//				};
//				
//				coords = struct.getFunctionBlock("fluid_out");
//				if (coords != null)
//				{
//					currentSearchFluidOut.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 6);
//				};
//				
//				coords = struct.getFunctionBlock("energy_in");
//				if (coords != null)
//				{
//					currentSearchEnergyIn.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 6);
//				};
//				
//				coords = struct.getFunctionBlock("energy_out");
//				if (coords != null)
//				{
//					currentSearchEnergyOut.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 6);
//				};
//
//				int i;
//				for (i=0; i<getSizeInventory(); i++)
//				{
//					ItemStack stack = getStackInSlot(i);
//					if (stack != null)
//					{
//						Item item = stack.getItem();
//						if (item instanceof IChemicalReactorComponent)
//						{
//							IChemicalReactorComponent comp = (IChemicalReactorComponent) item;
//							comp.updateCR(stack, this);
//						};
//
//						resist += MaddRegistry.thermalResistance.get(item) * stack.stackSize;
//					};
//				};
//
//				ChemicalReactorRegistry reg = (ChemicalReactorRegistry) MaddRegistry.chemicalReactor;
//				reg.updateCR(this);
//			};
//
//			float factor = 0.01F * (1.0F - (resist/(float)(9*8*64)));
//			int delta = (int)(-factor*(temperature-300));
//			if ((temperature > 300) && (delta > -1))
//			{
//				delta = -1;
//			};
//			if ((temperature < 300) && (delta < 1))
//			{
//				delta = 1;
//			};
//			heatUp(delta);
//			if (temperature < 0)
//			{
//				temperature = 0;
//			};
//
//			if (temperature >= 20000)
//			{
//				int i;
//				for (i=0; i<getSizeInventory(); i++)
//				{
//					setInventorySlotContents(i, null);
//				};
//
//				worldObj.createExplosion(null, xCoord, yCoord, zCoord, (float)(temperature/1000), true);
//				return; 	// i'm dead
//			};
//
//			if (getRedstonePower() != signalStr)
//			{
//				signalStr = getRedstonePower();
//			};
//
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	// CHEMICAL REACTOR STUFF.
//	@Override
//	public int getTemperature()
//	{
//		return temperature;
//	};
//
//	public void readRedstonePower()
//	{
//		if (struct == null)
//		{
//			redstonePowerIn = 0;
//			return;
//		};
//		
//		Coords coords = struct.getFunctionBlock("redstone_in");
//		if (coords == null)
//		{
//			redstonePowerIn = 0;
//			return;
//		};
//		
//		redstonePowerIn = worldObj.getStrongestIndirectPower(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z);
//	};
//
//	@Override
//	public int getRedstonePower()
//	{
//		return redstonePowerIn;
//	};
//	
//	@Override
//	public void heatUp(int dt)
//	{
//		temperature += dt;
//	};
//
//	@Override
//	public float getPressure()
//	{
//		int numItems = 0;
//		int i;
//
//		for (i=0; i<9*8; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				numItems += stack.stackSize;
//			};
//		};
//
//		return (float)numItems / 72.0f;
//	};
//
//	@Override
//	public void pushStack(ItemStack stack)
//	{
//		int i;
//		for (i=0; i<getSizeInventory(); i++)
//		{
//			ItemStack rstack = getStackInSlot(i);
//			if (rstack == null)
//			{
//				setInventorySlotContents(i, stack);
//				return;
//			};
//
//			if (rstack.getItem() == stack.getItem())
//			{
//				int toPut = stack.stackSize;
//				int maxPut = rstack.getItem().getItemStackLimit(rstack) - rstack.stackSize;
//				if (toPut > maxPut)
//				{
//					toPut = maxPut;
//				};
//				rstack.stackSize += toPut;
//				stack.stackSize -= toPut;
//				if (stack.stackSize == 0)
//				{
//					return;
//				};
//				setInventorySlotContents(i, rstack);
//			};
//		};
//
//		Random rand = new Random();
//
//		float rx = rand.nextFloat() * 0.8F + 0.1F;
//		float ry = rand.nextFloat() * 0.8F + 0.1F;
//		float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//		EntityItem entityItem = new EntityItem(worldObj,
//			xCoord + rx, yCoord + ry, zCoord + rz,
//			new ItemStack(stack.getItem(), stack.stackSize, stack.getItemDamage()));
//        		
//		if (stack.hasTagCompound())
//		{
//			entityItem.getEntityItem().setTagCompound((NBTTagCompound) stack.getTagCompound().copy());
//		}
//
//		float factor = 0.05F;
//		entityItem.motionX = rand.nextGaussian() * factor;
//		entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//		entityItem.motionZ = rand.nextGaussian() * factor;
//		worldObj.spawnEntityInWorld(entityItem);
//	};
//
//	@Override
//	public int pullLiquid(String type, int amount)
//	{
//		if (getRedstonePower() == 0)
//		{
//			// Cannot pull liquids if the reactor is off.
//			return 0;
//		};
//
//		int liquidSoFar = 0;
//		int i;
//		for (i=0; i<getSizeInventory(); i++)
//		{
//			if (liquidSoFar == amount) break;
//			ItemStack stack = getStackInSlot(i);
//			if (stack != null)
//			{
//				Item item = stack.getItem();
//				if (item instanceof ItemCapsule)
//				{
//					ItemCapsule capsule = (ItemCapsule) item;
//					if (capsule.getContent(stack).equals(type))
//					{
//						int liquidToPull = amount - liquidSoFar;
//						liquidSoFar += capsule.pullLiquid(stack, liquidToPull);
//					};
//				};
//			};
//		};
//
//		return liquidSoFar;
//	};
//
//	@Override
//	public boolean isHot()
//	{
//		return (temperature > 370) || (temperature < 250);
//	};
//
//	@Override
//	public void updateBlock()
//	{
//
//	};
//
//	public boolean canTakeStack(EntityPlayer player)
//	{
//		int i;
//		for (i=0; i<4; i++)
//		{
//			ItemStack stack = player.inventory.armorInventory[i];
//			if (stack != null)
//			{
//				if (stack.getItem() == MaddTech.itemSafetySuit)
//				{
//					return true;
//				};
//			};
//		};
//
//		return !isHot();
//	};
//
//	@Override
//	public int countItems(ItemStack searchStack)
//	{
//		int count = 0;
//		int i;
//		for (i=0; i<getSizeInventory(); i++)
//		{
//			ItemStack stack = getStackInSlot(i);
//			if (stack != null)
//			{
//				if (stack.isItemEqual(searchStack))
//				{
//					count += stack.stackSize;
//				};
//			};
//		};
//
//		return count;
//	};
//
//	@Override
//	public boolean canRemoveStack(ItemStack searchStack)
//	{
//		return countItems(searchStack) >= searchStack.stackSize;
//	};
//
//	@Override
//	public void removeStack(ItemStack searchStack)
//	{
//		int toRemove = searchStack.stackSize;
//		int i;
//		for (i=0; i<getSizeInventory(); i++)
//		{
//			if (toRemove == 0) break;
//
//			ItemStack stack = getStackInSlot(i);
//			if (stack != null)
//			{
//				if (stack.isItemEqual(searchStack))
//				{
//					int canRemove = stack.stackSize;
//					if (canRemove > toRemove)
//					{
//						canRemove = toRemove;
//					};
//					stack.stackSize -= canRemove;
//					if (stack.stackSize == 0)
//					{
//						setInventorySlotContents(i, null);
//					};
//
//					toRemove -= canRemove;
//				};
//			};
//		};
//	};
//
//	@Override
//	public int pullFluidFromCables(String id, int amount)
//	{
//		return currentSearchFluidIn.pullFluid(id, amount);
//	};
//
//	@Override
//	public int pushFluidToCables(String id, int amount)
//	{
//		return currentSearchFluidOut.pushFluid(id, amount);
//	};
//
//	@Override
//	public void generateEnergy(int energy)
//	{
//		currentSearchEnergyOut.pushEnergy(energy);
//	};
//
//	@Override
//	public int pullEnergyFromCables(int max)
//	{
//		return currentSearchEnergyIn.pullEnergy(max);
//	};
};
