/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.block.*;

import com.madd.maddtech.api.*;
import com.madd.maddtech.multiblock.MachineStruct;

import java.io.*;
import net.minecraft.entity.item.*;
import java.util.*;

public class TileEntityCompressor extends TileEntity implements IFluidMachine
{
	public TileEntityCompressor(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private MachineStruct struct = null;
	private int sideFacing = 0;
	private SearchFluid currentSearchFluidIn;
	private SearchFluid currentSearchFluidOut;
	private SearchEnergy currentSearchEnergyIn;

	private String content = "maddtech:empty";
	private int pressure = 1000;
	private int temperature = 300;
	@Override
	public boolean canConnectFluid(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setString("Content", content);
//		comp.setInteger("Temperature", temperature);
//		comp.setInteger("Pressure", pressure);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		content = comp.getString("Content");
//		temperature = comp.getInteger("Temperature");
//		pressure = comp.getInteger("Pressure");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setString("Content", content);
//		comp.setInteger("Temperature", temperature);
//		comp.setInteger("Pressure", pressure);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		content = comp.getString("Content");
//		temperature = comp.getInteger("Temperature");
//		pressure = comp.getInteger("Pressure");
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//
//		if (this.isInvalid())
//		{
//			return;
//		};
//
//		if (!worldObj.isRemote)
//		{
//			if (struct == null)
//			{
//				struct = LibTech.fitStruct(worldObj, xCoord, yCoord, zCoord, LibTech.machCompressor);
//				if (struct == null)
//				{
//					BlockMaddCompressor block = (BlockMaddCompressor) worldObj.getBlock(xCoord, yCoord, zCoord);
//					block.pop(worldObj, xCoord, yCoord, zCoord);
//					invalidate();
//					return;
//				};
//				sideFacing = struct.sideFacing;
//			};
//
//			if (!struct.isFoundAt(worldObj, xCoord, yCoord, zCoord))
//			{
//				BlockMaddCompressor block = (BlockMaddCompressor) worldObj.getBlock(xCoord, yCoord, zCoord);
//				block.pop(worldObj, xCoord, yCoord, zCoord);
//				if (!worldObj.isRemote) invalidate();
//				return;
//			};
//			
//			Coords coords = struct.getFunctionBlock("energy_in");
//			int energyIn = 0;
//			if (coords != null)
//			{
//				int x = coords.x + xCoord;
//				int y = coords.y + yCoord;
//				int z = coords.z + zCoord;
//				
//				SearchEnergy se = new SearchEnergy(worldObj);
//				se.execute(x, y, z, 0);
//				
//				energyIn = se.pullEnergy(25);
//			};
//			
//			int deltaPressure = 50 * energyIn / 25;
//			
//			if (pressure >= 5000)
//			{
//				if (deltaPressure > 10) deltaPressure = 10;
//			};
//			
//			if (pressure >= 8000)
//			{
//				if (deltaPressure > 1) deltaPressure = 1;
//			};
//			
//			if (pressure >= 10000)
//			{
//				deltaPressure = 0;
//			};
//			
//			double volume = (double) pressure / (double) temperature;
//			pressure += deltaPressure;
//			temperature = (int) ((double)pressure / volume);
//
//			float factor = 0.0005F;
//			int delta = (int)(-factor*(temperature-300));
//			
//			if (energyIn == 0)
//			{
//				// if we're not pressurizing then it's ok to lose/gain heat
//				if ((temperature < 300) && (delta < 1))
//				{
//					delta = 1;
//				};
//				
//				if ((temperature > 300) && (delta > -1))
//				{
//					delta = -1;
//				};
//			};
//			
//			temperature += delta;
//			
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//	
//	public int getTemperature()
//	{
//		return temperature;
//	};
//	
//	public int getPressure()
//	{
//		return pressure;
//	};
//	
//	/**
//	 * Returns the temperature of released fluid, i.e. the effective temperature after pressure
//	 * drops to one atmosphere.
//	 */
//	public int getOutTemperature()
//	{
//		double volume = (double) pressure / (double) temperature;
//		return (int) (1000.0 / volume);
//	};
//	
//	@Override
//	public boolean canConnectFluid(int side)
//	{
//		// can only pull fluids from the shell output ("side 7")
//		return side == 7;
//	};
//	
//	@Override
//	public String getFluidType(int side)
//	{
//		// TODO: other fluids
//		return "maddtech:naq";
//	};
//	
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		if (id.equals("maddtech:naq"))
//		{
//			if (side == 7)
//			{
//				if (max > 1000)
//				{
//					max = 1000;
//				};
//				
//				int deltaPressure = max/20;
//				if ((pressure-deltaPressure) < 1000)
//				{
//					deltaPressure = pressure - 1000;
//				};
//				
//				double volume = (double) pressure / (double) temperature;
//				pressure -= deltaPressure;
//				temperature = (int) ((double)pressure / volume);
//				
//				if (getOutTemperature() < 70)
//				{
//					return deltaPressure * 20;
//				};
//			};
//		};
//		
//		return 0;
//	};
//	
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		return 0;
//	};
};
