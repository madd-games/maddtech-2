/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.item.*;
import net.minecraft.item.crafting.*;
import net.minecraft.nbt.*;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.block.*;
import net.minecraft.entity.player.PlayerEntity;

import com.madd.maddtech.api.*;
import com.madd.maddtech.multiblock.MachineStruct;

public class TileEntityElectricFurnace extends TileEntityProc
{
	public TileEntityElectricFurnace(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private int progress = 0;
	private MachineStruct struct = null;
	private int sideFacing = 0;
	public int cookTime = 192;
	private int energyBuffer = 0;
	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}


//	public void feedEnergy(int energy)
//	{
//		energyBuffer += energy;
//	};
//	
//	@Override
//	public String getMachineName()
//	{
//		return "maddtech.electricFurnace";
//	};
//
//	private int getTimeToCook()
//	{
//		int time = (int) (192.0F * (1.0F + getTimeIncrease()));
//		if (!worldObj.isRemote)
//		{
//			cookTime = time;
//		};
//		return cookTime;
//	};
//
//	private int getInputPower()
//	{
//		return (int) ((((float)(192 * 20)) * (1.0F + getPowerIncrease())) / (float) getTimeToCook());
//	};
//
//	@Override
//	public int getMachineProgress(int scale)
//	{
//		if (getTimeToCook() == 0)
//		{
//			return scale;
//		};
//
//		return progress * scale / getTimeToCook();
//	};
//
//	@Override
//	public void writeProcStateToNBT(NBTTagCompound comp)
//	{
//		comp.setInteger("SmeltingProgress", progress);
//		comp.setInteger("CookTime", cookTime);
//		comp.setInteger("FurnaceEnergyBuffer", energyBuffer);
//	};
//
//	@Override
//	public void readProcStateFromNBT(NBTTagCompound comp)
//	{
//		progress = comp.getInteger("SmeltingProgress");
//		cookTime = comp.getInteger("CookTime");
//		energyBuffer = comp.getInteger("FurnaceEnergyBuffer");
//	};
//
//	private boolean canSmelt()
//	{
//		ItemStack input = getStackInSlot(0);
//		ItemStack output = getStackInSlot(1);
//
//		if (input == null)
//		{
//			return false;
//		};
//
//		ItemStack result = FurnaceRecipes.smelting().getSmeltingResult(input);
//
//		if (result == null)
//		{
//			return false;
//		};
//
//		if (output == null)
//		{
//			return true;
//		};
//
//		if (!result.isItemEqual(output))
//		{
//			return false;
//		};
//
//		int totalSize = output.stackSize + result.stackSize;
//		return totalSize <= getInventoryStackLimit() && totalSize <= output.getMaxStackSize();
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//
//		if (this.isInvalid())
//		{
//			return;
//		};
//
//		if (!worldObj.isRemote)
//		{
//			if (struct == null)
//			{
//				struct = LibTech.fitStruct(worldObj, xCoord, yCoord, zCoord, LibTech.machElectricFurnace);
//				if (struct == null)
//				{
//					BlockElectricFurnace block = (BlockElectricFurnace) worldObj.getBlock(xCoord, yCoord, zCoord);
//					block.pop(worldObj, xCoord, yCoord, zCoord);
//					invalidate();
//					return;
//				};
//				sideFacing = struct.sideFacing;
//			};
//
//			if (!struct.isFoundAt(worldObj, xCoord, yCoord, zCoord))
//			{
//				BlockElectricFurnace block = (BlockElectricFurnace) worldObj.getBlock(xCoord, yCoord, zCoord);
//				block.pop(worldObj, xCoord, yCoord, zCoord);
//				if (!worldObj.isRemote) invalidate();
//				return;
//			};
//
//
//			Block block = worldObj.getBlock(xCoord, yCoord, zCoord);
//			Block newBlock = block;
//			if (canSmelt())
//			{
//				Coords coords = struct.getFunctionBlock("energy_in");
//				int power = getInputPower();
//				if (coords != null)
//				{
//					SearchEnergy se = new SearchEnergy(worldObj);
//					se.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 6);
//					int energyToPull = power - energyBuffer;
//					int gotEnergy = se.pullEnergy(power);
//					energyBuffer += gotEnergy;
//				};
//				
//				if (energyBuffer < power)
//				{
//					if (block != MaddTech.blockElectricFurnaceOff)
//					{
//						newBlock = MaddTech.blockElectricFurnaceOff;
//					};
//				}
//				else
//				{
//					energyBuffer -= power;
//					progress++;
//					if (progress >= getTimeToCook())
//					{
//						doSmelt();
//					};
//
//					if (block != MaddTech.blockElectricFurnaceOn)
//					{
//						newBlock = MaddTech.blockElectricFurnaceOn;
//					};
//				};
//			}
//			else
//			{
//				progress = 0;
//				if (block != MaddTech.blockElectricFurnaceOff)
//				{
//					newBlock = MaddTech.blockElectricFurnaceOff;
//				};
//			};
//
//			BlockElectricFurnace.shouldDropItems = false;
//			setBlockTo(newBlock);
//			BlockElectricFurnace.shouldDropItems = true;
//
//			Coords coords = struct.getFunctionBlock("redstone_out");
//			if (coords != null)
//			{
//				worldObj.notifyBlocksOfNeighborChange(
//					xCoord+coords.x, yCoord+coords.y, zCoord+coords.z,
//					MaddTech.blockTinShellRedstoneOut);		/* doesn't actually matter which block */
//			};
//
//			markDirty();
//		};
//	};
//
//	private void doSmelt()
//	{
//		ItemStack input = getStackInSlot(0);
//		ItemStack output = getStackInSlot(1);
//		ItemStack result = FurnaceRecipes.smelting().getSmeltingResult(input);
//
//		input.stackSize--;
//		if (input.stackSize == 0)
//		{
//			setInventorySlotContents(0, null);
//		};
//
//		if (output == null)
//		{
//			setInventorySlotContents(1, result.copy());
//		}
//		else
//		{
//			output.stackSize += result.stackSize;
//		};
//
//		progress = 0;
//	};
};
