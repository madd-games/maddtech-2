/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.*;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;

import net.minecraft.world.*;
import com.madd.maddtech.api.*;

public class TileEntityEngTable extends TileEntity implements ISidedInventory, IEnergyMachine
{
	public TileEntityEngTable(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private ItemStack[] inv = new ItemStack[56];
	
	public static int INPUT_SLOT_BASE = 0;
	public static int OUTPUT_SLOT_BASE = INPUT_SLOT_BASE + (5*5);
	public static int TOOL_SLOT_BASE = OUTPUT_SLOT_BASE + (5*5);
	public static int TEMPLATE_LOAD_SLOT = TOOL_SLOT_BASE + 3;
	public static int TEMPLATE_SAVE_SLOT = TOOL_SLOT_BASE + 4;
	public static int INV_SIZE = TOOL_SLOT_BASE + 5;
	
	private IEngTableRecipe currentRecipe = null;
	private int progress;				// numbers of ticks that we work on the specified recipe
	private int duration;				// total time to work on this recipe
	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
//	@Override
//	public int getSizeInventory()
//	{
//		return INV_SIZE;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "EngTable";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		if (i == TEMPLATE_SAVE_SLOT)
//		{
//			if (istack.getItem() == Items.paper) return true;
//			else return false;
//		}
//		else if (i == TEMPLATE_LOAD_SLOT)
//		{
//			if (istack.getItem() == MaddTech.itemTemplate) return true;
//			else return false;
//		}
//		else
//		{
//			return true;
//		}
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound tag = new NBTTagCompound();
//		tag.setInteger("Progress", progress);
//		tag.setInteger("Duration", duration);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, tag);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound tag = pkt.func_148857_g();
//		progress = tag.getInteger("Progress");
//		duration = tag.getInteger("Duration");
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//	
//	private int getDurationOfUnitTool(String type)
//	{
//		int duration = 0;
//		int i;
//		for (i=0; i<3; i++)
//		{
//			ItemStack stack = getStackInSlot(TOOL_SLOT_BASE+i);
//			if (stack != null)
//			{
//				Item item = stack.getItem();
//				if ((item.getMaxDamage() == 0) || (stack.getItemDamage() < item.getMaxDamage()))
//				{
//					if (item instanceof IEngTableTool)
//					{
//						IEngTableTool tool = (IEngTableTool) item;
//						if (tool.getToolType().equals(type))
//						{
//							int dur = tool.getTimeToWork();
//							if (dur > duration) duration = dur;
//						};
//					};
//				};
//			};
//		};
//		
//		return duration;
//	};
//	
//	private int getProgressForTool(String type)
//	{
//		int count = 0;
//		int i;
//		for (i=0; i<3; i++)
//		{
//			ItemStack stack = getStackInSlot(TOOL_SLOT_BASE+i);
//			if (stack != null)
//			{
//				Item item = stack.getItem();
//				if (item instanceof IEngTableTool)
//				{
//					IEngTableTool tool = (IEngTableTool) item;
//					if (tool.getToolType().equals(type))
//					{
//						count++;
//					};
//				};
//			};
//		};
//		
//		return count;
//	};
//	
//	private int getProgressPerTick()
//	{
//		int prog = 0;
//		
//		ToolSpec[] tools = currentRecipe.getTools();
//		int i;
//		for (i=0; i<tools.length; i++)
//		{
//			prog += getProgressForTool(tools[i].type);
//		};
//		
//		return prog;
//	};
//	
//	private int getPowerUse()
//	{
//		int power = 0;
//		int i;
//		for (i=0; i<3; i++)
//		{
//			int slot = TOOL_SLOT_BASE + i;
//			ItemStack stack = getStackInSlot(slot);
//			
//			if (stack != null)
//			{
//				Item item = stack.getItem();
//				if (item instanceof IEngTableTool)
//				{
//					IEngTableTool tool = (IEngTableTool) item;
//					power += tool.getToolPower();
//				};
//			};
//		};
//		
//		return power;
//	};
//	
//	private void reset()
//	{
//		if (currentRecipe == null)
//		{
//			duration = 1;
//			progress = 0;
//		}
//		else
//		{
//			duration = 0;
//			
//			ToolSpec[] tools = currentRecipe.getTools();
//			int i;
//			for (i=0; i<tools.length; i++)
//			{
//				duration += getDurationOfUnitTool(tools[i].type) * tools[i].count;
//			};
//			
//			if (duration == 0)
//			{
//				duration = 1;
//			};
//			
//			progress = 0;
//		};
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			if (inv[TEMPLATE_SAVE_SLOT] != null)
//			{
//				ItemStack istack = inv[TEMPLATE_SAVE_SLOT];
//				if (istack.getItem() == Items.paper || istack.getItem() == MaddTech.itemTemplate)
//				{
//					ItemStack newStack = new ItemStack(MaddTech.itemTemplate, 1);
//					ItemTemplate templ = (ItemTemplate) MaddTech.itemTemplate;
//					templ.setLayout(newStack, this);
//					inv[TEMPLATE_SAVE_SLOT] = newStack;
//					markDirty();
//					worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//				};
//			};
//			
//			if (inv[TEMPLATE_LOAD_SLOT] != null)
//			{
//				ItemStack istack = inv[TEMPLATE_LOAD_SLOT];
//				if (istack.getItem() == MaddTech.itemTemplate && istack.hasTagCompound())
//				{
//					if (worldObj.getStrongestIndirectPower(xCoord, yCoord, zCoord) > 0)
//					{
//						// template is loaded and we are receiving a redstone signal: try to
//						// pull items into any slots which currently lack one
//						NBTTagCompound comp = istack.getTagCompound();
//						NBTTagList itemList = comp.getTagList("Inputs", comp.getId());
//						
//						SearchItem si = new SearchItem(worldObj);
//						si.execute(xCoord, yCoord-1, zCoord, 1);
//						
//						boolean dirty = false;
//						
//						int i;
//						for (i=0; i<itemList.tagCount(); i++)
//						{
//							NBTTagCompound tag = itemList.getCompoundTagAt(i);
//							ItemStack ref = ItemStack.loadItemStackFromNBT(tag);
//							int slot = (int) tag.getByte("Slot") + INPUT_SLOT_BASE;
//							
//							if (inv[slot] == null)
//							{
//								ItemStack s = si.pullStack(ref);
//								if (s != null)
//								{
//									inv[slot] = s;
//									dirty = true;
//								}
//								else
//								{
//									if (dirty)
//									{
//										markDirty();
//										worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//									};
//									
//									return;
//								};
//							};
//						};
//
//						if (dirty)
//						{
//							markDirty();
//							worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//						};
//					};
//				};
//			};
//
//			if (currentRecipe == null)
//			{
//				currentRecipe = MaddRegistry.engTable.get(this);
//				reset();
//				if (currentRecipe == null)
//				{
//					markDirty();
//					worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//					return;
//				};
//			};
//			
//			if (!currentRecipe.isMatchingRecipe(this))
//			{
//				currentRecipe = null;
//				reset();
//				markDirty();
//				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//				return;
//			};
//			
//			int power = getPowerUse();
//			SearchEnergy se = new SearchEnergy(worldObj);
//			int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//			int inputSide = 2 + (meta % 4);
//			
//			se.execute(xCoord+SideUtil.getSideX(inputSide), yCoord+SideUtil.getSideY(inputSide),
//				zCoord+SideUtil.getSideZ(inputSide), SideUtil.opposite(inputSide));
//			
//			int gotPower = se.pullEnergy(power);
//			
//			int rate = 1;
//			if (power != 0)
//			{
//				rate = gotPower / power;
//			};
//			
//			if (currentRecipe != null)
//			{	
//				if (progress < duration)
//				{
//					progress += getProgressPerTick() * rate;
//				}
//				else
//				{
//					progress = 0;
//					currentRecipe.execRecipe(this);
//				};
//			};
//
//			int i;
//			for (i=0; i<3; i++)
//			{
//				int slot = TOOL_SLOT_BASE + i;
//				ItemStack stack = getStackInSlot(slot);
//				if (stack != null)
//				{
//					if ((stack.getItem().getMaxDamage() != 0) && (stack.getItemDamage() < stack.getItem().getMaxDamage()))
//					{
//						stack.setItemDamage(stack.getItemDamage()+1);
//					};
//				};
//			};
//			
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//	
//	public int getProgressScaled(int scale)
//	{
//		if (duration == 0)
//		{
//			return 0;
//		};
//		
//		return progress * scale / duration;
//	};
//	
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//		int inputSide = 2 + (meta%4);
//		return side == inputSide;
//	};
//	
//	@Override
//	public int pushEnergy(int energy, int side)
//	{
//		return 0;
//	};
//	
//	@Override
//	public int pullEnergy(int energy, int side)
//	{
//		return 0;
//	};
//	
//	@Override
//	public int[] getAccessibleSlotsFromSide(int side)
//	{
//		if (side == 0)
//		{
//			// bottom side is for input
//			int[] list = new int[5*5];
//			int i;
//			for (i=0; i<5*5; i++)
//			{
//				list[i] = i + INPUT_SLOT_BASE;
//			};
//			return list;
//		}
//		else if (side == 1)
//		{
//			// top side is for output
//			int[] list = new int[5*5];
//			int i;
//			for (i=0; i<5*5; i++)
//			{
//				list[i] = i + OUTPUT_SLOT_BASE;
//			};
//			return list;
//		}
//		else
//		{
//			// nothing is accessible from other sides
//			return new int[0];
//		}
//	};
//	
//	@Override
//	public boolean canInsertItem(int slot, ItemStack stack, int side)
//	{
//		if (side == 0)
//		{
//			// we can insert items into the input slots from the bottom side
//			return (slot >= INPUT_SLOT_BASE && slot < OUTPUT_SLOT_BASE);
//		};
//		
//		// cannot automatically insert anywhere else
//		return false;
//	};
//	
//	@Override
//	public boolean canExtractItem(int slot, ItemStack stack, int side)
//	{
//		if (side == 1)
//		{
//			// we can extract items from the output slots via the top side
//			return (slot >= OUTPUT_SLOT_BASE && slot < TOOL_SLOT_BASE);
//		};
//		
//		// cannot extract anywhere else
//		return false;
//	};
//	
//	public World getWorld()
//	{
//		return worldObj;
//	};
};
