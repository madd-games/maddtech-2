/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

public class TileEntityFluidDispenser extends TileEntity implements IFluidMachine
{
	public TileEntityFluidDispenser(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private boolean dispensed = false;

	@Override
	public boolean canConnectFluid(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

//	@Override
//	public void updateEntity()
//	{
//		dispensed = false;
//
//	};
//
//	@Override
//	public boolean canConnectFluid(int side)
//	{
//		int inputSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord) + 2;
//		return side == inputSide;
//	};
//
//	@Override
//	public String getFluidType(int side)
//	{
//		return "maddtech:empty";
//	};
//
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		return 0;
//	};
//
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		Fluid fluid = MaddRegistry.fluids.get(id);
//		int inputSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord) + 2;
//
//		if (side == inputSide && dispensed == false)
//		{
//			int x = xCoord - SideUtil.getSideX(side);
//			int y = yCoord - SideUtil.getSideY(side);
//			int z = zCoord - SideUtil.getSideZ(side);
//			dispensed = true;
//
//			return max - fluid.onDispense(worldObj, x, y, z, max);
//		}
//		else
//		{
//			return 0;
//		}
//	};
};
