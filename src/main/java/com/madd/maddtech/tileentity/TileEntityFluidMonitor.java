/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityFluidMonitor extends TileEntity
{
	public TileEntityFluidMonitor(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}


	/**
	 * OPS:
	 * 0 - countOfItem < opParam
	 * 1 - countOfItem <= opParam
	 * 2 - countOfItem == opParam
	 * 3 - countOfItem >= opParam
	 * 4 - countOfItem > opParam
	 * 5 - countOfItem != opParam
	 */
	private int whichOp = 0;
	private int opParam = 0;
	
	private static String[] opNames = new String[] {
		"<", "<=", "==", ">=", ">", "!="
	};


	private boolean evaluateOp(int count)
	{
		switch (whichOp)
		{
		case 0:
			return count < opParam;
		case 1:
			return count <= opParam;
		case 2:
			return count == opParam;
		case 3:
			return count >= opParam;
		case 4:
			return count > opParam;
		default:
			return count != opParam;
		}
	};

//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			TileEntity te = worldObj.getTileEntity(xCoord, yCoord+1, zCoord);
//			if (te != null)
//			{
//				if (te instanceof TileEntityBarrel)
//				{
//					TileEntityBarrel barrel = (TileEntityBarrel) te;
//					
//					int newMeta = 0;
//					if (evaluateOp(barrel.getFluidAmount()))
//					{
//						newMeta = 8;
//					};
//					
//					worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, newMeta, 3);
//				};
//			};
//		};
//	};
//
//	private void writeState(NBTTagCompound comp)
//	{
//		comp.setInteger("FluidOp", whichOp);
//		comp.setInteger("FluidParam", opParam);
//	};
//	
//	private void readState(NBTTagCompound comp)
//	{
//		whichOp = comp.getInteger("FluidOp");
//		opParam = comp.getInteger("FluidParam");
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		writeState(comp);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		readState(comp);
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		writeState(comp);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		readState(comp);
//	};
//
//	public String getOpName()
//	{
//		return opNames[whichOp];
//	};
//	
//	public void setOptions(int which, int param)
//	{
//		whichOp = which;
//		opParam = param;
//		markDirty();
//	};
//	
//	public String nextOp()
//	{
//		whichOp = (whichOp+1) % 6;
//		
//		PacketFluidMonitorUpdate update = new PacketFluidMonitorUpdate(worldObj, xCoord, yCoord, zCoord, whichOp, opParam);
//		LibTech.network.sendToServer(update);
//		
//		return getOpName();
//	};
//
//	public void setParam(int param)
//	{
//		opParam = param;
//		
//		PacketFluidMonitorUpdate update = new PacketFluidMonitorUpdate(worldObj, xCoord, yCoord, zCoord, whichOp, opParam);
//		LibTech.network.sendToServer(update);
//	};
//	
//	public int getParam()
//	{
//		return opParam;
//	};
};
