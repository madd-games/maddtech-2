/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityFluidShop extends TileEntity implements ISidedInventory, IFluidMachine, IItemMachine
{
	public TileEntityFluidShop(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private ItemStack[] inv = new ItemStack[3*9];
	private int fluidBuffer;
	private int fluidPerRuby;
	private int fluidRelease;
	private String fluidID = "maddtech:water";
	private String ownerName = "";
	
	private static final int MAX_FLUID = 25000;

	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canConnectItem(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canConnectFluid(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	

//	@Override
//	public int getSizeInventory()
//	{
//		return inv.length;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "FluidShop";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setInteger("FluidBuffer", fluidBuffer);
//		comp.setInteger("FluidPerRuby", fluidPerRuby);
//		comp.setInteger("FluidRelease", fluidRelease);
//		comp.setString("FluidID", fluidID);
//		comp.setString("Owner", ownerName);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//
//		fluidBuffer = comp.getInteger("FluidBuffer");
//		fluidPerRuby = comp.getInteger("FluidPerRuby");
//		fluidRelease = comp.getInteger("FluidRelease");
//		fluidID = comp.getString("FluidID");
//		ownerName = comp.getString("Owner");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("FluidBuffer", fluidBuffer);
//		comp.setInteger("FluidPerRuby", fluidPerRuby);
//		comp.setInteger("FluidRelease", fluidRelease);
//		comp.setString("FluidID", fluidID);
//		comp.setString("Owner", ownerName);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		fluidBuffer = comp.getInteger("FluidBuffer");
//		fluidPerRuby = comp.getInteger("FluidPerRuby");
//		fluidRelease = comp.getInteger("FluidRelease");
//		fluidID = comp.getString("FluidID");
//		ownerName = comp.getString("Owner");
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//		
//		if (!worldObj.isRemote)
//		{
//			int inputSide = getInputSide();
//			int outputSide = getOutputSide();
//			
//			if (fluidPerRuby == 0) return;
//			
//			if (fluidBuffer < MAX_FLUID)
//			{
//				int requestedFluid = MAX_FLUID - fluidBuffer;
//				
//				SearchFluid searchInput = new SearchFluid(worldObj);
//				searchInput.execute(
//					xCoord + SideUtil.getSideX(inputSide),
//					yCoord + SideUtil.getSideY(inputSide),
//					zCoord + SideUtil.getSideZ(inputSide),
//					outputSide
//				);
//				
//				fluidBuffer += searchInput.pullFluid(fluidID, requestedFluid);
//				markDirty();
//			};
//			
//			if (fluidBuffer < fluidPerRuby) return;
//			
//			// try to pull in a mini ruby
//			SearchItem searchRuby = new SearchItem(worldObj);
//			searchRuby.execute(xCoord, yCoord+1, zCoord, 0);
//			
//			ItemStack stack = searchRuby.pullStack(new ItemStack(MaddTech.itemMiniRuby));
//			if (stack == null) return;
//			
//			// we pulled a mini ruby, now try merging it into a stack
//			int i;
//			for (i=0; i<inv.length; i++)
//			{
//				if (inv[i] == null)
//				{
//					inv[i] = stack.copy();
//					break;
//				}
//				else if (inv[i].getItem() == MaddTech.itemMiniRuby && inv[i].stackSize < 64)
//				{
//					inv[i].stackSize++;
//					break;
//				};
//			};
//			
//			if (i == inv.length)
//			{
//				// could not merge with any stack; try pushing back, then fail
//				searchRuby.pushStack(stack);
//				return;
//			};
//			
//			// we merged the ruby, so release the fluid
//			fluidBuffer -= fluidPerRuby;
//			fluidRelease += fluidPerRuby;
//			markDirty();
//		};
//	};
//
//	public int getOutputSide()
//	{
//		return 2 + (worldObj.getBlockMetadata(xCoord, yCoord, zCoord) % 4);
//	};
//
//	public int getInputSide()
//	{
//		return getOutputSide() ^ 1;
//	};
//	
//	public int getRubySide()
//	{
//		return 1;
//	};
//	
//	@Override
//	public boolean canConnectFluid(int side)
//	{
//		return side == getInputSide() || side == getOutputSide();
//	};
//
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		if (side != getOutputSide()) return 0;
//		if (!id.equals(fluidID)) return 0;
//		
//		int willPull = max;
//		if (willPull > fluidRelease) willPull = fluidRelease;
//		
//		fluidRelease -= willPull;
//		return willPull;
//	};
//
//	@Override
//	public int pushFluid(String id, int de, int side)
//	{
//		return 0;
//	};
//	
//	@Override
//	public String getFluidType(int side)
//	{
//		return fluidID;
//	};
//	
//	public World getWorld()
//	{
//		return worldObj;
//	};
//	
//	@Override
//	public int[] getAccessibleSlotsFromSide(int side)
//	{
//		int[] slots = new int[inv.length];
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			slots[i] = i;
//		};
//		
//		return slots;
//	};
//	
//	@Override
//	public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int side)
//	{
//		return false;
//	};
//	
//	@Override
//	public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int side)
//	{
//		return false;
//	};
//	
//	public void setOwner(String owner)
//	{
//		ownerName = owner;
//		markDirty();
//	};
//	
//	public String getOwner()
//	{
//		return ownerName;
//	};
//	
//	public int getFluidPerRuby()
//	{
//		return fluidPerRuby;
//	};
//	
//	public void onPropsUpdate(String fluidID, int fluidPerRuby)
//	{
//		if (!fluidID.equals(this.fluidID))
//		{
//			fluidBuffer = fluidRelease = 0;
//		};
//		
//		this.fluidID = fluidID;
//		this.fluidPerRuby = fluidPerRuby;
//	};
//	
//	public void setProps(String fluidID, int fluidPerRuby)
//	{
//		PacketFluidShopUpdate packet = new PacketFluidShopUpdate(worldObj, xCoord, yCoord, zCoord, fluidID, fluidPerRuby);
//		LibTech.network.sendToServer(packet);
//	};
//	
//	@Override
//	public boolean canConnectItem(int side)
//	{
//		return side == 1 || side == 0;
//	};
//	
//	public String getFluidID()
//	{
//		return fluidID;
//	};
};
