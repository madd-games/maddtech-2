/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import net.minecraft.nbt.*;
import com.madd.maddtech.api.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;

public class TileEntityFluidTank extends TileEntity implements IFluidMachine
{
	public TileEntityFluidTank(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private String fluidType = "maddtech:empty";
	private int fluidAmount = 0;
	@Override
	public boolean canConnectFluid(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}


//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			if (!BlockMaddFluidTank.canTankBePlacedAt(worldObj, xCoord, yCoord, zCoord))
//			{
//				BlockMaddFluidTank.pop(worldObj, xCoord, yCoord, zCoord);
//			};
//
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//
//	@Override
//	public boolean canConnectFluid(int side)
//	{
//		return side == 1;
//	};
//
//	@Override
//	public String getFluidType(int side)
//	{
//		return fluidType;
//	};
//
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		if (!fluidType.equals(id))
//		{
//			return 0;
//		};
//
//		if (max > fluidAmount)
//		{
//			max = fluidAmount;
//		};
//
//		fluidAmount -= max;
//		markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		return max;
//	};
//
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		if (fluidType.equals("maddtech:empty"))
//		{
//			Fluid fluid = MaddRegistry.fluids.get(id);
//			if (fluid.getBarrelLevel() < BarrelType.NONE) fluidType = id;
//		};
//
//		if (!fluidType.equals(id))
//		{
//			return 0;
//		};
//
//		if ((fluidAmount+max) > 6000000)
//		{
//			max = 6000000 - fluidAmount;
//		};
//
//		fluidAmount += max;
//		markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		return max;
//	};
//
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//
//		comp.setString("FluidType", fluidType);
//		comp.setInteger("FluidAmount", fluidAmount);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//
//		fluidType = comp.getString("FluidType");
//		fluidAmount = comp.getInteger("FluidAmount");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setString("FluidType", fluidType);
//		comp.setInteger("FluidAmount", fluidAmount);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		fluidType = comp.getString("FluidType");
//		fluidAmount = comp.getInteger("FluidAmount");
//	};
//
//	/**
//	 * Those two are for the renderer.
//	 */
//
//	public Fluid getFluid()
//	{
//		if (fluidType.equals("maddtech:empty"))
//		{
//			return null;
//		};
//
//		return MaddRegistry.fluids.get(fluidType);
//	};
//
//	public int getFluidAmount()
//	{
//		return fluidAmount;
//	};
};
