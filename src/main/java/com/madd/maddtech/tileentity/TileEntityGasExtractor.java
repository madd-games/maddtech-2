/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import net.minecraft.nbt.*;

public class TileEntityGasExtractor extends TileEntity implements IFluidMachine
{
	public TileEntityGasExtractor(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}


	private int gasBuffer = 0;


	@Override
	public boolean canConnectFluid(int side)
	{
		return side == 1;
	}


	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	};

//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		comp.setInteger("GasBuffer", gasBuffer);
//	};
//
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		gasBuffer = comp.getInteger("GasBuffer");
//	};
//
//	@Override
//	public String getFluidType(int side)
//	{
//		return "maddtech:natgas";
//	};
//
//	private boolean isGasBlock(int x, int y, int z)
//	{
//		return worldObj.getBlock(x, y, z) == MaddTech.blockNaturalGasOre;
//	};
//
//	private void expandGasBuffer()
//	{
//		int startX, startY, startZ;
//		int endX, endY, endZ;
//
//		startX = xCoord;
//		startY = yCoord-1;
//		startZ = zCoord;
//
//		if (!isGasBlock(startX, startY, startZ))
//		{
//			return;
//		};
//
//		endY = startY;
//		// find the bottom
//		while (isGasBlock(startX, startY, startZ))
//		{
//			startY--;
//		};
//		startY++;
//
//		// find the "left" side
//		while (isGasBlock(startX, startY, startZ))
//		{
//			startX--;
//		};
//		startX++;
//
//		// find the "right" side
//		endX = startX;
//		while (isGasBlock(endX, startY, startZ))
//		{
//			endX++;
//		};
//		endX--;
//
//		// find the "front" (-Z)
//		while (isGasBlock(startX, startY, startZ))
//		{
//			startZ--;
//		};
//		startZ++;
//
//		// find the "back" (+Z)
//		endZ = startZ;
//		while (isGasBlock(startX, startY, endZ))
//		{
//			endZ++;
//		};
//		endZ--;
//
//		// scan the block
//		int x, y, z;
//		for (x=startX; x<=endX; x++)
//		{
//			for (y=startY; y<=endY; y++)
//			{
//				for (z=startZ; z<=endZ; z++)
//				{
//					if (isGasBlock(x, y, z))
//					{
//						int meta = worldObj.getBlockMetadata(x, y, z);
//						if (meta > 0)
//						{
//							meta--;
//							gasBuffer += 100;
//							worldObj.setBlockMetadataWithNotify(x, y, z, meta, 3);
//							return;
//						};
//					};
//				};
//			};
//		};
//	};
//
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		if (side != 1)
//		{
//			return 0;
//		};
//
//		if (!id.equals("maddtech:natgas"))
//		{
//			return 0;
//		};
//
//		expandGasBuffer();
//
//		if (max > gasBuffer)
//		{
//			max = gasBuffer;
//		};
//
//		gasBuffer -= max;
//		return max;
//	};
//
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		if (id.equals("maddtech:natgas") && (side == 1))
//		{
//			gasBuffer += max;
//			return max;
//		};
//
//		return 0;
//	};
};
