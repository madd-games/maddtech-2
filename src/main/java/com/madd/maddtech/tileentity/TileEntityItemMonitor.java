/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.world.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import net.minecraft.nbt.*;
import net.minecraft.inventory.*;
import net.minecraft.entity.player.*;
import java.util.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityItemMonitor extends TileEntity implements IItemMachine, ISidedInventory
{
	public TileEntityItemMonitor(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private ItemStack[] inv = new ItemStack[1];
	
	/**
	 * OPS:
	 * 0 - countOfItem < opParam
	 * 1 - countOfItem <= opParam
	 * 2 - countOfItem == opParam
	 * 3 - countOfItem >= opParam
	 * 4 - countOfItem > opParam
	 * 5 - countOfItem != opParam
	 */
	private int whichOp = 0;
	private int opParam = 0;
	
	private static String[] opNames = new String[] {
		"<", "<=", "==", ">=", ">", "!="
	};

	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canConnectItem(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
//	@Override
//	public int getSizeInventory()
//	{
//		return 1;
//	};
//	
//	private boolean evaluateOp(int count)
//	{
//		switch (whichOp)
//		{
//		case 0:
//			return count < opParam;
//		case 1:
//			return count <= opParam;
//		case 2:
//			return count == opParam;
//		case 3:
//			return count >= opParam;
//		case 4:
//			return count > opParam;
//		default:
//			return count != opParam;
//		}
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "Item Transporter";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setInteger("MonitorOp", whichOp);
//		comp.setInteger("MonitorParam", opParam);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//		
//		whichOp = comp.getInteger("MonitorOp");
//		opParam = comp.getInteger("MonitorParam");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("MonitorOp", whichOp);
//		comp.setInteger("MonitorParam", opParam);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		whichOp = comp.getInteger("MonitorOp");
//		opParam = comp.getInteger("MonitorParam");
//	};
//	
//	@Override
//	public boolean canConnectItem(int side)
//	{
//		return (side^1) == (worldObj.getBlockMetadata(xCoord, yCoord, zCoord) & 7);
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//	
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			if (inv[0] != null)
//			{
//				int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//				int outSide = meta & 7;
//				int inSide = outSide ^ 1;
//				
//				SearchItem si = new SearchItem(worldObj);
//				si.execute(
//					xCoord + SideUtil.getSideX(inSide),
//					yCoord + SideUtil.getSideY(inSide),
//					zCoord + SideUtil.getSideZ(inSide),
//				outSide);
//				
//				ArrayList<IInventory> invs = si.getInventories();
//				int i;
//				
//				int count = 0;
//				ItemFilter filter = new ItemFilterOnly(inv);
//				for (i=0; i<invs.size(); i++)
//				{
//					IInventory inv = invs.get(i);
//					
//					int j;
//					for (j=0; j<inv.getSizeInventory(); j++)
//					{
//						ItemStack stack = inv.getStackInSlot(j);
//						if (stack != null)
//						{
//							if (filter.isAcceptedStack(stack))
//							{
//								count += stack.stackSize;
//							};
//						};
//					};
//				};
//				
//				int mask = 0;
//				if (evaluateOp(count))
//				{
//					mask = 8;
//				};
//				
//				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, mask | outSide, 3);
//				markDirty();
//				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//			};
//		};
//	};
//	
//	public String getOpName()
//	{
//		return opNames[whichOp];
//	};
//	
//	public void setOptions(int which, int param)
//	{
//		whichOp = which;
//		opParam = param;
//		markDirty();
//	};
//	
//	public String nextOp()
//	{
//		whichOp = (whichOp+1) % 6;
//		
//		PacketItemMonitorUpdate update = new PacketItemMonitorUpdate(worldObj, xCoord, yCoord, zCoord, whichOp, opParam);
//		LibTech.network.sendToServer(update);
//		
//		return getOpName();
//	};
//
//	public void setParam(int param)
//	{
//		opParam = param;
//		
//		PacketItemMonitorUpdate update = new PacketItemMonitorUpdate(worldObj, xCoord, yCoord, zCoord, whichOp, opParam);
//		LibTech.network.sendToServer(update);
//	};
//	
//	public int getParam()
//	{
//		return opParam;
//	};
//
//	@Override
//	public int[] getAccessibleSlotsFromSide(int p_94128_1_)
//	{
//		return new int[0];
//	};
//
//	@Override
//	public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int p_102007_3_)
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int p_102008_3_)
//	{
//		return false;
//	};
};
