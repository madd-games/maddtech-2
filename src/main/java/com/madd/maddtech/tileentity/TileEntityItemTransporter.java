/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.world.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import net.minecraft.nbt.*;
import net.minecraft.inventory.*;
import net.minecraft.entity.player.*;
import java.util.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityItemTransporter extends TileEntity implements IItemMachine, ISidedInventory
{
	public TileEntityItemTransporter(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private ItemStack[] inv = new ItemStack[5];
	private int whichFilter = 1;
	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean canConnectItem(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	
//	@Override
//	public int getSizeInventory()
//	{
//		return 5;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "Item Transporter";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setInteger("TransFilter", whichFilter);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//		
//		whichFilter = comp.getInteger("TransFilter");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("TransFilter", whichFilter);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		whichFilter = comp.getInteger("TransFilter");
//	};
//	
//	@Override
//	public boolean canConnectItem(int side)
//	{
//		return true;
//	};
//
//	private boolean isIOSide(int side)
//	{
//		int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//		return ((meta == side) || (meta == SideUtil.opposite(side)));
//	};
//
//	@Override
//	public int[] getAccessibleSlotsFromSide(int side)
//	{
//		if (isIOSide(side))
//		{
//			// We cannot put items into the input/output sides.
//			int[] list = new int[0];
//			return list;
//		}
//		else
//		{
//			int[] list = new int[1];
//			list[0] = 0;
//			return list;
//		}
//	};
//
//	@Override
//	public boolean canInsertItem(int slot, ItemStack stack, int side)
//	{
//		return false;
//	};
//
//	@Override
//	public boolean canExtractItem(int slot, ItemStack stack, int side)
//	{
//		return false;
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	private ItemFilter getFilterObject()
//	{
//		switch (whichFilter)
//		{
//		case 1:
//			return new ItemFilterAll();
//		case 2:
//			return new ItemFilterOnly(inv);
//		default:
//			return new ItemFilterExcept(inv);
//		}
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			if (worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
//			{
//				int inputSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//				int outputSide = SideUtil.opposite(inputSide);
//
//				SearchItem searchInput = new SearchItem(worldObj);
//				searchInput.execute(xCoord+SideUtil.getSideX(inputSide),
//							yCoord+SideUtil.getSideY(inputSide),
//							zCoord+SideUtil.getSideZ(inputSide),
//				outputSide);
//				SearchItem searchOutput = new SearchItem(worldObj);
//				searchOutput.execute(xCoord+SideUtil.getSideX(outputSide),
//							yCoord+SideUtil.getSideY(outputSide),
//							zCoord+SideUtil.getSideZ(outputSide),
//				inputSide);
//
//				ItemStack stack;
//				if (whichFilter == 0)
//				{
//					stack = searchInput.pullStack(inv[0]);
//				}
//				else
//				{
//					stack = searchInput.pullStack(getFilterObject());
//				};
//				
//				if (stack != null)
//				{
//					stack = searchOutput.pushStack(stack);
//				};
//
//				if (stack != null)
//				{
//					// try pushing back to input
//					stack = searchInput.pushStack(stack);
//				};
//				
//				if (stack != null)
//				{
//					// didn't work, guess we just have to dump it.
//					Random rand = new Random();
//					float rx = rand.nextFloat() * 0.8F + 0.1F;
//					float ry = rand.nextFloat() * 0.8F + 0.1F;
//					float rz = rand.nextFloat() * 0.8F + 0.1F;
//
//					EntityItem entityItem = new EntityItem(worldObj,
//						xCoord + rx, yCoord + ry, zCoord + rz,
//						new ItemStack(stack.getItem(), stack.stackSize, stack.getItemDamage()));
//        		
//					if (stack.hasTagCompound())
//					{
//						entityItem.getEntityItem().setTagCompound((NBTTagCompound) stack.getTagCompound().copy());
//					}
//
//					float factor = 0.1F;
//					entityItem.motionX = rand.nextGaussian() * factor;
//					entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
//					entityItem.motionZ = rand.nextGaussian() * factor;
//					worldObj.spawnEntityInWorld(entityItem);
//				};
//			};
//		};
//	};
//	
//	public int getFilter()
//	{
//		return whichFilter;
//	};
//	
//	public void setFilter(int which)
//	{
//		whichFilter = which;
//		markDirty();
//	};
//	
//	public int nextFilter()
//	{
//		int offFilter = whichFilter % 3;
//		whichFilter = offFilter + 1;
//		
//		PacketItemTransporterUpdate update = new PacketItemTransporterUpdate(worldObj, xCoord, yCoord, zCoord, whichFilter);
//		LibTech.network.sendToServer(update);
//		
//		return whichFilter;
//	};
};
