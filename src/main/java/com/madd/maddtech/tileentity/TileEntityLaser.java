/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;


public class TileEntityLaser extends TileEntity implements IEnergyMachine
{
	public TileEntityLaser(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private static int POWER_USAGE = 20;
	private static int LASER_STR = 64;

	private int energyBuffer = 0;	// WARNING: Not written to NBT! Should it be?

	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	

//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			int glowSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//			boolean shouldCast = false;
//			if (worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
//			{
//				SearchEnergy search = new SearchEnergy(worldObj);
//				search.execute(xCoord-SideUtil.getSideX(glowSide),
//						yCoord-SideUtil.getSideY(glowSide),
//						zCoord-SideUtil.getSideZ(glowSide),
//						glowSide);
//				energyBuffer += search.pullEnergy(POWER_USAGE);
//				if (energyBuffer >= POWER_USAGE)
//				{
//					energyBuffer -= POWER_USAGE;
//					shouldCast = true;
//				};
//			};
//
//			if (energyBuffer >= POWER_USAGE)
//			{
//				energyBuffer -= POWER_USAGE;
//				shouldCast = true;
//			};
//
//			if (shouldCast)
//			{
//				castLaserBeam();
//			};
//		};
//	};
//
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return side != worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//	};
//
//	@Override
//	public int pushEnergy(int max, int side)
//	{
//		if (canConnectEnergy(side))
//		{
//			energyBuffer += max;
//			//if (max >= 5)
//			//{
//			//castLaserBeam();
//			//return 5;
//			//};
//		};
//
//		return 0;
//	};
//
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		return 0;
//	};
//
//	private void castLaserBeam()
//	{
//		int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//		int x = xCoord + SideUtil.getSideX(meta);
//		int y = yCoord + SideUtil.getSideY(meta);
//		int z = zCoord + SideUtil.getSideZ(meta);
//
//		if (meta > 1)
//		{
//			y++;
//		};
//
//		int[] sides;
//		if (meta == 0)
//		{
//			sides = new int[] {2, 4, 3, 5};
//		}
//		else if (meta == 1)
//		{
//			sides = new int[] {2, 4, 3, 5};
//		}
//		else if (meta == 2)
//		{
//			sides = new int[] {0, 4, 1, 5};
//		}
//		else if (meta == 3)
//		{
//			sides = new int[] {0, 4, 1, 5};
//		}
//		else if (meta == 4)
//		{
//			sides = new int[] {0, 2, 1, 3};
//		}
//		else
//		{
//			sides = new int[] {0, 2, 1, 3};
//		};
//
//		int i;
//		for (i=0; i<4; i++)
//		{
//			int side = sides[i];
//			int nextSide;
//			if (i == 3)
//			{
//				nextSide = sides[0];
//			}
//			else
//			{
//				nextSide = sides[i+1];
//			};
//
//			int lx = x + SideUtil.getSideX(side);
//			int ly = y + SideUtil.getSideY(side);
//			int lz = z + SideUtil.getSideZ(side);
//			LaserBeam beam = new LaserBeam(worldObj, lx, ly, lz, meta, LASER_STR);
//			beam.cast();
//			lx += SideUtil.getSideX(nextSide);
//			ly += SideUtil.getSideY(nextSide);
//			lz += SideUtil.getSideZ(nextSide);
//			beam = new LaserBeam(worldObj, lx, ly, lz, meta, LASER_STR);
//			beam.cast();
//		};
//
//		LaserBeam centerBeam = new LaserBeam(worldObj, x, y, z, meta, LASER_STR);
//		centerBeam.cast();
//	};
};
