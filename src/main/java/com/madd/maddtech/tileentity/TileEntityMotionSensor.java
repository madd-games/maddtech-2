/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import net.minecraft.util.*;
import com.madd.maddtech.api.*;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;
import java.util.*;

public class TileEntityMotionSensor extends TileEntity
{
	public TileEntityMotionSensor(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}


	private int metadataPrev = 0;
	private Block blockFrame;
	private Block blockDoor;
	private int metaFrame;
	private int metaDoor;
	private int doorState = 0;		// 0 = fully closed, 1 = half-open/half-closed, 2 = fully open
	

	private boolean isMaster(int meta)
	{
		return (meta & 1) == 1;
	};

//	private Block getRelBlock(int meta, int u, int y)
//	{
//		if ((meta & 2) == 0)
//		{
//			return worldObj.getBlock(xCoord+u, yCoord+y, zCoord);
//		};
//		
//		return worldObj.getBlock(xCoord, yCoord+y, zCoord+u);
//	};
//	
//	private int getRelMeta(int meta, int u, int y)
//	{
//		if ((meta & 2) == 0)
//		{
//			return worldObj.getBlockMetadata(xCoord+u, yCoord+y, zCoord);
//		};
//		
//		return worldObj.getBlockMetadata(xCoord, yCoord+y, zCoord+u);
//	};
//	
//	private void setRelBlock(int meta, int u, int y, Block block)
//	{
//		if ((meta & 2) == 0)
//		{
//			worldObj.setBlock(xCoord+u, yCoord+y, zCoord, block);
//			return;
//		};
//		
//		worldObj.setBlock(xCoord, yCoord+y, zCoord+u, block);
//	};
//	
//	private void setRelMeta(int meta, int u, int y, int targetMeta)
//	{
//		if ((meta & 2) == 0)
//		{
//			worldObj.setBlockMetadataWithNotify(xCoord+u, yCoord+y, zCoord, targetMeta, 3);
//			return;
//		};
//		
//		worldObj.setBlockMetadataWithNotify(xCoord, yCoord+y, zCoord+u, targetMeta, 3);
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			markDirty();		// always dirty (we keep changing state)
//			AxisAlignedBB bb;
//			
//			int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//			if ((meta & 2) == 0)
//			{
//				// X-axis door, so Z-axis scan
//				bb = AxisAlignedBB.getBoundingBox(
//					(double) xCoord, (double) yCoord - 1.0, (double) zCoord - 3.0,
//					(double) xCoord + 8.0, (double) yCoord + 4.0, (double) zCoord + 4.0
//				);
//			}
//			else
//			{
//				// Z-azis door, so X-axis scan
//				bb = AxisAlignedBB.getBoundingBox(
//					(double) xCoord - 3.0, (double) yCoord - 1.0, (double) zCoord,
//					(double) xCoord + 4.0, (double) yCoord + 4.0, (double) zCoord + 8.0
//				);
//			};
//			
//			if ((!isMaster(metadataPrev)) && isMaster(meta))
//			{
//				// we were not the master but we became it, so find the door paramters
//				doorState = 0;
//				blockFrame = getRelBlock(meta, 0, 1);
//				blockDoor = getRelBlock(meta, 2, 1);
//				metaFrame = getRelMeta(meta, 0, 1);
//				metaDoor = getRelMeta(meta, 2, 1);
//				
//				int relU;
//				for (relU=0; relU<8; relU++)
//				{
//					if (getRelBlock(meta, relU, 0) != MaddTech.blockMotionSensor)
//					{
//						// try again
//						return;
//					};
//				};
//				
//				if ((blockFrame == Blocks.air) || (blockDoor == Blocks.air))
//				{
//					// try again on next tick
//					return;
//				};
//			};
//			
//			// remember the state
//			metadataPrev = meta;
//			
//			if (isMaster(meta))
//			{
//				// make sure the sensors are there.
//				int relU, relY;
//				
//				for (relU=0; relU<8; relU++)
//				{
//					if (getRelBlock(meta, relU, 0) != MaddTech.blockMotionSensor)
//					{
//						// reset
//						worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 0, 3);
//						metadataPrev = 0;
//						return;
//					};
//				};
//				
//				List people = worldObj.getEntitiesWithinAABB(EntityPlayer.class, bb);
//				int tends = -1;
//				if (people.size() != 0)
//				{
//					tends = 1;
//				};
//				
//				int newState = doorState + tends;
//				if ((newState >= 0) && (newState <= 2))
//				{
//					doorState = newState;
//					
//					// "render" the state
//					for (relY=1; relY<=3; relY++)
//					{
//						for (relU=0; relU<2; relU++)
//						{
//							Block target = Blocks.air;
//							if (relU >= doorState)
//							{
//								target = blockDoor;
//							};
//							
//							int u1 = 3 - relU;
//							int u2 = 4 + relU;
//							
//							setRelBlock(meta, u1, relY, target);
//							setRelBlock(meta, u2, relY, target);
//							setRelMeta(meta, u1, relY, metaDoor);
//							setRelMeta(meta, u2, relY, metaDoor);
//						};
//					};
//				};
//			};
//		};
//	};
//
//	private String getBlockName(Block x)
//	{
//		if (x == null)
//		{
//			return ":null:";
//		};
//		
//		return GameRegistry.findUniqueIdentifierFor(x).toString();
//	};
//	
//	private Block getBlockFromName(String name)
//	{
//		GameRegistry.UniqueIdentifier uid = new GameRegistry.UniqueIdentifier(name);
//		return GameRegistry.findBlock(uid.modId, uid.name);
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setInteger("SensorMetadataPrev", metadataPrev);
//		comp.setString("SensorFrameBlock", getBlockName(blockFrame));
//		comp.setString("SensorDoorBlock", getBlockName(blockDoor));
//		comp.setInteger("SensorFrameMeta", metaFrame);
//		comp.setInteger("SensorDoorMeta", metaDoor);
//		comp.setInteger("SensorDoorState", doorState);
//	};
//
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		metadataPrev = comp.getInteger("SensorMetadataPrev");
//		blockFrame = getBlockFromName(comp.getString("SensorFrameBlock"));
//		blockDoor = getBlockFromName(comp.getString("SensorDoorBlock"));
//		metaFrame = comp.getInteger("SensorFrameMeta");
//		metaDoor = comp.getInteger("SensorDoorMeta");
//		doorState = comp.getInteger("SensorDoorState");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//	};
};
