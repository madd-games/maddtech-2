/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.world.*;
import net.minecraft.inventory.*;
import net.minecraft.item.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;
import java.io.*;
import net.minecraft.entity.item.*;
import java.util.*;
import net.minecraft.block.*;
import com.madd.maddtech.api.*;

/**
 * Do not register this tile entity class, as it is abstract and should be
 * derived. It is a base class for other tile entities, which perform processing
 * of items and use the "proc" GUI and container.
 */
public abstract class TileEntityProc extends TileEntity implements ISidedInventory, IRedstoneStateMachine
{
	public TileEntityProc(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}



	/**
	 * One input slot, one output slot, and four upgrade slots.
	 */
	private ItemStack[] inv = new ItemStack[6];

	

	@Override
	public int getRedstoneState()
	{
		if (inv[0] == null)
		{
			// nothing in input slot, so emit redstone signal!
			return 15;
		};
		
		// input slot is taken, so no redstone output
		return 0;
	};
	
//	@Override
//	public int[] getAccessibleSlotsFromSide(int side)
//	{
//		if (side == 6)
//		{
//			// shell item input
//			return new int[] {0};
//		}
//		else if (side == 7)
//		{
//			// shell item output
//			return new int[] {1};
//		}
//		else
//		{
//			return new int[] {};
//		}
//	};
//	
//	@Override
//	public boolean canInsertItem(int slot, ItemStack stack, int side)
//	{
//		return ((side == 6) && (slot == 0));
//	};
//	
//	@Override
//	public boolean canExtractItem(int slot, ItemStack stack, int side)
//	{
//		return ((side == 7) && (slot == 1));
//	};
//	
//	@Override
//	public int getSizeInventory()
//	{
//		return 6;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return getMachineName();
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		// never allow the insertion of items into the output slot.
//		return i != 1;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		writeProcStateToNBT(comp);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//
//		readProcStateFromNBT(comp);
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		writeProcStateToNBT(comp);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		readProcStateFromNBT(comp);
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	/**
//	 * Returns the unlocalized name of this machine.
//	 */
//	public abstract String getMachineName();
//
//	/**
//	 * Writes the state of the machine to an NBTTagCompound.
//	 * Do not write inventory contents, just custom state info.
//	 */
//	public void writeProcStateToNBT(NBTTagCompound comp)
//	{
//	};
//
//	/**
//	 * Reads the state of the machine from an NBTTagCompound.
//	 * Do not read out inventory contents, just custom state info.
//	 */
//	public void readProcStateFromNBT(NBTTagCompound comp)
//	{
//	};
//
//	/**
//	 * Returns the machine progress out of 'scale'.
//	 */
//	public abstract int getMachineProgress(int scale);
//
//	@Override
//	public void markDirty()
//	{
//		super.markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//	};
//
//	public void setBlockTo(Block block)
//	{
//		worldObj.setBlock(xCoord, yCoord, zCoord, block);
//		validate();
//		worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 0, 2);
//		worldObj.setTileEntity(xCoord, yCoord, zCoord, this);
//	};
//
//	/**
//	 * Call this on clients to get updated versions of the tile entity.
//	 */
//	public TileEntityProc getUpdatedEntity()
//	{
//		return (TileEntityProc) worldObj.getTileEntity(xCoord, yCoord, zCoord);
//	};
//
//	/**
//	 * Returns the factor of processing time increase (0-1), or decrease (if negative) based on the upgrade slots.
//	 */
//	protected float getTimeIncrease()
//	{
//		float result = 0.0F;
//		int i;
//		for (i=2; i<6; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				Item item = stack.getItem();
//				if (item instanceof IMachineUpgrade)
//				{
//					IMachineUpgrade mach = (IMachineUpgrade) item;
//					result += mach.getTimeIncrease() * stack.stackSize;
//				};
//			};
//		};
//
//		if (result < -1.0F)
//		{
//			result = -1.0F;
//		};
//
//		return result;
//	};
//
//	/**
//	 * Returns the factor of power usage increase (0-1), or decrease (if negative) based on the upgrade slots.
//	 */
//	protected float getPowerIncrease()
//	{
//		float result = 0.0F;
//		int i;
//		for (i=2; i<6; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				Item item = stack.getItem();
//				if (item instanceof IMachineUpgrade)
//				{
//					IMachineUpgrade mach = (IMachineUpgrade) item;
//					result += mach.getPowerIncrease() * stack.stackSize;
//				};
//			};
//		};
//
//		if (result < -1.0F)
//		{
//			result = -1.0F;
//		};
//
//		return result;
//	};
};
