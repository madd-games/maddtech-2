/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;

public class TileEntityPulser extends TileEntity
{
	public TileEntityPulser(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	public int cycleDuration = 0;
	private int cycleTimer = 0;
	private int timerTillUpdate = 0;
	

//	@Override
//	public void updateEntity()
//	{
//		if (cycleDuration > 0) cycleTimer = (cycleTimer+1) % cycleDuration;
//		if (!worldObj.isRemote)
//		{
//			int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//			int emitSide = 0;
//			if (cycleDuration > 0) emitSide = ((4 * cycleTimer)/cycleDuration)%4;
//			int newMeta = (emitSide << 2) | (meta & 3);
//			
//			int flags = 1;
//			if (timerTillUpdate >= 20)
//			{
//				flags = 3;
//				timerTillUpdate = 0;
//			}
//			else
//			{
//				timerTillUpdate++;
//			};
//			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, newMeta, flags);
//		}
//		else
//		{
//			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, worldObj.getBlockMetadata(xCoord, yCoord, zCoord)^1, 3);
//		};
//	};
//
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setInteger("Timer", cycleTimer);
//		comp.setInteger("Cycle", cycleDuration);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		cycleTimer = comp.getInteger("Timer");
//		cycleDuration = comp.getInteger("Cycle");
//		//System.out.println("UPDATE TIMER: " + cycleTimer);
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("Cycle", cycleDuration);
//		comp.setInteger("Timer", cycleTimer);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		cycleDuration = comp.getInteger("Cycle");
//		cycleTimer = comp.getInteger("Timer");
//	};
//	
//	/**
//	 * Return the angle that we are rotated by.
//	 */
//	public float getAngle()
//	{
//		if (cycleDuration == 0)
//		{
//			return 0.0F;
//		};
//		
//		// subtract 45 degrees for visual effect. like its supposed to switch signal as it crosses onto a block.
//		// whatever just accept it.
//		return ((float) cycleTimer / (float) cycleDuration * (float)Math.PI * 2.0F) - ((float)Math.PI / 4.0F);
//	};
//	
//	public void sendUpdateToServer()
//	{
//		PacketPulserUpdate update = new PacketPulserUpdate(worldObj, xCoord, yCoord, zCoord, cycleDuration);
//		LibTech.network.sendToServer(update);
//	};
};
