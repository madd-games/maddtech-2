/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;

public class TileEntityPump extends TileEntity
{
	public TileEntityPump(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	/**
	 * The rate at which the pump transports fluids in u/t (must be between 1-1000 inclusive).
	 */
	private int pumpRate = 1000;
	
	/**
	 * Which fluid should be pumped through. If set to "*", then the closest available fluid is
	 * pumped through.
	 */
	private String pumpFilter = "*";
	

//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			if (worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
//			{
//				int inputSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//				int outputSide = SideUtil.opposite(inputSide);
//
//				SearchFluid searchInput = new SearchFluid(worldObj);
//				searchInput.execute(xCoord+SideUtil.getSideX(inputSide),
//							yCoord+SideUtil.getSideY(inputSide),
//							zCoord+SideUtil.getSideZ(inputSide),
//				outputSide);
//				SearchFluid searchOutput = new SearchFluid(worldObj);
//				searchOutput.execute(xCoord+SideUtil.getSideX(outputSide),
//							yCoord+SideUtil.getSideY(outputSide),
//							zCoord+SideUtil.getSideZ(outputSide),
//				inputSide);
//
//				String id = pumpFilter;
//				if (id.equals("*"))
//				{
//					id = searchInput.getClosestFluidType();
//				};
//				
//				if (!id.equals("maddtech:empty"))
//				{
//					int buffer = searchInput.pullFluid(id, pumpRate);
//					buffer = searchOutput.pushFluid(id, buffer);
//					searchInput.pushFluid(id, buffer);
//				};
//			};
//		};
//	};
//
//	private void writeState(NBTTagCompound comp)
//	{
//		comp.setInteger("PumpRate", pumpRate);
//		comp.setString("PumpFilter", pumpFilter);
//	};
//	
//	private void readState(NBTTagCompound comp)
//	{
//		pumpRate = comp.getInteger("PumpRate");
//		pumpFilter = comp.getString("PumpFilter");
//		
//		// MaddTech 1.0 compatibility; those keys were not present in the NBT,
//		// so if they return the impossible values of 0 and the empty string, we
//		// assume we're in a 1.0 world and set them to defaults, which match the
//		// 1.0 behavior.
//		if (pumpRate == 0)
//		{
//			pumpRate = 1000;
//		};
//		
//		if (pumpFilter.length() == 0)
//		{
//			pumpFilter = "*";
//		};
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		writeState(comp);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		readState(comp);
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		writeState(comp);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		readState(comp);
//	};
//	
//	private void sendUpdate()
//	{
//		PacketPumpUpdate update = new PacketPumpUpdate(worldObj, xCoord, yCoord, zCoord, pumpRate, pumpFilter);
//		LibTech.network.sendToServer(update);
//	};
//	
//	/**
//	 * Returns the rate at which this pump pumps fluid.
//	 */
//	public int getRate()
//	{
//		return pumpRate;
//	};
//	
//	/**
//	 * Returns the ID of the fluid that is being pumped by this pump, or "*" for any fluid.
//	 */
//	public String getFilter()
//	{
//		return pumpFilter;
//	};
//	
//	/**
//	 * Loads the next filter, sends an update to the server, and returns the new filter.
//	 */
//	public String nextFilter()
//	{
//		FluidRegistry freg = (FluidRegistry) MaddRegistry.fluids;
//		String[] names = freg.getFluids();
//		
//		int i;
//		for (i=0; i<names.length; i++)
//		{
//			if (names[i].equals(pumpFilter))
//			{
//				break;
//			};
//		};
//		
//		i = (i+1) % names.length;
//		pumpFilter = names[i];
//		sendUpdate();
//		return pumpFilter;
//	};
//	
//	/**
//	 * Set the rate, and send an update to the server.
//	 */
//	public void setRate(int rate)
//	{
//		if ((rate > 0) && (rate <= 1000))
//		{
//			pumpRate = rate;
//			sendUpdate();
//		};
//	};
//	
//	/**
//	 * Sets the properties. Called upon receiving an update packet on a server.
//	 */
//	public void setOptions(String filter, int rate)
//	{
//		pumpFilter = filter;
//		pumpRate = rate;
//	};
};
