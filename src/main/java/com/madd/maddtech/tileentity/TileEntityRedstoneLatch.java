/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import net.minecraft.nbt.*;
import net.minecraft.inventory.*;
import net.minecraft.entity.player.*;
import java.util.*;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;
import net.minecraft.block.*;

public class TileEntityRedstoneLatch extends TileEntity
{
	public TileEntityRedstoneLatch(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private int latchResetValue = 0;
	private int latchValue = 0;
	
	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setInteger("LatchResetValue", latchResetValue);
//		comp.setInteger("LatchValue", latchValue);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		latchResetValue = comp.getInteger("LatchResetValue");
//		latchValue = comp.getInteger("LatchValue");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("LatchResetValue", latchResetValue);
//		comp.setInteger("LatchValue", latchValue);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		latchResetValue = comp.getInteger("LatchResetValue");
//		latchValue = comp.getInteger("LatchValue");
//	};
//
//	private int getPowerFrom(IBlockAccess world, int x, int y, int z, int side)
//	{
//		int bx = x + SideUtil.getSideX(side);
//		int by = y + SideUtil.getSideY(side);
//		int bz = z + SideUtil.getSideZ(side);
//
//		Block block = world.getBlock(bx, by, bz);
//		return block.isProvidingWeakPower(world, bx, by, bz, side);
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			int outSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord) & 7;
//			int inSide = outSide ^ 1;
//			
//			int inPower = getPowerFrom(worldObj, xCoord, yCoord, zCoord, inSide);
//			if (inPower > 0)
//			{
//				latchValue = latchResetValue;
//			};
//			
//			int mask = 0;
//			if (latchValue != 0)
//			{
//				latchValue--;
//				mask = 8;
//			};
//			
//			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, mask | outSide, 3);
//			markDirty();
//		};
//	};
//	
//	public void setOptions(int resetValue)
//	{
//		latchResetValue = resetValue;
//		latchValue = 0;
//		markDirty();
//	};
//
//	public void setResetValue(int value)
//	{
//		latchResetValue = value;
//		
//		PacketRedstoneLatchUpdate update = new PacketRedstoneLatchUpdate(worldObj, xCoord, yCoord, zCoord, value);
//		LibTech.network.sendToServer(update);
//	};
//	
//	public int getResetValue()
//	{
//		return latchResetValue;
//	};
};
