/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.client.renderer.tileentity.*;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.texture.*;
import net.minecraft.tileentity.*;
import net.minecraft.util.*;
import net.minecraft.world.*;
import net.minecraft.client.renderer.*;
import net.minecraft.block.*;
import net.minecraft.world.*;
import org.lwjgl.opengl.*;
import java.io.*;
import net.minecraft.client.*;
import net.minecraft.util.*;
import net.minecraft.tileentity.*;
import net.minecraft.client.renderer.texture.*;
import com.madd.maddtech.api.*;

//@SideOnly(Side.CLIENT)
public class TileEntityRenderFluidTank
{
//	@Override
//	public void renderTileEntityAt(TileEntity te, double x, double y, double z, float scale)
//	{
//		TileEntityFluidTank tank = (TileEntityFluidTank) te;
//		Fluid fluid = tank.getFluid();
//		if (fluid == null) return;
//		if (tank.getFluidAmount() == 0) return;
//
//		float height = (float) tank.getFluidAmount() / 6000000.0F * 9.9F;
//
//		Tessellator tess = Tessellator.instance;
//
//		TextureManager texman = Minecraft.getMinecraft().getTextureManager();
//		texman.bindTexture(fluid.getTexture());
//
//		tess.startDrawingQuads();
//		tess.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
//
//		float x1 = (float) x - 0.9F;
//		float x2 = x1 + 2.8F;
//		float y1 = (float) (y-9);
//		float y2 = y1 + height;
//		float z1 = (float) z - 0.9F;
//		float z2 = z1 + 2.8F;
//
//		// The texture coordinates are for the sides, the top is different; see below.
//		float u1 = 0.0F;
//		float u2 = 9.0F*16.0F/256.0F;
//		float v1 = 1.0F-(height*3.0F*16.0F/256.0F);
//		float v2 = 1.0F;
//
//		tess.setNormal(0.0F, 1.0F, 0.0F);
//		tess.addVertexWithUV(x1, y2, z1, 9.0F*16.0F/256.0F, 9.0F*16.0F/256.0F);
//		tess.addVertexWithUV(x1, y2, z2, 9.0F*16.0F/256.0F, 0.0F);
//		tess.addVertexWithUV(x2, y2, z2, 0.0F, 0.0F);
//		tess.addVertexWithUV(x2, y2, z1, 0.0F, 9.0F*16.0F/256.0F);
//
//		// Front side
//		tess.setNormal(0.0F, 0.0F, -1.0F);
//		tess.addVertexWithUV(x1, y1, z1, u1, v2);
//		tess.addVertexWithUV(x1, y2, z1, u1, v1);
//		tess.addVertexWithUV(x2, y2, z1, u2, v1);
//		tess.addVertexWithUV(x2, y1, z1, u2, v2);
//
//		// Back side
//		tess.setNormal(0.0F, 0.0F, 1.0F);
//		tess.addVertexWithUV(x1, y1, z2, u1, v2);
//		tess.addVertexWithUV(x2, y1, z2, u2, v2);
//		tess.addVertexWithUV(x2, y2, z2, u2, v1);
//		tess.addVertexWithUV(x1, y2, z2, u1, v1);
//
//		// "Left" side
//		tess.setNormal(1.0F, 0.0F, 0.0F);
//		tess.addVertexWithUV(x1, y1, z1, u1, v2);
//		tess.addVertexWithUV(x1, y1, z2, u2, v2);
//		tess.addVertexWithUV(x1, y2, z2, u2, v1);			
//		tess.addVertexWithUV(x1, y2, z1, u1, v1);
//
//		// "Right" side
//		tess.setNormal(-1.0F, 0.0F, 0.0F);
//		tess.addVertexWithUV(x2, y1, z1, u1, v2);
//		tess.addVertexWithUV(x2, y2, z1, u1, v1);
//		tess.addVertexWithUV(x2, y2, z2, u2, v1);
//		tess.addVertexWithUV(x2, y1, z2, u2, v2);
//
//		tess.draw();			
//
//		tess.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
//	};
};
