/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.entity.player.*;
import net.minecraft.inventory.*;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.util.Direction;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import com.madd.maddtech.api.*;

public class TileEntityShop extends TileEntity implements ISidedInventory, IItemMachine
{
	public TileEntityShop(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	public static final int PRODUCT_SLOT_BASE = 9*3;
	
	/**
	 * The slots into which clients place money.
	 */
	public static final int MONEY_SLOT_BASE = PRODUCT_SLOT_BASE + 4;
	
	/**
	 * The slots from which clients remove products after purchase.
	 */
	public static final int OUTPUT_SLOT_BASE = MONEY_SLOT_BASE + 4;
	
	private ItemStack[] inv = new ItemStack[9*3 + 4 + 4 + 4];
	
	/**
	 * The price (in mini-rubies) of each product in the product slots.
	 */
	public int[] prices = new int[] {0, 0, 0, 0};
	
	/**
	 * Name of the player who owns this shop (placer of the block).
	 */
	private String owner = "<unknown>";

	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canConnectItem(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int[] getSlotsForFace(Direction p_180463_1_) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean canPlaceItemThroughFace(int p_180462_1_, ItemStack p_180462_2_, Direction p_180462_3_) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int p_180461_1_, ItemStack p_180461_2_, Direction p_180461_3_) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
//	@Override
//	public int getSizeInventory()
//	{
//		return inv.length;
//	};
//	
//	@Override
//	public boolean canConnectItem(int side)
//	{
//		return side >= 0 && side < 2;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "Shop";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		if (i < PRODUCT_SLOT_BASE)
//		{
//			return true;
//		};
//		
//		if ((i >= PRODUCT_SLOT_BASE) && (i < PRODUCT_SLOT_BASE+4))
//		{
//			return true;
//		};
//		
//		if ((i >= MONEY_SLOT_BASE) && (i < MONEY_SLOT_BASE+4))
//		{
//			if (istack == null)
//			{
//				return true;
//			};
//			
//			return istack.getItem() == MaddTech.itemMiniRuby;
//		};
//		
//		// nothing is valid for output slots
//		return false;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		writeExtraInfo(comp);
//	};
//	
//	private void writeExtraInfo(NBTTagCompound comp)
//	{
//		comp.setString("ShopOwner", owner);
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			comp.setInteger("ShopPrice"+i, prices[i]);
//		};
//	};
//	
//	private void readExtraInfo(NBTTagCompound comp)
//	{
//		owner = comp.getString("ShopOwner");
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			prices[i] = comp.getInteger("ShopPrice"+i);
//		};
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//		readExtraInfo(comp);
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound tag = new NBTTagCompound();
//		writeExtraInfo(tag);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, tag);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		readExtraInfo(pkt.func_148857_g());
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//	
//	@Override
//	public int[] getAccessibleSlotsFromSide(int p_94128_1_)
//	{
//		return new int[] {};
//	};
//	
//	@Override
//	public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2_, int p_102007_3_)
//	{
//		return false;
//	};
//	
//	@Override
//	public boolean canExtractItem(int p_102008_1_, ItemStack p_102008_2_, int p_102008_3_)
//	{
//		return false;
//	};
//	
//	public void setOwner(String owner)
//	{
//		this.owner = owner;
//		markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//	};
//	
//	public String getOwner()
//	{
//		return owner;
//	};
//
//	public void sendUpdateToServer()
//	{
//		PacketShopUpdate update = new PacketShopUpdate(worldObj, xCoord, yCoord, zCoord, prices);
//		LibTech.network.sendToServer(update);
//	};
//
//	/**
//	 * Place money into the container, return true if it worked.
//	 */
//	private boolean placeMoneyIntoContainer(int amount)
//	{
//		// count how much space we have first
//		int space = 0;
//		
//		int i;
//		for (i=0; i<(9*3); i++)
//		{
//			if (inv[i] == null)
//			{
//				space += 64;
//			}
//			else
//			{
//				if (inv[i].getItem() == MaddTech.itemMiniRuby)
//				{
//					space += 64 - inv[i].stackSize;
//				};
//			};
//		};
//		
//		// if not enough space, reject
//		if (space < amount)
//		{
//			return false;
//		};
//		
//		// prefer to fill stacks
//		for (i=0; i<(9*3); i++)
//		{
//			if (inv[i] != null)
//			{
//				if (inv[i].getItem() == MaddTech.itemMiniRuby)
//				{
//					int max = 64-inv[i].stackSize;
//					if (max > amount) max = amount;
//					inv[i].stackSize += max;
//					amount -= max;
//					
//					if (amount == 0) break;
//				};
//			};
//		};
//		
//		// if any left, drop into empty slot
//		if (amount > 0)
//		{
//			for (i=0; i<(9*3); i++)
//			{
//				if (inv[i] == null)
//				{
//					inv[i] = new ItemStack(MaddTech.itemMiniRuby, amount);
//					break;
//				};
//			};
//		};
//		
//		return true;
//	};
//	
//	/**
//	 * Try to pull a single item of the given type from item cables underneath the shop,
//	 * and store it in the shop inventory. Return true if it succeeded.
//	 */
//	private boolean tryPullStack(ItemStack template)
//	{
//		// first try to find space
//		int slot;
//		ItemStack stack = null;
//		
//		for (slot=0; slot<(9*3); slot++)
//		{
//			if (inv[slot] == null)
//			{
//				// use free slot if possible
//				break;
//			};
//			
//			if ((template.getItem() == inv[slot].getItem()) && (template.getItemDamage() == inv[slot].getItemDamage()))
//			{
//				// matching item, see if there is any stack space left
//				if (inv[slot].stackSize < inv[slot].getMaxStackSize())
//				{
//					stack = inv[slot];
//					break;
//				};
//			};
//		};
//		
//		// no space?
//		if (slot == (9*3))
//		{
//			return false;
//		};
//		
//		SearchItem si = new SearchItem(worldObj);
//		si.execute(xCoord, yCoord-1, zCoord, 1);
//		ItemStack inStack = si.pullStack(template);
//		if (inStack == null)
//		{
//			// no stack to pull
//			return false;
//		};
//		
//		if (stack == null)
//		{
//			inv[slot] = inStack.copy();
//		}
//		else
//		{
//			stack.stackSize++;
//		};
//		
//		return true;
//	};
//	
//	/**
//	 * Remove the given amount of items from the inventory, and return true if successful.
//	 */
//	private boolean tryRemoveStack(ItemStack template)
//	{
//		// never remove NBT items!
//		if (template.hasTagCompound())
//		{
//			return false;
//		};
//
//		// count how many of this item we have
//		int count = 0;
//		
//		int i;
//		for (i=0; i<(9*3); i++)
//		{
//			if (inv[i] != null)
//			{
//				if ((template.getItem() == inv[i].getItem()) && (template.getItemDamage() == inv[i].getItemDamage()))
//				{
//					count += inv[i].stackSize;
//				};
//			};
//		};
//		
//		if (count < template.stackSize)
//		{
//			int remaining = template.stackSize - count;
//			while ((remaining--) != 0)
//			{
//				if (!tryPullStack(template))
//				{
//					return false;
//				};
//			};
//		};
//		
//		// now remove
//		int togo = template.stackSize;
//		for (i=0; i<(9*3); i++)
//		{
//			if (inv[i] != null)
//			{
//				if ((template.getItem() == inv[i].getItem()) && (template.getItemDamage() == inv[i].getItemDamage()))
//				{
//					int max = inv[i].stackSize;
//					if (max > togo) max = togo;
//					inv[i].stackSize -= max;
//					if (inv[i].stackSize == 0) inv[i] = null;
//					togo -= max;
//					if (togo == 0) break;
//				};
//			};
//		};
//		
//		return true;
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//
//		if (this.isInvalid())
//		{
//			return;
//		};
//
//		if (!worldObj.isRemote)
//		{
//			if (worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
//			{
//				if (inv[PRODUCT_SLOT_BASE] != null && prices[0] > 0)
//				{
//					// try to pull in the required number of rubies
//					ItemStack rubyStack = new ItemStack(MaddTech.itemMiniRuby);
//					rubyStack.stackSize = 0;
//					
//					SearchItem siRuby = new SearchItem(worldObj);
//					siRuby.execute(xCoord, yCoord+1, zCoord, 0);
//					
//					while (rubyStack.stackSize < prices[0])
//					{
//						if (siRuby.pullStack(rubyStack) == null)
//						{
//							break;
//						}
//						else
//						{
//							rubyStack.stackSize++;
//						};
//					};
//					
//					if (rubyStack.stackSize < prices[0])
//					{
//						if (rubyStack.stackSize != 0)
//						{
//							siRuby.pushStack(rubyStack);
//						};
//					}
//					else
//					{
//						// got the rubies, so try to extract the product
//						ItemStack template = inv[PRODUCT_SLOT_BASE];
//						ItemStack productStack = new ItemStack(template.getItem(), 1, template.getItemDamage());
//						productStack.stackSize = 0;
//						
//						SearchItem siOwner = new SearchItem(worldObj);
//						siOwner.execute(xCoord, yCoord-1, zCoord, 1);
//						
//						while (productStack.stackSize < template.stackSize)
//						{
//							if (siOwner.pullStack(template) == null)
//							{
//								break;
//							}
//							else
//							{
//								productStack.stackSize++;
//							};
//						};
//						
//						if (productStack.stackSize < template.stackSize)
//						{
//							// could not pull of it, return the product and the rubies
//							if (productStack.stackSize != 0)
//							{
//								siOwner.pushStack(productStack);
//							};
//							
//							siRuby.pushStack(rubyStack);
//						}
//						else
//						{
//							// do the exchange
//							siRuby.pushStack(productStack);
//							siOwner.pushStack(rubyStack);
//						};
//					};
//				};
//			};
//			
//			int i;
//			for (i=0; i<4; i++)
//			{
//				if ((inv[PRODUCT_SLOT_BASE+i] != null) && (prices[i] > 0))
//				{
//					// a product to sell
//					if (inv[MONEY_SLOT_BASE+i] != null)
//					{
//						// someone wants to buy it!
//						if (inv[MONEY_SLOT_BASE+i].stackSize >= prices[i])
//						{
//							// and they have enough money!
//							if (inv[OUTPUT_SLOT_BASE+i] == null)
//							{
//								// and the output slot is empty!
//								if (placeMoneyIntoContainer(prices[i]))
//								{
//									// we managed to put the money in, so perform the transaction.
//									inv[MONEY_SLOT_BASE+i].stackSize -= prices[i];
//									if (inv[MONEY_SLOT_BASE+i].stackSize == 0)
//									{
//										inv[MONEY_SLOT_BASE+i] = null;
//									};
//								
//									inv[OUTPUT_SLOT_BASE+i] = inv[PRODUCT_SLOT_BASE+i].copy();
//									if (!tryRemoveStack(inv[PRODUCT_SLOT_BASE+i]))
//									{
//										// could not remove this stack from the inventory, so we are
//										// out of stock
//										inv[PRODUCT_SLOT_BASE+i] = null;
//									};
//								
//									// inventory changed.
//									markDirty();
//								};
//							};
//						};
//					};
//				};
//			};
//		};
//	};
};
