/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;

import net.minecraft.util.*;

public class TileEntitySolarPanel extends TileEntity implements IEnergyMachine
{
	public TileEntitySolarPanel(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private boolean pulled = false;

	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}


//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			pulled = false;
//		};
//	};
//
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return side == 0;
//	};
//
//	@Override
//	public int pushEnergy(int max, int side)
//	{
//		return 0;
//	};
//
//	@Override
//	public int pullEnergy(int energy, int side)
//	{
//		if (side != 0)
//		{
//			return 0;
//		};
//
//		if (energy == 0)
//		{
//			return 0;
//		};
//
//		if (!pulled)
//		{
//			pulled = true;
//			MTDimensionInfo info = MaddRegistry.dimensions.get(worldObj.provider.dimensionId);
//			return info.getSolarEnergy(worldObj, xCoord, yCoord, zCoord);
//		};
//
//		return 0;
//	};
};
