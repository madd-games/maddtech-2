/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import com.madd.maddtech.api.*;
import net.minecraft.block.*;


public class TileEntitySpotlight extends TileEntity implements IEnergyMachine
{
	public TileEntitySpotlight(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	public static int POWER_USAGE = 1;
	public static int CAST_DISTANCE = 24;
	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}


	/**
	 * We need this because MaddTech.blockLight is also considered air block in our case :)
	 */
//	private static boolean isAirBlock(World world, int x, int y, int z)
//	{
//		Block block = world.getBlock(x, y, z);
//		return (block == Blocks.air) || (block == MaddTech.blockLight);
//	};
//
//	public static void setGlow(World world, int xc, int yc, int zc, boolean shouldGlow, int glowSide)
//	{
//		// update the beam :) :) :)
//		Block blockPlacing = Blocks.air;
//		if (shouldGlow)
//		{
//			blockPlacing = MaddTech.blockLight;
//		};
//
//		int dx = SideUtil.getSideX(glowSide);
//		int dy = SideUtil.getSideY(glowSide);
//		int dz = SideUtil.getSideZ(glowSide);
//
//		int x = xc + dx;
//		int y = yc + dy;
//		int z = zc + dz;
//
//		int i;
//		for (i=0; i<CAST_DISTANCE; i++)
//		{
//			if (isAirBlock(world, x, y, z))
//			{
//				world.setBlock(x, y, z, blockPlacing);
//			}
//			else
//			{
//				blockPlacing = Blocks.air;
//			};
//
//			x += dx;
//			y += dy;
//			z += dz;
//		};
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{	
//			int glowSide = worldObj.getBlockMetadata(xCoord, yCoord, zCoord) % 6;
//			boolean shouldGlow = false;
//			if (worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
//			{
//				SearchEnergy search = new SearchEnergy(worldObj);
//				search.execute(xCoord-SideUtil.getSideX(glowSide),
//						yCoord-SideUtil.getSideY(glowSide),
//						zCoord-SideUtil.getSideZ(glowSide),
//						glowSide);
//				if (search.pullEnergy(POWER_USAGE) == POWER_USAGE)
//				{
//					shouldGlow = true;
//				};
//			};
//
//			int meta = glowSide;
//			if (shouldGlow) meta += 6;
//			worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
//			
//			setGlow(worldObj, xCoord, yCoord, zCoord, shouldGlow, glowSide);
//		};
//	};
//
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return side == SideUtil.opposite(worldObj.getBlockMetadata(xCoord, yCoord, zCoord) % 6);
//	};
//
//	@Override
//	public int pushEnergy(int max, int side)
//	{
//		return 0;
//	};
//
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		return 0;
//	};
};
