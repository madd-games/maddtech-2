/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import com.madd.maddtech.api.*;

import java.io.*;
import net.minecraft.world.*;

public class TileEntitySteamGenerator extends TileEntity implements IInventory, IEnergyMachine, IFluidMachine
{
	public TileEntitySteamGenerator(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private ItemStack[] inv = new ItemStack[1];
	private int waterBuffer = 0;
	private int temperature = 200;

	// how long the current item will keep burning
	private int timeToBurn = 0;
	private int maxBurnTime = 0;
	@Override
	public void clearContent() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean canConnectFluid(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String getFluidType(int side) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int pullFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pushFluid(String id, int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int getContainerSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public ItemStack getItem(int p_70301_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItem(int p_70298_1_, int p_70298_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ItemStack removeItemNoUpdate(int p_70304_1_) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setItem(int p_70299_1_, ItemStack p_70299_2_) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean stillValid(PlayerEntity p_70300_1_) {
		// TODO Auto-generated method stub
		return false;
	}


//	@Override
//	public int getSizeInventory()
//	{
//		return 1;
//	};
//	
//	@Override
//	public ItemStack getStackInSlot(int i)
//	{
//		return inv[i];
//	};
//	
//	@Override
//	public ItemStack decrStackSize(int slot, int amt)
//	{
//		ItemStack out = null;
//
//		if (inv[slot] != null)
//		{
//			if (inv[slot].stackSize <= amt)
//			{
//				out = inv[slot];
//				inv[slot] = null;
//			}
//			else
//			{
//				out = inv[slot].splitStack(amt);
//				if (inv[slot].stackSize == 0)
//				{
//					inv[slot] = null;
//				};
//			};
//		};
//
//		return out;
//	};
//	
//	@Override
//	public ItemStack getStackInSlotOnClosing(int i)
//	{
//		ItemStack out = inv[i];
//		inv[i] = null;
//		return out;
//	};
//	
//	@Override
//	public void setInventorySlotContents(int i, ItemStack istack)
//	{
//		inv[i] = istack;
//	};
//	
//	@Override
//	public String getInventoryName()
//	{
//		return "SteamGenerator";
//	};
//	
//	@Override
//	public boolean hasCustomInventoryName()
//	{
//		return true;
//	};
//	
//	@Override
//	public int getInventoryStackLimit()
//	{
//		return 64;
//	};
//	
//	@Override
//	public boolean isUseableByPlayer(EntityPlayer player)
//	{
//		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this &&
//				player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
//	};
//	
//	@Override
//	public boolean isItemValidForSlot(int i, ItemStack istack)
//	{
//		return true;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		
//		NBTTagList itemList = new NBTTagList();
//		int i;
//		for (i=0; i<inv.length; i++)
//		{
//			ItemStack stack = inv[i];
//			if (stack != null)
//			{
//				NBTTagCompound tag = new NBTTagCompound();
//				tag.setByte("Slot", (byte) i);
//				stack.writeToNBT(tag);
//				itemList.appendTag(tag);
//			};
//		};
//		comp.setTag("Inventory", itemList);
//		comp.setInteger("BurnTime", timeToBurn);
//		comp.setInteger("MaxBurnTime", maxBurnTime);
//		comp.setInteger("Temperature", temperature);
//		comp.setInteger("WaterBuffer", waterBuffer);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		
//		NBTTagList itemList = comp.getTagList("Inventory", comp.getId());
//		int i;
//		for (i=0; i<itemList.tagCount(); i++)
//		{
//			NBTTagCompound tag = (NBTTagCompound) itemList.getCompoundTagAt(i);
//			byte slot = tag.getByte("Slot");
//			inv[slot] = ItemStack.loadItemStackFromNBT(tag);
//		};
//
//		timeToBurn = comp.getInteger("BurnTime");
//		maxBurnTime = comp.getInteger("MaxBurnTime");
//		temperature = comp.getInteger("Temperature");
//		waterBuffer = comp.getInteger("WaterBuffer");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("BurnTime", timeToBurn);
//		comp.setInteger("MaxBurnTime", maxBurnTime);
//		comp.setInteger("Temperature", temperature);
//		comp.setInteger("WaterBuffer", waterBuffer);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		timeToBurn = comp.getInteger("BurnTime");
//		maxBurnTime = comp.getInteger("MaxBurnTime");
//		temperature = comp.getInteger("Temperature");
//		waterBuffer = comp.getInteger("WaterBuffer");
//	};
//
//	@Override
//	public void openInventory() {};
//	public void closeInventory() {};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//		if (!worldObj.isRemote)
//		{
//			int outside = getOutputSide();
//
//			if (timeToBurn == 0)
//			{
//				timeToBurn = TileEntityFurnace.getItemBurnTime(inv[0]) / 5;
//				if (timeToBurn == 0)
//				{
//					if (temperature < 1000)
//					{
//						temperature -= 1;
//					}
//					else
//					{
//						temperature -= 2;
//					};
//
//					if (temperature < 200) temperature = 200;
//				}
//				else
//				{
//					maxBurnTime = timeToBurn;
//					decrStackSize(0, 1);
//				};
//			};
//
//			if (timeToBurn > 0)
//			{
//				timeToBurn--;
//
//				if (temperature < 1000)
//				{
//					temperature += 2;
//				}
//				else
//				{
//					temperature += 1;
//				};
//			};
//
//			if (temperature > 3500) temperature = 3500;
//			int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
//			int newMeta = (meta&3) | ((temperature/1000) << 2);
//			if (newMeta != meta)
//			{
//				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, newMeta, 3);
//			};
//			
//			SearchFluid search = new SearchFluid(worldObj);
//			search.execute(xCoord, yCoord+1, zCoord, 0);
//			int toPull = 1000 - waterBuffer;
//			waterBuffer += search.pullFluid("maddtech:water", toPull);
//
//			if ((waterBuffer >= 1000) && (temperature >= 1000))
//			{
//				waterBuffer -= 1000;
//				SearchEnergy se = new SearchEnergy(worldObj);
//				se.execute(xCoord+SideUtil.getSideX(outside), yCoord+SideUtil.getSideY(outside), zCoord+SideUtil.getSideZ(outside),
//					SideUtil.opposite(outside));
//				se.pushEnergy((temperature/1000) * 5);
//			};
//
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//
//	public int getEnergy()
//	{
//		return 0;
//	};
//
//	private int getOutputSide()
//	{
//		return (worldObj.getBlockMetadata(xCoord, yCoord, zCoord) & 3)+2;
//	};
//
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return (side == getOutputSide());
//	};
//
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		return 0;
//	};
//
//	@Override
//	public int pushEnergy(int de, int side)
//	{
//		return 0;
//	};
//
//	@Override
//	public void markDirty()
//	{
//		super.markDirty();
//	};
//
//	public int getBurnTimeRemainingScaled(int scale)
//	{
//		if (maxBurnTime == 0) return 0;
//
//		return scale * timeToBurn / maxBurnTime;
//	};
//
//	@Override
//	public boolean canConnectFluid(int side)
//	{
//		return side == 1;
//	};
//
//	@Override
//	public String getFluidType(int side)
//	{
//		return "maddtech:empty";
//	};
//
//	@Override
//	public int pullFluid(String id, int max, int side)
//	{
//		return 0;
//	};
//
//	@Override
//	public int pushFluid(String id, int max, int side)
//	{
//		if ((side == 0) && (id.equals("maddtech:hotair")))
//		{
//			int maxToTake = 3500 - temperature;
//			if (max > maxToTake) max = maxToTake;
//			temperature += max;
//			return max;
//		}
//		else if ((side == 1) && (id.equals("maddtech:water")))
//		{
//			int left = 5000 - waterBuffer;
//			if (max > left) max = left;
//			waterBuffer += max;
//			return max;
//		};
//
//		return 0;
//	};
//
//	public int getTemperature()
//	{
//		return temperature/10;
//	};
//
//	public int getWater()
//	{
//		return waterBuffer;
//	};
//	
//	public World getWorld()
//	{
//		return worldObj;
//	};
};
