/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.inventory.*;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.item.*;
import net.minecraft.world.*;

import com.madd.maddtech.api.*;
import com.madd.maddtech.multiblock.MachineStruct;

import java.io.*;

public class TileEntitySubstation extends TileEntity implements IEnergyMachine
{
	public TileEntitySubstation(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}


	public static final int MAX_ENERGY = 500000;
	
	private int energy = 0;
	private MachineStruct struct;
	
	
	public int getEnergy()
	{
		return energy;
	}


	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	};

//	public World getWorld()
//	{
//		return worldObj;
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setInteger("SubstationEnergy", energy);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		energy = comp.getInteger("SubstationEnergy");
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		comp.setInteger("SubstationEnergy", energy);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		energy = comp.getInteger("SubstationEnergy");
//	};
//	
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//		
//		if (this.isInvalid())
//		{
//			return;
//		};
//		
//		if (!worldObj.isRemote)
//		{
//			if (struct == null)
//			{
//				struct = LibTech.fitStruct(worldObj, xCoord, yCoord, zCoord, LibTech.machSubstation);
//				if (struct == null)
//				{
//					BlockMaddSubstation block = (BlockMaddSubstation) worldObj.getBlock(xCoord, yCoord, zCoord);
//					block.pop(worldObj, xCoord, yCoord, zCoord);
//					invalidate();
//					return;
//				};
//			};
//
//			if (!struct.isFoundAt(worldObj, xCoord, yCoord, zCoord))
//			{
//				BlockMaddSubstation block = (BlockMaddSubstation) worldObj.getBlock(xCoord, yCoord, zCoord);
//				block.pop(worldObj, xCoord, yCoord, zCoord);
//				if (!worldObj.isRemote) invalidate();
//				return;
//			};
//			
//			Coords coords = struct.getFunctionBlock("energy_in");
//			if (coords != null)
//			{
//				SearchEnergy se = new SearchEnergy(worldObj);
//				se.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 6);
//				
//				int toPull = MAX_ENERGY - energy;
//				energy += se.pullEnergy(toPull);
//			};
//			
//			int meta = (energy * 4 / MAX_ENERGY);
//			if (meta > 3) meta = 3;
//			
//			if (worldObj.getBlockMetadata(xCoord, yCoord, zCoord) != meta)
//			{
//				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta, 3);
//			};
//
//			markDirty();
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		};
//	};
//	
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return (side == 6) || (side == 7);
//	};
//	
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		if (side != 7) return 0;
//		
//		if (max > energy)
//		{
//			max = energy;
//		};
//
//		energy -= max;
//		markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		return max;
//	};
//
//	@Override
//	public int pushEnergy(int de, int side)
//	{
//		if (side != 6) return 0;
//		
//		if ((energy+de) > MAX_ENERGY)
//		{
//			de = MAX_ENERGY - energy;
//		};
//
//		energy += de;
//		markDirty();
//		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//		return de;
//	};
};
