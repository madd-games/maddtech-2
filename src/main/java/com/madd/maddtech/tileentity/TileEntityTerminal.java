/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;


import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;
import net.minecraft.tileentity.*;
import net.minecraft.nbt.*;
import net.minecraft.network.*;
import net.minecraft.network.play.server.*;
import net.minecraft.block.*;
import com.madd.maddtech.api.*;
import java.io.*;
import net.minecraft.entity.item.*;
import java.util.*;

public class TileEntityTerminal extends TileEntity implements IEnergyMachine
{
	public TileEntityTerminal(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}

	private int energyBuffer = 0;
	
	/**
	 * Describes the 4 links of the terminal. The indices indicate which side it
	 * is describing:
	 * 0 = red, 1 = green, 2 = blue, 3 = white
	 * The string can either be "*", indicating that anyone can simply connect to this
	 * terminal to form a network of links (as if all cables were connected together),
	 * an empty (or otherwise invalid) string indicates no link, or otherwise a string
	 * indicating a specific side of a specific terminal, in the following format:
	 * "X.Y.Z.S", where "X", "Y" and "Z" simply denote the coordinates of the remote
	 * terminal (and can be negative), and "S" is one of the numbers 1-4, indicating
	 * the color of the side on the remote terminal, through which this link would leave.
	 * The other side must accept the link!
	 */
	private String[] links = new String[] {"", "", "", ""};

	@Override
	public boolean canConnectEnergy(int side) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int pushEnergy(int energy, int side) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pullEnergy(int max, int side) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
//	private void writeInfo(NBTTagCompound comp)
//	{
//		comp.setInteger("TermEnergyBuffer", energyBuffer);
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			comp.setString("TermLink" + i, "/" + links[i]);
//		};
//	};
//	
//	private void readInfo(NBTTagCompound comp)
//	{
//		energyBuffer = comp.getInteger("TermEnergyBuffer");
//		
//		int i;
//		for (i=0; i<4; i++)
//		{
//			links[i] = comp.getString("TermLink" + i).substring(1);
//		};
//	};
//	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		writeInfo(comp);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		readInfo(comp);
//	};
//
//	@Override
//	public Packet getDescriptionPacket()
//	{
//		NBTTagCompound comp = new NBTTagCompound();
//		writeInfo(comp);
//		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, comp);
//	};
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
//	{
//		NBTTagCompound comp = pkt.func_148857_g();
//		readInfo(comp);
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		super.updateEntity();
//
//		if (this.isInvalid())
//		{
//			return;
//		};
//
//		if (!worldObj.isRemote)
//		{
//		
//		};
//	};
//	
//	@Override
//	public boolean canConnectEnergy(int side)
//	{
//		return side == 1;
//	};
//	
//	@Override
//	public int pushEnergy(int energy, int side)
//	{
//		if (side == 1)
//		{
//			energyBuffer += energy;
//			return energy;
//		};
//		
//		return 0;
//	};
//	
//	@Override
//	public int pullEnergy(int max, int side)
//	{
//		return 0;
//	};
//	
//	/**
//	 * Returns the target "glownet address" of the specified side of the terminal.
//	 */
//	public String getPeerAddr(int side)
//	{
//		return links[side-2];
//	};
//	
//	/**
//	 * Returns the full "glownet address" of the specified side of the terminal.
//	 */
//	public String getSideAddr(int side)
//	{
//		return "" + xCoord + "." + yCoord + "." + zCoord + "." + (side-1);
//	};
//	
//	/**
//	 * Returns true "sourceAddr" is allowed to connect to "side".
//	 */
//	public boolean canGlownetConnect(String sourceAddr, int side)
//	{
//		String link = links[side-2];
//		if (link.equals("*"))
//		{
//			return true;
//		};
//		
//		return link.equals(sourceAddr);
//	};
//
//	/**
//	 * Attempt to fill the energy buffer up to the specified amount.
//	 */
//	private void tryFillEnergyBuffer(int energy)
//	{
//		if (energyBuffer < energy)
//		{
//			int toPull = energy - energyBuffer;
//			
//			SearchEnergy se = new SearchEnergy(worldObj);
//			se.execute(xCoord, yCoord+1, zCoord, 0);
//			energyBuffer += se.pullEnergy(toPull);
//		};
//	};
//	
//	/**
//	 * Returns an ArrayList of coordinates to search by a cable connected to a specified side,
//	 * and pulls the amount of energy necessary.
//	 */
//	public ArrayList<Coords> getPeers(int side)
//	{
//		SearchGlownet sg = new SearchGlownet(worldObj);
//		sg.execute(xCoord, yCoord-1, zCoord, 1, getSideAddr(side), links[side-2]);
//		
//		int dist = sg.getDistance();
//		int cost = dist / 10 + 1;
//		tryFillEnergyBuffer(cost);
//		
//		if (energyBuffer < cost)
//		{
//			return new ArrayList<Coords>();
//		};
//		
//		energyBuffer -= cost;
//		return sg.getPeerCoords();
//	};
//	
//	/**
//	 * Change the target of the given side to 'target'. The linkno is in the range 0-3, unlike
//	 * the 2-5 used in some places above. On the server, it sets the link, and marks the block
//	 * for update, and the tile entity as dirty. On the client, it sends an update packet.
//	 */
//	public void setTarget(int linkno, String target)
//	{
//		if (!worldObj.isRemote)
//		{
//			links[linkno] = target;
//			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
//			markDirty();
//		}
//		else
//		{
//			PacketTerminalUpdate update = new PacketTerminalUpdate(worldObj, xCoord, yCoord, zCoord, linkno, target);
//			LibTech.network.sendToServer(update);
//		}
//	};
};
