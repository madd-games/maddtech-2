/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.tileentity;

import net.minecraft.tileentity.*;
import net.minecraft.world.*;
import net.minecraft.item.*;

import com.madd.maddtech.api.*;
import com.madd.maddtech.multiblock.MachineStruct;

import net.minecraft.nbt.*;
import net.minecraft.inventory.*;
import net.minecraft.entity.player.*;
import java.util.*;

public class TileEntityUraniumRefinery extends TileEntity
{
	public TileEntityUraniumRefinery(TileEntityType<?> p_i48289_1_) {
		super(p_i48289_1_);
		// TODO Auto-generated constructor stub
	}
	private MachineStruct struct = null;
	private int sideFacing = 0;
	private int energyBuffer = 0;
	private int uraniumToGo = 0;
	private Random random = new Random();
	
	
//	@Override
//	public void writeToNBT(NBTTagCompound comp)
//	{
//		super.writeToNBT(comp);
//		comp.setInteger("UraniumToGo", uraniumToGo);
//		comp.setInteger("EnergyBuffer", energyBuffer);
//	};
//	
//	@Override
//	public void readFromNBT(NBTTagCompound comp)
//	{
//		super.readFromNBT(comp);
//		uraniumToGo = comp.getInteger("UraniumToGo");
//		energyBuffer = comp.getInteger("EnergyBuffer");
//	};
//
//	@Override
//	public void updateEntity()
//	{
//		if (!worldObj.isRemote)
//		{
//			if (struct == null)
//			{
//				struct = LibTech.fitStruct(worldObj, xCoord, yCoord, zCoord, LibTech.machUraniumRefinery);
//				if (struct == null)
//				{
//					BlockMaddUraniumRefinery block = (BlockMaddUraniumRefinery) worldObj.getBlock(xCoord, yCoord, zCoord);
//					block.pop(worldObj, xCoord, yCoord, zCoord);
//					invalidate();
//					return;
//				};
//				sideFacing = struct.sideFacing;
//			};
//
//			if (!struct.isFoundAt(worldObj, xCoord, yCoord, zCoord))
//			{
//				BlockMaddUraniumRefinery block = (BlockMaddUraniumRefinery) worldObj.getBlock(xCoord, yCoord, zCoord);
//				block.pop(worldObj, xCoord, yCoord, zCoord);
//				if (!worldObj.isRemote) invalidate();
//				return;
//			};
//
//			Coords coords = struct.getFunctionBlock("redstone_in");
//			if (coords != null)
//			{
//				if (worldObj.isBlockIndirectlyGettingPowered(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z))
//				{
//					if (uraniumToGo == 0)
//					{
//						coords = struct.getFunctionBlock("item_in");
//						if (coords != null)
//						{
//							SearchItem searchItem = new SearchItem(worldObj);
//							searchItem.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 0);
//							ItemStack stack = searchItem.pullStack(new ItemStack(MaddTech.itemUraniumDust, 1, 0));
//							if (stack != null)
//							{
//								uraniumToGo += 250;
//							};
//						};
//					};
//					
//					if (uraniumToGo > 0)
//					{
//						if (energyBuffer < 10)
//						{
//							coords = struct.getFunctionBlock("energy_in");
//							if (coords != null)
//							{
//								SearchEnergy searchEnergy = new SearchEnergy(worldObj);
//								searchEnergy.execute(xCoord+coords.x, yCoord+coords.y, zCoord+coords.z, 0);
//								energyBuffer += searchEnergy.pullEnergy(10-energyBuffer);
//							};
//						};
//						
//						if (energyBuffer >= 10)
//						{
//							energyBuffer -= 10;
//							SearchFluid searchFluid = new SearchFluid(worldObj);
//							searchFluid.execute(xCoord-SideUtil.getSideX(sideFacing),
//										yCoord-SideUtil.getSideY(sideFacing),
//										zCoord-SideUtil.getSideZ(sideFacing),
//										0);
//							int buffer = 1;
//							uraniumToGo--;
//							buffer = searchFluid.pushFluid("maddtech:refu", 1);
//							uraniumToGo += buffer;
//						
//							int dist;
//							for (dist=2; dist<5; dist++)
//							{
//								int centX = xCoord - dist * SideUtil.getSideX(sideFacing);
//								int centY = yCoord - dist * SideUtil.getSideY(sideFacing) - 1;
//								int centZ = zCoord - dist * SideUtil.getSideZ(sideFacing);
//							
//								int meta = worldObj.getBlockMetadata(centX, centY, centZ);
//								int add = 1;
//								if (random.nextInt(100) < 50)
//								{
//									add = 3;
//								};
//								worldObj.setBlockMetadataWithNotify(centX, centY, centZ, (meta+add)%16, 2);
//							};
//						};
//					};
//				};
//			};
//		};
//	};
};
