/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * LibTech.java
 * Various utility functions.
 */

package com.madd.maddtech.util;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.*;
import net.minecraft.item.*;
import net.minecraft.world.*;

import com.madd.maddtech.MaddTech;
import com.madd.maddtech.api.*;
import com.madd.maddtech.block.BlockLampOff;
import com.madd.maddtech.block.BlockLampOn;
import com.madd.maddtech.block.BlockLampOnInv;
import com.madd.maddtech.fluid.FluidHotAir;
import com.madd.maddtech.fluid.FluidLava;
import com.madd.maddtech.fluid.FluidNaturalGas;
import com.madd.maddtech.fluid.FluidPlutonium;
import com.madd.maddtech.fluid.FluidUranium;
import com.madd.maddtech.fluid.FluidWater;
import com.madd.maddtech.integration.Integrator;
import com.madd.maddtech.multiblock.MachineStruct;
import com.madd.maddtech.registry.ChemicalReactorRecipeDiamond;
import com.madd.maddtech.registry.EngTableRecipeCrafting;

import net.minecraftforge.client.*;
import net.minecraft.client.renderer.entity.*;
import org.lwjgl.opengl.*;
import net.minecraftforge.common.util.*;
import java.io.*;

public class LibTech
{
	public static int lampGlowRenderID;
	public static int fakeRenderID;
	public static int renderCableID;
	public static int renderDynamoID;
	public static int renderFluidTankID;
	public static int renderCentrifugeID;
	public static int renderLightmapID;
	public static int renderRotatingID;
	public static int renderRedstonePulserID;
	public static int renderAlternatorID;
	
	public static MachineStruct machChemicalReactor;
	public static MachineStruct machElectricFurnace;
	public static MachineStruct machMacerator;
	public static MachineStruct machUraniumRefinery;
	public static MachineStruct machNuclearReactor;
	public static MachineStruct machCompressor;
	public static MachineStruct machSubstation;
	
	//public static SimpleNetworkWrapper network;

	//public static ItemArmor.ArmorMaterial MAT_SAFETY_SUIT = EnumHelper.addArmorMaterial("MAT_SAFETY_SUIT", 7, new int[] {2, 4, 3, 2}, 1);

	public static void init()
	{
//		GameRegistry.registerTileEntity(TileEntityCrate.class, "MT_TileEntityCrate");
//		GameRegistry.registerTileEntity(TileEntityChemicalReactor.class, "MT_TileEntityChemicalReactor");
//		GameRegistry.registerTileEntity(TileEntityBarrel.class, "MT_TileEntityBarrel");
//		GameRegistry.registerTileEntity(TileEntityPump.class, "MT_TileEntityPump");
//		GameRegistry.registerTileEntity(TileEntityGasExtractor.class, "MT_TileEntityGasExtractor");
//		GameRegistry.registerTileEntity(TileEntityItemTransporter.class, "MT_TileEntityItemTransporter");
//		GameRegistry.registerTileEntity(TileEntityAccumulator.class, "MT_TileEntityAccumulator");
//		GameRegistry.registerTileEntity(TileEntityEnergyShop.class, "MT_EnergyShop");
//		GameRegistry.registerTileEntity(TileEntityFluidShop.class, "MT_FluidShop");
//		GameRegistry.registerTileEntity(TileEntityElectricFurnace.class, "MT_TileEntityElectricFurnace");
//		GameRegistry.registerTileEntity(TileEntityMacerator.class, "MT_TileEntityMacerator");
//		GameRegistry.registerTileEntity(TileEntityUraniumRefinery.class, "MT_TileEntityUraniumRefinery");
//		GameRegistry.registerTileEntity(TileEntityNuclearReactor.class, "MT_TileEntityNuclearReactor");
//		GameRegistry.registerTileEntity(TileEntitySpotlight.class, "MT_TileEntitySpotlight");
//		GameRegistry.registerTileEntity(TileEntitySolarPanel.class, "MT_TileEntitySolarPanel");
//		GameRegistry.registerTileEntity(TileEntityLaser.class, "MT_TileEntityLaser");
//		GameRegistry.registerTileEntity(TileEntityFluidDispenser.class, "MT_TileEntityFluidDispenser");
//		GameRegistry.registerTileEntity(TileEntitySteamGenerator.class, "MT_TileEntitySteamGenerator");
//		GameRegistry.registerTileEntity(TileEntityFluidTank.class, "MT_TileEntityFluidTank");
//		GameRegistry.registerTileEntity(TileEntityEngTable.class, "MT_EngTable");
//		GameRegistry.registerTileEntity(TileEntityCompressor.class, "MT_Compressor");
//		GameRegistry.registerTileEntity(TileEntityPlacer.class, "MT_BlockPlacer");
//		GameRegistry.registerTileEntity(TileEntityPulser.class, "MT_RedstonePulser");
//		GameRegistry.registerTileEntity(TileEntityShop.class, "MT_Shop");
//		GameRegistry.registerTileEntity(TileEntityIronCrate.class, "MT_IronCrate");
//		GameRegistry.registerTileEntity(TileEntityMotionSensor.class, "MT_MotionSensor");
//		GameRegistry.registerTileEntity(TileEntityTerminal.class, "MT_Terminal");
//		GameRegistry.registerTileEntity(TileEntityItemMonitor.class, "MT_ItemMonitor");
//		GameRegistry.registerTileEntity(TileEntityRedstoneLatch.class, "MT_RedstoneLatch");
//		GameRegistry.registerTileEntity(TileEntityFluidMonitor.class, "MT_FluidMonitor");
//		GameRegistry.registerTileEntity(TileEntitySubstation.class, "MT_Substation");
//		Integrator.registerAllTileEntities();
//		
//		NetworkRegistry.INSTANCE.registerGuiHandler(MaddTech.instance, new GuiHandler());

		machChemicalReactor = new MachineStruct();
		machChemicalReactor.set(2, 2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(1, -2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 1, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(0, -1, 4, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(0, 2, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, -1, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(0, 2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, -2, 3, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, 0, 1, Blocks.air);
		machChemicalReactor.set(-2, -1, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, 0, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, 2, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(2, -2, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(1, 1, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(2, -2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, 0, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, 2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, -1, 4, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(1, 2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 0, 0, MaddTech.blockTinShell, 5);
		//machChemicalReactor.set(1, 1, 3, Blocks.air);
		machChemicalReactor.set(2, 2, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, 1, 1, Blocks.air);
		machChemicalReactor.set(1, 1, 4, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(2, 1, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(1, 2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, -2, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(0, 1, 0, MaddTech.blockTinShell, 0);
		machChemicalReactor.set(-1, -2, 2, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(-1, 0, 3, Blocks.air);
		machChemicalReactor.set(0, -1, 0, MaddTech.blockTinShell, 1);
		machChemicalReactor.set(1, 2, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(-1, -2, 4, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(0, 1, 2, Blocks.air);
		//machChemicalReactor.set(-1, 0, 1, Blocks.air);
		machChemicalReactor.set(-1, -1, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(0, 1, 4, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(1, -1, 4, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(2, -2, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(1, 0, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(2, 0, 2, MaddTech.blockTinShellRedstoneIn, 2);
		machChemicalReactor.set(0, -2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, -1, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(0, 0, 3, Blocks.air);
		//machChemicalReactor.set(-1, -1, 1, Blocks.air);
		machChemicalReactor.set(2, 2, 2, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, 0, 2, Blocks.air);
		//machChemicalReactor.set(0, 0, 1, Blocks.air);
		machChemicalReactor.set(-2, 2, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(2, 0, 4, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, -1, 3, Blocks.air);
		machChemicalReactor.set(-2, 1, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, 1, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(0, -2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(1, -2, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(-1, -1, 2, Blocks.air);
		machChemicalReactor.set(-2, 1, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(-1, 1, 2, Blocks.air);
		machChemicalReactor.set(-1, 2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, 1, 4, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(1, 0, 4, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(1, 2, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, 0, 3, Blocks.air);
		machChemicalReactor.set(-1, -2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, -1, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(1, -2, 3, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, -1, 1, Blocks.air);
		machChemicalReactor.set(2, -2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(0, 2, 0, MaddTech.blockTinShell, 0);
		//machChemicalReactor.set(0, -1, 3, Blocks.air);
		machChemicalReactor.set(0, 2, 2, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(0, -1, 1, Blocks.air);
		machChemicalReactor.set(0, 2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, 2, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 0, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, 2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, 0, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, -2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 1, 2, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(0, -1, 2, Blocks.air);
		machChemicalReactor.set(1, -1, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(-2, -2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 1, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(-2, -2, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 2, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(-2, 0, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(-2, 2, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, -2, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(-1, 0, 2, Blocks.air);
		machChemicalReactor.set(2, 1, 4, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(0, 1, 1, Blocks.air);
		//machChemicalReactor.set(-1, -1, 3, Blocks.air);
		machChemicalReactor.set(-1, 0, 0, MaddTech.blockTinShell, 4);
		//machChemicalReactor.set(0, 1, 3, Blocks.air);
		machChemicalReactor.set(-2, -1, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, -2, 0, MaddTech.blockTinShell, 4);
		machChemicalReactor.set(2, -1, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(-2, 0, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(1, -2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, -1, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, -2, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(2, 0, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(0, 0, 4, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(1, 2, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, 2, 4, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(0, 0, 2, Blocks.air);
		machChemicalReactor.set(0, -2, 0, MaddTech.blockTinShell, 1);
		machChemicalReactor.set(-2, 1, 4, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(0, -2, 1, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, 1, 2, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-1, 0, 4, MaddTech.blockTinShell, 5);
		//machChemicalReactor.set(-1, 1, 1, Blocks.air);
		machChemicalReactor.set(-1, 2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(0, -2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, -1, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, -1, 1, MaddTech.blockTinShell, 2);
		//machChemicalReactor.set(1, -1, 2, Blocks.air);
		machChemicalReactor.set(-2, 1, 0, MaddTech.blockTinShell, 4);
		//machChemicalReactor.set(-1, 1, 3, Blocks.air);
		//machChemicalReactor.set(1, 1, 2, Blocks.air);
		machChemicalReactor.set(1, -2, 0, MaddTech.blockTinShell, 5);
		machChemicalReactor.set(-2, 2, 3, MaddTech.blockTinShell, 2);
		machChemicalReactor.set(-2, -1, 0, MaddTech.blockTinShell, 4);

		machElectricFurnace = new MachineStruct();
		machElectricFurnace.set(0, 1, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(-1, 0, 0, MaddTech.blockTinShell, 4);
		machElectricFurnace.set(-1, 0, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(1, -1, 0, MaddTech.blockTinShell, 5);
		machElectricFurnace.set(0, 1, 2, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(1, 0, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(1, 0, 0, MaddTech.blockTinShell, 5);
		machElectricFurnace.set(0, 0, 2, MaddTech.blockTinShell, 5);
		machElectricFurnace.set(1, 0, 2, MaddTech.blockTinShell, 2);
		//machElectricFurnace.set(0, 0, 1, Blocks.air);
		machElectricFurnace.set(0, -1, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(1, 1, 2, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(0, -1, 0, MaddTech.blockTinShell, 1);
		machElectricFurnace.set(-1, 1, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(0, 1, 0, MaddTech.blockTinShell, 0);
		machElectricFurnace.set(1, 1, 0, MaddTech.blockTinShell, 5);
		machElectricFurnace.set(1, -1, 2, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(0, -1, 2, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(1, 1, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(1, -1, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(-1, 1, 0, MaddTech.blockTinShell, 4);
		machElectricFurnace.set(-1, -1, 0, MaddTech.blockTinShell, 4);
		machElectricFurnace.set(-1, 1, 2, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(-1, -1, 1, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(-1, 0, 2, MaddTech.blockTinShell, 2);
		machElectricFurnace.set(-1, -1, 2, MaddTech.blockTinShell, 2);

		machMacerator = new MachineStruct();
		machMacerator.set(0, 1, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(-1, 0, 0, MaddTech.blockSteelShell, 4);
		machMacerator.set(-1, 0, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(1, -1, 0, MaddTech.blockSteelShell, 5);
		machMacerator.set(0, 1, 2, MaddTech.blockSteelShell, 2);
		machMacerator.set(1, 0, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(1, 0, 0, MaddTech.blockSteelShell, 5);
		machMacerator.set(0, 0, 2, MaddTech.blockSteelShell, 5);
		machMacerator.set(1, 0, 2, MaddTech.blockSteelShell, 2);
		//machMacerator.set(0, 0, 1, Blocks.air);
		machMacerator.set(0, -1, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(1, 1, 2, MaddTech.blockSteelShell, 2);
		machMacerator.set(0, -1, 0, MaddTech.blockSteelShell, 1);
		machMacerator.set(-1, 1, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(0, 1, 0, MaddTech.blockSteelShell, 0);
		machMacerator.set(1, 1, 0, MaddTech.blockSteelShell, 5);
		machMacerator.set(1, -1, 2, MaddTech.blockSteelShell, 2);
		machMacerator.set(0, -1, 2, MaddTech.blockSteelShell, 2);
		machMacerator.set(1, 1, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(1, -1, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(-1, 1, 0, MaddTech.blockSteelShell, 4);
		machMacerator.set(-1, -1, 0, MaddTech.blockSteelShell, 4);
		machMacerator.set(-1, 1, 2, MaddTech.blockSteelShell, 2);
		machMacerator.set(-1, -1, 1, MaddTech.blockSteelShell, 2);
		machMacerator.set(-1, 0, 2, MaddTech.blockSteelShell, 2);
		machMacerator.set(-1, -1, 2, MaddTech.blockSteelShell, 2);

		machUraniumRefinery = new MachineStruct();
		//machUraniumRefinery.set(-1, 0, 5, Blocks.air);
		machUraniumRefinery.set(-2, -1, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, -1, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 0, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, 1, 3, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(2, -1, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 0, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, -1, 2, MaddTech.blockSteelShell);
		machUraniumRefinery.set(2, -1, 2, MaddTech.blockLeadShell, 2);
		//machUraniumRefinery.set(1, 0, 5, Blocks.air);
		machUraniumRefinery.set(0, -1, 5, MaddTech.blockSteelShell);
		machUraniumRefinery.set(-2, -1, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 0, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, -1, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 1, 0, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(1, -2, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 0, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 0, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, -2, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, -1, 4, MaddTech.blockSteelShell);
		machUraniumRefinery.set(-1, -2, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, -2, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -2, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 1, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, -2, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, -2, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, 1, 1, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(2, 1, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, -2, 0, MaddTech.blockLeadShell, 1);
		machUraniumRefinery.set(2, 0, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 1, 0, MaddTech.blockLeadShell, 0);
		machUraniumRefinery.set(-1, -2, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 0, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 1, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, -1, 5, MaddTech.blockSteelShell);
		machUraniumRefinery.set(0, 1, 2, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(1, -1, 6, MaddTech.blockLeadShell, 4);
		//machUraniumRefinery.set(-1, 0, 1, Blocks.air);
		machUraniumRefinery.set(1, 1, 6, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(0, 1, 4, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(2, -2, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 1, 6, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(2, 0, 0, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(2, 0, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 0, 5, MaddTech.blockFluidCable);
		machUraniumRefinery.set(1, 0, 0, MaddTech.blockLeadShell, 5);
		//machUraniumRefinery.set(1, 0, 4, Blocks.air);
		machUraniumRefinery.set(-1, -1, 3, MaddTech.blockSteelShell);
		machUraniumRefinery.set(2, -2, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 0, 3, MaddTech.blockFluidCable);
		machUraniumRefinery.set(-1, -1, 1, MaddTech.blockSteelShell);
		machUraniumRefinery.set(0, -2, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 0, 1, MaddTech.blockFluidCable);
		machUraniumRefinery.set(2, -2, 0, MaddTech.blockLeadShell, 1);
		machUraniumRefinery.set(-2, 1, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -2, 0, MaddTech.blockLeadShell, 1);
		machUraniumRefinery.set(1, -2, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 1, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, 1, 0, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(0, -2, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, -2, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 1, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 1, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -1, 2, MaddTech.blockCentrifuge);
		machUraniumRefinery.set(-1, -1, 0, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(-1, 1, 2, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(-1, 1, 4, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(0, -1, 0, MaddTech.blockLeadShell, 1);
		machUraniumRefinery.set(2, -2, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, 1, 3, MaddTech.blockVolcanicGlass);
		//machUraniumRefinery.set(-1, 0, 3, Blocks.air);
		machUraniumRefinery.set(-1, 1, 6, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(2, 0, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, 1, 5, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(1, -1, 4, MaddTech.blockSteelShell);
		machUraniumRefinery.set(2, -1, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, -1, 3, MaddTech.blockSteelShell);
		machUraniumRefinery.set(-1, -1, 5, MaddTech.blockSteelShell);
		machUraniumRefinery.set(2, -1, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 0, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, -2, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -1, 3, MaddTech.blockCentrifuge);
		machUraniumRefinery.set(-2, 0, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, 0, 0, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(-2, 0, 0, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(-2, -2, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, -1, 2, MaddTech.blockSteelShell);
		machUraniumRefinery.set(-2, 0, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -1, 6, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(-2, -2, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, 1, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, 0, 6, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(1, -2, 0, MaddTech.blockLeadShell, 1);
		machUraniumRefinery.set(-1, -2, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, -2, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, 1, 0, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(-2, -1, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, -2, 0, MaddTech.blockLeadShell, 1);
		machUraniumRefinery.set(2, 1, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(1, -1, 1, MaddTech.blockSteelShell);
		machUraniumRefinery.set(-1, -2, 1, MaddTech.blockLeadShell, 2);
		//machUraniumRefinery.set(-1, 0, 2, Blocks.air);
		machUraniumRefinery.set(1, 1, 4, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(0, -2, 5, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 1, 1, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(-1, -2, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, -1, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, -2, 3, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, 1, 3, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(-1, 0, 6, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(1, -1, 0, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(0, 1, 5, MaddTech.blockVolcanicGlass);
		//machUraniumRefinery.set(-1, 0, 4, Blocks.air);
		machUraniumRefinery.set(0, 0, 6, MaddTech.blockFluidCable, 4);
		machUraniumRefinery.set(2, -2, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -1, 4, MaddTech.blockCentrifuge);
		machUraniumRefinery.set(-2, -1, 5, MaddTech.blockLeadShell, 2);
		//machUraniumRefinery.set(1, 0, 1, Blocks.air);
		machUraniumRefinery.set(0, 0, 4, MaddTech.blockFluidCable);
		machUraniumRefinery.set(-1, -1, 6, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(-2, 1, 6, MaddTech.blockLeadShell, 2);
		//machUraniumRefinery.set(1, 0, 3, Blocks.air);
		machUraniumRefinery.set(0, 0, 2, MaddTech.blockFluidCable);
		machUraniumRefinery.set(-2, 1, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -2, 1, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 1, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-1, 1, 1, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(-2, -1, 4, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(0, -2, 3, MaddTech.blockLeadShell, 2);
		//machUraniumRefinery.set(1, 0, 2, Blocks.air);
		machUraniumRefinery.set(1, -2, 2, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(-2, 1, 0, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(0, -1, 1, MaddTech.blockSteelShell);
		machUraniumRefinery.set(1, 1, 2, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(2, -2, 6, MaddTech.blockLeadShell, 2);
		machUraniumRefinery.set(2, -1, 0, MaddTech.blockLeadShell, 5);
		machUraniumRefinery.set(-1, 1, 5, MaddTech.blockVolcanicGlass);
		machUraniumRefinery.set(-2, -1, 0, MaddTech.blockLeadShell, 4);
		machUraniumRefinery.set(1, -2, 6, MaddTech.blockLeadShell, 2);

		machNuclearReactor = new MachineStruct();
		machNuclearReactor.set(2, 2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(1, -2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 1, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(0, -1, 4, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(0, 2, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, -1, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(0, 2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, -2, 3, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, 0, 1, Blocks.air);
		machNuclearReactor.set(-2, -1, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, 0, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, 2, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(2, -2, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(1, 1, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(2, -2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, 0, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, 2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, -1, 4, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(1, 2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 0, 0, MaddTech.blockLeadShell, 5);
		//machNuclearReactor.set(1, 1, 3, Blocks.air);
		machNuclearReactor.set(2, 2, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, 1, 1, Blocks.air);
		machNuclearReactor.set(1, 1, 4, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(2, 1, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(1, 2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, -2, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(0, 1, 0, MaddTech.blockLeadShell, 0);
		machNuclearReactor.set(-1, -2, 2, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(-1, 0, 3, Blocks.air);
		machNuclearReactor.set(0, -1, 0, MaddTech.blockLeadShell, 1);
		machNuclearReactor.set(1, 2, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(-1, -2, 4, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(0, 1, 2, Blocks.air);
		//machNuclearReactor.set(-1, 0, 1, Blocks.air);
		machNuclearReactor.set(-1, -1, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(0, 1, 4, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(1, -1, 4, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(2, -2, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(1, 0, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(2, 0, 2, MaddTech.blockLeadShellRedstoneIn, 2);
		machNuclearReactor.set(0, -2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, -1, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(0, 0, 3, Blocks.air);
		//machNuclearReactor.set(-1, -1, 1, Blocks.air);
		machNuclearReactor.set(2, 2, 2, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, 0, 2, Blocks.air);
		//machNuclearReactor.set(0, 0, 1, Blocks.air);
		machNuclearReactor.set(-2, 2, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(2, 0, 4, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, -1, 3, Blocks.air);
		machNuclearReactor.set(-2, 1, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, 1, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(0, -2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(1, -2, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(-1, -1, 2, Blocks.air);
		machNuclearReactor.set(-2, 1, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(-1, 1, 2, Blocks.air);
		machNuclearReactor.set(-1, 2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, 1, 4, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(1, 0, 4, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(1, 2, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, 0, 3, Blocks.air);
		machNuclearReactor.set(-1, -2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, -1, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(1, -2, 3, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, -1, 1, Blocks.air);
		machNuclearReactor.set(2, -2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(0, 2, 0, MaddTech.blockLeadShell, 0);
		//machNuclearReactor.set(0, -1, 3, Blocks.air);
		machNuclearReactor.set(0, 2, 2, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(0, -1, 1, Blocks.air);
		machNuclearReactor.set(0, 2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, 2, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 0, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, 2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, 0, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, -2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 1, 2, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(0, -1, 2, Blocks.air);
		machNuclearReactor.set(1, -1, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(-2, -2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 1, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(-2, -2, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 2, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(-2, 0, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(-2, 2, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, -2, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(-1, 0, 2, Blocks.air);
		machNuclearReactor.set(2, 1, 4, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(0, 1, 1, Blocks.air);
		//machNuclearReactor.set(-1, -1, 3, Blocks.air);
		machNuclearReactor.set(-1, 0, 0, MaddTech.blockLeadShell, 4);
		//machNuclearReactor.set(0, 1, 3, Blocks.air);
		machNuclearReactor.set(-2, -1, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, -2, 0, MaddTech.blockLeadShell, 4);
		machNuclearReactor.set(2, -1, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(-2, 0, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(1, -2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, -1, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, -2, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(2, 0, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(0, 0, 4, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(1, 2, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, 2, 4, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(0, 0, 2, Blocks.air);
		machNuclearReactor.set(0, -2, 0, MaddTech.blockLeadShell, 1);
		machNuclearReactor.set(-2, 1, 4, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(0, -2, 1, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, 1, 2, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-1, 0, 4, MaddTech.blockLeadShell, 5);
		//machNuclearReactor.set(-1, 1, 1, Blocks.air);
		machNuclearReactor.set(-1, 2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(0, -2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, -1, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, -1, 1, MaddTech.blockLeadShell, 2);
		//machNuclearReactor.set(1, -1, 2, Blocks.air);
		machNuclearReactor.set(-2, 1, 0, MaddTech.blockLeadShell, 4);
		//machNuclearReactor.set(-1, 1, 3, Blocks.air);
		//machNuclearReactor.set(1, 1, 2, Blocks.air);
		machNuclearReactor.set(1, -2, 0, MaddTech.blockLeadShell, 5);
		machNuclearReactor.set(-2, 2, 3, MaddTech.blockLeadShell, 2);
		machNuclearReactor.set(-2, -1, 0, MaddTech.blockLeadShell, 4);

		machCompressor = new MachineStruct();
		machCompressor.set(0, -4, 1, MaddTech.blockSteelShell, 1);
		//machCompressor.set(0, -1, 0, Blocks.air);
		machCompressor.set(0, -4, 0, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, -2, -1, MaddTech.blockSteelShell, 4);
		machCompressor.set(-1, -3, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, 0, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(1, -3, -1, MaddTech.blockSteelShell, 5);
		machCompressor.set(1, -2, -1, MaddTech.blockSteelShell, 5);
		machCompressor.set(-1, 0, 0, MaddTech.blockSteelShell, 4);
		machCompressor.set(1, -1, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(-1, -3, -1, MaddTech.blockSteelShell, 4);
		//machCompressor.set(0, -3, 0, Blocks.air);
		machCompressor.set(0, -2, -1, MaddTech.blockSteelShell, 1);
		machCompressor.set(0, 0, -1, MaddTech.blockSteelShell, 3);
		machCompressor.set(1, 0, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(0, -3, 1, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -4, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(1, 0, 0, MaddTech.blockSteelShell, 5);
		machCompressor.set(-1, -4, -1, MaddTech.blockSteelShell, 4);
		machCompressor.set(-1, -4, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -4, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(-1, -1, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, -1, -1, MaddTech.blockSteelShell, 4);
		machCompressor.set(-1, -4, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, -3, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -4, -1, MaddTech.blockSteelShell, 5);
		machCompressor.set(0, -3, -1, MaddTech.blockSteelShell, 1);
		machCompressor.set(0, 0, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, 0, -1, MaddTech.blockSteelShell, 4);
		machCompressor.set(1, 0, -1, MaddTech.blockSteelShell, 5);
		machCompressor.set(0, -1, 1, MaddTech.blockSteelShell, 1);
		machCompressor.set(0, -2, 1, MaddTech.blockSteelShell, 1);
		//machCompressor.set(0, -2, 0, Blocks.air);
		machCompressor.set(0, -1, -1, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -3, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -2, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(1, -3, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, -1, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(-1, -2, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -2, 0, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -1, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(-1, -2, 1, MaddTech.blockSteelShell, 2);
		machCompressor.set(0, -4, -1, MaddTech.blockSteelShell, 1);
		machCompressor.set(1, -1, -1, MaddTech.blockSteelShell, 5);

		machSubstation = new MachineStruct();
		//machSubstation.set(0, 1, 1, Blocks.air);
		machSubstation.set(0, 1, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, 0, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, 0, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, -1, 0, MaddTech.blockSteelShell, 4);
		machSubstation.set(0, 2, 1, MaddTech.blockSteelShell, 2);
		machSubstation.set(1, -2, 0, MaddTech.blockSteelShell, 5);
		machSubstation.set(-1, 1, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(0, 2, 0, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, 1, 0, MaddTech.blockSteelShell, 4);
		machSubstation.set(1, 0, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(0, 2, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, 0, 0, MaddTech.blockSteelShell, 4);
		machSubstation.set(1, 0, 1, MaddTech.blockSteelShell, 0);
		//machSubstation.set(0, -1, 1, Blocks.air);
		machSubstation.set(1, 0, 0, MaddTech.blockSteelShell, 5);
		machSubstation.set(-1, 2, 0, MaddTech.blockSteelShell, 4);
		machSubstation.set(-1, -1, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, 2, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(0, 0, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(1, -1, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, 2, 2, MaddTech.blockSteelShell, 0);
		//machSubstation.set(0, 0, 1, Blocks.air);
		machSubstation.set(0, -2, 1, MaddTech.blockSteelShell, 2);
		machSubstation.set(1, 1, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(0, -1, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(0, -2, 0, MaddTech.blockSteelShell, 1);
		machSubstation.set(-1, 1, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(1, 2, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(1, 1, 0, MaddTech.blockSteelShell, 5);
		machSubstation.set(1, -2, 2, MaddTech.blockSteelShell, 2);
		machSubstation.set(0, -2, 2, MaddTech.blockSteelShell, 2);
		machSubstation.set(1, 2, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(1, 1, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(1, -2, 1, MaddTech.blockSteelShell, 2);
		machSubstation.set(-1, -1, 2, MaddTech.blockSteelShell, 0);
		machSubstation.set(1, 2, 0, MaddTech.blockSteelShell, 5);
		machSubstation.set(-1, -2, 0, MaddTech.blockSteelShell, 4);
		machSubstation.set(1, -1, 0, MaddTech.blockSteelShell, 5);
		machSubstation.set(1, -1, 1, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, -2, 1, MaddTech.blockSteelShell, 2);
		machSubstation.set(0, 1, 0, MaddTech.blockSteelShell, 0);
		machSubstation.set(-1, -2, 2, MaddTech.blockSteelShell, 2);
		machSubstation.set(0, -1, 0, MaddTech.blockSteelShell, 1);

		try
		{
			Class.forName("com.madd.maddtech.intcg.Main").newInstance();
		}
		catch (Throwable e)
		{
			System.out.println("[MADDTECH] Not integrating with CraftGuide");
			e.printStackTrace();
		};
	};

	public static void configLamp(Block on, Block off, Block onInv, Block offInv, Item glowdust)
	{
		((BlockLampOn)on).setBlockOff(off);
		((BlockLampOff)off).setBlockOn(on);
		((BlockLampOnInv)onInv).setBlockOff(offInv);
		//((BlockLampOffInv)offInv).setBlockOn(onInv);

//		GameRegistry.addRecipe(new ItemStack(off, 1), new Object[] {
//			"PRP",
//			"PGP",
//			"PRP",
//			'P', Blocks.glass_pane, 'R', Items.redstone, 'G', glowdust
//		});
//
//		GameRegistry.addRecipe(new ItemStack(onInv, 1), new Object[] {
//			"PRP",
//			"PGP",
//			"PTP",
//			'P', Blocks.glass_pane, 'R', Items.redstone, 'G', glowdust, 'T', Blocks.redstone_torch
//		});
	};

	public static void addMetal(Block ore, Item dust, Item ingot, Block block, Item nugget, String name)
	{
//		GameRegistry.addSmelting(ore, new ItemStack(ingot, 1), 1.0F);
//		GameRegistry.addSmelting(dust, new ItemStack(ingot, 1), 1.0F);
//		GameRegistry.addShapelessRecipe(new ItemStack(ingot, 9), block);
//		GameRegistry.addRecipe(new ItemStack(block, 1), new Object[] {
//			"###",
//			"###",
//			"###",
//			'#', ingot
//		});
//		GameRegistry.addShapelessRecipe(new ItemStack(nugget, 9), ingot);
//		GameRegistry.addRecipe(new ItemStack(ingot, 1), new Object[] {
//			"###",
//			"###",
//			"###",
//			'#', nugget
//		});

//		OreDictionary.registerOre("ore" + name, ore);
//		OreDictionary.registerOre("block" + name, block);
//		OreDictionary.registerOre("dust" + name, dust);
//		OreDictionary.registerOre("ingot" + name, ingot);
//		OreDictionary.registerOre("nugget" + name, nugget);
//
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(dust, 2),
//			"D",
//			"D",
//			'D', "dust" + name
//		));

		//MaddRegistry.macerator.addRecipe(new ItemStack(ore, 1), new ItemStack(dust, 2));
		//MaddRegistry.macerator.addRecipe(new ItemStack(ingot, 1), new ItemStack(dust, 1));
	};

//	public static void addAlloy(Item dust, Item ingot, Block block, Item nugget, String name)
//	{
//		GameRegistry.addSmelting(dust, new ItemStack(ingot, 1), 1.0F);
//		GameRegistry.addShapelessRecipe(new ItemStack(ingot, 9), block);
//		GameRegistry.addRecipe(new ItemStack(block, 1), new Object[] {
//			"###",
//			"###",
//			"###",
//			'#', ingot
//		});
//		GameRegistry.addShapelessRecipe(new ItemStack(nugget, 9), ingot);
//		GameRegistry.addRecipe(new ItemStack(ingot, 1), new Object[] {
//			"###",
//			"###",
//			"###",
//			'#', nugget
//		});
//
//		OreDictionary.registerOre("block" + name, block);
//		OreDictionary.registerOre("dust" + name, dust);
//		OreDictionary.registerOre("ingot" + name, ingot);
//		OreDictionary.registerOre("nugget" + name, nugget);
//
//		//MaddRegistry.macerator.addRecipe(new ItemStack(ingot, 1), new ItemStack(dust, 1));
//	};
//
//	public static void addMacerateable(Block ore, Item dust, Item ingot)
//	{
//		GameRegistry.addSmelting(dust, new ItemStack(ingot, 1), 1.0F);
//		MaddRegistry.macerator.addRecipe(new ItemStack(ingot, 1), new ItemStack(dust, 1));
//		//MaddRegistry.macerator.addRecipe(new ItemStack(ore, 1), new ItemStack(dust, 2));
//	};
//
//	public static void addSplitable(Item ingot, Item nugget)
//	{
//		GameRegistry.addRecipe(new ItemStack(ingot, 1), new Object[] {
//			"###",
//			"###",
//			"###",
//			'#', nugget
//		});
//		GameRegistry.addShapelessRecipe(new ItemStack(nugget, 9), ingot);
//	};
//
//	public static void addToolchain(Item.ToolMaterial mat, String matName, Item craftComp)
//	{
//		Item pickaxe = new ItemMaddPickaxe(mat, matName);
//		GameRegistry.registerItem(pickaxe, pickaxe.getUnlocalizedName().substring(4));
//		if (matName == "Copper")
//		{
//			MaddTech.itemCopperPickaxe = pickaxe;
//		};
//
//		Item axe = new ItemMaddAxe(mat, matName);
//		GameRegistry.registerItem(axe, axe.getUnlocalizedName().substring(4));
//		Item shovel = new ItemMaddShovel(mat, matName);
//		GameRegistry.registerItem(shovel, shovel.getUnlocalizedName().substring(4));
//		Item sword = new ItemMaddSword(mat, matName);
//		GameRegistry.registerItem(sword, sword.getUnlocalizedName().substring(4));
//		Item hoe = new ItemMaddHoe(mat, matName);
//		GameRegistry.registerItem(hoe, hoe.getUnlocalizedName().substring(4));
//		
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(pickaxe, 1),
//			"###",
//			" S ",
//			" S ",
//			'#', "ingot" + matName, 'S', Items.stick
//		));
//
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(axe, 1),
//			"##",
//			"#S",
//			" S",
//			'#', "ingot" + matName, 'S', Items.stick
//		));
//
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(shovel, 1),
//			"#",
//			"S",
//			"S",
//			'#', "ingot" + matName, 'S', Items.stick
//		));
//
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(sword, 1),
//			"#",
//			"#",
//			"S",
//			'#', "ingot" + matName, 'S', Items.stick
//		));
//
//		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(hoe, 1),
//			"##",
//			" S",
//			" S",
//			'#', "ingot" + matName, 'S', Items.stick
//		));
//	};

	/**
	 * Returns the appropriate rotation of a machine structure for the specified location,
	 * or null if the structure is not there.
	 */
//	public static MachineStruct fitStruct(World world, int x, int y, int z, MachineStruct base)
//	{
//		if (base == null)
//		{
//			throw new RuntimeException("maddtech: null strucutre passed to fitStruct()");
//		};
//					
//		int i;
//		for (i=2; i<6; i++)
//		{
//			MachineStruct s = base.spin(i);
//			if (s.isFoundAt(world, x, y, z))
//			{
//				return s;
//			};
//		};
//
//		return null;
//	};

	public static void registerFluids()
	{
		MaddRegistry.fluids.add(new FluidWater());
		MaddRegistry.fluids.add(new FluidLava());
		MaddRegistry.fluids.add(new Fluid("maddtech:empty", "Empty", 0, 0));
		MaddRegistry.fluids.add(new FluidNaturalGas());
		MaddRegistry.fluids.add(new FluidUranium());
		MaddRegistry.fluids.add(new FluidHotAir());
		MaddRegistry.fluids.add(new FluidPlutonium());
		MaddRegistry.fluids.add(new Fluid("maddtech:naq", "Liquid Nitrogen", BarrelType.STEEL | BarrelType.DEWAR, CapsuleType.NONE));
		MaddRegistry.fluids.add(new Fluid("maddtech:milk", "Milk", BarrelType.WOODEN, CapsuleType.TIN));
		
		// fluid buckets
//		MaddRegistry.fluids.addBucket(Items.water_bucket, "maddtech:water");
//		MaddRegistry.fluids.addBucket(Items.lava_bucket, "maddtech:lava");
//		MaddRegistry.fluids.addBucket(Items.milk_bucket, "maddtech:milk");
//		
//		// forge/maddtech mapping
//		MaddRegistry.fluids.addForgeFluid(net.minecraftforge.fluids.FluidRegistry.WATER.getID(), "maddtech:water");
//		MaddRegistry.fluids.addForgeFluid(net.minecraftforge.fluids.FluidRegistry.LAVA.getID(), "maddtech:lava");
//		
		Integrator.registerAllFluids();
	};

	public static void registerChemicalReactorRecipies()
	{
//		MaddRegistry.chemicalReactor.addRecipe(
//			new ItemStack[] { new ItemStack(Items.iron_ingot, 1), new ItemStack(Items.coal, 3) },
//			new ItemStack[] { new ItemStack(MaddTech.itemSteelIngot, 1) },
//			420, 60
//		);

		MaddRegistry.chemicalReactor.addRecipe(
			new ItemStack[] { new ItemStack(MaddTech.itemIronDust, 1), new ItemStack(MaddTech.itemCoalDust, 3) },
			new ItemStack[] { new ItemStack(MaddTech.itemSteelIngot, 2) },
			400, 30
		);
		
		MaddRegistry.chemicalReactor.addRecipe(
			new ItemStack[] { new ItemStack(MaddTech.itemBrassIngot, 1), new ItemStack(MaddTech.itemCopperIngot, 1) },
			new ItemStack[] { new ItemStack(MaddTech.itemBronzeIngot, 1) },
			550, 50
		);
		
		MaddRegistry.chemicalReactor.addRecipe(
			new ItemStack[] { new ItemStack(MaddTech.itemCopperDust, 1), new ItemStack(MaddTech.itemBrassDust, 1) },
			new ItemStack[] { new ItemStack(MaddTech.itemBronzeIngot, 2) },	// intentionally 2
			500, 20
		);

		MaddRegistry.chemicalReactor.addRecipe(
			new ItemStack[] { new ItemStack(MaddTech.itemCopperIngot, 1), new ItemStack(MaddTech.itemTinIngot, 1) },
			new ItemStack[] { new ItemStack(MaddTech.itemBrassIngot, 1) },
			480, 15
		);

		MaddRegistry.chemicalReactor.addRecipe(
			new ItemStack[] { new ItemStack(MaddTech.itemCopperDust, 1), new ItemStack(MaddTech.itemTinDust, 1) },
			new ItemStack[] { new ItemStack(MaddTech.itemBrassIngot, 2) },
			450, 10
		);

//		MaddRegistry.chemicalReactor.addRecipe(
//			new ItemStack[] { new ItemStack(Items.redstone, 5), new ItemStack(MaddTech.itemNickelDust, 5), new ItemStack(MaddTech.itemGoldDust, 2) },
//			new ItemStack[] { new ItemStack(MaddTech.itemRedAlloyIngot, 3) },
//			1000, 40
//		);

//		MaddRegistry.chemicalReactor.addRecipe(
//			new ItemStack[] { new ItemStack(MaddTech.itemRedAlloyIngot, 3) },
//			new ItemStack[] { new ItemStack(Items.redstone, 5), new ItemStack(MaddTech.itemNickelDust, 5), new ItemStack(MaddTech.itemGoldDust, 2) },
//			800, 15
//		);

//		MaddRegistry.chemicalReactor.addRecipe(
//			new ItemStack[] { new ItemStack(MaddTech.blockVolcanicSand, 1), new ItemStack(Blocks.sand, 1), new ItemStack(MaddTech.itemLeadDust, 4) },
//			new ItemStack[] { new ItemStack(MaddTech.blockVolcanicGlass, 8) },
//			5000, 60
//		);

//		MaddRegistry.chemicalReactor.addRecipe(
//			new ItemStack[] { new ItemStack(MaddTech.blockVolcanicSand, 1), new ItemStack(Blocks.sand, 1), new ItemStack(MaddTech.itemLeadIngot, 5) },
//			new ItemStack[] { new ItemStack(MaddTech.blockVolcanicGlass, 8) },
//			6000, 120
//		);

		MaddRegistry.chemicalReactor.addRecipe(new ChemicalReactorRecipeDiamond());
	};

	public static void registerMaceratorRecipies()
	{
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.stone, 1), new ItemStack(Blocks.cobblestone, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.cobblestone, 1), new ItemStack(Blocks.gravel, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.gravel, 1), new ItemStack(Blocks.sand, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(MaddTech.blockLimestone, 1), new ItemStack(Items.dye, 1, 15));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.obsidian, 1), new ItemStack(MaddTech.blockVolcanicSand, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.redstone_ore, 1), new ItemStack(Items.redstone, 8));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.lapis_ore, 1), new ItemStack(Items.dye, 16, 4));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.ice, 1), new ItemStack(Blocks.snow, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(MaddTech.blockBasalt, 1), new ItemStack(MaddTech.blockBasaltCobblestone, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.netherrack, 1), new ItemStack(MaddTech.itemNetherDust, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.cactus, 1), new ItemStack(Items.slime_ball, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Items.reeds, 1), new ItemStack(Items.slime_ball, 1));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.wool, 1), new ItemStack(Items.string, 2));
//		MaddRegistry.macerator.addRecipe(new ItemStack(Blocks.sandstone, 1), new ItemStack(Blocks.sand, 4));
	};
	
	public static void registerEngTableRecipies()
	{	
		MaddRegistry.engTable.add(
		new EngTableRecipe(
			"Nuke",
			5, 5,
			new Object[] {
				"ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel",
				"ingotSteel", "ingotLead", "ingotRedAlloy", "ingotLead", "ingotSteel",
				"ingotSteel", new EngTableFluidInput("maddtech:refu", 550), MaddTech.itemMicroprocessor, new EngTableFluidInput("maddtech:refu", 550), "ingotSteel",
				"ingotSteel", "ingotLead", "ingotRedAlloy", "ingotLead", "ingotSteel",
				"ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel"
			},
			new ToolSpec[] {
				new ToolSpec("screwdriver", 16),
				new ToolSpec("welder", 3)
			},
			1, 1,
			new ItemStack[] { new ItemStack(MaddTech.blockNuke, 1) }
		));
		
//		MaddRegistry.engTable.add(
//		new EngTableRecipe(
//			"Speed Upgrade",
//			3, 3,
//			new Object[] {
//				"ingotTin", "ingotSilver", "ingotTin",
//				"ingotTin", Items.redstone, "ingotTin",
//				"ingotTin", new EngTableFluidInput("maddtech:water", 50), "ingotTin"
//			},
//			new ToolSpec[] {
//				new ToolSpec("welder", 1)
//			},
//			1, 1,
//			new ItemStack[] { new ItemStack(MaddTech.itemSpeedUpgrade, 1) }
//		));
		
//		MaddRegistry.engTable.add(
//		new EngTableRecipe(
//			"Gas Welder",
//			3, 4,
//			new Object[] {
//				"ingotSteel", null, "ingotSteel",
//				"ingotSteel", null, "ingotSteel",
//				"ingotSteel", Items.flint_and_steel, "ingotSteel",
//				"ingotTin", new EngTableFluidInput("maddtech:natgas", 1000), "ingotTin"
//			},
//			new ToolSpec[] {
//				new ToolSpec("screwdriver", 2)
//			},
//			1, 1,
//			new ItemStack[] { new ItemStack(MaddTech.itemGasWelder, 1) }
//		));
		
		MaddRegistry.engTable.add(
		new EngTableRecipe(
			"Dewar",
			5, 4,
			new Object[] {
				"ingotSteel", MaddTech.blockVolcanicGlass, null, MaddTech.blockVolcanicGlass, "ingotSteel",
				"ingotSteel", null, MaddTech.blockSteelBarrel, null, "ingotSteel",
				"ingotSteel", null, null, null, "ingotSteel",
				"ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel"
			},
			new ToolSpec[] {
				new ToolSpec("welder", 1)
			},
			1, 1,
			new ItemStack[] { new ItemStack(MaddTech.blockDewar, 1) }
		));
		
		MaddRegistry.engTable.add(new EngTableRecipeCrafting());
		//Integrator.registerAllRecipies();
	};

//	public static Item addUpgrade(String uclass, String name, String tex, float ti, float pi)
//	{
//		Item item = new ItemBasicUpgrade(uclass, name, tex, ti, pi);
//		GameRegistry.registerItem(item, item.getUnlocalizedName().substring(4));
//		return item;
//	};
//
//	public static void registerThermalResistances()
//	{
//		MaddRegistry.thermalResistance.set(MaddTech.itemAirVent, -38.4F);
//		MaddRegistry.thermalResistance.set(MaddTech.itemThermalInsulator, 1.0F);
//	};
//
//	public static Item addItem(Item itm)
//	{
//		GameRegistry.registerItem(itm, itm.getUnlocalizedName().substring(4));
//		return itm;
//	};
//
//	public static void registerDimensionInfo()
//	{
//		MaddRegistry.dimensions.add(0, new MTDimensionInfoOverworld());
//	};
//
//	public static void addLevelingRecipe(ItemStack lvl1, ItemStack lvl2, ItemStack lvl3)
//	{
//		GameRegistry.addRecipe(new ShapedOreRecipe(lvl2,
//			"BGB",
//			"BXB",
//			"BGB",
//			'B', "ingotBrass", 'G', Items.gold_ingot, 'X', lvl1
//		));
//
//		GameRegistry.addRecipe(new ShapedOreRecipe(lvl3,
//			"BSB",
//			"BXB",
//			"BSB",
//			'B', "ingotBronze", 'S', "ingotSilver", 'X', lvl2
//		));
//	};

	/**
	 * Registers special items.
	 */
//	public static void initSpecialItems()
//	{
//		// Also set up the API.
//		MaddRegistry.chemicalReactor = new ChemicalReactorRegistry();
//		MaddRegistry.fluids = new FluidRegistry();
//		MaddRegistry.macerator = MaceratorRegistry.instance();
//
//		registerFluids();
//		GameRegistry.registerFuelHandler(new MTFuelHandler());
//
//		MaddTech.itemCapsule = new ItemCapsule();
//		GameRegistry.registerItem(MaddTech.itemCapsule, MaddTech.itemCapsule.getUnlocalizedName().substring(4));
//
//		MaddTech.itemFillerWater = new ItemFiller("WaterFiller", "maddtech:water");
//		GameRegistry.registerItem(MaddTech.itemFillerWater, MaddTech.itemFillerWater.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerLava = new ItemFiller("LavaFiller", "maddtech:lava");
//		GameRegistry.registerItem(MaddTech.itemFillerLava, MaddTech.itemFillerLava.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerNatgas = new ItemFiller("NatgasFiller", "maddtech:natgas");
//		GameRegistry.registerItem(MaddTech.itemFillerNatgas, MaddTech.itemFillerNatgas.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerRefu = new ItemFiller("RefuFiller", "maddtech:refu");
//		GameRegistry.registerItem(MaddTech.itemFillerRefu, MaddTech.itemFillerRefu.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerPlutonium = new ItemFiller("PlutoniumFiller", "maddtech:plutonium");
//		GameRegistry.registerItem(MaddTech.itemFillerPlutonium, MaddTech.itemFillerPlutonium.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerNaq = new ItemFiller("NaqFiller", "maddtech:naq");
//		GameRegistry.registerItem(MaddTech.itemFillerNaq, MaddTech.itemFillerNaq.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerMilk = new ItemFiller("MilkFiller", "maddtech:milk");
//		GameRegistry.registerItem(MaddTech.itemFillerMilk, MaddTech.itemFillerMilk.getUnlocalizedName().substring(4));
//		MaddTech.itemFillerHotAir = new ItemFiller("HotAirFiller", "maddtech:hotair");
//		GameRegistry.registerItem(MaddTech.itemFillerHotAir, MaddTech.itemFillerHotAir.getUnlocalizedName().substring(4));
//		
//		MaddTech.itemTurbine = new ItemTurbine();
//		GameRegistry.registerItem(MaddTech.itemTurbine, MaddTech.itemTurbine.getUnlocalizedName().substring(4));
//		MaddTech.itemBattery = new ItemBattery();
//		GameRegistry.registerItem(MaddTech.itemBattery, MaddTech.itemBattery.getUnlocalizedName().substring(4));
//
//		MaddTech.itemFanTin = addItem(new ItemFan("Tin", 50));
//		MaddTech.itemFanBrass = addItem(new ItemFan("Brass", 100));
//		MaddTech.itemFanBronze = addItem(new ItemFan("Bronze", 500));
//		MaddTech.itemNaqCooler = addItem(new ItemNaqCooler());
//		MaddTech.itemEnergyCrystal = addItem(new ItemEnergyCrystal());
//		MaddTech.itemElectricDrill = addItem(new ItemElectricDrill());
//		
//		MaddTech.itemCoil = addItem(new ItemCoil());
//		MaddTech.itemSafetySuit = addItem((new ItemSafetySuit(MAT_SAFETY_SUIT)).setUnlocalizedName("itemSafetySuit").setTextureName("maddtech:SafetySuit").setCreativeTab(MaddTech.tabMach));
//		MaddTech.itemSteelDoor = addItem(new ItemMaddDoor("Steel"));
//		MaddTech.itemLeadDoor = addItem(new ItemMaddDoor("Lead"));
//		MaddTech.itemTemplate = addItem(new ItemTemplate());
//
//		MaddTech.itemSpeedUpgrade = addUpgrade("maddtech:upspeed", "itemSpeedUpgrade", "maddtech:SpeedUpgrade", -(1.0F/65.0F), 0.005F);
//		MaddTech.itemEfficiencyUpgrade = addUpgrade("maddtech:upeff", "itemEfficiencyUpgrade", "maddtech:EfficiencyUpgrade", 0.0F, -0.01F);
//
//		Integrator.registerAllItems();
//		
//		registerThermalResistances();
//		registerDimensionInfo();
//
//		network = NetworkRegistry.INSTANCE.newSimpleChannel("maddtech");
//		network.registerMessage(PacketPulserUpdate.Handler.class, PacketPulserUpdate.class, 0, Side.SERVER);
//		network.registerMessage(PacketShopUpdate.Handler.class, PacketShopUpdate.class, 1, Side.SERVER);
//		network.registerMessage(PacketTerminalUpdate.Handler.class, PacketTerminalUpdate.class, 2, Side.SERVER);
//		network.registerMessage(PacketPumpUpdate.Handler.class, PacketPumpUpdate.class, 3, Side.SERVER);
//		network.registerMessage(PacketItemTransporterUpdate.Handler.class, PacketItemTransporterUpdate.class, 4, Side.SERVER);
//		network.registerMessage(PacketItemMonitorUpdate.Handler.class, PacketItemMonitorUpdate.class, 5, Side.SERVER);
//		network.registerMessage(PacketRedstoneLatchUpdate.Handler.class, PacketRedstoneLatchUpdate.class, 6, Side.SERVER);
//		network.registerMessage(PacketFluidMonitorUpdate.Handler.class, PacketFluidMonitorUpdate.class, 7, Side.SERVER);
//		/* 8-10 inclusive reserved for BuildCraft integration! */
//		network.registerMessage(PacketEnergyShopUpdate.Handler.class, PacketEnergyShopUpdate.class, 11, Side.SERVER);
//		network.registerMessage(PacketFluidShopUpdate.Handler.class, PacketFluidShopUpdate.class, 12, Side.SERVER);
//		
//		network.registerMessage(PacketInvokeEffect.Handler.class, PacketInvokeEffect.class, 100, Side.CLIENT);
//		
//		Integrator.registerAllMessages();
//		MaddTech.proxy.registerMessages(network);
//		MaddTech.proxy.initRender();
//	};
};
