/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * MaddRenderHelper.java
 * A rendering helper for MaddTech.
 */

package com.madd.maddtech.util;


import net.minecraft.client.renderer.*;

import net.minecraft.client.renderer.*;
import net.minecraft.block.*;
import net.minecraft.world.*;
import org.lwjgl.opengl.*;
import java.io.*;
import net.minecraft.client.*;
import net.minecraft.util.*;
import java.nio.*;

public class MaddRenderHelper
{
	/**
	 * We need to do some matrix stuff on the CPU because of reasons.
	 */
	private static float[] matrix = new float[] {
		1.0F, 0.0F, 0.0F, 0.0F,
		0.0F, 1.0F, 0.0F, 0.0F,
		0.0F, 0.0F, 1.0F, 0.0F,
		0.0F, 0.0F, 0.0F, 1.0F
	};

	public static void clearTransformations()
	{
		matrix = new float[] {
			1.0F, 0.0F, 0.0F, 0.0F,
			0.0F, 1.0F, 0.0F, 0.0F,
			0.0F, 0.0F, 1.0F, 0.0F,
			0.0F, 0.0F, 0.0F, 1.0F
		};
	};

	public static void addTransformationMatrix(float[] b)
	{
		// we're calcuting (b * matrix), so we take rows from B and columns from the current matrix.
		float[] newMatrix = new float[16];

		// COLUMN 0
		newMatrix[0] = b[0]*matrix[0] + b[4]*matrix[1] + b[8]*matrix[2] + b[12]*matrix[3];		// row 0
		newMatrix[1] = b[1]*matrix[0] + b[5]*matrix[1] + b[9]*matrix[2] + b[13]*matrix[3];		// row 1
		newMatrix[2] = b[2]*matrix[0] + b[6]*matrix[1] + b[10]*matrix[2] + b[14]*matrix[3];		// row 2
		newMatrix[3] = b[3]*matrix[0] + b[7]*matrix[1] + b[11]*matrix[2] + b[15]*matrix[3];		// row 3

		// COLUMN 1
		newMatrix[4] = b[0]*matrix[4] + b[4]*matrix[5] + b[8]*matrix[6] + b[12]*matrix[7];		// row 0
		newMatrix[5] = b[1]*matrix[4] + b[5]*matrix[5] + b[9]*matrix[6] + b[13]*matrix[7];		// row 1
		newMatrix[6] = b[2]*matrix[4] + b[6]*matrix[5] + b[10]*matrix[6] + b[14]*matrix[7];		// row 2
		newMatrix[7] = b[3]*matrix[4] + b[7]*matrix[5] + b[11]*matrix[6] + b[15]*matrix[7];		// row 3

		// COLUMN 2
		newMatrix[8] = b[0]*matrix[8] + b[4]*matrix[9] + b[8]*matrix[10] + b[12]*matrix[11];		// row 0
		newMatrix[9] = b[1]*matrix[8] + b[5]*matrix[9] + b[9]*matrix[10] + b[13]*matrix[11];		// row 1
		newMatrix[10] = b[2]*matrix[8] + b[6]*matrix[9] + b[10]*matrix[10] + b[14]*matrix[11];		// row 2
		newMatrix[11] = b[3]*matrix[8] + b[7]*matrix[9] + b[11]*matrix[10] + b[15]*matrix[11];		// row 3

		// COLUMN 3
		newMatrix[12] = b[0]*matrix[12] + b[4]*matrix[13] + b[8]*matrix[14] + b[12]*matrix[15];		// row 0
		newMatrix[13] = b[1]*matrix[12] + b[5]*matrix[13] + b[9]*matrix[14] + b[13]*matrix[15];		// row 1
		newMatrix[14] = b[2]*matrix[12] + b[6]*matrix[13] + b[10]*matrix[14] + b[14]*matrix[15];	// row 2
		newMatrix[15] = b[3]*matrix[12] + b[7]*matrix[13] + b[11]*matrix[14] + b[15]*matrix[15];	// row 3

		// use that as the new matrix
		matrix = newMatrix;
	};

	/**
	 * Instructs the renderer to perform a translation of [x, y, z] after the current transformations are
	 * performed.
	 */
	public static void addTranslation(float x, float y, float z)
	{
		// this is a transpose, remember.
		float[] translationMatrix = new float[] {
			1.0F,	0.0F,	0.0F,	0.0F,
			0.0F,	1.0F,	0.0F,	0.0F,
			0.0F,	0.0F,	1.0F,	0.0F,
			x,	y,	z,	1.0F
		};
		addTransformationMatrix(translationMatrix);
	};

	/**
	 * Instructions the renderer to rotate all future vertices (after performing all current transformations)
	 * clockwise around the specified vector, on a line which passes through the origin. The angle is in radians.
	 */
	public static void addRotation(float angle, float x, float y, float z)
	{
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		float[] rotateMatrix = new float[] {
			x*x*(1.0F-c)+c,		y*x*(1.0F-c)+z*s,	x*z*(1.0F-c)-y*s,	0.0F,
			x*y*(1.0F-c)-z*s, 	y*y*(1.0F-c)+c,		y*z*(1.0F-c)+x*s,	0.0F,
			x*z*(1.0F-c)+y*s,	y*z*(1.0F-c)-x*s,	z*z*(1.0F-c)+c,		0.0F,
			0.0F,			0.0F,			0.0F,			1.0F
		};

		addTransformationMatrix(rotateMatrix);
	};

//	public static void tessCube(Tessellator tess, float x1, float y1, float z1, float x2, float y2, float z2,
//					float u1, float v1, float u2, float v2)
//	{
//		tessCube(tess, x1, y1, z1, x2, y2, z2, u1, v1, u2, v2, false, false);
//	};

//	public static void setNormal(Tessellator tess, float x, float y, float z)
//	{
//		tess.setNormal(
//			x*matrix[0] + y*matrix[4] + z*matrix[8],
//			x*matrix[1] + y*matrix[5] + z*matrix[9],
//			x*matrix[2] + y*matrix[6] + z*matrix[10]
//		);
//	};
//	
//	public static void putVertex(Tessellator tess, float x, float y, float z, float u, float v)
//	{
//		//tess.addVertexWithUV(x, y, z, u, v);
//		tess.addVertexWithUV(
//			x*matrix[0] + y*matrix[4] + z*matrix[8] + matrix[12],
//			x*matrix[1] + y*matrix[5] + z*matrix[9] + matrix[13],
//			x*matrix[2] + y*matrix[6] + z*matrix[10] + matrix[14],
//			u, v
//		);
//	};
//
//	/**
//	 * Add the vertices of a cube to the tesselator.
//	 */
//	public static void tessCube(Tessellator tess, float x1, float y1, float z1, float x2, float y2, float z2,
//					float u1, float v1, float u2, float v2, boolean spinTop, boolean spinSides)
//	{
//		if (spinTop)
//		{
//			// Top side
//			tess.setNormal(0.0F, 1.0F, 0.0F);
//			putVertex(tess, x1, y2, z1, u2, v2);
//			putVertex(tess, x1, y2, z2, u2, v1);
//			putVertex(tess, x2, y2, z2, u1, v1);
//			putVertex(tess, x2, y2, z1, u1, v2);
//
//			// Bottom side
//			tess.setNormal(0.0F, -1.0F, 0.0F);
//			putVertex(tess, x1, y1, z1, u1, v2);
//			putVertex(tess, x2, y1, z1, u2, v2);
//			putVertex(tess, x2, y1, z2, u2, v1);
//			putVertex(tess, x1, y1, z2, u1, v1);
//		}
//		else
//		{
//			// Top side
//			tess.setNormal(0.0F, 1.0F, 0.0F);
//			putVertex(tess, x1, y2, z1, u1, v2);
//			putVertex(tess, x1, y2, z2, u2, v2);
//			putVertex(tess, x2, y2, z2, u2, v1);
//			putVertex(tess, x2, y2, z1, u1, v1);
//
//			// Bottom side
//			tess.setNormal(0.0F, -1.0F, 0.0F);
//			putVertex(tess, x1, y1, z1, u1, v1);
//			putVertex(tess, x2, y1, z1, u1, v2);
//			putVertex(tess, x2, y1, z2, u2, v2);
//			putVertex(tess, x1, y1, z2, u2, v1);
//		};
//
//		if (spinSides)
//		{
//			// Front side
//			tess.setNormal(0.0F, 0.0F, -1.0F);
//			putVertex(tess, x1, y1, z1, u1, v2);
//			putVertex(tess, x1, y2, z1, u2, v2);
//			putVertex(tess, x2, y2, z1, u2, v1);
//			putVertex(tess, x2, y1, z1, u1, v1);
//
//			// Back side
//			tess.setNormal(0.0F, 0.0F, 1.0F);
//			putVertex(tess, x1, y1, z2, u2, v2);
//			putVertex(tess, x2, y1, z2, u2, v1);
//			putVertex(tess, x2, y2, z2, u1, v1);
//			putVertex(tess, x1, y2, z2, u1, v2);
//
//			// "Left" side
//			tess.setNormal(1.0F, 0.0F, 0.0F);
//			putVertex(tess, x1, y1, z1, u2, v2);
//			putVertex(tess, x1, y1, z2, u2, v1);
//			putVertex(tess, x1, y2, z2, u1, v1);			
//			putVertex(tess, x1, y2, z1, u1, v2);
//
//			// "Right" side
//			tess.setNormal(-1.0F, 0.0F, 0.0F);
//			putVertex(tess, x2, y1, z1, u1, v2);
//			putVertex(tess, x2, y2, z1, u2, v2);
//			putVertex(tess, x2, y2, z2, u2, v1);
//			putVertex(tess, x2, y1, z2, u1, v1);
//		}
//		else
//		{
//			// Front side
//			tess.setNormal(0.0F, 0.0F, -1.0F);
//			putVertex(tess, x1, y1, z1, u1, v1);
//			putVertex(tess, x1, y2, z1, u1, v2);
//			putVertex(tess, x2, y2, z1, u2, v2);
//			putVertex(tess, x2, y1, z1, u2, v1);
//
//			// Back side
//			tess.setNormal(0.0F, 0.0F, 1.0F);
//			putVertex(tess, x1, y1, z2, u1, v2);
//			putVertex(tess, x2, y1, z2, u2, v2);
//			putVertex(tess, x2, y2, z2, u2, v1);
//			putVertex(tess, x1, y2, z2, u1, v1);
//
//			// "Left" side
//			tess.setNormal(1.0F, 0.0F, 0.0F);
//			putVertex(tess, x1, y1, z1, u1, v2);
//			putVertex(tess, x1, y1, z2, u2, v2);
//			putVertex(tess, x1, y2, z2, u2, v1);			
//			putVertex(tess, x1, y2, z1, u1, v1);
//
//			// "Right" side
//			tess.setNormal(-1.0F, 0.0F, 0.0F);
//			putVertex(tess, x2, y1, z1, u1, v1);
//			putVertex(tess, x2, y2, z1, u1, v2);
//			putVertex(tess, x2, y2, z2, u2, v2);
//			putVertex(tess, x2, y1, z2, u2, v1);
//		};
//	};
};
