/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SideUtil.java
 * A class which helps to easily convert side numbers to vectors.
 */
package com.madd.maddtech.util;

public class SideUtil
{
	/**
	 * Values returned by getSideAxis().
	 */
	public static final int X_AXIS = 1;
	public static final int Y_AXIS = 2;
	public static final int Z_AXIS = 3;
	
	public static int getSideX(int side)
	{
		switch (side)
		{
		case 4:
			return -1;
		case 5:
			return 1;
		default:
			return 0;
		}
	};
	
	public static int getSideY(int side)
	{
		switch (side)
		{
		case 1:
			return 1;
		case 0:
			return -1;
		default:
			return 0;
		}
	};
	
	public static int getSideZ(int side)
	{
		switch (side)
		{
		case 2:
			return -1;
		case 3:
			return 1;
		default:
			return 0;
		}
	};
	
	/**
	 * Returns the side opposite to @side.
	 */
	public static int opposite(int side)
	{
		switch (side)
		{
		case 2:
			return 3;
		case 3:
			return 2;
		case 4:
			return 5;
		case 5:
			return 4;
		case 1:
			return 0;
		case 0:
			return 1;
		case 6:
			return 6;
		};
		
		return 0;
	};
	
	/**
	 * Returns the axis that @side lies on.
	 */
	public static int getSideAxis(int side)
	{
		if (getSideX(side) != 0) return X_AXIS;
		if (getSideY(side) != 0) return Y_AXIS;
		if (getSideZ(side) != 0) return Z_AXIS;
		return 0;
	};
};
