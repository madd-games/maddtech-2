/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.madd.maddtech.worldgen;

import net.minecraft.world.chunk.*;
import net.minecraft.world.*;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.biome.*;
import java.util.Random;
import java.io.*;

public class MTWorldGenerator// implements IWorldGenerator
{
//	@Override
//	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
//	{
//		switch (world.provider.dimensionId)
//		{
//		case 0:
//			generateInOverworld(world, random, chunkX * 16, chunkZ * 16);
//		};
//	};
//
//	private boolean isPlaceForVolcano(int chunkX, int chunkZ)
//	{
//		return ((chunkX % 128) == 0) && ((chunkZ % 128) == 0);
//	};
//
//	private boolean isPlaceForNaturalGas(int chunkX, int chunkZ)
//	{
//		return ((chunkX % 256) == 0) && ((chunkZ % 256) == 0);
//	};
//
//	public void printVolcanoNotif(int x, int y, int z, int height, int radius)
//	{
//		System.out.println("[MADDTECH] Generating volcano at (" + x + ", " + y + ", " + z + ") with radius=" + radius + ", max height=" + height);
//	};
//
//	public void printNaturalGasNotif(int x, int z, int height)
//	{
//		System.out.println("[MADDTECH] Generating gas chunk at (" + x + ", " + z + ") with height " + height);
//	};
//
//	private void generateInOverworld(World world, Random random, int chunkX, int chunkZ)
//	{
//		int i;
//		BiomeGenBase biome = world.getBiomeGenForCoords(chunkX, chunkZ);
//if (biome instanceof BiomeGenPlains || biome instanceof BiomeGenSwamp)
//{
//	if (MaddTech.confGenMarble)
//	{
//		for (i=0; i<10; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 25 + random.nextInt(65-25);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate Marble at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockMarble, 45)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenCopperOre)
//	{
//		for (i=0; i<25; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(50-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate CopperOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockCopperOre, 8)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenLeadOre)
//	{
//		for (i=0; i<10; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(40-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate LeadOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockLeadOre, 10)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenTinOre)
//	{
//		for (i=0; i<22; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(70-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate TinOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockTinOre, 12)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenSilverOre)
//	{
//		for (i=0; i<15; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(50-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate SilverOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockSilverOre, 4)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenUraniumOre)
//	{
//		for (i=0; i<12; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(60-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate UraniumOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockUraniumOre, 3)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenRubyOre)
//	{
//		for (i=0; i<1; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(30-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate RubyOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockRubyOre, 3)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (biome instanceof BiomeGenTaiga || biome instanceof BiomeGenHills)
//{
//	if (MaddTech.confGenDolomite)
//	{
//		for (i=0; i<8; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 25 + random.nextInt(65-25);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate Dolomite at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockDolomite, 45)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (biome instanceof BiomeGenSnow || biome instanceof BiomeGenSavanna)
//{
//	if (MaddTech.confGenGranite)
//	{
//		for (i=0; i<8; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 25 + random.nextInt(65-25);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate Granite at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockGranite, 45)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (biome instanceof BiomeGenJungle)
//{
//	if (MaddTech.confGenKaolin)
//	{
//		for (i=0; i<8; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 25 + random.nextInt(65-25);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate Kaolin at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockKaolin, 45)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (biome instanceof BiomeGenForest)
//{
//	if (MaddTech.confGenLimestone)
//	{
//		for (i=0; i<6; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 25 + random.nextInt(65-25);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate Limestone at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockLimestone, 45)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenSulfurOre)
//	{
//		for (i=0; i<15; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(30-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate SulfurOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockSulfurOre, 10)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenNickelOre)
//	{
//		for (i=0; i<15; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 0 + random.nextInt(30-0);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate NickelOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockNickelOre, 4)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenLigniteOre)
//	{
//		for (i=0; i<10; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 40 + random.nextInt(70-40);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate LigniteOre at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockLigniteOre, 15)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//if (true)
//{
//	if (MaddTech.confGenPeatBlock)
//	{
//		for (i=0; i<3; i++)
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 55 + random.nextInt(70-55);
//			int z = chunkZ + random.nextInt(16);
//
//			//System.out.println("[MADDTECH] Generate PeatBlock at (" + x + ", " + y + ", " + z + ")");
//
//			(new WorldGenMinable(MaddTech.blockPeatBlock, 30)).generate(world, random, x, y, z);
//		};
//	};
//};
//
//
//		if ((isPlaceForVolcano(chunkX, chunkZ) && (random.nextInt(100) < 10)) || ((chunkX == 0) && (chunkZ == 0)))
//		{
//			int x = chunkX + random.nextInt(16);
//			int y = 40 + random.nextInt(10);
//			int z = chunkZ + random.nextInt(16);
//			int height = 50 + random.nextInt(20);
//			int radius = 20 + random.nextInt(12);
//			printVolcanoNotif(x, y, z, height, radius);
//			(new WorldGenVolcano(radius, height)).generate(world, random, x, y, z);
//		};
//
//		if (isPlaceForNaturalGas(chunkX, chunkZ) && (random.nextInt(100) < 50))
//		{
//			int height = 5 + random.nextInt(3);
//			printNaturalGasNotif(chunkX, chunkZ, height);
//			(new WorldGenNaturalGas(height)).generate(world, random, chunkX, 0, chunkZ);
//		};
//	};
};
