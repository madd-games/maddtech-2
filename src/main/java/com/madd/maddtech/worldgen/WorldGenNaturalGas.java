/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * WorldGenNaturalGas.java
 * Generates natural gas ores.
 */

package com.madd.maddtech.worldgen;

import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.world.World;


public class WorldGenNaturalGas// extends WorldGenerator
{
	private int height;
	public WorldGenNaturalGas(int height)
	{
		this.height = height;
	};

//	@Override
//	public boolean generate(World world, Random random, int x, int y, int z)
//	{
//		int offX, offY, offZ;
//		for (offX=0; offX<16; offX++)
//		{
//			for (offZ=0; offZ<16; offZ++)
//			{
//				for (offY=0; offY<height; offY++)
//				{
//					world.setBlock(x+offX, offY+1, z+offZ, MaddTech.blockNaturalGasOre, 15, 3);
//				};
//			};
//		};
//
//		return true;
//	};
};
