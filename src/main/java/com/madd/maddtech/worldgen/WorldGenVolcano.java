/*
	MaddTech -- a technical mod for minecraft

	Copyright (c) 2022, the MaddTech team.
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright notice, this
	  list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright notice,
	  this list of conditions and the following disclaimer in the documentation
	  and/or other materials provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * WorldGenVolcano.java
 * Generates volcanoes.
 */

package com.madd.maddtech.worldgen;

import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.world.World;


public class WorldGenVolcano// extends WorldGenerator
{
	private int radius;
	private int height;
	private int countLayers;

//	public static final Block[] replaceBlocks = new Block[] {
//		Blocks.stone,
//		Blocks.cobblestone,
//		Blocks.dirt,
//		Blocks.air,
//		Blocks.flowing_water,
//		Blocks.water,
//		Blocks.grass
//	};
//
//	/**
//	 * Constructor for the volcano generator.
//	 * @param radius The width and length of the volcano.
//	 * @param height The height of the volcano.
//	 */
//	public WorldGenVolcano(int radius, int height)
//	{
//		this.radius = radius;
//		this.height = height;
//		countLayers = 0;
//	};
//
//	public void generateBlockAt(World world, Random random, int x, int y, int z)
//	{
//		world.setBlock(x, y, z, MaddTech.blockBasalt);
//	};
//
//	public void maybeGenerateBlockAt(World world, Random random, int x, int y, int z)
//	{
//		if (random.nextInt(100) < 60)
//		{
//			generateBlockAt(world, random, x, y, z);
//		};
//	};
//
//	public void generateLayer(World world, Random random, int x, int y, int z)
//	{
//		generateBlockAt(world, random, x, y, z);
//		countLayers++;
//		boolean shorten = (random.nextInt(100) < 45) && (radius != 1) && (countLayers > 3);
//
//		if (shorten)
//		{
//			countLayers = 0;
//		};
//
//		if (countLayers > 5)
//		{
//			shorten = true;
//			countLayers = 0;
//		};
//
//		float r = (float)radius * (1.0f-(float)Math.sin(((float)y/(float)(y+height))*0.5*Math.PI));
//
//		int px, pz;
//		for (px=-radius; px<=radius; px++)
//		{
//			for (pz=-radius; pz<=radius; pz++)
//			{
//				float bias = random.nextFloat();
//				if (bias+px*px+bias+pz*pz <= r*r)
//				{
//					generateBlockAt(world, random, x+px, y, z+pz);
//				};
//			};
//		};
//	};
//
//	@Override
//	public boolean generate(World world, Random random, int x, int y, int z)
//	{
//		Block base = world.getBlock(x, y, z);
//		if ((base == Blocks.water) || (base == Blocks.flowing_water))
//		{
//			return false;
//		};
//
//		int top = y + height;
//		for (; y<top; y++)
//		{
//			generateLayer(world, random, x, y, z);
//			if (radius == 1)
//			{
//				break;
//			};
//		};
//
//		world.setBlock(x, y+1, z, Blocks.flowing_lava);
//		return true;
//	};
};
